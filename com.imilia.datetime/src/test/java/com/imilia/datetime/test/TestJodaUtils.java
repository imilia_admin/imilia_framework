package com.imilia.datetime.test;

import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Locale;

import org.joda.time.DateMidnight;
import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.imilia.datetime.JodaUtils;


public class TestJodaUtils {

	private final static Logger logger = LoggerFactory.getLogger(TestJodaUtils.class);
	
	@Test
	public void testEaster(){
		for (int i = 2012;i < 2020;i++){
			DateMidnight easterSunday = JodaUtils.getEasterSunday(i);
			
			logger.info(easterSunday.toString());
		}
	}
	
	
	@Test
	public void testAddDays(){
		DateMidnight d = new DateMidnight(2012,1,1);
		
		DateTime dt = JodaUtils.addWeekDays(10, d.toDateTime());
		
		int dayOfMonth = dt.getDayOfMonth();
		
		assertTrue(dayOfMonth == 13);
	}
	
	@Test
	public void testSplitDays(){
		Interval interval =  new Interval(
				new DateTime(2012,1,1, 8,0,0,0),
				new DateTime(2012,1,8, 20,0,0,0));
		
		final List<Interval> intervals = 
			JodaUtils.splitIntoDays(interval);
		
		assertTrue(intervals.size() == 8);
		
	}
	
	@Test
	public void testErraneousDateMidnight(){
		final DateTimeFormatter dfShortDate = DateTimeFormat.shortDate().withLocale(Locale.GERMANY);
		
		final String candidates[] = {
				"01.01.13",
				"01.1.13",
				"1.1.13",
				"1.1.2013",
				"1,2,12"
				};
		
		for (String candidate:candidates){
			logger.info(candidate + " >> " + dfShortDate.parseDateTime(JodaUtils.fixDateMidnight(candidate)));
		}
	}

	@Test
	public void testErraneousDateTime(){
		final DateTimeFormatter dfShortDate = DateTimeFormat.shortDateTime().withLocale(Locale.GERMANY);
		
		final String candidates[] = {
				"01.01.13",
				"01.1.13",
				"1.1.13",
				"1.1.2013",
				"1,1,2013"
		};
		
		for (String candidate:candidates){
			logger.info(candidate + " >> " + dfShortDate.parseDateTime(JodaUtils.fixDateTime(candidate)));
		}
	}

	@Test
	public void testErraneousDateTimeCustomPattern(){
		final DateTimeFormatter dfShortDate = DateTimeFormat.forPattern("dd.MM.yy HH:mm").withLocale(Locale.GERMANY);
		
		final String candidates[] = {
				"1.1.12 00:00",
				"1.1.2012 00:00",
				"01.01.13",
				"01.1.13",
				"1.1.13",
				"1.1.2013",
				"1,1,2013",
				"1.1.2012 00:00",
				"1,1,2013 04:15"
		};
		
		for (String candidate:candidates){
			logger.info(candidate + " >> " + dfShortDate.parseDateTime(JodaUtils.fixDateTime(candidate)));
		}
	}
	
	
	@Test
	public void testMonthBlockGermany(){
		final Locale locale = Locale.GERMANY;
		// Sept 2013
		final Interval sept = JodaUtils.getMonthWeeksInterval(locale, new DateMidnight(2013, 9,1));
		assertTrue(sept.getStart().toDateMidnight().equals(new DateMidnight(2013,8,26)));
		assertTrue(sept.getEnd().toDateMidnight().equals(new DateMidnight(2013,10,6)));
		logger.info("Sept >> " + sept);
		
		// Oct 2013
		final Interval oct = JodaUtils.getMonthWeeksInterval(locale, new DateMidnight(2013, 10,1));
		assertTrue(oct.getStart().toDateMidnight().equals(new DateMidnight(2013,9,30)));
		logger.info("Oct >> " + oct);
		assertTrue(oct.getEnd().toDateMidnight().equals(new DateMidnight(2013,11,3)));

		// Nov 2013
		final Interval nov = JodaUtils.getMonthWeeksInterval(locale, new DateMidnight(2013, 11,1));
		assertTrue(nov.getStart().toDateMidnight().equals(new DateMidnight(2013,10,28)));
		assertTrue(nov.getEnd().toDateMidnight().equals(new DateMidnight(2013,12,1)));
		logger.info("Nov >> " + nov);
		
		final Interval dec = JodaUtils.getMonthWeeksInterval(locale, new DateMidnight(2013, 12,1));
		assertTrue(dec.getStart().toDateMidnight().equals(new DateMidnight(2013,11,25)));
		assertTrue(dec.getEnd().toDateMidnight().equals(new DateMidnight(2014,1,5)));
		logger.info("Dec >> " + dec);
	}
	
	@Test
	public void testMonthBlockUS(){
		final Locale locale = Locale.US;
		// Sept 2013
		final Interval sept = JodaUtils.getMonthWeeksInterval(locale, new DateMidnight(2013, 9,1));
		assertTrue(sept.getStart().toDateMidnight().equals(new DateMidnight(2013,9,1)));
		assertTrue(sept.getEnd().toDateMidnight().equals(new DateMidnight(2013,10,5)));
		logger.info("Sept >> " + sept);
		
		// Oct 2013
		final Interval oct = JodaUtils.getMonthWeeksInterval(locale, new DateMidnight(2013, 10,1));
		assertTrue(oct.getStart().toDateMidnight().equals(new DateMidnight(2013,9,29)));
		logger.info("Oct >> " + oct);
		assertTrue(oct.getEnd().toDateMidnight().equals(new DateMidnight(2013,11,2)));

		// Nov 2013
		final Interval nov = JodaUtils.getMonthWeeksInterval(locale, new DateMidnight(2013, 11,1));
		assertTrue(nov.getStart().toDateMidnight().equals(new DateMidnight(2013,10,27)));
		assertTrue(nov.getEnd().toDateMidnight().equals(new DateMidnight(2013,11,30)));
		logger.info("Nov >> " + nov);
		
		final Interval dec = JodaUtils.getMonthWeeksInterval(locale, new DateMidnight(2013, 12,1));
		assertTrue(dec.getStart().toDateMidnight().equals(new DateMidnight(2013,12,1)));
		assertTrue(dec.getEnd().toDateMidnight().equals(new DateMidnight(2014,1,4)));
		logger.info("Dec >> " + dec);
	}
	
}
