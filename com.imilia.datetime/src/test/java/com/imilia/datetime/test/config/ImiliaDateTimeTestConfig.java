package com.imilia.datetime.test.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.imilia.datetime.dynamic.DynamicIntervalParser;
import com.imilia.datetime.dynamic.parser.DynamicIntervalParserImpl;

@Configuration
public class ImiliaDateTimeTestConfig {
	@Bean
	public DynamicIntervalParser getDynamicIntervalParser(){
		return new DynamicIntervalParserImpl();
	}
}
