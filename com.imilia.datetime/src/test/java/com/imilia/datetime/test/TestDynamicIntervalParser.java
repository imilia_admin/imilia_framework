package com.imilia.datetime.test;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateMidnight;
import org.joda.time.Interval;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.imilia.datetime.dynamic.DynamicInterval;
import com.imilia.datetime.dynamic.DynamicIntervalParser;
import com.imilia.datetime.dynamic.ExprTreeBuilderImpl;
import com.imilia.datetime.test.config.ImiliaDateTimeTestConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=ImiliaDateTimeTestConfig.class)
public class TestDynamicIntervalParser {
	private final static Logger logger = LoggerFactory.getLogger(TestDynamicIntervalParser.class);
	
	@Autowired
	private DynamicIntervalParser dynamicIntervalParser;
	
	@Test
	public void testEaster() throws Exception{
		ExprTreeBuilderImpl exprTreeBuilderImpl = new ExprTreeBuilderImpl();

		//  
		dynamicIntervalParser.parse(new StringReader("first Monday in May,first Monday in August,last Thursday in November, easter[-2D;+1D],05.01[-0D;+1D],easter[+39D;+1D],easter[+49D;+2D],10.3,12.25[+0D;+2D] "), exprTreeBuilderImpl);
		
		List<DynamicInterval> dynamicIntervals = exprTreeBuilderImpl.getDynamicIntervals();
		
		final Interval thisYear = new Interval(new DateMidnight(2012,1,1), new DateMidnight(2012,12,31) );
		
		List<Interval> intervals = new ArrayList<Interval>();
		
		for (DynamicInterval di:dynamicIntervals){
			di.generateIntervals(thisYear, intervals);
		}
		
		for (Interval i:intervals){
			logger.info(i.toString());
		}
	}

	public DynamicIntervalParser getDynamicIntervalParser() {
		return dynamicIntervalParser;
	}

	public void setDynamicIntervalParser(DynamicIntervalParser dynamicIntervalParser) {
		this.dynamicIntervalParser = dynamicIntervalParser;
	}
	
//	@Test
//	public void testAscentionThursday2012(){
//		DateMidnight d = new DateMidnight(2012,4,8);
//		
//		DateMidnight ascentionThursday = d.plusDays(39);
//		
//		logger.info(ascentionThursday);
//	}
//	
//	@Test
//	public void testPentacost2012(){
//		DateMidnight d = new DateMidnight(2012,4,8);
//		
//		DateMidnight ascentionThursday = d.plusDays(49);
//		
//		logger.info(ascentionThursday);
//	}
}
