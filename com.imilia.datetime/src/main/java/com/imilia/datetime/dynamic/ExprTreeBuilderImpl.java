package com.imilia.datetime.dynamic;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import org.joda.time.Period;

import com.imilia.datetime.dynamic.impl.MonthDayDynamicInterval;
import com.imilia.datetime.dynamic.impl.MonthWeekDayDynamicInterval;
import com.imilia.datetime.dynamic.impl.NamedHoliday;
import com.imilia.datetime.dynamic.impl.NamedHolidayDynamicInterval;

public class ExprTreeBuilderImpl implements ExprTreeBuilder {

	private final Stack<Object> stack = new Stack<Object>();
	
	private final List<DynamicInterval> dynamicIntervals = new ArrayList<DynamicInterval>();
	
	public void beginParse() {
		pushFrame();
	}

	@Override
	public void endParse() {
		Stack<Object> objects = popFrame();
		
		stack.addAll(objects);
	}

	
	public void handleEasterRef() {
		push(new NamedHolidayDynamicInterval(NamedHoliday.Easter));
	}

	public void handleMonthDayRef(int month, int day) {
		push(new MonthDayDynamicInterval(month,day));
	}

	public void handleMonthWeekDayRef(Occurrence occurrence, int dayOfWeek,	int month) {
		push(new MonthWeekDayDynamicInterval(occurrence, dayOfWeek, month));
	}

	public void handleOffsetPeriod(OffsetPeriod.Type type, int months, int days, int hours, int minutes){
		push(new OffsetPeriod(type, new Period(0,months,0, days, hours,minutes, 0, 0)));
	}
	
	void pushFrame(){
		this.stack.push(new Stack<Object>());
	}
	
	@SuppressWarnings("unchecked")
	Stack<Object> popFrame(){
		return (Stack<Object>) this.stack.pop();
	}
	
	@SuppressWarnings("unchecked")
	Stack<Object> peekFrame(){
		return (Stack<Object>) this.stack.peek();
	}
	
	Object peek(){
		return peekFrame().peek();
	}
	
	void push(Object obj){
		peekFrame().push(obj);
		
		if (obj instanceof DynamicInterval){
			dynamicIntervals.add((DynamicInterval) obj);
		}
	}

	@Override
	public void beginOffsetRange() {
		pushFrame();
	}

	@Override
	public void endOffsetRange() {
		Stack<Object> frame = popFrame();
		
		DynamicInterval dynamicInterval = (DynamicInterval) peek();
		
		dynamicInterval.setOffsetPeriodEnd((OffsetPeriod) frame.pop());
		dynamicInterval.setOffsetPeriodStart((OffsetPeriod) frame.pop());
	}

	public Stack<Object> getStack() {
		return stack;
	}

	public List<DynamicInterval> getDynamicIntervals(){
		return dynamicIntervals;
	}

	
}
