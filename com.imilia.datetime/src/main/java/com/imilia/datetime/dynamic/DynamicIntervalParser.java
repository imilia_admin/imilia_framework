package com.imilia.datetime.dynamic;

import java.io.Reader;

public interface DynamicIntervalParser {
    public void parse(Reader reader, ExprTreeBuilder builder) throws Exception;
}
