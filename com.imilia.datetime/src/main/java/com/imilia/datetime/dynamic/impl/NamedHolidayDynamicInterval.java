package com.imilia.datetime.dynamic.impl;

import org.joda.time.DateMidnight;

import com.imilia.datetime.JodaUtils;

public class NamedHolidayDynamicInterval extends AbstractDynamicInterval {

	private NamedHoliday namedHoliday;
	
	public NamedHolidayDynamicInterval() {
		super();
	}

	public NamedHolidayDynamicInterval(NamedHoliday namedHoliday) {
		super();
		this.namedHoliday = namedHoliday;
	}
	

	public NamedHoliday getNamedHoliday() {
		return namedHoliday;
	}

	public void setNamedHoliday(NamedHoliday namedHoliday) {
		this.namedHoliday = namedHoliday;
	}

	public DateMidnight getRefDate(int year) {
		switch(namedHoliday){
			case Easter:
				return JodaUtils.getEasterSunday(year);
		}
		
		return null;
	}
}
