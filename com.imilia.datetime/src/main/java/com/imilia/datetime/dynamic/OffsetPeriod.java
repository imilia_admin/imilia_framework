package com.imilia.datetime.dynamic;

import org.joda.time.DateTime;
import org.joda.time.Period;

public class OffsetPeriod {
	
	public enum Type{
		Positive,
		Negative
	};
	
	private Type type = Type.Positive;
	
	private Period period;
	
	public OffsetPeriod(){
	}

	public OffsetPeriod(Type type, Period period) {
		super();
		this.type = type;
		this.period = period;
	}

	public Period getPeriod() {
		return period;
	}

	public void setPeriod(Period period) {
		this.period = period;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}
	
	public DateTime adjust(DateTime input){
		switch(type){
			case Positive:
				return input.plus(period);
			case Negative:
				return input.minus(period);
		}
		
		return null;
	}
}
