package com.imilia.datetime.dynamic;

public enum Occurrence{
	First,
	Second,
	Third,
	Fourth, 
	Last
}