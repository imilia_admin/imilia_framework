package com.imilia.datetime.dynamic;

import java.util.List;

import org.joda.time.Interval;

public interface DynamicInterval {
	public void generateIntervals(Interval targetInterval, List<Interval> intervals);
	
	public void setOffsetPeriodStart(OffsetPeriod offset);
	public OffsetPeriod getOffsetPeriodStart();
	public void setOffsetPeriodEnd(OffsetPeriod offset);
	public OffsetPeriod getOffsetPeriodEnd();
}
