package com.imilia.datetime.dynamic.impl;

import org.joda.time.DateMidnight;

import com.imilia.datetime.dynamic.Occurrence;

public class MonthWeekDayDynamicInterval extends AbstractDynamicInterval {

	private Occurrence occurrence;
	
	private int month;

	private int dayOfWeek;
	
	
	public MonthWeekDayDynamicInterval(Occurrence occurrence, int dayOfWeek, int month) {
		super();
		this.occurrence = occurrence;
		this.month = month;
		this.dayOfWeek = dayOfWeek;
	}


	@Override
	public DateMidnight getRefDate(int year) {
		DateMidnight d = new DateMidnight(year, month, 1);
		final DateMidnight nextMonth = d.plusMonths(1);
		
		DateMidnight candidate = d;
		int count = 0;
		while (d.isBefore(nextMonth)){
			if (d.getDayOfWeek() == dayOfWeek){
				switch(occurrence){
				case First:
				case Second:
				case Third:
				case Fourth:
					if (count == occurrence.ordinal()){
						return d;
					}
					break;
					default:
						// Last 
						candidate = d;
				}
				count++;
			}
			d = d.plusDays(1);
		}
		return candidate;
	}
}
