package com.imilia.datetime.dynamic.impl;

import org.joda.time.DateMidnight;

public class MonthDayDynamicInterval extends AbstractDynamicInterval {

	private int month;

	private int day;
	
	
	public MonthDayDynamicInterval(int month, int day) {
		super();
		this.month = month;
		this.day = day;
	}

	public DateMidnight getRefDate(int year) {
		return new DateMidnight(year, month, day);
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

}
