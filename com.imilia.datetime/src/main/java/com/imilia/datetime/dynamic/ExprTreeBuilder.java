package com.imilia.datetime.dynamic;

public interface ExprTreeBuilder {
	public void beginParse();
	public void endParse();
	public void handleEasterRef();
	public void handleMonthDayRef(int month, int day);
	public void handleMonthWeekDayRef(Occurrence occurrence,  int dayOfWeek, int month);
	
	public void beginOffsetRange();
	public void endOffsetRange();
	
	
	public void handleOffsetPeriod(OffsetPeriod.Type type, int months, int days, int hours, int minutes);
}
