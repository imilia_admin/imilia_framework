/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2012 - all rights reserved
 *
 * Created on: Mar 2, 2012
 * Created by: emcgreal
 */
package com.imilia.datetime.dynamic.impl;

import java.util.List;

import org.joda.time.DateMidnight;
import org.joda.time.DateTime;
import org.joda.time.Interval;

import com.imilia.datetime.dynamic.DynamicInterval;
import com.imilia.datetime.dynamic.OffsetPeriod;

/**
 * The Class AbstractDynamicInterval.
 */
public abstract class AbstractDynamicInterval implements DynamicInterval {
	
	/** The offset period start. */
	private OffsetPeriod offsetPeriodStart;
	
	/** The offset period end. */
	private OffsetPeriod offsetPeriodEnd;	
	
	
	/* (non-Javadoc)
	 * @see com.imilia.utils.joda.dynamic.DynamicInterval#generateIntervals(org.joda.time.Interval, java.util.List)
	 */
	public void generateIntervals(Interval targetInterval, List<Interval> intervals){
		int year = targetInterval.getStart().getYear();
		final int endYear = targetInterval.getEnd().getYear();
		
		while (year <= endYear){
			DateTime refDate = getRefDate(year).toDateTime();
			
			DateTime startInterval = refDate;
			if (offsetPeriodStart != null){
				startInterval = offsetPeriodStart.adjust(refDate);
			}
			
			DateTime endInterval = null;
			if (offsetPeriodEnd != null){
				endInterval = offsetPeriodEnd.adjust(startInterval);
			}else{
				endInterval = startInterval.plusDays(1);
			}
			
			intervals.add(new Interval(startInterval, endInterval));
			
			year++;
		}
	}

	/**
	 * Gets the ref date.
	 *
	 * @param year the year
	 * @return the ref date
	 */
	public abstract DateMidnight getRefDate(int year);

	/* (non-Javadoc)
	 * @see com.imilia.utils.joda.dynamic.DynamicInterval#getOffsetPeriodStart()
	 */
	public OffsetPeriod getOffsetPeriodStart() {
		return offsetPeriodStart;
	}

	/* (non-Javadoc)
	 * @see com.imilia.utils.joda.dynamic.DynamicInterval#setOffsetPeriodStart(com.imilia.utils.joda.dynamic.OffsetPeriod)
	 */
	public void setOffsetPeriodStart(OffsetPeriod offsetPeriodStart) {
		this.offsetPeriodStart = offsetPeriodStart;
	}

	/* (non-Javadoc)
	 * @see com.imilia.utils.joda.dynamic.DynamicInterval#getOffsetPeriodEnd()
	 */
	public OffsetPeriod getOffsetPeriodEnd() {
		return offsetPeriodEnd;
	}

	/* (non-Javadoc)
	 * @see com.imilia.utils.joda.dynamic.DynamicInterval#setOffsetPeriodEnd(com.imilia.utils.joda.dynamic.OffsetPeriod)
	 */
	public void setOffsetPeriodEnd(OffsetPeriod offsetPeriodEnd) {
		this.offsetPeriodEnd = offsetPeriodEnd;
	}
	

}
