/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2010 - all rights reserved
 * Created on: Jul 5, 2010
 * Created by: emcgreal
 */
package com.imilia.datetime;

import org.joda.time.DateMidnight;
import org.joda.time.base.BaseDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.imilia.utils.i18n.DomainLocale;
import com.imilia.utils.i18n.LocalizedProperty;

public class JodaLocalizedProperty<T extends BaseDateTime> implements LocalizedProperty{

	private T baseDateTime;

	public interface Formatter<T>{
		public String format(T t, DomainLocale locale);
	};
	
	public static class DateMidnightFormatter implements Formatter<DateMidnight>{
		public String format(DateMidnight t, DomainLocale locale) {
			DateTimeFormatter dtf = DateTimeFormat.forPattern(locale.getShortDayDateFormat()).withLocale(locale.getLocale());
			return dtf.print(t);
		}
		
	};
	
	
	private Formatter<T> formatter;
	
	/**
	 * Instantiates a new joda localized property.
	 * 
	 * @param t the t
	 */
	public JodaLocalizedProperty(T t, Formatter<T> formatter){
		this.baseDateTime = t;
	}
	
	public final T getBaseDateTime() {
		return baseDateTime;
	}

	public final void setBaseDateTime(T baseDateTime) {
		this.baseDateTime = baseDateTime;
	}

	public String getText(DomainLocale locale) {
		return formatter.format(this.baseDateTime, locale);
	}

	public void setText(DomainLocale locale, String text) {
		// Shouldn't be needed
	}

	public final Formatter<T> getFormatter() {
		return formatter;
	}

	public final void setFormatter(Formatter<T> formatter) {
		this.formatter = formatter;
	}

	public String format(DateMidnight t, DomainLocale locale) {
		// TODO Auto-generated method stub
		return null;
	}
}
