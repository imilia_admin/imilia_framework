package com.imilia.datetime;

import java.util.Locale;

import org.joda.time.DateTimeConstants;
import org.springframework.context.MessageSource;

import com.imilia.utils.i18n.Localized;

public enum DurationUnit implements Localized {
	Seconds,
	Minutes,
	Hours,
	Days,
	Weeks;

	@Override
	public String getLocalizedMessage(MessageSource messageSource, Locale locale) {
		return messageSource.getMessage(getClass().getName() + "." + name(), null, locale);
	}
	
	public int getMillisPerUnit(){
		switch(this){
		case Seconds:
			return DateTimeConstants.MILLIS_PER_SECOND;
		case Minutes:
			return DateTimeConstants.MILLIS_PER_MINUTE;
		case Hours:
			return DateTimeConstants.MILLIS_PER_HOUR;
		case Days:
			return DateTimeConstants.MILLIS_PER_DAY;
		case Weeks:
			return DateTimeConstants.MILLIS_PER_WEEK;
		}
		
		return 0;
	}
	
	public long getMillis(long n){
		switch(this){
		case Seconds:
			return DateTimeConstants.MILLIS_PER_SECOND * n;
		case Minutes:
			return DateTimeConstants.MILLIS_PER_MINUTE * n;
		case Hours:
			return DateTimeConstants.MILLIS_PER_HOUR * n;
		case Days:
			return DateTimeConstants.MILLIS_PER_DAY * n;
		case Weeks:
			return DateTimeConstants.MILLIS_PER_WEEK * n;
		}
		
		return 0;
	}

	public long getNrUnits(long n){
		switch(this){
		case Seconds:
			return n/DateTimeConstants.MILLIS_PER_SECOND;
		case Minutes:
			return n/DateTimeConstants.MILLIS_PER_MINUTE;
		case Hours:
			return n/DateTimeConstants.MILLIS_PER_HOUR;
		case Days:
			return n/DateTimeConstants.MILLIS_PER_DAY;
		case Weeks:
			return n/DateTimeConstants.MILLIS_PER_WEEK;
		}
		
		return 0;
	}
	public static DurationUnit mapToNearestUnit(long millis){
		
		for (int i = values().length-1;i>=0;i--){
			DurationUnit d = values()[i];
			
			if (millis / d.getMillisPerUnit() > 0 && millis % d.getMillisPerUnit() == 0) {
				return d;
			} 
		}
		
		// Default to Seconds if nothing works
		return DurationUnit.Seconds;
	}
}
