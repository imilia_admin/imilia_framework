/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2010 - all rights reserved
 * Created on: Jun 1, 2010
 * Created by: emcgreal
 */
package com.imilia.datetime;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import org.joda.time.DateMidnight;
import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.joda.time.Interval;
import org.joda.time.base.BaseDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * The Class JodaUtils.
 */
public class JodaUtils {
	
	/** The Constant TIME_FORMAT_PATTERNS. */
	private final static String[] TIME_FORMAT_PATTERNS = {"H", "h"};
	
	/**
	 * Subtract.
	 *
	 * @param lhsIntervals the lhs intervals
	 * @param rhsIntervals the rhs intervals
	 * @return the list of intervals
	 */
	public List<Interval> subtract(final List<Interval> lhsIntervals, final List<Interval> rhsIntervals){

		final List<Interval> result = new ArrayList<Interval>();
		
		
		final List<Interval> inputLhsIntervals = new ArrayList<Interval>();
		
		for (Interval lhs:lhsIntervals){

			inputLhsIntervals.clear();
			inputLhsIntervals.add(lhs);
			for (Interval rhsInterval:rhsIntervals){
				List<Interval> subtractionResult = null;

				for (Interval t:inputLhsIntervals){
					List<Interval> tmp = subtract(t, rhsInterval);
					
					if (subtractionResult == null){
						subtractionResult = tmp;
					}else{
						subtractionResult.addAll(tmp);
					}
				}
				
				result.addAll(subtractionResult);
			}
		}
		
		
		return result;
	}
	
	
	/**
	 * Subtract.
	 *
	 * @param lhs the lhs
	 * @param rhs the rhs
	 * @return the list
	 */
	public List<Interval> subtract(Interval lhs, Interval rhs){
		final List<Interval> result = new ArrayList<Interval>();
		
		if (rhs.contains(lhs)){
			// rhs completely contains lhs - so don't add anything to result
		}else if (lhs.contains(rhs)){
			
			// Split into a before (lhs) and after inclusion
			// Before
			final Interval lhsPart = new Interval(lhs.getStart(), rhs.getStart());
			
			if (lhsPart.toDurationMillis() != 0){
				result.add(lhsPart);
			}
			
			// After
			final Interval rhsPart = new Interval(rhs.getEnd(), lhs.getEnd());
			
			if (rhsPart.toDurationMillis() != 0){
				result.add(rhsPart);
			}
		} else {
			// If it overlaps then take the no overlapping part of lhs
			final Interval overlap = lhs.overlap(rhs);
			if (overlap != null){
				// Interval overlaps beginning
				if (lhs.getStart().isBefore(rhs.getStart())){
					final Interval lhsPart = new Interval(lhs.getStart(), rhs.getStart());
					if (lhsPart.toDurationMillis() != 0){
						result.add(lhsPart);
					}
				}else{
					final Interval rhsPart = new Interval(rhs.getEnd(), lhs.getEnd());
					
					if (rhsPart.toDurationMillis() != 0){
						result.add(rhsPart);
					}
				}
			}
		}
		
		return result;
	}
	

	/**
	 * Gets the first day of quarter.
	 * 
	 * @param d the d
	 * 
	 * @return the first day of quarter
	 */
	public static DateMidnight getFirstDayOfQuarter(DateMidnight d){
		if (d != null){
			int month = 1;
			switch(d.getMonthOfYear()){
			case 1:
			case 2:
			case 3:
				month = 1;
				break;
			case 4:
			case 5:
			case 6:
				month = 4;
				break;
			case 7:
			case 8:
			case 9:
				month = 6;
				break;
			default:
				month = 9;
			}
			return new DateMidnight(d.getYear(), month, 1);
		}
		
		return null;
	}
	
	/**
	 * Prints the timestamp.
	 * 
	 * @param baseDate the base date
	 * @param locale the locale
	 * 
	 * @return the string
	 */
	public static String printTimestamp(BaseDateTime baseDate, Locale locale){
		if (baseDate != null){
			final DateTimeFormatter formatter= DateTimeFormat.mediumDateTime().withLocale(locale);
			return formatter.print(baseDate);  
		}
		
		
		return null;
	}
	
	/**
	 * Gets the easter sunday.
	 *
	 * @param year the year
	 * @return the easter sunday
	 */
	public static DateMidnight getEasterSunday(int year){
		int a = year % 19;
		int b = year / 100;
		int c = year % 100;
		int d = b / 4;
		int e = b % 4;
		int f = (b + 8) / 25;
		int g = (b - f + 1) / 3;
		int h = (19 * a + b - d - g + 15) % 30;
		int i = c / 4;
		int k = c % 4;
		int L = (32 + 2 * e + 2 * i - h - k) % 7;
		int m = (a + 11 * h + 22 * L) / 451;

		int month = (h + L - 7 * m + 114) / 31;
		int day = ((h + L - 7 * m + 114) % 31) + 1;

		return new DateMidnight(year, month, day);
	}
	
	/**
	 * Adds the week days.
	 *
	 * @param daysToAdd the days to add
	 * @param d the d
	 * @return the date time
	 */
	public static DateTime addWeekDays(int daysToAdd, DateTime d){
		return addDays(daysToAdd, d, DateTimeConstants.SATURDAY, DateTimeConstants.SUNDAY);
	}
	
	/**
	 * Adds the days.
	 *
	 * @param daysToAdd the days to add
	 * @param d the d
	 * @param days2Skip the days2 skip
	 * @return the date time
	 */
	public static DateTime addDays(int daysToAdd, DateTime d, int...days2Skip){
		int count = 0;
		do{
			d = d.plusDays(1);
			
			int dayOfWeek = d.getDayOfWeek();
			
			boolean skip = false;
			for (int day2Skip:days2Skip){
				if (dayOfWeek == day2Skip){
					skip = true;
					break;
				}
			}
			
			if (!skip){
				count++;
			}
			
		}while(count < daysToAdd);
		
		return d;
	}
	
	/**
	 * Split into days.
	 *
	 * @param interval the interval
	 * @return the list
	 */
	public static List<Interval> splitIntoDays(Interval interval){
		final List<Interval> intervals = new ArrayList<Interval>();
		
		
		DateTime start = interval.getStart();
		final DateTime end = interval.getEnd();
		
		while (start.isBefore(end)){
			DateMidnight nextDay = start.plusDays(1).toDateMidnight();
			
			DateTime endInterval = nextDay.toDateTime();
			if (nextDay.isAfter(end)){
				endInterval = end;
			}
			
			intervals.add(new Interval(start, endInterval));
			
			start = nextDay.toDateTime();
		}
		
		return intervals;
	}
	
	/**
	 * Fix date midnight.
	 *
	 * @param input the input
	 * @return the string
	 */
	public static String fixDateMidnight(String input){
		if (input != null){
			input = input.replace(',', '.');
		}
		
		return input;
	}
	
	/**
	 * Fix date time.
	 *
	 * @param input the input
	 * @return the string
	 */
	public static String fixDateTime(String input){
		if (input != null){
			input = fixDateMidnight(input);
			
			if (!input.contains(":")){
				input = input + " 00:00";
			}
		}
		
		return input;
	}
	
	/**
	 * Format string contains time.
	 *
	 * @param formatString the format string
	 * @return true, if successful
	 */
	public static boolean formatStringContainsTime(String formatString){
		if (formatString != null){
			for (String s:TIME_FORMAT_PATTERNS){
				if (formatString.contains(s)){
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * Gets the first day of week for the given Locale
	 * US is Sunday whereas most other use Monday
	 *
	 * @param locale the locale
	 * @return the first day of week
	 */
	public static final int getFirstDayOfWeek(Locale locale) {
		 return ((Calendar.getInstance(locale).getFirstDayOfWeek() + 5) % 7) + 1;
	}
	
	/**
	 * Gets the week number in accordance to the first day of the week for the given locale
	 *
	 * @param locale the locale
	 * @param day the day
	 * @return the weeke number
	 */
	public static final int getWeekNumber(Locale locale, DateMidnight day) {
		 Calendar c = Calendar.getInstance(locale);
		 
		 c.setTime(day.toDate());
		 
		 return c.get(java.util.Calendar.WEEK_OF_YEAR);
	}
	
	
	/**
	 * Gets the month weeks interval. 
	 * Starts with the first day of the first week to 6 days past the last first day of the last week 
	 * 
	 * @param locale the locale
	 * @param month the month
	 * @return the month weeks interval
	 */
	public static Interval getMonthWeeksInterval(Locale locale, DateMidnight month){
		DateMidnight firstDayOfMonth = month.dayOfMonth().withMinimumValue();
		DateMidnight lastDayOfMonth = month.dayOfMonth().withMaximumValue();
		
		int firstDayOfWeek = getFirstDayOfWeek(locale);
		
		// Work back from first day of month to first day of week
		while (firstDayOfMonth.getDayOfWeek() != firstDayOfWeek){
			firstDayOfMonth = firstDayOfMonth.minusDays(1);
		}

		// Work back from last first day of week
		while (lastDayOfMonth.getDayOfWeek() != firstDayOfWeek){
			lastDayOfMonth = lastDayOfMonth.minusDays(1);
		}
		
		// Add 6 days to the last first day of last week
		lastDayOfMonth = lastDayOfMonth.plusDays(6);

		return new Interval(firstDayOfMonth, lastDayOfMonth);
	}
}
