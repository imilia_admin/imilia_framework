/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2011 - all rights reserved
 *
 * Created on: Oct 31, 2011
 * Created by: emcgreal
 */
package com.imilia.datetime;

import javax.validation.constraints.NotNull;

import org.joda.time.Interval;
import org.joda.time.base.BaseDateTime;

/**
 * The Class EditableInterval.
 */
public class EditableInterval<T extends BaseDateTime> {
	
	/** The begin. */
	@NotNull
	private T begin;
	
	/** The end. */
	@NotNull
	//@Expression(applyIf = "begin is not null and end is not null", value = "beginBeforeEnd is true", errorCode="invalid")
	private T end;

	
	/**
	 * Instantiates a new editable interval.
	 */
	public EditableInterval() {
		super();
	}

	/**
	 * Instantiates a new editable interval.
	 *
	 * @param begin the begin
	 * @param end the end
	 */
	public EditableInterval(T begin, T end) {
		super();
		this.begin = begin;
		this.end = end;
	}


	/**
	 * Gets the begin.
	 *
	 * @return the begin
	 */
	public T getBegin() {
		return begin;
	}


	/**
	 * Sets the begin.
	 *
	 * @param begin the new begin
	 */
	public void setBegin(T begin) {
		this.begin = begin;
	}


	/**
	 * Gets the end.
	 *
	 * @return the end
	 */
	public T getEnd() {
		return end;
	}


	/**
	 * Sets the end.
	 *
	 * @param end the new end
	 */
	public void setEnd(T end) {
		this.end = end;
	}
	
	/**
	 * Checks if is begin before end.
	 *
	 * @return true, if is begin before end
	 */
	public boolean isBeginBeforeEnd(){
		return begin.isBefore(end);
	}

	/**
	 * Gets the interval.
	 *
	 * @return the interval
	 */
	public Interval getInterval(){
		return new Interval(begin, end);
	}
}
