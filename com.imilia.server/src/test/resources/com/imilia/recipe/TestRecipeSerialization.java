package com.imilia.recipe;

import static org.junit.Assert.*;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.imilia.server.domain.CurrentUser;
import com.imilia.server.domain.common.Person;
import com.imilia.server.domain.filter.CrudFilter;
import com.imilia.server.domain.filter.FilterModel;
import com.imilia.server.domain.filter.PropertyFilter;
import com.imilia.server.domain.filter.Recipe;
import com.imilia.server.domain.filter.RecipeSerializer;

public class TestRecipeSerialization {

	private final static Logger logger = LoggerFactory.getLogger(TestRecipeSerialization.class);
	
	@Test
	public void testDateTimeSerialization() {
		DateTime dt = new DateTime(2014,1,6, 10,0,0,0);
		String s = dt.toString(ISODateTimeFormat.basicDateTime());
		
		logger.debug(s);
		assertTrue("20140106T100000.000+0100".equals(s));
		
		DateTimeFormatter dtf = ISODateTimeFormat.basicDateTime();
		DateTime dt2 = dtf.parseDateTime(s);
		
		assertNotNull(dt2);
	}
	
	@Test
	public void testPersonRecipeSerilization(){
		CrudFilter<Person> p = new CrudFilter<Person>(Person.class);
		
		//p.addFetch("address");
		
		Recipe<Person> r = new Recipe<Person>(Person.class, p);

		FilterModel<Recipe<Person>, CurrentUser> model = new FilterModel(r);
		p.createPropertyFiltersFromAnnotations(model);

		
		RecipeSerializer serializer = new RecipeSerializer();
		serializer.serialize(r);
		
		String serialized = serializer.getBuffer().toString();
		
		assertNotNull(serializer);
		
		Recipe<Person> r1 = new Recipe<Person>(Person.class, p);
		serializer.deserialize(r1, serialized);
		
		for (CrudFilter<?> c:r1.getCrudFilters()){
			for (PropertyFilter pf:c.getPropertyFilters()){
				String mid= pf.getPropertyNameLabelMessageID();
				logger.debug(mid);
			}
		}
		
		assertNotNull(r1);
	}

}
