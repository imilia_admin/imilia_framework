/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 *
 * Created on: Jul 28, 2009
 * Created by: emcgreal
 */
package com.imilia.server.script

import com.imilia.server.test.script.GroovyTester;


/**
 * @author emcgreal
 *
 */
public class GroovyTesterImpl implements GroovyTester{

	 public String getMessage(){
		 return "test"
	 }
}
