package com.imilia.server.test.models;

import javax.validation.constraints.NotNull;


public class TestBean {
	
	@NotNull
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
