package com.imilia.server.test.validation;

import static org.junit.Assert.assertTrue;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.imilia.server.config.ImiliaServerConfig;
import com.imilia.server.config.ImiliaServerValidationConfig;
import com.imilia.server.domain.common.UserAccount;
import com.imilia.server.test.config.ImiliaServerTestConfig;
import com.imilia.server.test.models.TestBean;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ImiliaServerConfig.class, ImiliaServerValidationConfig.class, ImiliaServerTestConfig.class})
public class TestSimpleValidation {
	
	private final static Logger logger = LoggerFactory.getLogger(TestSimpleValidation.class);
	
	@Autowired
	private Validator validator;

	@Test
	public void testInvalidTestBean(){
		TestBean b = new TestBean();
		Set<ConstraintViolation<Object>> values = validate(b);
		assertTrue(values.size() == 1);
	}

	@Test
	public void testValidTestBean(){
		TestBean b = new TestBean();
		b.setName("test");
		Set<ConstraintViolation<Object>> values = validate(b);
		assertTrue(values.size() == 0);
	}

	@Test
	public void testInvalidUserAccounrUsername(){
		UserAccount ua = new UserAccount();
		Set<ConstraintViolation<Object>> values = validate(ua);
		assertTrue(values.size() == 2);
		
	}
	
	@Test
	public void testValidUserAccounrUsername(){
		UserAccount ua = new UserAccount();
		ua.setUsername("Test");
		ua.setPassword("it's a secret");
		Set<ConstraintViolation<Object>> values = validate(ua);
		assertTrue(values.size() == 0);
	}
	
	private Set<ConstraintViolation<Object>> validate(Object o){
		Set<ConstraintViolation<Object>> violations = validator.validate(o);

		if (violations != null){
			logger.debug("" + violations.size());
			for (ConstraintViolation<?> c:violations){
				logger.debug(c.toString());
			}
		}
		
		return violations;
	}
	
	public Validator getValidator() {
		return validator;
	}

	public void setValidator(Validator validator) {
		this.validator = validator;
	}
}
