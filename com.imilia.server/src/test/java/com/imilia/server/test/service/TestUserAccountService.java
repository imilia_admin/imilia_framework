/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 *
 * Created on: Mar 3, 2009
 * Created by: emcgreal
 */
package com.imilia.server.test.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import javax.persistence.PersistenceException;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.imilia.server.config.ImiliaServerConfig;
import com.imilia.server.config.ImiliaServerValidationConfig;
import com.imilia.server.domain.common.Person;
import com.imilia.server.domain.common.UserAccount;
import com.imilia.server.domain.validation.ValidationModel;
import com.imilia.server.domain.validation.ValidationTransaction;
import com.imilia.server.service.AuthenticationService;
import com.imilia.server.service.PersonService;
import com.imilia.server.service.UserAccountService;
import com.imilia.server.test.config.ImiliaServerTestConfig;
import com.imilia.server.validation.ValidationException;
import com.imilia.utils.i18n.DomainLocale;
import com.imilia.utils.observer.Observable;
import com.imilia.utils.observer.Observer;

/**
 * The Class TestUserAccountService test the UserAccount service.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ImiliaServerConfig.class, ImiliaServerValidationConfig.class, ImiliaServerTestConfig.class})
public class TestUserAccountService {
	final static String PW = "Haljk2.W7=";
	/** The user account service. */
	@Autowired
	private UserAccountService userAccountService;

	/** The authentication service. */
	@Autowired
	private AuthenticationService authenticationService;

	@Autowired
	private PersonService personService;

	/**
	 * Setup.
	 */
	@Before
	public void setup(){
		authenticationService.login(UserAccount.INIT_JOB_AUTHENTICATION_REQUEST);
	}

	/**
	 * Test know user - should find the given user.
	 */
	@Test
	public void testKnowUser(){
		UserDetails userDetails = userAccountService.loadUserByUsername(UserAccount.INIT_JOB);
		assertNotNull(userDetails);
	}

	/**
	 * Test unknow user.
	 */
	@Test(expected=UsernameNotFoundException.class)
	public void testUnKnowUser(){
		userAccountService.loadUserByUsername("abc");
	}

	/**
	 * Try saving invalid user account.
	 *
	 * @throws ValidationException the validation exception
	 */
	@Test(expected=ValidationException.class)
	public void trySavingInvalidUserAccount() throws ValidationException{
		UserAccount userAccount = new UserAccount();
		userAccount.setUsername("user");
		userAccount.setPassword("1234");

		userAccountService.save(userAccount);
	}

	/**
	 * Try saving invalid user account with unsaved person.
	 *
	 * @throws ValidationException the validation exception
	 */
	@Test(expected=PersistenceException.class)
	public void trySavingInvalidUserAccountWithUnsavedPerson() throws ValidationException{
		UserAccount initJob = userAccountService.find(UserAccount.class, 1);

		UserAccount userAccount = new UserAccount();
		userAccount.setUsername("trySavingInvalidUserAccountWithUnsavedPerson");
		userAccount.setPassword(PW);
		userAccount.setCreatedOn(new DateTime());
		userAccount.setChangedOn(new DateTime());
		userAccount.setCreatedBy(initJob);
		userAccount.setChangedBy(initJob);

		userAccount.setPerson(new Person());

		// Should throw a persistence exception because the person is not saved
		userAccountService.save(userAccount);
	}

	/**
	 * Save find delete user account.
	 *
	 * @throws ValidationException the validation exception
	 */
	@Test
	public void saveFindDeleteUserAccount() throws ValidationException{
		UserAccount someUser = userAccountService.find(UserAccount.class, 1);

		UserAccount userAccount = new UserAccount();
		userAccount.setUsername("saveFindDelete");
		userAccount.setPassword(PW);
		userAccount.setCreatedOn(new DateTime());
		userAccount.setChangedOn(new DateTime());
		userAccount.setCreatedBy(someUser);
		userAccount.setChangedBy(someUser);

		Person p = new Person();
		p.setDomainLocale(DomainLocale.en_US);
		p.setLastName("Test");
		
		personService.save(p);
		
		userAccount.setPerson(p);

		userAccountService.save(userAccount);

		UserAccount ua = userAccountService.find(UserAccount.class, userAccount.getOid());

		assertNotNull(ua);

		userAccountService.purge(ua);
	}

	/**
	 * Save init job.
	 *
	 * @throws ValidationException the validation exception
	 */
	@Test
	public void saveInitJob() throws ValidationException{
		UserAccount initJob = userAccountService.find(UserAccount.class, 1);

		assertNotNull(initJob);

		initJob.setChangePasswordRequired();

		// Should throw a persistence exception because the person is not saved
		userAccountService.save(initJob);

	}

	/**
	 * Try saving invalid user account with validation transaction.
	 */
	@Test
	public void trySavingInvalidUserAccountWithValidationTransaction(){
		final boolean[] ret = new boolean[1];
		ret[0] = false;

		final ValidationModel validationModel = new ValidationModel();
		validationModel.addObserver(new Observer(){

			public void update(Observable observable, Object o) {
				// Assert that the model is passed as the observable
				assertTrue(observable == validationModel);
				ret[0] = true;
			}});

		validationModel.doInValidationTransaction(new ValidationTransaction(){

			public void execute() throws ValidationException {
				final UserAccount userAccount = new UserAccount();
				userAccount.setUsername("user");
				userAccount.setPassword("1234");
				userAccountService.save(userAccount);
			}});

		assertTrue(ret[0]);
	}

	//-------------------------------------------------------------------------
	/**
	 * Gets the user account service.
	 *
	 * @return the userAccountService
	 */
	public UserAccountService getUserAccountService() {
		return userAccountService;
	}

	/**
	 * Sets the user account service.
	 *
	 * @param userAccountService the userAccountService to set
	 */
	public void setUserAccountService(UserAccountService userAccountService) {
		this.userAccountService = userAccountService;
	}

	/**
	 * Gets the authentication service.
	 *
	 * @return the authenticationService
	 */
	public AuthenticationService getAuthenticationService() {
		return authenticationService;
	}

	/**
	 * Sets the authentication service.
	 *
	 * @param authenticationService the authenticationService to set
	 */
	public void setAuthenticationService(AuthenticationService authenticationService) {
		this.authenticationService = authenticationService;
	}

	/**
	 * @return the personService
	 */
	public PersonService getPersonService() {
		return personService;
	}

	/**
	 * @param personService the personService to set
	 */
	public void setPersonService(PersonService personService) {
		this.personService = personService;
	}

}
