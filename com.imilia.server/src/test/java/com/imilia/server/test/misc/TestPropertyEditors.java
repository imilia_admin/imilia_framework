package com.imilia.server.test.misc;

import static org.junit.Assert.assertTrue;

import java.util.Locale;

import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.junit.Test;
import org.springframework.beans.MutablePropertyValues;
import org.springframework.beans.PropertyValue;
import org.springframework.validation.DataBinder;

import com.imilia.server.databinding.DataBinderFactory;

public class TestPropertyEditors {

	private final static Interval TEST_INTERVAL =
		new Interval(new DateTime(2009,1,1,8,0,0,0), new DateTime(2009,1,1,8,30,0,0));
	private Interval interval = TEST_INTERVAL;

	private String test = "01.01.2009 08:00:00-01.01.2009 08:30:00"; 
	

	@Test
	public void testGetIntervalPropertyEditor(){
		DataBinder dataBinder = 
			DataBinderFactory.getDataBinder(this, "test", Locale.GERMAN);
		MutablePropertyValues propertyValues = new MutablePropertyValues();

		dataBinder.bind(propertyValues);
		
		Object  i = dataBinder.getBindingResult().getFieldValue("interval");
		String s = i.toString();
		assertTrue(s.equals(test));
	}

	@Test
	public void testSetIntervalPropertyEditor(){
		DataBinder dataBinder = 
			DataBinderFactory.getDataBinder(this, "test", Locale.GERMAN);
		
		interval = null;
		MutablePropertyValues propertyValues = new MutablePropertyValues();
		
		propertyValues.addPropertyValue(new PropertyValue("interval", test));
		dataBinder.bind(propertyValues);
		
		assertTrue(TEST_INTERVAL.equals(interval));
	}

	public Interval getInterval() {
		return interval;
	}

	public void setInterval(Interval interval) {
		this.interval = interval;
	}

	public String getTest() {
		return test;
	}

	public void setTest(String test) {
		this.test = test;
	}
}
