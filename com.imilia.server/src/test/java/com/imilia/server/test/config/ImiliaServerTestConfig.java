/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2014 - all rights reserved
 * Created on: Mar 14, 2014
 * Created by: emcgreal
 */
package com.imilia.server.test.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.avaje.ebean.config.EncryptDeployManager;
import com.avaje.ebean.config.EncryptKeyManager;
import com.avaje.ebean.config.ServerConfig;
import com.imilia.server.config.AbstractEbeanConfig;
import com.imilia.server.config.ImiliaServerConfig;
import com.imilia.server.config.ImiliaServerSecurityConfig;
import com.imilia.server.config.ImiliaServerValidationConfig;
import com.imilia.server.domain.Crud;
import com.imilia.server.domain.CurrentUser;
import com.imilia.server.domain.access.CrudTarget;
import com.imilia.server.domain.access.ModelCrud;
import com.imilia.server.domain.access.Role;
import com.imilia.server.domain.access.Subscription;
import com.imilia.server.domain.access.SubscriptionRole;
import com.imilia.server.domain.common.Address;
import com.imilia.server.domain.common.Person;
import com.imilia.server.domain.common.PhoneContact;
import com.imilia.server.domain.common.UserAccount;
import com.imilia.server.domain.filter.Recipe;
import com.imilia.server.domain.filter.RecipeHolder;
import com.imilia.server.domain.i18n.LocaleText;
import com.imilia.server.domain.i18n.LocaleTextHolder;
import com.imilia.server.domain.query.entities.QueryJoin;
import com.imilia.server.domain.query.entities.QueryProperty;
import com.imilia.server.domain.query.entities.QueryRestriction;
import com.imilia.server.domain.query.entities.QueryRoleRestriction;
import com.imilia.server.domain.query.entities.QueryTemplate;
import com.imilia.server.domain.query.entities.QueryView;
import com.imilia.server.domain.validation.ValidationClassSpec;
import com.imilia.server.domain.validation.ValidationPropertySpec;
import com.imilia.server.test.models.DefaultCurrentUser;
import com.imilia.utils.ResourceBean;

/**
 * The Class ImiliaServerTestConfig.
 */
@Configuration
@Import({ImiliaServerValidationConfig.class, ImiliaServerConfig.class, ImiliaServerSecurityConfig.class })
@EnableTransactionManagement
public class ImiliaServerTestConfig extends AbstractEbeanConfig {
	
	/** The resource loader. */
	@Autowired
	protected ResourceLoader resourceLoader;
	
	/* (non-Javadoc)
	 * @see com.imilia.server.config.AbstractEbeanConfig#getDatabaseJndiName()
	 */
	@Override
	protected String getDatabaseJndiName() {
		return "java:comp/env/jdbc/imiliaserver";
	}

	/* (non-Javadoc)
	 * @see com.imilia.server.config.AbstractEbeanConfig#getEncryptDeployManager()
	 */
	@Override
	public EncryptDeployManager getEncryptDeployManager() {
		return null;
	}

	/* (non-Javadoc)
	 * @see com.imilia.server.config.AbstractEbeanConfig#getEncryptKeyManager()
	 */
	@Override
	public EncryptKeyManager getEncryptKeyManager() {
		return null;
	}

	/* (non-Javadoc)
	 * @see com.imilia.server.config.AbstractEbeanConfig#registerClasses(com.avaje.ebean.config.ServerConfig)
	 */
	@Override
	protected void registerClasses(ServerConfig serverConfig) {
		registerClasses(serverConfig, 
				Person.class,
				Address.class,
				PhoneContact.class,
				UserAccount.class,
				Role.class,
				SubscriptionRole.class,
				Subscription.class,
				ModelCrud.class,
				CrudTarget.class,
				QueryJoin.class,
				QueryProperty.class,
				QueryRestriction.class,
				QueryRoleRestriction.class,
				QueryTemplate.class,
				QueryView.class,
				Crud.class,
				LocaleText.class,
				LocaleTextHolder.class,
				ValidationClassSpec.class,
				ValidationPropertySpec.class
				);
	}
	

	@Bean(name="domainConfigJson")
	public ResourceBean getPersonJson(){
		return new ResourceBean(resourceLoader.getResource("data/DomainConfig.json"));
	}

	@Bean
	public CurrentUser getCurrentUser(){
		return new DefaultCurrentUser();
	}

	/**
	 * Gets the message source using the server messages
	 *
	 * @return the message source 
	 */
	@Bean(name="messageSource")
	public MessageSource getMessageSource(){
		final ReloadableResourceBundleMessageSource r = new ReloadableResourceBundleMessageSource();
		r.setBasenames("com/imilia/server/resource/localization/Messages");
		return r;
	}
	/**
	 * Gets the resource loader.
	 *
	 * @return the resource loader
	 */
	public ResourceLoader getResourceLoader() {
		return resourceLoader;
	}

	/**
	 * Sets the resource loader.
	 *
	 * @param resourceLoader the new resource loader
	 */
	public void setResourceLoader(ResourceLoader resourceLoader) {
		this.resourceLoader = resourceLoader;
	}
	
}
