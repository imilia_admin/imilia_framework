/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 *
 * Created on: Mar 3, 2009
 * Created by: emcgreal
 */
package com.imilia.server.test.service;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.imilia.server.config.ImiliaServerConfig;
import com.imilia.server.config.ImiliaServerValidationConfig;
import com.imilia.server.domain.common.Address;
import com.imilia.server.domain.common.UserAccount;
import com.imilia.server.domain.validation.ValidationClassSpec;
import com.imilia.server.domain.validation.ValidationPropertySpec;
import com.imilia.server.service.AddressService;
import com.imilia.server.service.AuthenticationService;
import com.imilia.server.test.config.ImiliaServerTestConfig;
import com.imilia.server.validation.ValidationException;

/**
 * The Class TestUserAccountService test the UserAccount service.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ImiliaServerConfig.class, ImiliaServerValidationConfig.class, ImiliaServerTestConfig.class})
public class TestAddressService  {
	final static String PW = "Haljk2.W7=";
	
	static final Logger logger = LoggerFactory.getLogger(TestAddressService.class);

	/** The authentication service. */
	@Autowired
	private AuthenticationService authenticationService;

	@Autowired
	private AddressService addressService;

	/**
	 * Setup.
	 */
	@Before
	public void setup(){
		authenticationService.login(UserAccount.INIT_JOB_AUTHENTICATION_REQUEST);
	}

	public final AddressService getAddressService() {
		return addressService;
	}

	public final void setAddressService(AddressService addressService) {
		this.addressService = addressService;
	}
	
	@Test(expected=ValidationException.class)
	public void testInvalidAddress() throws ValidationException {
		Address address = new Address();
		final ValidationPropertySpec propetySpec = new ValidationPropertySpec("street");
		propetySpec.setMandatory(true);

		ValidationClassSpec spec = new ValidationClassSpec();
		spec.add(propetySpec);
		address.setValidationClassSpec(spec);
		
		addressService.save(address);
	}
	
	@Test
	public void testValidAddress() throws ValidationException {
		Address address = new Address();
		address.setStreet("Main Street");
		final ValidationPropertySpec propetySpec = new ValidationPropertySpec("street");
		propetySpec.setMandatory(true);

		ValidationClassSpec spec = new ValidationClassSpec();
		spec.add(propetySpec);
		
		addressService.save(address);
	}
	
	@Test
	public void testAutoDetectMandatory(){
		ValidationClassSpec v = new ValidationClassSpec(Address.class);
		
		v.addMandatoryFromClass();
		
		assertTrue(v.getPropertySpecs().size() == 0);
	}

}
