package com.imilia.server.test.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.avaje.ebean.config.ServerConfig;
import com.imilia.server.domain.importer.DomainConfigImporter;
import com.imilia.server.domain.importer.DomainConfigJsonImporterImpl;
import com.imilia.server.init.InitDatabase;
import com.imilia.server.service.InitDatabaseService;
import com.imilia.server.service.impl.InitDatabaseServiceImpl;
import com.imilia.server.test.init.InitDatabaseImpl;
import com.imilia.utils.i18n.DomainLocale;

@Configuration
public class ImiliaServerInitTestConfig extends ImiliaServerTestConfig {
	
	@Bean
	public ServerConfig getServerConfig(){
		final ServerConfig serverConfig = super.getServerConfig();
		serverConfig.setDdlGenerate(true);
		serverConfig.setDdlRun(true);
		return serverConfig;
	}
	
	
	@Bean
	public InitDatabase getInitDatabase(){
		return new InitDatabaseImpl();
	}
	
	
	@Bean
	public InitDatabaseService getInitDatabaseService(){
		return new InitDatabaseServiceImpl();
	}
	
	@Bean
	public DomainConfigImporter getDomainConfigImporter(){
		DomainConfigJsonImporterImpl domainConfigImporter = new DomainConfigJsonImporterImpl();
		domainConfigImporter.setDomainLocale(DomainLocale.de_DE);
		
		return domainConfigImporter;
	}
}
