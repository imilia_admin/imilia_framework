package com.imilia.server.test.mvc;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.imilia.server.domain.CurrentUser;
import com.imilia.server.domain.model.impl.DefaultAppModel;
import com.imilia.server.domain.model.manager.impl.ModelManager;
import com.imilia.server.domain.model.property.impl.ApplicationPropertyCrudProviderImpl;

public class TestModelManager {

	@Test
	public void testBuildFromAnnotations(){
		
		TestModel m = new TestModel();
		DefaultAppModel<CurrentUser> appModel = new DefaultAppModel<CurrentUser>();
		appModel.setApplicationPropertyCrudProvider(new ApplicationPropertyCrudProviderImpl());
		m.setAppModel(appModel);
		
		ModelManager<TestModel> modelManager = new ModelManager<TestModel>(m);
		modelManager.init();
		
		assertTrue(modelManager.getProperties().size()== 2);
	}
}
