package com.imilia.server.test.misc;

import static org.junit.Assert.assertTrue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.junit.Test;

import com.imilia.server.domain.common.Address;

public class TestAddress {
	// The logger
	private static Logger logger = LoggerFactory.getLogger(TestAddress.class);
	
	@Test
	public void testSetStreetAndHouseNumber(){
		final Address address = new Address();
		
		String[] streets = {"Main Str ", "Street14 ", "No number", "Kottbusser Str. "};
		String[] houseNumbers = {"17", "14A", null, "3"};
		
		for (int i = 0;i < streets.length;i++){
			address.setHouseNumber(null);
			
			final String refHouseNumber = houseNumbers[i];
			
			address.setStreet(streets[i] + (refHouseNumber == null ? "" : refHouseNumber));
			String houseNumber = address.getHouseNumber();
			logger.debug("houseNumber:" + houseNumber);
			assertTrue((houseNumber == null && refHouseNumber == null) || refHouseNumber.equals(houseNumber));
		}
	}
}
