package com.imilia.server.test.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.imilia.server.config.ImiliaServerConfig;
import com.imilia.server.config.ImiliaServerSecurityConfig;
import com.imilia.server.config.ImiliaServerValidationConfig;
import com.imilia.server.domain.access.AuthenticationRequest;
import com.imilia.server.domain.common.UserAccount;
import com.imilia.server.domain.password.PasswordCandidate;
import com.imilia.server.service.AuthenticationService;
import com.imilia.server.service.UserAccountService;
import com.imilia.server.service.impl.PasswordStrengthChecker;
import com.imilia.server.test.config.ImiliaServerTestConfig;
import com.imilia.server.validation.ValidationException;

/**
 * The Class TestAuthenticationService.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ImiliaServerConfig.class, ImiliaServerValidationConfig.class, ImiliaServerSecurityConfig.class, ImiliaServerTestConfig.class})
public class TestAuthenticationService {

	private Logger logger = LoggerFactory.getLogger(TestAuthenticationService.class);
	/** The authentication service. */
	@Autowired
	private AuthenticationService authenticationService;
	
	/** The user account service. */
	@Autowired
	private UserAccountService userAccountService;

	/** The password strength checker. */
	@Autowired
	private PasswordStrengthChecker passwordStrengthChecker;

	@Autowired
	private MessageSource messageSource;
	/**
	 * Instantiates a new test authentication service.
	 */
	public TestAuthenticationService() {
	}

	/**
	 * Test init job authentication.
	 */
	@Before
	public void testInitJobAuthentication(){
//		UserAccount ac = userAccountService.find(UserAccount.class, 1);
//		PasswordCandidate passwordCandidate = new PasswordCandidate(UserAccount.INIT_JOB_PW, UserAccount.INIT_JOB_PW);
//
//		try {
//			authenticationService.changePassword(ac, passwordCandidate);
//		} catch (ValidationException e) {
//		}

		authenticationService.login(UserAccount.INIT_JOB_AUTHENTICATION_REQUEST);
	}

	/**
	 * Test encode password.
	 * 
	 * @throws ValidationException the validation exception
	 */
	@Test
	public void testEncodePassword() throws ValidationException{
		String encoded = authenticationService.encode("hallo1");

		assertTrue("RfWVoa/C8mazkg3J9tsAZY1nR44=".equals(encoded));
	}

	/**
	 * Test change password.
	 * 
	 * @throws ValidationException the validation exception
	 */
	@Test
	public void testChangePassword() throws ValidationException{
		UserAccount ac = (UserAccount) authenticationService.login(UserAccount.INIT_JOB_AUTHENTICATION_REQUEST).getPrincipal();

		assertNotNull(ac);
		long oid = ac.getOid();
		
		ac = userAccountService.find(UserAccount.class, oid);
		assertNotNull(ac);
		logger.info("1. UserAccount version: " + ac.getVersion());

		String pw = "Abcdef12.$";
		PasswordCandidate passwordCandidate = new PasswordCandidate(pw, pw);
		authenticationService.changePassword(ac, passwordCandidate, passwordStrengthChecker, messageSource);
		logger.info("2. UserAccount version: " + ac.getVersion());

		authenticationService.login(new AuthenticationRequest(UserAccount.INIT_JOB, pw));

		passwordCandidate = new PasswordCandidate(UserAccount.INIT_JOB_PW, UserAccount.INIT_JOB_PW);
		ac = userAccountService.find(UserAccount.class, ac.getOid());
		authenticationService.changePassword(ac, passwordCandidate, passwordStrengthChecker, messageSource);
	}
	
	/**
	 * Test password strengths.
	 */
	@Test
	public void testPasswordStrengths(){
		String pw = "HfN5.8LrsM%g0#v>";

		StringBuffer logBuffer = new StringBuffer();
		int strength = passwordStrengthChecker.checkPasswordStrength(pw, logBuffer);
		
		logger.debug("Strength: " + strength);
		
		assertTrue(strength == 56);
	}

	/**
	 * Gets the authentication service.
	 * 
	 * @return the authenticationService
	 */
	public AuthenticationService getAuthenticationService() {
		return authenticationService;
	}

	/**
	 * Sets the authentication service.
	 * 
	 * @param authenticationService the authenticationService to set
	 */
	public void setAuthenticationService(AuthenticationService authenticationService) {
		this.authenticationService = authenticationService;
	}

	/**
	 * Gets the user account service.
	 * 
	 * @return the user account service
	 */
	public final UserAccountService getUserAccountService() {
		return userAccountService;
	}

	/**
	 * Sets the user account service.
	 * 
	 * @param userAccountService the new user account service
	 */
	public final void setUserAccountService(UserAccountService userAccountService) {
		this.userAccountService = userAccountService;
	}

	/**
	 * Gets the password strength checker.
	 * 
	 * @return the password strength checker
	 */
	public PasswordStrengthChecker getPasswordStrengthChecker() {
		return passwordStrengthChecker;
	}

	/**
	 * Sets the password strength checker.
	 * 
	 * @param passwordStrengthChecker the new password strength checker
	 */
	public void setPasswordStrengthChecker(
			PasswordStrengthChecker passwordStrengthChecker) {
		this.passwordStrengthChecker = passwordStrengthChecker;
	}

	public MessageSource getMessageSource() {
		return messageSource;
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

}
