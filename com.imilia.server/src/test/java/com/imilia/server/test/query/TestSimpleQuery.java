/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2014 - all rights reserved
 * Created on: Feb 27, 2014
 * Created by: emcgreal
 */
package com.imilia.server.test.query;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.imilia.server.domain.common.UserAccount;
import com.imilia.server.domain.query.Query;
import com.imilia.server.domain.query.QueryResult;
import com.imilia.server.domain.query.execution.Comparison;
import com.imilia.server.domain.query.execution.QueryExecution;
import com.imilia.server.domain.query.execution.values.BooleanQueryPropertyValue;
import com.imilia.server.service.QueryService;
import com.imilia.server.service.RoleService;
import com.imilia.server.test.config.TestConfig;

/**
 * The Class TestSimpleQuery.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestConfig.class)
public class TestSimpleQuery {
	
	/** The role query template. */
	@Autowired
	private Query<UserAccount> queryUserAccount;
	
	/** The query service. */
	@Autowired
	private QueryService queryService;
	
	@Autowired
	private RoleService roleService;
	/**
	 * Test find roles.
	 */
	@Test
	public void testUserAccountsWithRoleAdmin(){
		QueryResult<UserAccount> result = new QueryResult<>();
		
		queryUserAccount.deploy(roleService.findRoles("admin"));
		
		QueryExecution<UserAccount> queryExecution = 
				new QueryExecution<UserAccount>(queryUserAccount, this);
		
		queryService.query(queryExecution, result);
		assertTrue(result.size() == 5);
	}

	@Test
	public void testUserAccountsNonLockedWithRoleAdmin(){
		QueryResult<UserAccount> result = new QueryResult<>();
		
		queryUserAccount.deploy(roleService.findRoles("admin"));
		
		QueryExecution<UserAccount> queryExecution = 
				new QueryExecution<UserAccount>(queryUserAccount, this);
		
		BooleanQueryPropertyValue v = 
				(BooleanQueryPropertyValue) queryUserAccount.getQueryPropertyValue("accountNonLocked");
		
		v.setBooleanValue(true);
		v.setComparison(Comparison.Equals);
		queryService.query(queryExecution, result);
		assertTrue(result.size() == 5);
	}

	public void testUserAccountsLockedWithRoleAdmin(){
		QueryResult<UserAccount> result = new QueryResult<>();
		
		queryUserAccount.deploy(roleService.findRoles("admin"));
		
		QueryExecution<UserAccount> queryExecution = 
				new QueryExecution<UserAccount>(queryUserAccount, this);
		
		BooleanQueryPropertyValue v = 
				(BooleanQueryPropertyValue) queryUserAccount.getQueryPropertyValue("accountNonLocked");
		
		v.setBooleanValue(false);
		v.setComparison(Comparison.Equals);
		queryService.query(queryExecution, result);
		assertTrue(result.size() == 0);
	}
	
	@Test
	public void testUserAccountsWithRoleManager(){
		QueryResult<UserAccount> result = new QueryResult<>();
		
		queryUserAccount.deploy(roleService.findRoles("manager"));
		
		QueryExecution<UserAccount> queryExecution = 
				new QueryExecution<UserAccount>(queryUserAccount, this);
		queryService.query(queryExecution, result);
		assertTrue(result.size() == 1);
	}
	
//	@Test
//	public void testFindPersonAsManager(){
//		QueryResult<Person> result = new QueryResult<>();
//		
//		QueryExection<Person> ctx = new QueryExection<Person>(personQueryTemplate, roleService.findRoles("manager"), this);
//		
//		queryService.query(ctx, result);
//		assertTrue(!result.isEmpty());
//	}

	public String getModelAuthority(){
		return "user";
	}
	
	/**
	 * Gets the query service.
	 *
	 * @return the query service
	 */
	public QueryService getQueryService() {
		return queryService;
	}

	/**
	 * Sets the query service.
	 *
	 * @param queryService the new query service
	 */
	public void setQueryService(QueryService queryService) {
		this.queryService = queryService;
	}

	public Query<UserAccount> getQueryUserAccount() {
		return queryUserAccount;
	}

	public void setQueryUserAccount(Query<UserAccount> queryUserAccount) {
		this.queryUserAccount = queryUserAccount;
	}

	public RoleService getRoleService() {
		return roleService;
	}

	public void setRoleService(RoleService roleService) {
		this.roleService = roleService;
	}
	
}
