package com.imilia.server.test.xml;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.XMLEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import com.imilia.server.domain.common.Address;
import com.imilia.server.domain.common.Person;
import com.imilia.server.test.xml.TestStax;
import com.imilia.server.xml.DefaultStaxHandler;
import com.imilia.server.xml.DefaultStaxObjectFactory;
import com.imilia.server.xml.StaxHandler;
import com.imilia.server.xml.StaxHandlerFactory;
import com.imilia.server.xml.StaxObjectFactory;
import com.imilia.utils.ResourceBean;
import com.imilia.utils.i18n.DomainLocale;

@ContextConfiguration(locations={"/xml-test.xml"})
public class TestStax extends AbstractJUnit4SpringContextTests {
	// The logger
	private static Logger logger = LoggerFactory.getLogger(TestStax.class);
	
	@Autowired
	private ResourceBean xmlDoc;

	@Test
	public void testStax(){
		
		try {
			XMLStreamReader xmlStreamReader =
			        XMLInputFactory.newInstance().
			            createXMLStreamReader(xmlDoc.getResource().getInputStream());
			
			StaxHandlerFactory factory = new StaxHandlerFactory(){

				public StaxHandler<?> create(QName elementName, XMLStreamReader xmlStreamReader, StaxObjectFactory objectFactory) {
					final String className = elementName.getLocalPart();
					
					if ("person".equals(className)){
						return new DefaultStaxHandler<Person>(this, new Person(), objectFactory);
					}else if ("address".equals(className)){
						return new DefaultStaxHandler<Address>(this, new Address(), objectFactory);
					}else

					return null;
				}

				@Override
				public void register(String elementName, Class<?> clazz) {
				}

				@Override
				public void register(Class<?>... clazzs) {
				}};
			
			while (xmlStreamReader.hasNext()){
				switch(xmlStreamReader.next()){
				case XMLEvent.START_ELEMENT:
					QName qn = xmlStreamReader.getName();
					
					final DefaultStaxObjectFactory objectFactory = new DefaultStaxObjectFactory(DomainLocale.en_US);
					StaxHandler<?> handler = factory.create(qn, xmlStreamReader, objectFactory);
					Person p = (Person) handler.handle(xmlStreamReader);

					logger.debug("Person:" + p.getLastNameCommaFirst());
					
				}
			}
		} catch (Exception ex){
			logger.error("Test stax failed", ex);
		}
	}

	public final ResourceBean getXmlDoc() {
		return xmlDoc;
	}

	public final void setXmlDoc(ResourceBean xmlDoc) {
		this.xmlDoc = xmlDoc;
	}
}
