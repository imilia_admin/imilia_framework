package com.imilia.server.test.mvc;

import com.imilia.server.domain.model.event.IEventSource.Listener;
import com.imilia.server.domain.model.event.IInvoker;
import com.imilia.server.domain.model.event.impl.Event;
import com.imilia.server.domain.model.event.impl.Invoker;
import com.imilia.server.validation.ValidationException;

public class TestModelController {
	private final TestModel testModel;

	private IInvoker saveCommand = new Invoker<TestModelController>(this);
	
	public TestModelController(TestModel testModel) {
		super();
		this.testModel = testModel;
		init();
	}

	public void save() throws ValidationException{
		testModel.save(saveCommand);
	}
	
	private void init(){
		testModel.getSaved().register(new Listener<Event<TestModel>>() {
			public void fired(Event<TestModel> event) {
				// Did this controller initiate the event?
				if (event.getInvoker() == saveCommand){
					
				}else{
					// Just ignore this event
				}
			}
		});

		testModel.getDeleted().register(new Listener<Event<TestModel>>() {
			public void fired(Event<TestModel> event) {
				// Model deleted the user account
			}
		});
	}
}
