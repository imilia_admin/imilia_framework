package com.imilia.server.test.utils;

import static org.junit.Assert.assertTrue;

import java.lang.reflect.Field;

import org.junit.Test;

import com.imilia.server.domain.common.Address;
import com.imilia.utils.classes.ClassUtils;

public class TestClassUtils {
	
	@Test
	public void testPathWithList(){
		Field f1 = ClassUtils.findFieldFromPath(Address.class, "phoneContacts.comment");
			
		assertTrue(String.class.equals(f1.getType()));	
	}
}
