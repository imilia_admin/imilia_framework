/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 *
 * Created on: Mar 3, 2009
 * Created by: emcgreal
 */
package com.imilia.server.test.service;

import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.imilia.server.config.ImiliaServerConfig;
import com.imilia.server.config.ImiliaServerValidationConfig;
import com.imilia.server.domain.common.Person;
import com.imilia.server.domain.common.UserAccount;
import com.imilia.server.service.AuthenticationService;
import com.imilia.server.service.PersonService;
import com.imilia.server.service.UserAccountService;
import com.imilia.server.test.config.ImiliaServerTestConfig;
import com.imilia.server.validation.ValidationException;
import com.imilia.utils.i18n.DomainLocale;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ImiliaServerConfig.class, ImiliaServerValidationConfig.class, ImiliaServerTestConfig.class})
public class TestPersonService {

	/** The user account service. */
	@Autowired
	private PersonService personService;

	@Autowired
	private UserAccountService userAccountService;

	@Autowired
	private AuthenticationService authenticationService;

	public TestPersonService() {
		super();
	}

	@Before
	public void setup(){
		authenticationService.login(UserAccount.INIT_JOB_AUTHENTICATION_REQUEST);
	}

	@Test
	public void saveFindDeleteCreatePerson() throws ValidationException {


		final Person person = new Person();
		person.setLastName("test");
		person.setDomainLocale(DomainLocale.en_US);

		personService.save(person);

		try{
			Person p = personService.find(Person.class, person.getOid());
			assertNotNull(p);

			personService.delete(person);
		}catch(ValidationException ex){
			throw ex;
		}
	}

	/**
	 * @return the personService
	 */
	public PersonService getPersonService() {
		return personService;
	}

	/**
	 * @param personService
	 *            the personService to set
	 */
	public void setPersonService(PersonService personService) {
		this.personService = personService;
	}

	/**
	 * @return the userAccountService
	 */
	public UserAccountService getUserAccountService() {
		return userAccountService;
	}

	/**
	 * @param userAccountService the userAccountService to set
	 */
	public void setUserAccountService(UserAccountService userAccountService) {
		this.userAccountService = userAccountService;
	}

	/**
	 * @return the authenticationService
	 */
	public AuthenticationService getAuthenticationService() {
		return authenticationService;
	}

	/**
	 * @param authenticationService the authenticationService to set
	 */
	public void setAuthenticationService(AuthenticationService authenticationService) {
		this.authenticationService = authenticationService;
	}

}
