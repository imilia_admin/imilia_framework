package com.imilia.server.test.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.io.ResourceLoader;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.imilia.server.domain.access.Role;
import com.imilia.server.domain.common.Person;
import com.imilia.server.domain.common.UserAccount;
import com.imilia.server.domain.query.Query;

@Configuration
@Import({ImiliaServerTestConfig.class})
@EnableTransactionManagement
public class TestConfig {

	@Autowired
	private ResourceLoader resourceLoader;
	
	@Bean(name="roleQueryTemplate")
	public Query<Role> getRoleQueryTemplate(){
		return new Query<Role>(Role.class, "com.imilia.server.domain.access.Role.findRole");
	}

	@Bean(name="personQueryTemplate")
	public Query<Person> getPersonQueryTemplate(){
		return new Query<Person>(Person.class, "com.imilia.server.domain.common.Person.findPerson");
	}

	@Bean(name="userAccountQueryTemplate")
	public Query<UserAccount> getUserAccountQueryTemplate(){
		return new Query<UserAccount>(UserAccount.class, "findUserAccount");
	}
}
