package com.imilia.server.test.mvc;

import com.imilia.server.domain.common.UserAccount;
import com.imilia.server.domain.model.annotations.Cmd;
import com.imilia.server.domain.model.annotations.Evt;
import com.imilia.server.domain.model.annotations.Model;
import com.imilia.server.domain.model.annotations.Prp;
import com.imilia.server.domain.model.event.IInvoker;
import com.imilia.server.domain.model.event.impl.Event;
import com.imilia.server.domain.model.event.impl.EventSource;
import com.imilia.server.domain.model.impl.DefaultModel;
import com.imilia.server.validation.ValidationException;

@Model(
	name="TestModel",
	prps = { 
			@Prp(value="name", path="userAccount.username"), 
			@Prp(value="password", path="userAccount.password") 
	},
	cmds = {
		@Cmd(value="save"),	
		@Cmd(value="delete")	
	},
	evts = {
		@Evt(name="saved"),	
		@Evt(name="deleted")	
	}
)
public class TestModel extends DefaultModel{

	// Entity
	private UserAccount userAccount;
	
	// Event Sources
	private EventSource<Event<TestModel>> saved = new EventSource<Event<TestModel>>();
	private EventSource<Event<TestModel>> deleted = new EventSource<Event<TestModel>>();
	
	// Commands
	public void save(IInvoker command) throws ValidationException{
		//save userAccount etc
		//....
		saved.fire(new Event<TestModel>(this, command));
	}
	
	public void delete(IInvoker command) throws ValidationException{
		deleted.fire(new Event<TestModel>(this, command));
	}
	
	// Getter and setters 
	public EventSource<Event<TestModel>> getSaved(){
		return saved;
	}
	
	public EventSource<Event<TestModel>> getDeleted(){
		return deleted;
	}

	public UserAccount getUserAccount() {
		return userAccount;
	}

	public void setUserAccount(UserAccount userAccount) {
		this.userAccount = userAccount;
	}
}
