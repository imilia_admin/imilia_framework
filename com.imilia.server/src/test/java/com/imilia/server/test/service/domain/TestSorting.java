package com.imilia.server.test.service.domain;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Locale;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.imilia.server.config.ImiliaServerConfig;
import com.imilia.server.config.ImiliaServerValidationConfig;
import com.imilia.server.domain.i18n.DomainLocaleComparator;
import com.imilia.server.test.config.ImiliaServerTestConfig;
import com.imilia.utils.i18n.DomainLocale;
import com.imilia.utils.i18n.Localized;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ImiliaServerConfig.class, ImiliaServerValidationConfig.class, ImiliaServerTestConfig.class})
public class TestSorting {

	@Autowired
	private DomainLocaleComparator<Object> domainLocaleComparator;

	class TestObject{

		private String x;
		
		private Localized l = null;
		
		public TestObject(String x){
			this.x = x;
		}
		
		public Localized getLocalized(){
			if (l == null){
				l = new Localized() {
				
					public String getLocalizedMessage(MessageSource messageSource, Locale locale) {
						return messageSource.getMessage(x, null, locale);
					}
				};
			}
			return l;
		}
		
	};
	
	@Test
	public void testIt(){
		final int count = 20;
		TestObject x1 = new TestObject("Generic.false");
		TestObject x2 = new TestObject("Generic.true");
		
		ArrayList<TestObject> list = new ArrayList<TestObject>();
		for (int i = 0;i < count;i++){
			list.add(x1);
			list.add(x2);
		}

		Collections.sort(list, new Comparator<TestObject>() {

			public int compare(TestObject lhs, TestObject rhs) {
				return domainLocaleComparator.compare(lhs.getLocalized(), rhs.getLocalized(), DomainLocale.en_US);
			}
		});

		for (int i = 0;i < list.size();i++){
			if (i < count){
				assertTrue("Generic.false".equals(list.get(i).x));
			}else{
				assertTrue("Generic.true".equals(list.get(i).x));

			}
		}
	}

	public DomainLocaleComparator<Object> getDomainLocaleComparator() {
		return domainLocaleComparator;
	}

	public void setDomainLocaleComparator(
			DomainLocaleComparator<Object> domainLocaleComparator) {
		this.domainLocaleComparator = domainLocaleComparator;
	}


}
