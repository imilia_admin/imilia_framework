package com.imilia.server.test.init;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.imilia.server.init.InitDatabase;
import com.imilia.server.service.InitDatabaseService;
import com.imilia.server.test.config.ImiliaServerInitTestConfig;
import com.imilia.server.test.init.InitDatabaseImpl;
import com.imilia.utils.i18n.DomainLocale;

public class InitDatabaseImpl implements InitDatabase {
	/** Logger used to log warnings, errors and debug messages */
	private final static Logger logger = LoggerFactory.getLogger(InitDatabaseImpl.class);

	private static ConfigurableApplicationContext context;

	@Autowired
	private InitDatabaseService initDatabaseService;

	public static void main(String[] args) {
		context = new AnnotationConfigApplicationContext(ImiliaServerInitTestConfig.class);
		context.registerShutdownHook();
		
		final InitDatabase initDatabase = context.getBean(InitDatabase.class);
		initDatabase.init();
	}

	public void init() {
		try {
			logger.info("create bootstrap entries...");
			initDatabaseService.createBootstrapEntries();
			logger.info("create bootstrap entries completed");
	
			logger.info("authenticate init job...");
			initDatabaseService.authenticateInitJob();
			logger.info("authenticated init job...");
	
			logger.info("create seed data...");
		
			initDatabaseService.createSeedData(DomainLocale.de_DE);
		} catch (Exception e) {
			logger.error("Exception encountered during seed data creation", e);
		}
		logger.info("create seed data completed");
	}

	/**
	 * @return the initDatabaseService
	 */
	public InitDatabaseService getInitDatabaseService() {
		return initDatabaseService;
	}

	/**
	 * @param initDatabaseService the initDatabaseService to set
	 */
	public void setInitDatabaseService(InitDatabaseService initDatabaseService) {
		this.initDatabaseService = initDatabaseService;
	}
}
