/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2011 - all rights reserved
 * Created on: Jun 28, 2011
 * Created by: emcgreal
 */
package com.imilia.server.i18n;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.List;
import java.util.Properties;

import javax.persistence.Entity;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.type.classreading.MetadataReader;

import com.imilia.utils.SortedProperties;
import com.imilia.utils.classes.ClassFinder;
import com.imilia.utils.classes.ClassMatcher;
import com.imilia.utils.classes.ClassUtils;

/**
 * The Class RestDoc.
 */
public class I18nDocImpl implements I18nDoc {

	/** The Constant logger. */
	private final static Logger logger = LoggerFactory.getLogger(I18nDocImpl.class);

	private String[] BEGIN_IGNORE = {"_"};
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		I18nDocImpl impl = new I18nDocImpl();
		try {
			final Properties properties = new SortedProperties();
			properties.load(new FileInputStream(args[1]));
			
			impl.process(args[0], properties);
			
			final FileOutputStream fo = new FileOutputStream(args[1]);
			properties.store(fo, null);
			fo.close();
			
		} catch (Exception e) {
			logger.error("Error", e);
		}
	}
	
	/**
	 * Find and process classes.
	 * 
	 * @param basePackage
	 *            the base package
	 * @throws Exception
	 *             the exception
	 */
	public void process(String basePackage, Properties propertiesFile) throws Exception {
		ClassFinder classFinder = new ClassFinder();
		List<Class<?>> classes = classFinder.findTypes(basePackage, new ClassMatcher(){

			@Override
			public boolean isCandidate(MetadataReader metadataReader) throws ClassNotFoundException {
				final Class<?> c = Class.forName(metadataReader.getClassMetadata()
						.getClassName());
				return isCandidate(c);
			}

			@Override
			public boolean isCandidate(Field field) {
				return true;
			}

			@Override
			public boolean isCandidate(Class<?> clazz) {
				return (clazz.getAnnotation(Entity.class) != null); 
			}});

		for (Class<?> c : classes) {
			handleClass(c, propertiesFile);
		}
	}


	private void handleClass(Class<?> c, Properties propertiesFile) {
		List<Field> fields = ClassUtils.findFields(c);
		
		final String className = c.getName();
		
		if (!propertiesFile.containsKey(className)){
			propertiesFile.put(className, generateValue(c.getSimpleName()));
		}
		
		for (Field f:fields){
			
			if (isCandiate(f)){
				final String fieldName = f.getName();
				String key = className + "." + fieldName;
				
				if (!propertiesFile.containsKey(key)){
					propertiesFile.put(key, generateValue(fieldName));
				}
			}
		}
	}
	
	private String generateValue(String fieldName){
		StringBuilder builder = new StringBuilder();
		
		char[] chars = fieldName.toCharArray();
		
		for (char c:chars){
			if (builder.length() == 0){
				builder.append(Character.toUpperCase(c));
			}else{
				if (Character.isUpperCase(c)){
					builder.append(' ');
					builder.append(Character.toLowerCase(c));
				}else{
					builder.append(c);
				}
			}
		}
		
		return builder.toString();
	}
	
	
	
	private boolean isCandiate(Field field){
		if (Modifier.isStatic(field.getModifiers())){
			return false;
		}
		final String fieldName = field.getName();
		
		for (String s:BEGIN_IGNORE){
			if (fieldName.startsWith(s)){
				return false;
			}
		}
		
		return true;
	}
}
