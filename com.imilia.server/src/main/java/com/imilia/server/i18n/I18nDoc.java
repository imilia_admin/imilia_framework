/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2011 - all rights reserved
 * Created on: Jun 28, 2011
 * Created by: emcgreal
 */
package com.imilia.server.i18n;

import java.util.Properties;


/**
 * The Interface RestDoc.
 */
public interface I18nDoc {
	
	/**
	 * Process.
	 *
	 * @param basePackageName the base package name
	 * @param propertiesFile the properties file
	 * @throws Exception the exception
	 */
	public void process(String basePackageName, Properties propertiesFile) throws Exception;
}
