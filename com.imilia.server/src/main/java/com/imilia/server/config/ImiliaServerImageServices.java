package com.imilia.server.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.imilia.server.service.ImageService;
import com.imilia.server.service.impl.ImageServiceImpl;

/**
 * The Class ImiliaServerImageServices.
 */
@Configuration
public class ImiliaServerImageServices {
	
	/**
	 * Gets the image service.
	 *
	 * @return the image service
	 */
	@Bean
	public ImageService getImageService(){
		return new ImageServiceImpl();
	}

}
