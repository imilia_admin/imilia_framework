package com.imilia.server.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.validation.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.ehcache.EhCacheFactoryBean;
import org.springframework.cache.ehcache.EhCacheManagerFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.authentication.dao.SaltSource;
import org.springframework.security.authentication.dao.SystemWideSaltSource;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.security.core.userdetails.UserCache;
import org.springframework.security.core.userdetails.cache.EhCacheBasedUserCache;

import com.imilia.server.service.AuthenticationService;
import com.imilia.server.service.UserAccountService;
import com.imilia.server.service.impl.DefaultAuthenticationServiceImpl;
import com.imilia.server.service.impl.PasswordStrengthChecker;

@Configuration
public class ImiliaServerSecurityConfig {
	
	private final static Logger logger = LoggerFactory.getLogger(ImiliaServerSecurityConfig.class);
	
	@Autowired
	private UserAccountService userAccountService;
	
	@Autowired @Qualifier("jsr303Validator") 
	private Validator beanValidator;
	
	@Bean
	public EhCacheManagerFactoryBean getEhCacheManagerFactoryBean(){
		EhCacheManagerFactoryBean cacheManagerFb = new EhCacheManagerFactoryBean();
		cacheManagerFb.setConfigLocation(new ClassPathResource("/ehcache.xml", getClass()));
		cacheManagerFb.setCacheManagerName("UserCacheManager");
		return cacheManagerFb;
	}
	
	@Bean
	public EhCacheFactoryBean getEhCacheFactoryBean(){
		EhCacheFactoryBean cacheFactoryBean = new EhCacheFactoryBean();
		cacheFactoryBean.setCacheName("userCache");
		cacheFactoryBean.setCacheManager(getEhCacheManagerFactoryBean().getObject());
		return cacheFactoryBean;
	}
	
	@Bean
	public UserCache getUserCache() {
		EhCacheBasedUserCache c = new EhCacheBasedUserCache();
		c.setCache(getEhCacheFactoryBean().getObject());
		return c;
	}

	@Bean
	public SaltSource getSaltSource(){
		SystemWideSaltSource saltSource = new SystemWideSaltSource();
		saltSource.setSystemWideSalt("imilia");
		return saltSource;
	}
	
	@Bean
	public ShaPasswordEncoder getPasswordEncoder(){
		ShaPasswordEncoder encoder = new ShaPasswordEncoder();
		encoder.setEncodeHashAsBase64(true);
		return encoder;
	}

	@Bean(name="daoAuthenticationProvider")
	public DaoAuthenticationProvider getDaoAuthenticationProvider(){
		DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
		provider.setUserDetailsService(getUserAccountService());
		provider.setUserCache(getUserCache());
		provider.setSaltSource(getSaltSource());
		provider.setPasswordEncoder(getPasswordEncoder());
		return provider;
	}

	@Bean
	public ProviderManager getProviderManager(){
		ProviderManager p = new ProviderManager();
		
		List<Object> providers = new ArrayList<Object>();
		providers.add(getDaoAuthenticationProvider());
		p.setProviders(providers);
		return p;
	}

	@Bean
	public AuthenticationService getAuthenticationService(){
		DefaultAuthenticationServiceImpl a = new DefaultAuthenticationServiceImpl();
		a.setAuthenticationManager(getProviderManager());
		a.setPasswordEncoder(getPasswordEncoder());
		a.setSaltSource(getSaltSource());
		a.setBeanValidator(getBeanValidator());
		a.setUserAccountService(getUserAccountService());
		return a;
	}

	@Bean
	public PasswordStrengthChecker getPasswordStrengthChecker(){
		return new PasswordStrengthChecker();
	}
	
	public UserAccountService getUserAccountService() {
		return userAccountService;
	}

	public void setUserAccountService(UserAccountService userAccountService) {
		this.userAccountService = userAccountService;
	}

	public Validator getBeanValidator() {
		return beanValidator;
	}

	public void setBeanValidator(Validator beanValidator) {
		this.beanValidator = beanValidator;
	}

}
