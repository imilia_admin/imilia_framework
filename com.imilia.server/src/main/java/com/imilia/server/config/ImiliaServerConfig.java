package com.imilia.server.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.fasterxml.jackson.core.JsonFactory;
import com.imilia.datetime.dynamic.DynamicIntervalParser;
import com.imilia.datetime.dynamic.parser.DynamicIntervalParserImpl;
import com.imilia.server.domain.i18n.DefaultDomainLocaleComparator;
import com.imilia.server.service.AddressService;
import com.imilia.server.service.PersonService;
import com.imilia.server.service.QueryRestrictionService;
import com.imilia.server.service.QueryService;
import com.imilia.server.service.QueryViewService;
import com.imilia.server.service.RecipeHolderService;
import com.imilia.server.service.RecipeService;
import com.imilia.server.service.RoleService;
import com.imilia.server.service.UserAccountService;
import com.imilia.server.service.ValidationClassSpecService;
import com.imilia.server.service.impl.AddressServiceImpl;
import com.imilia.server.service.impl.PersonServiceImpl;
import com.imilia.server.service.impl.QueryServiceImpl;
import com.imilia.server.service.impl.QueryViewServiceImpl;
import com.imilia.server.service.impl.RecipeHolderServiceImpl;
import com.imilia.server.service.impl.RecipeServiceImpl;
import com.imilia.server.service.impl.RestrictionServiceImpl;
import com.imilia.server.service.impl.RoleServiceImpl;
import com.imilia.server.service.impl.UserAccountServiceImpl;
import com.imilia.server.service.impl.ValidationClassSpecServiceImpl;

/**
 * The Class ImiliaServerCoreServices.
 */
@Configuration
@EnableTransactionManagement
public class ImiliaServerConfig {
	
	/**
	 * Gets the default domain locale comparator.
	 *
	 * @return the default domain locale comparator
	 */
	@Bean
	public DefaultDomainLocaleComparator getDefaultDomainLocaleComparator(){
		return new DefaultDomainLocaleComparator();
	}

	/**
	 * Gets the user account service.
	 *
	 * @return the user account service
	 */
	@Bean
	public UserAccountService getUserAccountService(){
		return new UserAccountServiceImpl();
	}

	/**
	 * Gets the person service.
	 *
	 * @return the person service
	 */
	@Bean
	public PersonService getPersonService(){
		return new PersonServiceImpl();
	}
	
	/**
	 * Gets the address service.
	 *
	 * @return the address service
	 */
	@Bean
	public AddressService getAddressService(){
		return new AddressServiceImpl();
	}
	
	/**
	 * Gets the recipe service.
	 *
	 * @return the recipe service
	 */
	@Bean
	public RecipeService getRecipeService(){
		return new RecipeServiceImpl();
	}
	
	/**
	 * Gets the recipe holder service.
	 *
	 * @return the recipe holder service
	 */
	@Bean
	public RecipeHolderService getRecipeHolderService(){
		return new RecipeHolderServiceImpl();
	}
		
	/**
	 * Gets the validation class spec service.
	 *
	 * @return the validation class spec service
	 */
	@Bean
	public ValidationClassSpecService getValidationClassSpecService(){
		return new ValidationClassSpecServiceImpl();
	}
	
	/**
	 * Gets the dynamic interval parser.
	 *
	 * @return the dynamic interval parser
	 */
	@Bean
	public DynamicIntervalParser getDynamicIntervalParser(){
		return new DynamicIntervalParserImpl();
	}
	
	/**
	 * Gets the query service.
	 *
	 * @return the query service
	 */
	@Bean
	public QueryService getQueryService(){
		return new QueryServiceImpl();
	}
	
	@Bean
	public RoleService getRoleService(){
		return new RoleServiceImpl();
	}
	
	@Bean
	public QueryRestrictionService getRestrictionService(){
		return new RestrictionServiceImpl();
	}
	
	@Bean
	public QueryViewService getQueryViewService(){
		return new QueryViewServiceImpl();
	}
	
	@Bean
	public JsonFactory getJsonFactory(){
		return new JsonFactory();
	}
}
