package com.imilia.server.config;

import org.springframework.validation.Validator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import com.imilia.server.domain.password.PasswordCandidate;
import com.imilia.server.domain.validation.ConditionalValidator;

@Configuration
public class ImiliaServerValidationConfig {
	
	@Bean(name="jsr303Validator")
	public Validator getBeanValidator(){
		return new LocalValidatorFactoryBean();
	}
	
	
	
	@Bean
	public PasswordCandidate getPasswordCandidate(){
		return new PasswordCandidate();
	}
	
	@Bean
	public ConditionalValidator getConditionalValidator(){
		return new ConditionalValidator();
	}
}
