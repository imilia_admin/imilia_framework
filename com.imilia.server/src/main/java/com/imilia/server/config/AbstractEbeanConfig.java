package com.imilia.server.config;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jndi.JndiObjectFactoryBean;

import com.avaje.ebean.EbeanServer;
import com.avaje.ebean.config.EncryptDeployManager;
import com.avaje.ebean.config.EncryptKeyManager;
import com.avaje.ebean.config.ServerConfig;
import com.avaje.ebean.springsupport.factory.EbeanServerFactoryBean;
import com.imilia.server.domain.DomainObjectAuditor;
import com.imilia.server.server.Profile;
import com.imilia.server.server.ProfileImpl;

public abstract class AbstractEbeanConfig {
	private final static Logger logger = LoggerFactory.getLogger(AbstractEbeanConfig.class);
	
	@Bean
	public JndiObjectFactoryBean getJndiObjectFactoryBean(){
		JndiObjectFactoryBean factory = new JndiObjectFactoryBean();
		factory.setJndiName(getDatabaseJndiName());
		return factory;
	}
	
	@Bean
	public DataSource getImiliaDemoDataSource(){
		return (DataSource) getJndiObjectFactoryBean().getObject();
	}
	
	@Bean
	public DataSourceTransactionManager getDataSourceTransactionManager(){
		DataSourceTransactionManager txnMgr = new DataSourceTransactionManager();
		txnMgr.setDataSource(getImiliaDemoDataSource());
		return txnMgr;
	}
	
	@Bean
	public DomainObjectAuditor getDomainObjectAuditor(){
		return new DomainObjectAuditor();
	}
	
	
	@Bean
	public ServerConfig getServerConfig(){
		final ServerConfig serverConfig = new ServerConfig();
		serverConfig.setName("ImiliaDemoServer");
		
		serverConfig.setEncryptDeployManager(getEncryptDeployManager());
		serverConfig.setEncryptKeyManager(getEncryptKeyManager());
		
		registerClasses(serverConfig);
		
		serverConfig.setDataSource(getImiliaDemoDataSource());
		
		serverConfig.add(getDomainObjectAuditor());	
		return serverConfig;
	}

	protected void registerClasses(ServerConfig serverConfig, Class<?>...classes){
		for (Class<?> cl:classes){
			serverConfig.addClass(cl);
		}
	}
	
	@Bean
	public FactoryBean<EbeanServer> getEbeanServerFactoryBean(){
		EbeanServerFactoryBean factory =  new EbeanServerFactoryBean();
		factory.setServerConfig(getServerConfig());
		
		return factory;
	}
	
	@Bean
	public Profile getProfile(){
		return new ProfileImpl();
	}

	// Abstract methods
	protected abstract String getDatabaseJndiName();
	public abstract EncryptDeployManager getEncryptDeployManager();
	public abstract EncryptKeyManager getEncryptKeyManager();
	protected abstract void registerClasses(ServerConfig serverConfig);

}
