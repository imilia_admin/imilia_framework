/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 * 
 * Created on: Mar 5, 2009
 * Created by: emcgreal
 */
package com.imilia.server.validation;

import java.io.Serializable;

import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;

/**
 * The Class ValidationException.
 */
public class ValidationException extends Exception implements Serializable {

	/** The errors. */
	private final Errors errors;

	/** The context object which was being validated. */
	private final Object targetObject;

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -3915535503702224857L;

	/**
	 * Instantiates a new validation exception.
	 * 
	 * @param errors
	 *            the validation errors
	 * @param targetObject
	 *            the target object that was being validated
	 */
	public ValidationException(Errors errors, Object targetObject) {
		super();
		this.errors = errors;
		this.targetObject = targetObject;
	}
	
	/**
	 * Instantiates a new validation exception.
	 * 
	 * @param message the message
	 * @param errors the errors
	 * @param targetObject the target object
	 */
	public ValidationException(String message, Errors errors, Object targetObject) {
		super(message);
		this.errors = errors;
		this.targetObject = targetObject;
	}

	/**
	 * Gets the errors.
	 * 
	 * @return the errors
	 */
	public Errors getErrors() {
		return errors;
	}

	/**
	 * Gets the target object.
	 * 
	 * @return the targetObject
	 */
	public Object getTargetObject() {
		return targetObject;
	}

	/**
	 * Creates the field error.
	 * 
	 * @param targetObject the target object
	 * @param objectName the object name
	 * @param field the field
	 * @param rejectedValue the rejected value
	 * @param bindingFailure the binding failure
	 * @param codes the codes
	 * @param arguments the arguments
	 * @param defaultMessage the default message
	 * 
	 * @return the validation exception
	 */
	public static ValidationException createFieldError(Object targetObject,
			String objectName, String field, Object rejectedValue,
			boolean bindingFailure, String[] codes, Object[] arguments,
			String defaultMessage) {
		
		final FieldError o = new FieldError(objectName, 
				field, rejectedValue, bindingFailure,
				codes,
				arguments, 
				defaultMessage);

		final BeanPropertyBindingResult errors = new BeanPropertyBindingResult(
				targetObject, objectName);
		errors.addError(o);

		return new ValidationException(errors, targetObject);
	}

}
