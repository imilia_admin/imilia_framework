/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2011 - all rights reserved
 *
 * Created on: Jan 14, 2011
 * Created by: emcgreal
 */
package com.imilia.server.server;


/**
 * The Class ProfileImpl.
 */
public class ProfileImpl implements Profile {

	/** The profile. */
	private String profile;
	
	/** The version. */
	private String version;
	
	/** The build date. */
	private String buildDate;
	

	/* (non-Javadoc)
	 * @see com.imilia.server.server.Profile#getProfile()
	 */
	public String getProfile() {
		return profile;
	}
	
	/**
	 * Sets the profile.
	 *
	 * @param profile the new profile
	 */
	public void setProfile(String profile) {
		this.profile = profile;
	}
	
	/* (non-Javadoc)
	 * @see com.imilia.server.server.Profile#getVersion()
	 */
	public String getVersion() {
		return version;
	}
	
	/**
	 * Sets the version.
	 *
	 * @param version the new version
	 */
	public void setVersion(String version) {
		this.version = version;
	}
	
	/* (non-Javadoc)
	 * @see com.imilia.server.server.Profile#getBuildDate()
	 */
	public String getBuildDate() {
		return buildDate;
	}
	
	/**
	 * Sets the builds the date.
	 *
	 * @param buildDate the new builds the date
	 */
	public void setBuildDate(String buildDate) {
		this.buildDate = buildDate;
	}

	public String getSummary() {
		final StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(profile);

		if (version != null){
			stringBuilder.append(" ");
			stringBuilder.append(version);
		}

		if (buildDate != null){
			stringBuilder.append(" ");
			stringBuilder.append(buildDate);
		}
		return stringBuilder.toString();
	}
}
