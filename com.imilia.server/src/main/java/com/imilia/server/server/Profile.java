/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2011 - all rights reserved
 *
 * Created on: Jan 14, 2011
 * Created by: emcgreal
 */
package com.imilia.server.server;


/**
 * The Interface Profile.
 */
public interface Profile {
	
	/**
	 * Gets the version.
	 *
	 * @return the version
	 */
	public String getVersion();
	
	/**
	 * Gets the profile.
	 *
	 * @return the profile
	 */
	public String getProfile();
	
	/**
	 * Gets the builds the date.
	 *
	 * @return the builds the date
	 */
	public String getBuildDate();
	
	/**
	 * Gets the summary.
	 *
	 * @return the summary
	 */
	public String getSummary();
}
