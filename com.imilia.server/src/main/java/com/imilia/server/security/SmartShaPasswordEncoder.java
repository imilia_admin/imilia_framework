/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2011 - all rights reserved
 *
 * Created on: Apr 21, 2011
 * Created by: emcgreal
 */
package com.imilia.server.security;

import org.springframework.security.authentication.encoding.ShaPasswordEncoder;

/**
 * The Class SmartShaPasswordEncoder.
 * <p>Use this encoder only in secure environments. It can be used where the password is already hashed
 * and does not need to be encoded again</p>
 */
public class SmartShaPasswordEncoder extends ShaPasswordEncoder {

	/* (non-Javadoc)
	 * @see org.springframework.security.authentication.encoding.MessageDigestPasswordEncoder#isPasswordValid(java.lang.String, java.lang.String, java.lang.Object)
	 */
	@Override
	public boolean isPasswordValid(String encPass, String rawPass, Object salt) {
		if (encPass != null){
			if (encPass.equals(rawPass)){
				// The raw is the hashed password 
				return true;
			}
		}
		return super.isPasswordValid(encPass, rawPass, salt);
	}

}
