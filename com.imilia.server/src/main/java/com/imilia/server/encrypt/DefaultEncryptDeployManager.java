/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2011 - all rights reserved
 *
 * Created on: Apr 20, 2011
 * Created by: emcgreal
 */

package com.imilia.server.encrypt;

import java.util.HashMap;
import java.util.Map;

import com.avaje.ebean.config.EncryptDeploy;
import com.avaje.ebean.config.EncryptDeployManager;
import com.avaje.ebean.config.TableName;

/**
 * The Class BkfEncryptDeployManager.
 */
public class DefaultEncryptDeployManager implements EncryptDeployManager {

	/** The specs. */
	private Map<String, EncryptionSpec> specs = new HashMap<String, EncryptionSpec>();
	
	/**
	 * Sets the specs.
	 *
	 * @param specs the specs
	 */
	public void setSpecs(Map<String, EncryptionSpec> specs) {
		this.specs = specs;
	}

	/**
	 * Instantiates a new default encrypt deploy manager.
	 */
	public DefaultEncryptDeployManager(){
	}
	
	/**
	 * Adds the.
	 *
	 * @param spec the spec
	 */
	public void add(EncryptionSpec spec){
		specs.put(spec.getTableName().toLowerCase(), spec);
	}
	
	/* (non-Javadoc)
	 * @see com.avaje.ebean.config.EncryptDeployManager#getEncryptDeploy(com.avaje.ebean.config.TableName, java.lang.String)
	 */
	public EncryptDeploy getEncryptDeploy(TableName table, String column) {
		EncryptionSpec spec = specs.get(table.getName().toLowerCase());
		
		if (spec != null){
			return spec.getEncryptDeploy(column);
		}
		
		return EncryptDeploy.NO_ENCRYPT;
	}

	/**
	 * Gets the specs.
	 *
	 * @return the specs
	 */
	public final Map<String, EncryptionSpec> getSpecs() {
		return specs;
	}

}
