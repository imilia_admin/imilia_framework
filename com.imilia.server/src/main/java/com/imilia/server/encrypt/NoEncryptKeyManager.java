package com.imilia.server.encrypt;

import com.avaje.ebean.config.EncryptKey;
import com.avaje.ebean.config.EncryptKeyManager;

public class NoEncryptKeyManager implements EncryptKeyManager {

	public EncryptKey getEncryptKey(String tableName, String columnName) {
		return new EncryptKey() {
			
			public String getStringValue() {
				return "secret";
			}
		};
	}

	public void initialise() {
	}
}
