/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2011 - all rights reserved
 *
 * Created on: Apr 20, 2011
 * Created by: emcgreal
 */
package com.imilia.server.encrypt;

import com.avaje.ebean.config.EncryptKey;
import com.avaje.ebean.config.EncryptKeyManager;

// TODO: Auto-generated Javadoc
/**
 * The Class DefaultEncryptKeyManager.
 */
public class DefaultEncryptKeyManager implements EncryptKeyManager {
	
	/** The password. */
	protected String password;
	
	/* (non-Javadoc)
	 * @see com.avaje.ebean.config.EncryptKeyManager#getEncryptKey(java.lang.String, java.lang.String)
	 */
	public EncryptKey getEncryptKey(String tableName, String columnName) {
		return new EncryptKey() {
			
			public String getStringValue() {
				return password;
			}
		};
	}

	/* (non-Javadoc)
	 * @see com.avaje.ebean.config.EncryptKeyManager#initialise()
	 */
	public void initialise() {
		if (password == null){
			password = "l81Dn0s$x0gqk!9-";
		}
	}
}
