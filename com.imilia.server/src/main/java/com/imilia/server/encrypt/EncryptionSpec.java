/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2010 - all rights reserved
 * Created on: Mar 1, 2010
 * Created by: emcgreal
 */
package com.imilia.server.encrypt;

import com.avaje.ebean.config.EncryptDeploy;

/**
 * The Class EncryptionSpec.
 */
public class EncryptionSpec {
	
	/** The table name. */
	private final String tableName;
	
	/** The db encrypted columns. */
	private final String[] dbEncryptedColumns;
	
	/** The client encrypted columns. */
	private final String[] clientEncryptedColumns;

	/**
	 * Instantiates a new encryption spec.
	 * 
	 * @param tableName the table name
	 * @param dbEncryptedColumns the db encrypted columns
	 * @param clientEncryptedColumns the client encrypted columns
	 */
	public EncryptionSpec(String tableName, String[] dbEncryptedColumns,
			String[] clientEncryptedColumns) {
		super();
		this.tableName = tableName;
		this.dbEncryptedColumns = dbEncryptedColumns;
		this.clientEncryptedColumns = clientEncryptedColumns;
	}

	/**
	 * Instantiates a new encryption spec.
	 * 
	 * @param tableName the table name
	 * @param dbEncryptedColumns the db encrypted columns
	 */
	public EncryptionSpec(String tableName, String[] dbEncryptedColumns) {
		super();
		this.tableName = tableName;
		this.dbEncryptedColumns = dbEncryptedColumns;
		this.clientEncryptedColumns = new String[0];
	}
	
	/**
	 * Gets the table name.
	 * 
	 * @return the table name
	 */
	public final String getTableName() {
		return tableName;
	}

	/**
	 * Gets the db encrypted columns.
	 * 
	 * @return the db encrypted columns
	 */
	public final String[] getDbEncryptedColumns() {
		return dbEncryptedColumns;
	}

	/**
	 * Gets the client encrypted columns.
	 * 
	 * @return the client encrypted columns
	 */
	public final String[] getClientEncryptedColumns() {
		return clientEncryptedColumns;
	}
	
	public EncryptDeploy getEncryptDeploy(String column){
		
		for (String c:dbEncryptedColumns){
			if (c.equalsIgnoreCase(column)){
				return EncryptDeploy.ENCRYPT_DB;
			}
		}

		for (String c:clientEncryptedColumns){
			if (c.equalsIgnoreCase(column)){
				return EncryptDeploy.ENCRYPT_CLIENT;
			}
		}

		return EncryptDeploy.NO_ENCRYPT;
	}
}
