package com.imilia.server.encrypt;

import com.avaje.ebean.config.EncryptDeploy;
import com.avaje.ebean.config.EncryptDeployManager;
import com.avaje.ebean.config.TableName;

public class NoEncryptDeployManager implements EncryptDeployManager {


	public EncryptDeploy getEncryptDeploy(TableName table, String column) {
		return EncryptDeploy.NO_ENCRYPT;
	}

}
