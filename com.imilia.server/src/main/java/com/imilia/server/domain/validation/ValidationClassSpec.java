/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2010 - all rights reserved
 * Created on: May 13, 2010
 * Created by: emcgreal
 */
package com.imilia.server.domain.validation;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

import com.imilia.server.domain.DomainObject;
import com.imilia.server.domain.validation.properties.PropertyValidationModel;
import com.imilia.server.domain.validation.properties.PropertyValidationSpecification;
import com.imilia.utils.classes.ClassUtils;

/**
 * The Class ValidationClassSpec contains a list of ValidationPropertySpec
 * that define the validation rules for the properties of the given class.
 */
@Entity
public class ValidationClassSpec extends DomainObject implements PropertyValidationModel{

	/** The clazz. */
	@NotNull
	private Class<?> clazz;

	/** The property specs. */
	@OneToMany(mappedBy="validationClassSpec", cascade=CascadeType.ALL)
	private List<ValidationPropertySpec> propertySpecs;

	/**
	 * Instantiates a new validation class spec.
	 */
	public ValidationClassSpec(){
		super();
	}
	
	/**
	 * Instantiates a new validation class spec.
	 *
	 * @param clazz the clazz
	 */
	public ValidationClassSpec(Class<?> clazz){
		super();
		this.clazz = clazz;
	}
	
	
	/**
	 * Auto detect mandatory from class.
	 *
	 * @param clazz the clazz
	 */
	public void addMandatoryFromClass(){
		if (clazz != null){
			final List<Field> notNullFields = ClassUtils.findFields(clazz, NotNull.class);
			
			for (Field f:notNullFields){
				NotNull nn = f.getAnnotation(NotNull.class);
				
				if (nn != null){
					ValidationPropertySpec spec = new ValidationPropertySpec(f.getName());
					spec.setMandatory(true);
					add(spec);
				}
			}
		}
	}

	public void addMandatoryApplyIfFromClass(){
		if (clazz != null){
			final List<Field> notNullFields = ClassUtils.findFields(clazz, NotNull.class);
			
			for (Field f:notNullFields){
				NotNull nn = f.getAnnotation(NotNull.class);
				
				if (nn != null){
					ValidationPropertySpec spec = new ValidationPropertySpec(f.getName());
					spec.setMandatory(false);
					add(spec);
				}
			}
		}
	}
	
	/**
	 * Adds the spec if not already present and sets this as its parent.
	 *
	 * @param spec the spec
	 */
	public ValidationPropertySpec add(ValidationPropertySpec spec){
		final ValidationPropertySpec s = 
				(ValidationPropertySpec) getPropertyValidationSpecification(spec.getPropertyName());
		
		// Add only if not present
		if (s == null){
			spec.setValidationClassSpec(this);
			getPropertySpecs().add(spec);
		}else{
			return s;
		}
		
		return spec;
	}

	/**
	 * Adds all specs in the list.
	 *
	 * @param specs the specs
	 */
	public void addAll(Collection<ValidationPropertySpec> specs){
		for (ValidationPropertySpec s:specs){
			add(s);
		}
	}


	/**
	 * Gets the property specs.
	 * 
	 * @return the property specs
	 */
	public final List<ValidationPropertySpec> getPropertySpecs() {
		return propertySpecs;
	}

	/**
	 * Sets the property specs.
	 * 
	 * @param propertySpecs the new property specs
	 */
	public final void setPropertySpecs(List<ValidationPropertySpec> propertySpecs) {
		this.propertySpecs = propertySpecs;
	}

	/**
	 * Gets the clazz name.
	 *
	 * @return the clazz name
	 */
	public final Class<?> getClazz() {
		return clazz;
	}

	/**
	 * Sets the clazz name.
	 *
	 * @param clazzName the new clazz name
	 */
	public final void setClazz(Class<?> clazz) {
		this.clazz = clazz;
	}

	/* (non-Javadoc)
	 * @see com.imilia.server.domain.DomainObject#getPropertyValidationSpecification(java.lang.String)
	 */
	@Override
	public PropertyValidationSpecification getPropertyValidationSpecification(String property) {
		for (ValidationPropertySpec propSpec:getPropertySpecs()){
			if (propSpec.getPropertyName().equals(property)){
				return propSpec;
			}
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ValidationClassSpec [" + propertySpecs +"]";
	}
}
