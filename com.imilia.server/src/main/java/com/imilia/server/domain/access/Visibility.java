package com.imilia.server.domain.access;

public enum Visibility {
	Sudo,
	Private,
	Protected,
	Public;
}
