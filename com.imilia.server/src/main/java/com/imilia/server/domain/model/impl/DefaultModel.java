/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 *
 * Created on: Apr 16, 2009
 * Created by: eddiemcgreal
 */
package com.imilia.server.domain.model.impl;

import java.util.Set;

import javax.annotation.PostConstruct;
import javax.validation.ConstraintViolation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;

import com.imilia.server.domain.Crud;
import com.imilia.server.domain.CurrentUser;
import com.imilia.server.domain.model.IAppModel;
import com.imilia.server.domain.model.IModel;
import com.imilia.server.domain.model.manager.IModelManager;
import com.imilia.server.domain.model.manager.impl.ModelManager;
import com.imilia.server.domain.model.property.DefaultAttributes;
import com.imilia.server.domain.model.property.Property;
import com.imilia.server.domain.model.property.PropertyAttributeProvider;
import com.imilia.server.validation.ValidationException;

/**
 * The Class DefaultModel serves as a convenient base class for models
 * 
 * <p>It also extends DefaultObservable</p>.
 */
public class DefaultModel<U extends CurrentUser> implements IModel  {

	private String name;
	
	private ModelManager<DefaultModel<U>> modelManager;
	
	@Autowired
	private IAppModel<U> appModel;
	
	private DefaultAttributes attributes = new DefaultAttributes();
	
	/**
	 * Instantiates a new default model.
	 */
	public DefaultModel(){
		super();
	}
	
	@PostConstruct
	public void postConstruct(){
		setModelManager(new ModelManager<DefaultModel<U>>(this));
		getModelManager().init();
	}
	
	/**
	 * Validate.
	 * 
	 * @return the errors
	 * 
	 * @throws ValidationException the validation exception
	 */
	public Errors validate() throws ValidationException{
		return validate(this, this.getClass().getCanonicalName());
	}
	
	/**
	 * Validate.
	 * 
	 * @param object the object
	 * 
	 * @return the errors
	 * 
	 * @throws ValidationException the validation exception
	 */
	public Errors validate(Object object) throws ValidationException{
		return validate(object, this.getClass().getCanonicalName());
	}
	
	/**
	 * Validate.
	 * 
	 * @param targetName the target name
	 * @param object the object
	 * 
	 * @return the errors
	 * 
	 * @throws ValidationException the validation exception
	 */
	public Errors validate(Object object, String targetName) throws ValidationException{
		final BeanPropertyBindingResult beanPropertyBindingResult = 
				new BeanPropertyBindingResult(object, targetName != null 
				? targetName 
				:this.getClass().getCanonicalName());
		
		Set<ConstraintViolation<Object>> set = appModel.getBeanValidator().validate(object);
		if (beanPropertyBindingResult.hasErrors()){
			throw new ValidationException(beanPropertyBindingResult, this);
		}
		
		return beanPropertyBindingResult;
	}

	public IModelManager getModelManager() {
		return modelManager;
	}

	public void setModelManager(ModelManager<DefaultModel<U>> modelManager) {
		this.modelManager = modelManager;
	}

	public IAppModel<U> getAppModel() {
		return appModel;
	}
	
	public void setAppModel(IAppModel<U> appModel) {
		this.appModel = appModel;
	}
	
	protected MessageSource getMessageSource(){
		return appModel.getMessageSource();
	}
	
	@Deprecated
	public void fireEvent(){}

	@Override
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public Crud getCrud(Property property) {
		return appModel.getCrud(property);
	}

	@Override
	public Object getAttribute(Property property, String attributeName) {
		return attributes.getAttribute(property, attributeName);
	}

	@Override
	public PropertyAttributeProvider setAttribute(Property property,
			String attributeName, Object value) {
		attributes.setAttribute(property, attributeName, value);
		return this;
	}
}
