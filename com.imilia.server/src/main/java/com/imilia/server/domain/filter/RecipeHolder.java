package com.imilia.server.domain.filter;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.imilia.server.domain.DomainObject;
import com.imilia.server.domain.common.Person;

/**
 * The Class RecipeHolder.
 */
@Entity
public class RecipeHolder extends DomainObject {
	/** The owner. */
	@ManyToOne
	private Person owner;
	
	/** The recipe. */
	@ManyToOne(cascade=CascadeType.ALL)
	private Recipe<?> recipe;
	
	/**
	 * Instantiates a new recipe holder.
	 */
	public RecipeHolder() {
	}
	
	/**
	 * Instantiates a new recipe holder.
	 * 
	 * @param owner the owner
	 * @param recipe the recipe
	 */
	public RecipeHolder(Person owner, Recipe<?> recipe) {
		super();
		this.owner = owner;
		this.recipe = recipe;
	}

	/**
	 * Gets the owner.
	 * 
	 * @return the owner
	 */
	public final Person getOwner() {
		return owner;
	}

	/**
	 * Sets the owner.
	 * 
	 * @param owner the new owner
	 */
	public final void setOwner(Person owner) {
		this.owner = owner;
	}

	/**
	 * Gets the recipe.
	 * 
	 * @return the recipe
	 */
	public final Recipe<?> getRecipe() {
		return recipe;
	}

	/**
	 * Sets the recipe.
	 * 
	 * @param recipe the new recipe
	 */
	public final void setRecipe(Recipe<?> recipe) {
		this.recipe = recipe;
	}
}
