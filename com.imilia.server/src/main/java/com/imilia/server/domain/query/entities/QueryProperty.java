/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2014 - all rights reserved
 * Created on: Mar 14, 2014
 * Created by: emcgreal
 */
package com.imilia.server.domain.query.entities;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;

import com.imilia.server.domain.DomainObject;
import com.imilia.server.domain.access.Visibility;

/**
 * The Class QueryProperty.
 */
@Entity
public class QueryProperty extends DomainObject{
	
	/** The property name. */
	private String propertyName;
	
	/** The visibility. */
	@Enumerated(EnumType.ORDINAL)
	private Visibility visibility;
	
	/** The flags. */
	private long flags;
	
	/** The query view. */
	@ManyToOne
	private QueryView queryView;
	
	/**
	 * Instantiates a new query property.
	 */
	public QueryProperty(){
		super();
	}

	/**
	 * Gets the property name.
	 *
	 * @return the property name
	 */
	public String getPropertyName() {
		return propertyName;
	}

	/**
	 * Sets the property name.
	 *
	 * @param propertyName the new property name
	 */
	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	/**
	 * Gets the flags.
	 *
	 * @return the flags
	 */
	public long getFlags() {
		return flags;
	}

	/**
	 * Sets the flags.
	 *
	 * @param flags the new flags
	 */
	public void setFlags(long flags) {
		this.flags = flags;
	}

	/**
	 * Gets the query view.
	 *
	 * @return the query view
	 */
	public QueryView getQueryView() {
		return queryView;
	}

	/**
	 * Sets the query view.
	 *
	 * @param queryView the new query view
	 */
	public void setQueryView(QueryView queryView) {
		this.queryView = queryView;
	}

	public Visibility getVisibility() {
		return visibility;
	}

	public void setVisibility(Visibility visibility) {
		this.visibility = visibility;
	}

	@Override
	public String toString() {
		return "QueryProperty [propertyName=" + propertyName + ", visibility="
				+ visibility + ", flags=" + flags + ", queryView=" + queryView
				+ "]";
	}
}
