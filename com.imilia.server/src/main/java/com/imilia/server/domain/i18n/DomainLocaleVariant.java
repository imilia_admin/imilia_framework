/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2010 - all rights reserved
 * Created on: Mar 15, 2010
 * Created by: emcgreal
 */
package com.imilia.server.domain.i18n;

import java.util.Locale;

import com.imilia.utils.i18n.DomainLocale;

/**
 * The Class DomainLocaleVariant.
 */
public class DomainLocaleVariant {
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((domainLocale == null) ? 0 : domainLocale.hashCode());
		result = prime * result + ((locale == null) ? 0 : locale.hashCode());
		result = prime * result + ((variant == null) ? 0 : variant.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DomainLocaleVariant other = (DomainLocaleVariant) obj;
		if (domainLocale == null) {
			if (other.domainLocale != null)
				return false;
		} else if (!domainLocale.equals(other.domainLocale))
			return false;
		if (locale == null) {
			if (other.locale != null)
				return false;
		} else if (!locale.equals(other.locale))
			return false;
		if (variant == null) {
			if (other.variant != null)
				return false;
		} else if (!variant.equals(other.variant))
			return false;
		return true;
	}

	/** The domain locale. */
	private final DomainLocale domainLocale;
	
	/** The variant. */
	private final String variant;
	
	/** The locale. */
	private final Locale locale;
	
	/**
	 * Instantiates a new domain locale variant.
	 * 
	 * @param domainLocale the domain locale
	 * @param variant the variant
	 */
	public DomainLocaleVariant(DomainLocale domainLocale, String variant) {
		super();
		this.domainLocale = domainLocale;
		this.variant = variant;
		
		if (variant != null){
			this.locale = new Locale(domainLocale.getLocale().getLanguage(), 
						domainLocale.getLocale().getCountry(), 
						variant);
		}else{
			this.locale = domainLocale.getLocale(); 
		}
	}

	/**
	 * Gets the domain locale.
	 * 
	 * @return the domain locale
	 */
	public final DomainLocale getDomainLocale() {
		return domainLocale;
	}

	/**
	 * Gets the variant.
	 * 
	 * @return the variant
	 */
	public final String getVariant() {
		return variant;
	}

	/**
	 * Gets the locale variant.
	 * 
	 * @return the locale variant
	 */
	public final Locale getLocale() {
		return locale;
	}
	
	
}
