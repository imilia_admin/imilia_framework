/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 *
 * Created on: Apr 8, 2009
 * Created by: eddiemcgreal
 */
package com.imilia.server.domain;

import java.util.List;
import java.util.Locale;

import com.imilia.server.domain.access.Role;
import com.imilia.server.domain.access.Subscription;
import com.imilia.server.domain.common.UserAccount;
import com.imilia.server.domain.i18n.DomainLocaleVariant;
import com.imilia.utils.i18n.DomainLocale;
import com.imilia.utils.observer.DefaultObservable;

/**
 * The Class CurrentUser is used as a session bean to keep a track of current user
 * over the case of a session.
 */
public abstract class AbstractCurrentUser 
	extends DefaultObservable implements CurrentUser {
	
	// Members
	/** The user account. */
	private UserAccount userAccount;

	/** The domain locale variant. */
	private DomainLocaleVariant domainLocaleVariant;
	
	/** The subscription. */
	private Subscription subscription;
	
	private List<Role> roles;
	
	/**
	 * Instantiates a new current user.
	 */
	public AbstractCurrentUser(){
		super();
		domainLocaleVariant = new DomainLocaleVariant(DomainLocale.de_DE, null);
	}

	/**
	 * Gets the user account.
	 * 
	 * @return the user account
	 */
	public UserAccount getUserAccount() {
		return userAccount;
	}

	/**
	 * Sets the user account.
	 * 
	 * @param userAccount the new user account
	 */
	public void setUserAccount(UserAccount userAccount) {
		this.userAccount = userAccount;
	}

	/**
	 * Sets the user account.
	 * 
	 * @param userAccount the new user account
	 */
	public void setUserAccountOnly(UserAccount userAccount) {
		this.userAccount = userAccount;
	}
	
	/**
	 * Gets the locale of the current user.
	 * 
	 * @return the locale
	 */
	public Locale getLocale(){
		return getDomainLocaleVariant().getLocale();
	}

	/* (non-Javadoc)
	 * @see com.imilia.server.domain.CurrentUser#getDomainLocaleVariant()
	 */
	public DomainLocaleVariant getDomainLocaleVariant() {
		return domainLocaleVariant;
	}
	
	/**
	 * Sets the domain locale variant.
	 * 
	 * @param variant the new domain locale variant
	 */
	public void setDomainLocaleVariant(DomainLocaleVariant variant){
		if (this.domainLocaleVariant == null || !this.domainLocaleVariant.equals(variant) ){
			this.domainLocaleVariant = variant;
			
			fireEvent(CurrentUserEvent.DomainLocaleVariantChanged);
		}
	}

	/* (non-Javadoc)
	 * @see com.imilia.server.domain.CurrentUser#getSubscription()
	 */
	public Subscription getSubscription() {
		return subscription;
	}

	/**
	 * Sets the subscription.
	 *
	 * @param subscription the new subscription
	 */
	public void setSubscription(Subscription subscription) {
		this.subscription = subscription;
	}

	public List<Role> getRoles() {
		return roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}
}
