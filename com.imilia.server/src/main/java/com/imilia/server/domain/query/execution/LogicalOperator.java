package com.imilia.server.domain.query.execution;

public enum LogicalOperator {
	And,
	Or;
}
