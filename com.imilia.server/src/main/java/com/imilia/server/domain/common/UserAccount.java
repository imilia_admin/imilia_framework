/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 * Created on: Jan 23, 2009
 * Created by: emcgreal
 */

package com.imilia.server.domain.common;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.imilia.server.domain.DomainObject;
import com.imilia.server.domain.access.AuthenticationRequest;
import com.imilia.server.domain.filter.RestrictionCandidate;


/**
 * The Class UserAccount encapsulates a user that can log on to the system.
 */
@Entity
public class UserAccount extends DomainObject implements UserDetails, Serializable {

	private final static Logger logger = LoggerFactory.getLogger(UserAccount.class);
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -593636560975515883L;

	/** The Constant INIT_JOB. */
	public final static String INIT_JOB ="Init_job";
	
	/** The Constant INIT_JOB_PW. */
	public final static String INIT_JOB_PW ="Hallo.2U!";
	
	public final static String USERNAME = "username";
	public final static String PASSWORD = "password";
	public final static String SHA = "sha";
	
	/** The Constant INIT_JOB_AUTHENTICATION_REQUEST. */
	public final static AuthenticationRequest INIT_JOB_AUTHENTICATION_REQUEST = new AuthenticationRequest(INIT_JOB, INIT_JOB_PW); 

	public transient final int UserNameMinLen = 4;
	public transient final int UserNameMaxLen = 50;
	public transient final int PasswordMinLen = 8;
	public transient final int PasswordMaxLen = 28;
	
	/** The username. */
	@Length(min = UserNameMinLen, max = UserNameMaxLen)
	@Column(length = 50, unique=true)
	@Basic(optional = false)
	@NotNull
	@NotBlank
	@RestrictionCandidate
	private String username;

	/** The password. */
	@Column(length = 28)
	@Basic(optional = false)
	@Length(min = PasswordMinLen, max = PasswordMaxLen)
	private String password;

	/** The password last changed. */
	@Temporal(value=TemporalType.TIMESTAMP)
	@RestrictionCandidate(advanced=true)
	private DateTime passwordLastChanged;

	/** The password next change. */
	@Temporal(value=TemporalType.TIMESTAMP)
	@RestrictionCandidate(advanced=true)
	private DateTime passwordNextChange;

	/** The valid to. */
	@Temporal(value=TemporalType.TIMESTAMP)
	private DateTime validTo;

	/** The is account non locked. */
	@RestrictionCandidate(mandatory=false)
	private boolean accountNonLocked = true;

	/** The enabled. */
	@RestrictionCandidate(mandatory=false)
	private boolean enabled = true;

	/** The person. */
	//@NotNull
	@OneToOne(optional = true, fetch = FetchType.EAGER)
	private Person person;


	/** The last login. */
	@Temporal(value=TemporalType.TIMESTAMP)
	@RestrictionCandidate(advanced=true)
	private DateTime lastLogin;

	/** The login attempts. */
	private int loginAttempts;
	
	/** The login attempt. */
	@RestrictionCandidate(advanced=true)
	private DateTime loginAttempt;
	
	/** The login ip. */
	@Column(length=20)
	@RestrictionCandidate(advanced=true)
	private String loginIp;
	

	/** The authorities. */
	@Transient
	private transient List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();

	/** Extended authorities */
	@Transient
	private transient Map<String, Object> extendedProperties;

	/**
	 * Instantiates a new user account.
	 */
	public UserAccount() {
		super();
		//authorities[0] = new GrantedAuthorityImpl("ROLE_ALL");
	}

	/**
	 * Instantiates a new user account.
	 * 
	 * @param person the person
	 */
	public UserAccount(Person person){
		super();

		setPerson(person);
		setAccountNonLocked(true);
		setEnabled(true);
		setChangePasswordRequired();
	}

	/**
	 * Sets the authorities.
	 * 
	 * @param authorities the new authorities
	 */
	public void setAuthorities(List<GrantedAuthority> authorities) {
		this.authorities = authorities;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.acegisecurity.userdetails.UserDetails#getAuthorities()
	 */
	public Collection<GrantedAuthority> getAuthorities() {
		return authorities;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.acegisecurity.userdetails.UserDetails#getPassword()
	 */
	public String getPassword() {
		return this.password;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.acegisecurity.userdetails.UserDetails#isAccountNonExpired()
	 */
	public boolean isAccountNonExpired() {
//		if (validTo != null) {
//			return validTo.after(new Date());
//		}

		return true;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.acegisecurity.userdetails.UserDetails#isAccountNonLocked()
	 */
	public boolean isAccountNonLocked() {
		return accountNonLocked;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.acegisecurity.userdetails.UserDetails#isCredentialsNonExpired()
	 */
	public boolean isCredentialsNonExpired() {
//		if (passwordNextChange != null) {
//			return passwordNextChange.after(new Date());
//		}

		return true;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.acegisecurity.userdetails.UserDetails#isEnabled()
	 */
	public boolean isEnabled() {
		return enabled;
	}

	/**
	 * Sets the username.
	 * 
	 * @param username the new username
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * Sets the change password required.
	 */
	public void setChangePasswordRequired() {
		this.passwordNextChange = new DateTime();
	}

	/**
	 * Sets the account non locked.
	 * 
	 * @param isAccountNonLocked the new account non locked
	 */
	public void setAccountNonLocked(boolean isAccountNonLocked) {
		this.accountNonLocked = isAccountNonLocked;
	}

	/**
	 * Gets the password last changed.
	 * 
	 * @return the password last changed
	 */
	public DateTime getPasswordLastChanged() {
		return passwordLastChanged;
	}

	/**
	 * Sets the password last changed.
	 * 
	 * @param passwordLastChanged the new password last changed
	 */
	public void setPasswordLastChanged(DateTime passwordLastChanged) {
		this.passwordLastChanged = passwordLastChanged;
	}

	/**
	 * Gets the password next change.
	 * 
	 * @return the password next change
	 */
	public DateTime getPasswordNextChange() {
		return passwordNextChange;
	}

	/**
	 * Sets the password next change.
	 * 
	 * @param passwordNextChange the new password next change
	 */
	public void setPasswordNextChange(DateTime passwordNextChange) {
		this.passwordNextChange = passwordNextChange;
	}

	/**
	 * Gets the valid to.
	 * 
	 * @return the valid to
	 */
	public DateTime getValidTo() {
		return validTo;
	}

	/**
	 * Sets the valid to.
	 * 
	 * @param validTo the new valid to
	 */
	public void setValidTo(DateTime validTo) {
		this.validTo = validTo;
	}

	/**
	 * Sets the enabled.
	 * 
	 * @param enabled the new enabled
	 */
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	/**
	 * Sets the password.
	 * 
	 * @param password the new password
	 */
	public void setPassword(String password) {
		this.password = password;
		
		if (isNew()){
			setPasswordLastChanged(new DateTime());
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.acegisecurity.userdetails.UserDetails#getUsername()
	 */
	public String getUsername() {
		return this.username;
	}

	/**
	 * Gets the person.
	 * 
	 * @return the person
	 */
	public Person getPerson() {
		return person;
	}

	/**
	 * Sets the person (and sets the userAccount in the person).
	 * 
	 * @param person the new person
	 */
	public void setPerson(Person person) {
		this.person = person;
		if (person != null){
			if (person.getUserAccount() != this){
				person.setUserAccount(this);
			}
		}
	}

	/**
	 * Change password.
	 * 
	 * @param password the password
	 * 
	 * @throws PasswordException the password exception
	 */
	public void changePassword(String password) throws PasswordException {
		if (this.password == password) {
			throw new PasswordException(PasswordException.Problem.PasswordSameAsOld);
		}
		setPassword(password);
		setPasswordLastChanged(new DateTime());
		setPasswordNextChange(null);
	}

	/**
	 * Checks if is change password required.
	 * 
	 * @return true, if is change password required
	 */
	public boolean isChangePasswordRequired(){
		if (passwordNextChange != null) {
			return passwordNextChange.isBeforeNow();
		}

		return false;
	}

	/**
	 * Gets the last login.
	 * 
	 * @return the last login
	 */
	public DateTime getLastLogin() {
		return lastLogin;
	}


	/**
	 * Sets the last login.
	 * 
	 * @param lastLogin the new last login
	 */
	public void setLastLogin(DateTime lastLogin) {
		this.lastLogin = lastLogin;
	}

	/**
	 * Gets the login attempts.
	 * 
	 * @return the login attempts
	 */
	public final int getLoginAttempts() {
		return loginAttempts;
	}

	/**
	 * Sets the login attempts.
	 * 
	 * @param loginAttempts the new login attempts
	 */
	public final void setLoginAttempts(int loginAttempts) {
		this.loginAttempts = loginAttempts;
	}

	/**
	 * Gets the login attempt.
	 * 
	 * @return the login attempt
	 */
	public final DateTime getLoginAttempt() {
		return loginAttempt;
	}

	/**
	 * Sets the login attempt.
	 * 
	 * @param loginAttempt the new login attempt
	 */
	public final void setLoginAttempt(DateTime loginAttempt) {
		this.loginAttempt = loginAttempt;
	}

	/**
	 * Gets the login ip.
	 * 
	 * @return the login ip
	 */
	public final String getLoginIp() {
		return loginIp;
	}

	/**
	 * Sets the login ip.
	 * 
	 * @param loginIp the new login ip
	 */
	public final void setLoginIp(String loginIp) {
		if (loginIp != null && loginIp.length() > 20){
			logger.error("Login IP is too long: " + loginIp);
			loginIp = loginIp.substring(0, 20);
		}
		this.loginIp = loginIp;
	}
	
	/**
	 * Checks if is locked.
	 * 
	 * @return true, if is locked
	 */
	public boolean isLocked(){
		return !accountNonLocked;
	}
	
	/**
	 * Sets the locked.
	 * 
	 * @param lock the new locked
	 */
	public void setLocked(boolean lock){
		if (!lock){
			this.loginAttempts = 0;
			this.loginAttempt = null;
		}
		
		this.accountNonLocked = !lock;
	}
	
	/**
	 * Log failed.
	 * 
	 * @param authenticationRequest the authentication request
	 */
	public void logFailed(AuthenticationRequest authenticationRequest){
		setLoginAttempts(getLoginAttempts()+1);
		setLoginAttempt(new DateTime());
		
		setLoginIp(authenticationRequest.getSourceIP());
		
		if (getLoginAttempts() >= authenticationRequest.getMaxLoginAttemptsAllowed()){
			setAccountNonLocked(false);
		}
	}
	
	/**
	 * Log succeeded.
	 * 
	 * @param authenticationRequest the authentication request
	 */
	public void logSucceeded(AuthenticationRequest authenticationRequest){
		setLoginAttempts(0);
		setLastLogin(new DateTime());
		setLoginIp(authenticationRequest.getSourceIP());
	}
	
	public int getMinUsernameLen(){
		return 4;
	}

	public Map<String, Object> getExtendedProperties() {
		return extendedProperties;
	}

	public void setExtendedProperties(Map<String, Object> extendedProperties) {
		this.extendedProperties = extendedProperties;
	}
}
