/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 * 
 * Created on: Mar 15, 2009
 * Created by: emcgreal
 */
package com.imilia.server.domain.validation;

import com.imilia.server.validation.ValidationException;

/**
 * The Interface ValidationTransaction provides a simple interface to wrap
 * calls to methods that throw validation exceptions.
 * 
 * @see doInValidationException
 */
public interface ValidationTransaction {

	/**
	 * Execute the code that may throw validation exceptions.
	 * 
	 * @throws ValidationException the validation exception - caught within the method
	 * doInValidationException
	 */
	public void execute() throws ValidationException;
}

