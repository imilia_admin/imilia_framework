/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 *
 * Created on: Jun 8, 2009
 * Created by: emcgreal
 */
package com.imilia.server.domain.common;

import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamSource;

import com.imilia.server.domain.enums.MimeType;

/**
 * The Class EmailAttachmentImpl wraps a byte array as attachment
 *
 * <p>Useful for sending dynamic content</p>
 */
public class EmailByteArrayAttachment implements EmailAttachment {

	/** The name. */
	private String name;

	/** The contents. */
	private byte[] contents;

	/** The mime type. */
	private MimeType mimeType;

	/**
	 * Gets the input stream source.
	 *
	 * @return the input stream source
	 */
	public InputStreamSource getInputStreamSource() {
		return new ByteArrayResource(contents, name);
	}

	/* (non-Javadoc)
	 * @see com.imilia.server.domain.common.EMailAttachment#getName()
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the contents.
	 *
	 * @return the contents
	 */
	public byte[] getContents() {
		return contents;
	}

	/**
	 * Sets the contents.
	 *
	 * @param contents the contents to set
	 */
	public void setContents(byte[] contents) {
		this.contents = contents;
	}

	/**
	 * Gets the mime type.
	 *
	 * @return the mimeType
	 */
	public MimeType getMimeType() {
		return mimeType;
	}

	/**
	 * Sets the mime type.
	 *
	 * @param mimeType the mimeType to set
	 */
	public void setMimeType(MimeType mimeType) {
		this.mimeType = mimeType;
	}
}
