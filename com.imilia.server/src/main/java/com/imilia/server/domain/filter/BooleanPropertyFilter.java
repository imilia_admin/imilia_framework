/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2010 - all rights reserved
 * Created on: Aug 10, 2010
 * Created by: emcgreal
 */

package com.imilia.server.domain.filter;

import com.imilia.server.validation.ValidationException;

/**
 * The Class BitMaskPropertyFilter.
 */
public class BooleanPropertyFilter extends PropertyFilter {

	/** The Constant TAG_FILTER. */
	public final static String TAG_FILTER = "BooleanPropertyFilter";
	
	/** The Constant TAG_FILTERS. */
	public final static String TAG_FILTERS = "booleanValue";

	/** The boolean value. */
	private Boolean booleanValue;
	
	/**
	 * Instantiates a new string list property filter.
	 */
	public BooleanPropertyFilter() {
		super();
	}

	/**
	 * Instantiates a new boolean property filter.
	 *
	 * @param other the other
	 */
	public BooleanPropertyFilter(BooleanPropertyFilter other) {
		super(other);
		setBooleanValue(other.isBooleanValue());
	}

	/**
	 * Instantiates a new boolean property filter.
	 *
	 * @param propertyName the property name
	 */
	public BooleanPropertyFilter(String propertyName) {
		super(propertyName);
	}

	/**
	 * The Constructor.
	 * 
	 * @param classFilter the class filter
	 * @param propertyName the property name
	 */
	public BooleanPropertyFilter(ClassFilter<?> classFilter, String propertyName) {
		super(classFilter, propertyName);
	}

	
	/**
	 * The Constructor.
	 * 
	 * @param classFilter the class filter
	 * @param propertyName the property name
	 * @param propertyNameLabelMessageID the property name label message id
	 */
	public BooleanPropertyFilter(ClassFilter<?> classFilter, String propertyName, String propertyNameLabelMessageID) {
		super(classFilter, propertyName, propertyNameLabelMessageID);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.imilia.bkf.api.domain.filter.PropertyFilter#clear()
	 */
	@Override
	public void clear() {
		this.booleanValue = null;
	}

	/* (non-Javadoc)
	 * @see com.imilia.server.domain.filter.PropertyFilter#buildQuery(com.imilia.server.domain.filter.QueryBuilder)
	 */
	@Override
	public void buildQuery(QueryBuilder q) throws ValidationException {
		if (isValid()) {
			q.addRestriction(getQualifiedName() + " = " + q.addParam(isBooleanValue()));
		}
	}
	
	/*
	 * (non-Javadoc)
	 *
	 * @see com.imilia.bkf.api.domain.filter.PropertyFilter#isValid()
	 */
	@Override
	public boolean isValid() {
		return booleanValue != null;
	}


	/*
	 * (non-Javadoc)
	 *
	 * @see com.imilia.api.domain.filter.PropertyFilter#duplicate()
	 */
	@Override
	public PropertyFilter duplicate() {
		return new BooleanPropertyFilter(this);
	}

	/* (non-Javadoc)
	 * @see com.imilia.server.domain.filter.PropertyFilter#visit(com.imilia.server.domain.filter.PropertyFilterVisitor)
	 */
	@Override
	public <T> T visit(PropertyFilterVisitor<T> propertyFilterVisitor) {
		return propertyFilterVisitor.accept(this);		
	}

	/**
	 * Checks if is boolean value.
	 *
	 * @return the boolean
	 */
	public final Boolean isBooleanValue() {
		return booleanValue;
	}
	
	/**
	 * Gets the boolean value.
	 *
	 * @return the boolean value
	 */
	public final Boolean getBooleanValue() {
		return booleanValue;
	}

	/**
	 * Sets the boolean value.
	 *
	 * @param booleanValue the new boolean value
	 */
	public final void setBooleanValue(Boolean booleanValue) {
		this.booleanValue = booleanValue;
	}
	
	/**
	 * Configure.
	 *
	 * @param b the b
	 * @return the boolean property filter
	 */
	public BooleanPropertyFilter configure(Boolean b){
		setBooleanValue(b);
		return this;
	}
}
