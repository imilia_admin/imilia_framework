package com.imilia.server.domain.validation;

public interface ConditionalValidation {
	public ValidationClassSpec getValidationClassSpec();
}
