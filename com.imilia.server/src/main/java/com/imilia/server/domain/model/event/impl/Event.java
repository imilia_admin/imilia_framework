package com.imilia.server.domain.model.event.impl;

import com.imilia.server.domain.model.event.IInvoker;
import com.imilia.server.domain.model.event.IEvent;

public class Event<T> implements IEvent {
	
	private final T source; 
	
	private final IInvoker invoker;

	public Event(T source, IInvoker command) {
		super();
		this.source = source;
		this.invoker = command;
	}

	public IInvoker getInvoker() {
		return invoker;
	}

	public T getSource() {
		return source;
	}
}
