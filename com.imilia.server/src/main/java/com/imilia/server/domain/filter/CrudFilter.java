/**
 * Imilia Interactive Mobile Applications  GmbH.
 * Copyright (c) 2008
 * @author emcgreal
 * @created 12.03.2008
 * Last modified by:   	$Author:$
 * Last modified on:	$Date:$
 * Version:				$Revision:$
 */
package com.imilia.server.domain.filter;

import java.util.ArrayList;
import java.util.List;

import com.imilia.server.domain.Crud;

/**
 * The Class CrudFilter applies a CRUD to the resulting entities
 */

public class CrudFilter<T> extends ClassFilter<T> {

	public final static String TAG_FILTER = "crudFilter";
	public final static String TAG_CRUD = "cruds";
	

	/** The recipe. */
	protected Recipe<?> recipe;

	/** The cruds. */
	private long cruds = Crud.READABLE;

	/**
	 * Constructor.
	 */
	public CrudFilter() {
	}

	/**
	 * The Constructor.
	 *
	 * @param recipe
	 *            the recipe
	 */
	public CrudFilter(Recipe<T> recipe) {
		super(recipe.getClazz());
		this.recipe = recipe;
	}

	/**
	 * Instantiates a new crud filter.
	 *
	 * @param clazz
	 *            the clazz
	 */
	public CrudFilter(Class<T> clazz) {
		super(clazz);
	}

	/**
	 * Instantiates a new crud filter.
	 *
	 * @param clazz
	 *            the clazz
	 * @param path
	 *            the path
	 */
	public CrudFilter(Class<T> clazz, String path) {
		super(clazz, path);
	}

	/**
	 * Sets the recipe.
	 *
	 * @param recipe
	 *            the recipe to set
	 */
	public void setRecipe(Recipe<?> recipe) {
		this.recipe = recipe;
	}

	/**
	 * Find class filters.
	 *
	 * @param clazz
	 *            the clazz
	 *
	 * @return the list< class filter>
	 */
	public List<ClassFilter<?>> findClassFilters(Class<?> clazz) {
		List<ClassFilter<?>> filters = new ArrayList<ClassFilter<?>>();

		for (ClassFilter<?> cf : getClassFilters()) {
			if (cf.getClazz().equals(clazz)) {
				filters.add(cf);
			}
		}
		return filters;
	}

	/**
	 * Gets the recipe.
	 *
	 * @return the recipe
	 */
	public Recipe<?> getRecipe() {
		return recipe;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.imilia.api.domain.filter.ClassFilter#duplicate(com.imilia.api.domain
	 * .filter.Recipe)
	 */
	@Override
	public ClassFilter<T> duplicate(Recipe<T> recipe) {
		final CrudFilter<T> lhs = new CrudFilter<T>(recipe);
		lhs.setPath(getPath());
		lhs.setCruds(cruds);

		for (PropertyFilter pf : getPropertyFilters()) {
			PropertyFilter duplicate = pf.duplicate();
			
			lhs.add(duplicate);
		}

		for (ClassJoin cj : getClassJoins()) {
			cj.duplicate(lhs);
		}
		lhs.setRestriction(getRestriction());
		return lhs;
	}

	/**
	 * Gets the cruds.
	 *
	 * @return the cruds
	 */
	public long getCruds() {
		return cruds;
	}

	/**
	 * Sets the cruds.
	 *
	 * @param cruds
	 *            the new cruds
	 */
	public void setCruds(long cruds) {
		this.cruds = cruds;
	}

}
