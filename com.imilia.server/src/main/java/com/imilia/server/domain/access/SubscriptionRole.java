/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2014 - all rights reserved
 * Created on: Feb 21, 2014
 * Created by: emcgreal
 */
package com.imilia.server.domain.access;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.imilia.server.domain.DomainObject;

/**
 * The Class SubscriptionRole.
 */
@Entity
public class SubscriptionRole extends DomainObject{
	
	/** The subscription. */
	@ManyToOne
	private Subscription subscription;
	
	/** The role. */
	@ManyToOne
	private Role role;
	
	/**
	 * Instantiates a new subscription role.
	 */
	public SubscriptionRole(){
		super();
	}
	
	/**
	 * Instantiates a new subscription role.
	 *
	 * @param subscription the subscription
	 * @param r the r
	 */
	public SubscriptionRole(Subscription subscription, Role r) {
		super();
		this.subscription = subscription;
		this.role = r;
	}

	/**
	 * Gets the subscription.
	 *
	 * @return the subscription
	 */
	public Subscription getSubscription() {
		return subscription;
	}

	/**
	 * Sets the subscription.
	 *
	 * @param subscription the new subscription
	 */
	public void setSubscription(Subscription subscription) {
		this.subscription = subscription;
	}

	/**
	 * Gets the role.
	 *
	 * @return the role
	 */
	public Role getRole() {
		return role;
	}

	/**
	 * Sets the role.
	 *
	 * @param role the new role
	 */
	public void setRole(Role role) {
		this.role = role;
	}
}
