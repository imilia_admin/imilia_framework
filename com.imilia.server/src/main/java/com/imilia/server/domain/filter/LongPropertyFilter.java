/**
 * Imilia - Interactive Mobile Applications  GmbH.
 * Copyright (c) 2008
 *
 * @author:		emcgreal
 * @created:	Mar 13, 2008
 * Last modified by:   $Author:$
 * Last modfication:   $Date:$
 * Version:            $Revision:$
 */
package com.imilia.server.domain.filter;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.FieldError;

import com.imilia.server.validation.ValidationException;

// TODO: Auto-generated Javadoc
/**
 * The Class LongPropertyFilter.
 */
public class LongPropertyFilter extends PropertyFilter {
	
	/** The Constant TAG_FILTER. */
	public final static String TAG_FILTER = "longPropertyFilter";
	
	/** The Constant TAG_PREDICATES. */
	public final static String TAG_PREDICATES = "longPredicates";
	
	/** The Constant LONG_FORMAT_ERROR. */
	private static final String LONG_FORMAT_ERROR = "typeMismatch.java.lang.Long";

	/** The Constant logger. */
	private final static Logger logger = LoggerFactory
			.getLogger(LongPropertyFilter.class);

	/** The long predicates. */
	private String longPredicates;

	/** The longs. */
	private List<Long> longs = new ArrayList<Long>(); 

	/**
	 * Instantiates a new long property filter.
	 */
	public LongPropertyFilter() {
		super();
	}

	/**
	 * Instantiates a new long property filter.
	 *
	 * @param propertyFilter the property filter
	 */
	public LongPropertyFilter(LongPropertyFilter propertyFilter) {
		super(propertyFilter);
		
		try {
			setLongPredicates(propertyFilter.longPredicates);
		} catch (ValidationException e) {
		}
	}


	/**
	 * Instantiates a new long property filter.
	 * 
	 * @param propertyName the property name
	 */
	public LongPropertyFilter(String propertyName) {
		super(propertyName);
	}

	/**
	 * Instantiates a new long property filter.
	 *
	 * @param classFilter the class filter
	 * @param propertyName the property name
	 */
	public LongPropertyFilter(ClassFilter<?> classFilter, String propertyName) {
		super(classFilter, propertyName);
	}

	/**
	 * Instantiates a new long property filter.
	 *
	 * @param classFilter the class filter
	 * @param propertyName the property name
	 */
	public LongPropertyFilter(ClassFilter<?> classFilter, String propertyName, long flags) {
		super(classFilter, propertyName, flags);
	}

	/**
	 * Instantiates a new long property filter.
	 * 
	 * @param classFilter the class filter
	 * @param propertyName the property name
	 * @param propertyNameLabelMessageID the property name label message id
	 */
	public LongPropertyFilter(ClassFilter<?> classFilter, String propertyName,  long flags, String propertyNameLabelMessageID) {
		super(classFilter, propertyName, propertyNameLabelMessageID);
	}

	/**
	 * Parses the range.
	 *
	 * @param alias the alias
	 * @param q the q
	 * @param token the token
	 * @return between Criteria if a range was present
	 * @throws ValidationException the validation exception
	 */
	private String parseRange(String alias, QueryBuilder q, String token)
			throws ValidationException {
		String[] tokens = token.split(rangeDelimiter);

		if (tokens != null) {
			if (tokens.length == 2) {
				long lhs = parseLong(alias, tokens[0]);
				long rhs = parseLong(alias, tokens[1]);

				return addBetwen(q, lhs, rhs);
			} else {
				return getQualifiedName() + " = "
						+ q.addParam(parseLong(alias, tokens[0]));
			}
		}
		return null;
	}

	/**
	 * Tries to convert the token to an integer.
	 *
	 * @param alias the alias
	 * @param s the s
	 * @return long value
	 * @throws ValidationException the validation exception
	 */
	private long parseLong(String alias, String s) throws ValidationException {
		try {
			return Long.parseLong(s.trim());
		} catch (NumberFormatException ex) {
			logger.debug("Invalid long filter: " + s);

			final String clazzName = getClassFilter().getClazz()
					.getCanonicalName();
			FieldError o = new FieldError(clazzName, getQualifiedName(), s,
					true, new String[] { LONG_FORMAT_ERROR },
					new Object[] { new DefaultMessageSourceResolvable(
							LONG_FORMAT_ERROR) }, LONG_FORMAT_ERROR);
			BeanPropertyBindingResult errors = new BeanPropertyBindingResult(
					this, clazzName);
			errors.addError(o);
			throw new ValidationException(errors, s);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.imilia.server.domain.filter.PropertyFilter#isValid()
	 */
	public boolean isValid() {
		return longPredicates != null && !"".equals(longPredicates);
	}

	/**
	 * Gets the long predicates.
	 *
	 * @return the longPredicates
	 */
	public String getLongPredicates() {
		return longPredicates;
	}

	/**
	 * Sets the long predicates.
	 *
	 * @param longPredicates the longPredicates to set
	 * @throws ValidationException the validation exception
	 */
	public void setLongPredicates(String longPredicates) throws ValidationException {
		this.longPredicates = longPredicates;
		if (this.longs != null){
			this.longs.clear();
		}else{
			this.longs = new ArrayList<Long>();
		}
		
		if (this.longPredicates != null){
			String[] splits = this.longPredicates.split(";");
			
			for (String s:splits){
				this.longs.add(parseLong(null, s));
			}
		}
	}
	
	/**
	 * Sets the longs.
	 *
	 * @param longPredicates the new longs
	 */
	public void setLongs(List<Long> longPredicates) {
		this.longs = longPredicates;
		
		StringBuffer buffer = new StringBuffer();
		
		for (Long l:longPredicates){
			if (buffer.length() > 0){
				buffer.append(";");
			}
			buffer.append(l);
		}
		
		this.longPredicates = buffer.toString();
	}
	
	/**
	 * Gets the longs.
	 *
	 * @return the longs
	 */
	public List<Long> getLongs(){
		return longs;
	}
	
	/**
	 * Configure.
	 *
	 * @param oids the oids
	 * @return the long property filter
	 */
	public LongPropertyFilter configure(List<Long> oids){
		setLongs(oids);
		return this;
	}
	
	

	/* (non-Javadoc)
	 * @see com.imilia.server.domain.filter.PropertyFilter#buildQuery(com.imilia.server.domain.filter.QueryBuilder)
	 */
	@Override
	public void buildQuery(QueryBuilder q) throws ValidationException {
		final String path = getClassFilter().getPath();

		if (isValid()) {

			final StringBuffer buffer = new StringBuffer();
			if (!isNullOrNotNull(buffer, longPredicates)){
				// Split the longPredicates up using the delimiter ;
				String[] tokens = longPredicates.split(";");

				boolean parenthesis = false;
				for (String t : tokens) {
					String c = parseRange(path, q, t);

					if (c != null) {
						if (buffer.length() > 0) {
							parenthesis = true;
							buffer.append(" OR ");
						}
						buffer.append(c);
					}
				}
				
				if (parenthesis){
					buffer.insert(0, "(");
					buffer.append(")");
				}
			}
			final String query = buffer.toString();
			logger.debug(query);
			q.addRestriction(query);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.imilia.bkf.api.domain.filter.PropertyFilter#clear()
	 */
	@Override
	public void clear() {
		longPredicates = null;
		longs.clear();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.imilia.api.domain.filter.PropertyFilter#duplicate()
	 */
	@Override
	public PropertyFilter duplicate() {
		return new LongPropertyFilter(this);
	}

	/* (non-Javadoc)
	 * @see com.imilia.server.domain.filter.PropertyFilter#getFilterPropertyName()
	 */
	@Override
	public String getFilterPropertyName() {
		return "longs";
	}
	
	/* (non-Javadoc)
	 * @see com.imilia.server.domain.filter.PropertyFilter#visit(com.imilia.server.domain.filter.PropertyFilterVisitor)
	 */
	@Override
	public <T> T visit(PropertyFilterVisitor<T> propertyFilterVisitor) {
		return propertyFilterVisitor.accept(this);		
	}
}
