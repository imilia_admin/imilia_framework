package com.imilia.server.domain.query.execution.values;

import com.imilia.server.domain.query.entities.QueryProperty;

public class EnumQueryPropertyValue extends QueryPropertyValue {

	public EnumQueryPropertyValue(QueryProperty queryProperty) {
		super(queryProperty);
	}
}
