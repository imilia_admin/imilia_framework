/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 *
 * Created on: Mar 13, 2009
 * Created by: emcgreal
 */
package com.imilia.server.domain;

import java.util.Comparator;

public interface ObjectIdentity {

	public class ObjectIdentityComparator<T extends ObjectIdentity> implements Comparator<T>{

		public int compare(T lhs, T rhs) {

			if (lhs.getOid() < rhs.getOid()){
				return -1;
			}

			if (lhs.getOid() > rhs.getOid()){
				return 1;
			}

			return 0;
		}

	};

	/**
	 * Gets the oid.
	 *
	 * @return the oid
	 */
	public long getOid();
}
