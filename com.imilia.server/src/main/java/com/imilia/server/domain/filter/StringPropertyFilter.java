/**
 * Imilia - Interactive Mobile Applications  GmbH.
 * Copyright (c) 2008
 *
 * @author:		emcgreal
 * @created:	Mar 13, 2008
 * Last modified by:   $Author:$
 * Last modfication:   $Date:$
 * Version:            $Revision:$
 */
package com.imilia.server.domain.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.imilia.server.validation.ValidationException;

/**
 * The Class StringPropertyFilter.
 */
public class StringPropertyFilter extends PropertyFilter {
	
	/** The Constant TAG_FILTER. */
	public final static String TAG_FILTER = "stringPropertyFilter";
	
	/** The Constant TAG_PREDICATES. */
	public final static String TAG_PREDICATES = "stringPredicates";

	/** The Constant logger. */
	private final static Logger logger = LoggerFactory
			.getLogger(StringPropertyFilter.class);

	/** The string predicates. */
	private String stringPredicates;

	/**
	 * Instantiates a new string property filter.
	 */
	public StringPropertyFilter() {
		super();
	}

	/**
	 * Instantiates a new string property filter.
	 * 
	 * @param other the other
	 */
	public StringPropertyFilter(StringPropertyFilter other) {
		super(other);
		setStringPredicates(other.stringPredicates);
	}

	/**
	 * Instantiates a new string property filter.
	 * 
	 * @param propertyName the property name
	 */
	public StringPropertyFilter(String propertyName) {
		super(propertyName);
	}

	/**
	 * Instantiates a new string property filter.
	 *
	 * @param classFilter the class filter
	 * @param propertyName the property name
	 */
	public StringPropertyFilter(ClassFilter<?> classFilter, String propertyName) {
		super(classFilter, propertyName);
	}

	/**
	 * Instantiates a new string property filter.
	 *
	 * @param classFilter the class filter
	 * @param propertyName the property name
	 */
	public StringPropertyFilter(ClassFilter<?> classFilter, String propertyName, long flags) {
		super(classFilter, propertyName, flags);
	}
	/**
	 * Instantiates a new string property filter.
	 * 
	 * @param classFilter the class filter
	 * @param propertyName the property name
	 * @param propertyNameLabelMessageID the property name label message id
	 */
	public StringPropertyFilter(ClassFilter<?> classFilter, String propertyName, String propertyNameLabelMessageID) {
		super(classFilter, propertyName, propertyNameLabelMessageID);
	}
	
	/**
	 * Instantiates a new string property filter.
	 *
	 * @param classFilter the class filter
	 * @param propertyName the property name
	 * @param flags the flags
	 * @param propertyNameLabelMessageID the property name label message id
	 */
	public StringPropertyFilter(ClassFilter<?> classFilter, String propertyName, long flags, String propertyNameLabelMessageID) {
		super(classFilter, propertyName, flags, propertyNameLabelMessageID);
	}


	/*
	 * (non-Javadoc)
	 *
	 * @see com.imilia.server.domain.filter.PropertyFilter#isValid()
	 */
	public boolean isValid() {
		return stringPredicates != null && !"".equals(stringPredicates);
	}


	/* (non-Javadoc)
	 * @see com.imilia.server.domain.filter.PropertyFilter#buildQuery(com.imilia.server.domain.filter.QueryBuilder)
	 */
	@Override
	public void buildQuery(QueryBuilder q) throws ValidationException {
		if (isValid()) {
			final StringBuffer buffer = new StringBuffer();

			boolean parenthesis = false;
			// Check that its not just a simple null check
			if (!isNullOrNotNull(buffer, stringPredicates)){
				// Split the predicates up using the delimiter ;
				final String[] tokens = stringPredicates.split(";");

				for (String t : tokens) {
					if (buffer.length() > 0) {
						buffer.append(" OR ");
						parenthesis = true;
					}
					// Trim any empty characters
					t = t.trim();

					boolean negate = false;
					// Negate if necessary
					if ('!' == t.charAt(0)){
						negate = true;
						t = t.substring(1);			// Cut out the '!'
					}

					String[] range = t.split(":");

					if (range.length == 2) {
						buffer.append(getQualifiedName());
						if (negate){
							buffer.append(" not ");
						}
						buffer.append(" BETWEEN ");
						buffer.append(q.addParam(range[0]));
						buffer.append(" AND ");
						buffer.append(q.addParam(range[1]));
					} else {
						buffer.append(getQualifiedName());
						if (negate){
							buffer.append(" not ");
						}
						if (t.contains("*")){
							buffer.append(" like ");
						}else{
							buffer.append(" = ");
						}
						buffer.append(q.addParam(t.replace("*", "%")));
					}
				}
			}
			
			if (parenthesis){
				// An or should be inside of parenthesis 
				buffer.insert(0, "(");
				buffer.append(")");
			}
			
			final String query = buffer.toString();
			logger.debug(query);
			q.addRestriction(query);
		}
	}

	/**
	 * Gets the string predicates.
	 *
	 * @return the stringPredicates
	 */
	public String getStringPredicates() {
		return stringPredicates;
	}

	/**
	 * Sets the string predicates.
	 *
	 * @param stringPredicates
	 *            the stringPredicates to set
	 */
	public void setStringPredicates(String stringPredicates) {
		this.stringPredicates = stringPredicates;
	}

	/**
	 * Configure.
	 *
	 * @param stringPredicates the string predicates
	 * @return the string property filter
	 */
	public StringPropertyFilter configure(String stringPredicates) {
		setStringPredicates(stringPredicates);
		return this;
	}

		
	/*
	 * (non-Javadoc)
	 *
	 * @see com.imilia.bkf.api.domain.filter.PropertyFilter#clear()
	 */
	@Override
	public void clear() {
		this.stringPredicates = null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.imilia.api.domain.filter.PropertyFilter#duplicate()
	 */
	@Override
	public PropertyFilter duplicate() {
		return new StringPropertyFilter(this);
	}
	
	/* (non-Javadoc)
	 * @see com.imilia.server.domain.filter.PropertyFilter#visit(com.imilia.server.domain.filter.PropertyFilterVisitor)
	 */
	@Override
	public <T> T visit(PropertyFilterVisitor<T> propertyFilterVisitor) {
		return propertyFilterVisitor.accept(this);		
	}
}
