package com.imilia.server.domain.query.execution;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.imilia.server.domain.Crud;
import com.imilia.server.domain.CrudAware;
import com.imilia.server.domain.query.entities.QueryRestriction;
import com.imilia.server.domain.query.execution.values.QueryPropertyValue;
import com.imilia.server.domain.query.execution.values.QueryRestrictionValue;
import com.imilia.utils.strings.StringVariableReplacer;

public class QueryCrudExecution<T extends CrudAware> implements StringVariableReplacer {
	private final QueryExecution<T> queryExecution;
	
	private final Crud crud;
	
	private StringBuilder restrictionClause = new StringBuilder();
	
	private final HashMap<String, Object> parameters = new HashMap<>();
	
	private final List<QueryRestrictionValue> queryRestrictionValues = new ArrayList<>(); 

	
	public QueryCrudExecution(QueryExecution<T> queryExecution,
			Crud crud) {
		super();
		this.queryExecution = queryExecution;
		this.crud = crud;
	
	}
	
	/**
	 * Adds the parameter generating a new parame ID.
	 * 
	 * @param param - parameter value
	 * 
	 * @return the string
	 */
	public String addParameter(Object param) {
		String nextParam = "p" + parameters.size();
		parameters.put(nextParam, param);
		return ":" + nextParam;
	}
	
	public String addParameters(Object[] params){
		StringBuilder builder = new StringBuilder();
		
		for (Object p:params){
			if (builder.length() > 0){
				builder.append(", ");
			}
			builder.append(addParameter(p));
		}
		
		return builder.toString();
	}
	
	public void addRestrictionValue(String r, LogicalOperator op) {
		if (restrictionClause.length() > 0) {
			restrictionClause.append(" ");
			restrictionClause.append(op.name());
			restrictionClause.append(" ");
		}
		restrictionClause.append(r);
	}

	public Crud getCrud() {
		return crud;
	}

	public StringBuilder getRestrictionClause() {
		return restrictionClause;
	}

	public void setRestrictionClause(StringBuilder restrictionClause) {
		this.restrictionClause = restrictionClause;
	}

	public HashMap<String, Object> getParameters() {
		return parameters;
	}

	public QueryExecution<T> getQueryExecution() {
		return queryExecution;
	}

	public String toJQL() {
		String query = queryExecution.getQuery().getQueryTemplate().getQueryText();
		
		// Add in any query property values
		for (QueryPropertyValue v:queryExecution.getQuery().getQueryPropertyValues()){
			v.addToExecution(this);
		}
		
		for (QueryRestrictionValue r:queryRestrictionValues){
			r.addToExecution(this);
		}
		
		String anchor = null;
		if (query.contains("$where")){
			anchor = "$where";
		}else if (query.contains("$and")){
			anchor = "$and";
		}
		
		if (anchor != null){
			
			String replacement = restrictionClause.toString();
			
			if (replacement.length() > 0){
				String anchorWord = anchor.substring(1);
				query = query.replace(anchor, anchorWord + " " + replacement);
			}else{
				query = query.replace(anchor, "");
			}
		}

		return query;
	}

	@Override
	public String getReplacement(String variableName) {
		final Object value = queryExecution.getVariableValue(variableName);
		
		return addParameter(value);
	}

	public List<QueryRestrictionValue> getQueryRestrictionValues() {
		return queryRestrictionValues;
	}

	public void addRestrictions(List<QueryRestriction> restrictions) {
		for (QueryRestriction r:restrictions){
			queryRestrictionValues.add(new QueryRestrictionValue(r));
		}
	}

}