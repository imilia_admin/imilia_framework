/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 * Created on: Dec 23, 2009
 * Created by: emcgreal
 */
package com.imilia.server.domain.user;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.imilia.server.domain.DomainObject;

/**
 * The Class SubscriptionProperty.
 */
@Entity
public class UserProperty extends DomainObject {
	
	/** The owner. */
	@NotNull
	private Long owner;
	
	/** The instance id. */
	private String instanceId;
	
	/** The property path. */
	@NotNull
	@NotBlank
	private String propertyPath;
	
	/** The units. */
	private String units;
	
	/** The property value. */
	private String propertyValue;
	
	/**
	 * Instantiates a new user property.
	 */
	public UserProperty(){
		super();
	}
	
	/**
	 * Instantiates a new user property.
	 * 
	 * @param instanceId the instance id
	 * @param path the path
	 */
	public UserProperty(String instanceId, String path) {
		super();
		this.instanceId = instanceId;
		this.propertyPath = path;
	}

	/**
	 * Gets the instance id.
	 * 
	 * @return the instance id
	 */
	public String getInstanceId() {
		return instanceId;
	}

	/**
	 * Sets the instance id.
	 * 
	 * @param id the new instance id
	 */
	public void setInstanceId(String id) {
		this.instanceId = id;
	}
	
	/**
	 * Gets the property path.
	 * 
	 * @return the property path
	 */
	public String getPropertyPath() {
		return propertyPath;
	}

	/**
	 * Sets the property path.
	 * 
	 * @param propertyPath the new property path
	 */
	public void setPropertyPath(String propertyPath) {
		this.propertyPath = propertyPath;
	}

	/**
	 * Gets the units.
	 * 
	 * @return the units
	 */
	public String getUnits() {
		return units;
	}

	/**
	 * Sets the units.
	 * 
	 * @param units the new units
	 */
	public void setUnits(String units) {
		this.units = units;
	}

	/**
	 * Gets the property value.
	 * 
	 * @return the property value
	 */
	public String getPropertyValue() {
		return propertyValue;
	}

	/**
	 * Sets the property value.
	 * 
	 * @param propertyValue the new property value
	 */
	public void setPropertyValue(String propertyValue) {
		this.propertyValue = propertyValue;
	}

	/**
	 * Gets the owner.
	 * 
	 * @return the owner
	 */
	public Long getOwner() {
		return owner;
	}

	/**
	 * Sets the owner.
	 * 
	 * @param owner the new owner
	 */
	public void setOwner(Long owner) {
		this.owner = owner;
	}
}
