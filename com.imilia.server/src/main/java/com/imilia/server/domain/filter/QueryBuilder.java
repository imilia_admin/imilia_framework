/**
 * Imilia - Interactive Mobile Applications  GmbH.
 * Copyright (c) 2008
 *
 * @author:		emcgreal
 * @created:	Mar 14, 2008
 * Last modified by:   $Author:$
 * Last modfication:   $Date:$
 * Version:            $Revision:$
 */
package com.imilia.server.domain.filter;

import java.util.HashMap;

import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.avaje.ebean.config.dbplatform.DatabasePlatform;
import com.imilia.utils.i18n.DomainLocale;

/**
 * The Class QueryBuilder.
 * 
 * @author emcgreal
 */
public class QueryBuilder {
	
	/** The domain locale. */
	private final DomainLocale domainLocale;
	
	private final DatabasePlatform databasePlatform;

	/** The Constant logger. */
	private final static Logger logger = LoggerFactory.getLogger(QueryBuilder.class);

	/** The projection. */
	private final StringBuffer projection = new StringBuffer();

	/** The restriction. */
	private final StringBuffer restriction = new StringBuffer();

	/** The params. */
	private final HashMap<String, Object> params = new HashMap<String, Object>();
	
	private final StringBuilder orderBy = new StringBuilder();
	

	
	
	/**
	 * Instantiates a new query builder.
	 * 
	 * @param domainLocale the domain locale
	 * @param databasePlatform 
	 */
	public QueryBuilder(DomainLocale domainLocale, DatabasePlatform databasePlatform) {
		this.domainLocale = domainLocale;
		this.databasePlatform = databasePlatform;
	}

	/**
	 * Gets the projection.
	 * 
	 * @return the query
	 */
	public StringBuffer getProjection() {
		return projection;
	}

	/**
	 * Gets the params.
	 * 
	 * @return the parms
	 */
	public HashMap<String, Object> getParams() {
		return params;
	}

	/**
	 * Gets the next param name.
	 * 
	 * @return the next param name
	 */
	public String getNextParamName() {
		return "p" + params.size();
	}

	/**
	 * Adds the param.
	 * 
	 * @param o
	 *            the o
	 * 
	 * @return the string
	 */
	public String addParam(Object o) {
		String nextParam = "p" + params.size();
		params.put(nextParam, o);
		return ":" + nextParam;
	}

	/**
	 * Gets the restriction.
	 * 
	 * @return the restriction
	 */
	public StringBuffer getRestriction() {
		return restriction;
	}

	/**
	 * Adds the restriction.
	 * 
	 * @param r
	 *            the r
	 */
	public void addRestriction(String r) {
		if (restriction.length() > 0) {
			restriction.append(" AND ");
		}
		restriction.append(r);
	}

	public void addOrderBy(String o) {
		if (orderBy.length() > 0) {
			orderBy.append(", ");
		}
		orderBy.append(o);
	}

	/**
	 * To hql.
	 * 
	 * @return the string
	 */
	public String toHQL() {
		StringBuilder builder = new StringBuilder();
		builder.append(projection.toString());
		if (restriction.length() > 0) {
			builder.append(" WHERE ");
			builder.append(restriction.toString());
		}

		if (orderBy.length() > 0){
			builder.append(" order by ");
			builder.append(orderBy.toString());
		}
		
		if (logger.isDebugEnabled()) {
			logger.debug("OQL query:");
			logger.debug(builder.toString());
		}
		return builder.toString();
	}

	/**
	 * Bind parmeters.
	 * 
	 * @param q
	 *            the q
	 */
	public void bindParmeters(Query q) {
		if (logger.isDebugEnabled()) {
			logger.debug("Bind params begin >>>>>>>>>>>>>>");
		}

		for (String paramName : getParams().keySet()) {
			final Object paramValue = getParams().get(paramName);
			if (logger.isDebugEnabled()) {
				logger.debug(paramName + " value: " + paramValue);
			}
			q.setParameter(paramName, paramValue);
		}

		if (logger.isDebugEnabled()) {
			logger.debug("Bind params end <<<<<<<<<<<<<<<");
		}
	}

	public DomainLocale getDomainLocale() {
		return domainLocale;
	}

	public StringBuilder getOrderBy() {
		return orderBy;
	}

	public DatabasePlatform getDatabasePlatform() {
		return databasePlatform;
	}
}
