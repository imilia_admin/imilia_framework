/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2010 - all rights reserved
 * Created on: Aug 10, 2010
 * Created by: emcgreal
 */
package com.imilia.server.domain.filter;

/**
 * The Enum BitMaskOperator.
 */
public enum BitMaskOperator {
	AND,
	
	/** The OR. */
	OR
}
