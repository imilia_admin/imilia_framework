/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2014 - all rights reserved
 * Created on: Feb 26, 2014
 * Created by: emcgreal
 */

package com.imilia.server.domain.query.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.imilia.server.domain.Crud;
import com.imilia.server.domain.DomainObject;
import com.imilia.server.domain.access.Role;

/**
 * The Class Restriction.
 */
@Entity
public class QueryRestriction extends DomainObject {
	
	@Embedded
	private Crud restrictionCrud;
	
	/** The restriction. */
	@Lob
	@Column(length=16384)		
	private String restriction;
	
	@OneToMany(mappedBy="restriction", cascade=CascadeType.ALL)
	private List<QueryRoleRestriction> restrictionRoles;
	
	@ManyToOne
	private QueryTemplate queryTemplate;
	
	public QueryRestriction(){
		super();
	}
	
	public QueryRoleRestriction addRole(Role role) {
		return new QueryRoleRestriction(role, this);
	}


	public Crud getRestrictionCrud() {
		return restrictionCrud;
	}

	public void setRestrictionCrud(Crud crud) {
		this.restrictionCrud = crud;
	}

	public String getRestriction() {
		return restriction;
	}

	public void setRestriction(String text) {
		this.restriction = text;
	}

	public List<QueryRoleRestriction> getRestrictionRoles() {
		return restrictionRoles;
	}

	public void setRestrictionRoles(List<QueryRoleRestriction> restrictionRoles) {
		this.restrictionRoles = restrictionRoles;
	}


	@Override
	public String toString() {
		return "Restriction [queryTemplate=" + queryTemplate + ", restrictionCrud="
				+ restrictionCrud + ", restrictionText=" + restriction
				+ ", restrictionRoles=" + restrictionRoles + "]";
	}


	public QueryTemplate getQueryTemplate() {
		return queryTemplate;
	}


	public void setQueryTemplate(QueryTemplate queryTemplate) {
		this.queryTemplate = queryTemplate;
	}

	public boolean isApplicable(List<Role> roles){
		for (QueryRoleRestriction qr:getRestrictionRoles()){
			if (qr.isApplicable(roles)){
				return true;
			}
		}
		
		return false;
	}
}
