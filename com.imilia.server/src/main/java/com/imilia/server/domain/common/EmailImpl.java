/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 *
 * Created on: Jun 8, 2009
 * Created by: emcgreal
 */
package com.imilia.server.domain.common;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.mail.javamail.MimeMessageHelper;

/**
 * The Class EmailImpl.
 */
public class EmailImpl implements Email {

	/** The sender. */
	private String sender;

	/** The to recipients. */
	private List<String> toRecipients = new ArrayList<String>();

	/** The cc recipients. */
	private List<String> ccRecipients = new ArrayList<String>();

	/** The bcc recipients. */
	private List<String> bccRecipients = new ArrayList<String>();

	/** The subject. */
	private String subject;

	/** The body. */
	private String body;

	/** The html. */
	private boolean html;

	/** The attachments. */
	private List<EmailAttachment> attachments = new ArrayList<EmailAttachment>();

	private int mulitpartMode = MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED;
	/**
	 * Instantiates a new e mail impl.
	 */
	public EmailImpl(){
	}

	/**
	 * Gets the sender.
	 *
	 * @return the sender
	 */
	public String getSender() {
		return sender;
	}

	/**
	 * Sets the sender.
	 *
	 * @param sender the sender to set
	 */
	public void setSender(String sender) {
		this.sender = sender;
	}

	/**
	 * Gets the to recipients.
	 *
	 * @return the toRecipients
	 */
	public List<String> getToRecipients() {
		return toRecipients;
	}

	/**
	 * Sets the to recipients.
	 *
	 * @param toRecipients the toRecipients to set
	 */
	public void setToRecipients(List<String> toRecipients) {
		this.toRecipients = toRecipients;
	}
	
	public void setToRecipients(String...toRecipients) {
		this.toRecipients = new ArrayList<String>();
		
		for (String r:toRecipients){
			this.toRecipients.add(r);
		}
	}


	/**
	 * Gets the cc recipients.
	 *
	 * @return the ccRecipients
	 */
	public List<String> getCcRecipients() {
		return ccRecipients;
	}

	/**
	 * Sets the cc recipients.
	 *
	 * @param ccRecipients the ccRecipients to set
	 */
	public void setCcRecipients(List<String> ccRecipients) {
		this.ccRecipients = ccRecipients;
	}

	/**
	 * Gets the bcc recipients.
	 *
	 * @return the bccRecipients
	 */
	public List<String> getBccRecipients() {
		return bccRecipients;
	}

	/**
	 * Sets the bcc recipients.
	 *
	 * @param bccRecipients the bccRecipients to set
	 */
	public void setBccRecipients(List<String> bccRecipients) {
		this.bccRecipients = bccRecipients;
	}

	/**
	 * Sets the bcc recipients.
	 *
	 * @param bccRecipients the new bcc recipients
	 */
	public void setBccRecipients(String...bccRecipients) {
		this.bccRecipients = new ArrayList<String>();
		
		for (String r:bccRecipients){
			this.bccRecipients.add(r);
		}
	}

	/**
	 * Gets the subject.
	 *
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * Sets the subject.
	 *
	 * @param subject the subject to set
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}

	/**
	 * Gets the body.
	 *
	 * @return the body
	 */
	public String getBody() {
		return body;
	}

	/**
	 * Sets the body.
	 *
	 * @param body the body to set
	 */
	public void setBody(String body) {
		this.body = body;
	}

	/**
	 * Checks if is html.
	 *
	 * @return the html
	 */
	public boolean isHtml() {
		return html;
	}

	/**
	 * Sets the html.
	 *
	 * @param html the html to set
	 */
	public void setHtml(boolean html) {
		this.html = html;
	}

	/**
	 * Gets the attachments.
	 *
	 * @return the attachments
	 */
	public List<EmailAttachment> getAttachments() {
		return attachments;
	}

	/**
	 * Sets the attachments.
	 *
	 * @param attachments the attachments to set
	 */
	public void setAttachments(List<EmailAttachment> attachments) {
		this.attachments = attachments;
	}

	/* (non-Javadoc)
	 * @see com.imilia.server.domain.common.EMail#getSummary()
	 */
	public String getSummary() {
		StringBuffer  buffer = new StringBuffer();

		buffer.append("Sender: ");
		buffer.append(getSender());
		buffer.append("\nTo: ");
		buffer.append(getToRecipients());
		buffer.append("\nCC: ");
		buffer.append(getCcRecipients());
		buffer.append("\nBcc: ");
		buffer.append(getBccRecipients());
		buffer.append("\nSubject: ");
		buffer.append(getSubject());
		buffer.append("\nBody:");
		buffer.append(getBody());
		buffer.append("\nAttachments:");

		for (EmailAttachment a:getAttachments()){
			buffer.append(a.getName());
			buffer.append("\n");
		}

		return buffer.toString();
	}

	public Iterator<? extends EmailAttachment> getAttachmentsIterator() {
		return attachments.iterator();
	}

	/* (non-Javadoc)
	 * @see com.imilia.server.domain.common.Email#getMulitpartMode()
	 */
	@Override
	public int getMulitpartMode() {
		return mulitpartMode;
	}

	/* (non-Javadoc)
	 * @see com.imilia.server.domain.common.Email#setMulitpartMode(int)
	 */
	public void setMulitpartMode(int mulitpartMode) {
		this.mulitpartMode = mulitpartMode;
	}

	@Override
	public boolean hasAttachments() {
		return !attachments.isEmpty();
	}
}
