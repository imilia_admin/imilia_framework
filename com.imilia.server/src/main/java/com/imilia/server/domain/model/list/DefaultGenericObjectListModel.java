/**
 * Imilia - Interactive Mobile Applications  GmbH.
 * Copyright (c) 2008
 *
 * @author:		emcgreal
 * @created:	Mar 25, 2008
 * Last modified by:   $Author:$
 * Last modfication:   $Date:$
 * Version:            $Revision:$
 */
package com.imilia.server.domain.model.list;

import java.util.ArrayList;
import java.util.List;

import ognl.Ognl;
import ognl.OgnlException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;

import com.imilia.server.domain.i18n.DomainLocaleVariant;
import com.imilia.utils.i18n.Localized;
import com.imilia.utils.i18n.LocalizedProperty;
import com.imilia.utils.observer.DefaultObservable;

/**
 * The Class GenericObjectListModel.
 *
 * @param <T> the generic type
 * @author emcgreal
 */
public class DefaultGenericObjectListModel<T> extends DefaultObservable
		implements GenericObjectListModel<T> {

	/** The Constant logger. */
	private final static Logger logger = LoggerFactory
			.getLogger(DefaultGenericObjectListModel.class);

	/** The list. */
	protected List<T> list;

	/** The display property. */
	protected String displayProperty = ".";

	/** The store property. */
	protected String storeProperty = ".";
	
	/** The locale. */
	protected DomainLocaleVariant locale;
	
	/** The message source. */
	protected MessageSource messageSource;

	/**
	 * Instantiates a new generic object list model.
	 */
	public DefaultGenericObjectListModel() {
		this.list = new ArrayList<T>();
	}

	/**
	 * Instantiates a new generic object list model.
	 * 
	 * @param list the list
	 * @param displayProperty the display property
	 */
	public DefaultGenericObjectListModel(List<T> list, String displayProperty) {
		this.list = list;
		this.displayProperty = displayProperty;
	}
	
	/**
	 * Instantiates a new generic object list model.
	 * 
	 * @param list the list
	 * @param displayProperty the display property
	 * @param domainLocale the domainLocale
	 */
	public DefaultGenericObjectListModel(List<T> list, String displayProperty, DomainLocaleVariant domainLocale) {
		this.list = list;
		this.displayProperty = displayProperty;
		this.locale = domainLocale;
	}
	
	/**
	 * Instantiates a new generic object list model.
	 * 
	 * @param list the list
	 * @param displayProperty the display property
	 * @param domainLocale the domain locale
	 * @param messageSource the message source
	 */
	public DefaultGenericObjectListModel(List<T> list, String displayProperty, DomainLocaleVariant domainLocale, MessageSource messageSource) {
		this(list, displayProperty, domainLocale);
		this.messageSource = messageSource;
	}
	
	public DefaultGenericObjectListModel(DomainLocaleVariant domainLocale, MessageSource messageSource) {
		super();
		this.locale = domainLocale;
		this.messageSource = messageSource;
	}
	
	/**
	 * Instantiates a new generic object list model.
	 * 
	 * @param list the list
	 * @param displayProperty the display property
	 * @param domainLocale the domain locale
	 * @param messageSource the message source
	 */
	public DefaultGenericObjectListModel(List<T> list, String displayProperty, String storeProperty, DomainLocaleVariant domainLocale, MessageSource messageSource) {
		this(list, displayProperty, storeProperty, domainLocale);
		this.messageSource = messageSource;
	}
	/**
	 * Instantiates a new generic object list model.
	 * 
	 * @param list the list
	 * @param displayProperty the display property
	 * @param storeProperty the store property
	 */
	public DefaultGenericObjectListModel(List<T> list, String displayProperty,
			String storeProperty) {
		this.list = list;
		this.displayProperty = displayProperty;
		this.storeProperty = storeProperty;
	}
	
	/**
	 * Instantiates a new generic object list model.
	 * 
	 * @param list the list
	 * @param displayProperty the display property
	 * @param storeProperty the store property
	 * @param domainLocale the domainLocale
	 */
	public DefaultGenericObjectListModel(List<T> list, String displayProperty,
			String storeProperty, DomainLocaleVariant domainLocale) {
		this.list = list;
		this.displayProperty = displayProperty;
		this.storeProperty = storeProperty;
		this.locale = domainLocale;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see nextapp.echo2.app.list.ListModel#get(int)
	 */
	public Object get(int row) {
		Object o = list.get(row);

		// Dot means return the object itself e.g useful for a string
		if (!".".equals(displayProperty)) {
			try {
				return getLocalizedDisplayValue(Ognl.getValue(displayProperty, o));
			} catch (OgnlException e) {
				logger.error("Couldn't get display property: "
						+ displayProperty, e);
			}
		}
		return getLocalizedDisplayValue(o);
	}
	
	/* (non-Javadoc)
	 * @see com.imilia.server.domain.model.GenericObjectListModel#getLocalizedDisplayValue(java.lang.Object)
	 */
	public Object getLocalizedDisplayValue(Object v){
		if (v instanceof Localized && locale != null && messageSource != null){
			// If is is a localized then get the localized message
			return ((Localized)v).getLocalizedMessage(messageSource, locale.getLocale());
		}else if (v instanceof LocalizedProperty && locale != null){
			return ((LocalizedProperty) v).getText(locale.getDomainLocale());
		}else{
			return v;
		}
	}

	/**
	 * Gets the object.
	 * 
	 * @param row the row
	 * 
	 * @return the object
	 */
	public T getObject(int row) {
		return (T) list.get(row);
	}

	/**
	 * Gets the store object.
	 * 
	 * @param row the row
	 * 
	 * @return the store object
	 */
	public Object getStoreObject(int row) {
		if (".".equals(storeProperty)) {
			return getObject(row);
		}

		final Object o = list.get(row);

		try {
			Object v = Ognl.getValue(storeProperty, o);
			return v;
		} catch (OgnlException e) {
			logger.error("Couldn't get store property: " + storeProperty, e);
		}
		return o;
	}

	/**
	 * Find object.
	 * 
	 * @param storeValue the store value
	 * 
	 * @return the t
	 */
	public T findObject(Object storeValue) {
		if (storeValue == null) {
			return null;
		}

		for (T t : list) {
			try {
				Object v = null;

				if (".".equals(storeProperty)){
					v = t;
				}else{
					v = Ognl.getValue(storeProperty, t);
				}
				if (v != null && v.equals(storeValue)) {
					return t;
				}
			} catch (OgnlException e) {
				logger.error("Couldn't get store property: "
						+ storeProperty, e);
			}
		}
		return null;
	}

	/**
	 * Find object index.
	 * 
	 * @param t the t
	 * 
	 * @return the int
	 */
	public int findObjectIndex(Object t) {
		return list.indexOf(t);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see nextapp.echo2.app.list.ListModel#size()
	 */
	public int size() {
		return list.size();
	}

	/**
	 * Gets the display property.
	 * 
	 * @return the display property
	 */
	public String getDisplayProperty() {
		return displayProperty;
	}

	/**
	 * Gets the store property.
	 * 
	 * @return the store property
	 */
	public String getStoreProperty() {
		return storeProperty;
	}

	/**
	 * Gets the list.
	 * 
	 * @return the list
	 */
	public List<T> getList() {
		return list;
	}

	/* (non-Javadoc)
	 * @see com.imilia.server.domain.model.GenericObjectListModel#getLocale()
	 */
	public final DomainLocaleVariant getLocale() {
		return locale;
	}

	/* (non-Javadoc)
	 * @see com.imilia.server.domain.model.GenericObjectListModel#setLocale(com.imilia.server.domain.i18n.DomainLocaleVariant)
	 */
	public final void setLocale(DomainLocaleVariant locale) {
		this.locale = locale;
	}

	/* (non-Javadoc)
	 * @see com.imilia.server.domain.model.GenericObjectListModel#getMessageSource()
	 */
	public final MessageSource getMessageSource() {
		return messageSource;
	}

	/* (non-Javadoc)
	 * @see com.imilia.server.domain.model.GenericObjectListModel#setMessageSource(org.springframework.context.MessageSource)
	 */
	public final void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	/* (non-Javadoc)
	 * @see com.imilia.server.domain.model.GenericObjectListModel#getDisplayObject(int)
	 */
	public Object getDisplayObject(int row) {
		Object displayObject = null;
		if (".".equals(displayProperty)) {
			displayObject =  getObject(row).toString();
		}else{
			final Object o = getObject(row);
	
			try {
				displayObject = Ognl.getValue(displayProperty, o);
			} catch (OgnlException e) {
				logger.error("Couldn't get display property: " + displayProperty, e);
			}
		}
		
		return displayObject;
	}

	public void setList(List<T> list) {
		this.list = list;
	}

	public void setDisplayProperty(String displayProperty) {
		this.displayProperty = displayProperty;
	}

	public void setStoreProperty(String storeProperty) {
		this.storeProperty = storeProperty;
	}
}
