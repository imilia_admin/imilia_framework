/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 *
 * Created on: May 12, 2009
 * Created by: mtemm
 */
package com.imilia.server.domain.model.password;

import com.imilia.server.domain.common.UserAccount;
import com.imilia.server.domain.model.annotations.Cmd;
import com.imilia.server.domain.model.annotations.Model;
import com.imilia.server.domain.model.annotations.Prp;
import com.imilia.server.domain.model.annotations.Evt;
import com.imilia.server.domain.model.event.impl.Event;
import com.imilia.server.domain.model.event.impl.EventSource;
import com.imilia.server.domain.model.impl.DefaultModel;
import com.imilia.server.domain.password.PasswordCandidate;
import com.imilia.server.service.AuthenticationService;
import com.imilia.server.service.impl.PasswordStrengthChecker;
import com.imilia.server.service.impl.PasswordStrengthChecker.Strength;
import com.imilia.server.validation.ValidationException;

/**
 * The Class PasswordModel.
 */
@Model(prps = { 
		@Prp("paswordPolicyText"), 
		@Prp("passwordExpired"), 
		@Prp("passwordCandidate.password1"), 
		@Prp("passwordCandidate.password2"),
		@Prp("strength"), 
		@Prp("passwordScoreStyle"), 
		@Prp("score") 
	},
	cmds={
		@Cmd(value="save"),
		@Cmd(value="close")
	},
	evts={
		@Evt(name="save")
	}	
)
public class PasswordModel extends DefaultModel {
	
	/** The user account. */
	private UserAccount userAccount;	
	
	/** The password candidate. */
	private PasswordCandidate passwordCandidate;
	
	private PasswordStrengthChecker passwordStrengthChecker;

	
	/** The authentication service. */
	private AuthenticationService authenticationService;
	
	/** The score. */
	private int score;
	
	/** The strength. */
	private Strength strength;
	
	/** The password score log. */
	private StringBuffer passwordStrengthLog = new StringBuffer();
	
	private EventSource<Event<PasswordModel>> saved = new EventSource<Event<PasswordModel>>();
	

	/**
	 * Saves the subscription.
	 * 
	 * @throws ValidationException the validation exception
	 */
	public void save() throws ValidationException {
		passwordCandidate.validate();
		userAccount.setPasswordNextChange(null);
		authenticationService.changePassword(userAccount, passwordCandidate, passwordStrengthChecker, getMessageSource());
	}
	
	/**
	 * Gets the user account.
	 * 
	 * @return the userAccount
	 */
	public UserAccount getUserAccount() {
		return userAccount;
	}


	/**
	 * Sets the user account.
	 * 
	 * @param userAccount the new user account
	 */
	public void setUserAccount(UserAccount userAccount) {
		this.userAccount = userAccount;
		passwordCandidate.setPassword1("");
		passwordCandidate.setPassword2("");
		this.score = 0;
		this.strength = Strength.Invalid;
		
	}

	/**
	 * Gets the password candidate.
	 * 
	 * @return the password candidate
	 */
	public PasswordCandidate getPasswordCandidate() {
		return passwordCandidate;
	}

	/**
	 * Sets the password candidate.
	 * 
	 * @param passwordCandidate the new password candidate
	 */
	public void setPasswordCandidate(PasswordCandidate passwordCandidate) {
		this.passwordCandidate = passwordCandidate;
	}

	/**
	 * Gets the authentication service.
	 * 
	 * @return the authentication service
	 */
	public AuthenticationService getAuthenticationService() {
		return authenticationService;
	}

	/**
	 * Sets the authentication service.
	 * 
	 * @param authenticationService the new authentication service
	 */
	public void setAuthenticationService(AuthenticationService authenticationService) {
		this.authenticationService = authenticationService;
	}
	
	/**
	 * Gets the pasword policy text.
	 * 
	 * @return the pasword policy text
	 */
	public String getPaswordPolicyText(){
		return getAppModel().getMessageSource().getMessage(
				"com.imilia.server.domain.password.PasswordCandidate.policy.text",
				new Object[]{
						passwordStrengthChecker.getMinStrength(),
						passwordStrengthChecker.getPointsForLengthGreater7(),
						passwordStrengthChecker.getPointsForLengthGreater15(),
						passwordStrengthChecker.getPointsForLowerCase(),
						passwordStrengthChecker.getPointsForUpperCase(),
						passwordStrengthChecker.getPointsForNumber(),
						passwordStrengthChecker.getPointsFor1Number(),
						passwordStrengthChecker.getPointsFor2Numbers(),
						passwordStrengthChecker.getPointsForSpecial(),
						passwordStrengthChecker.getPointsForUpperLower(),
						passwordStrengthChecker.getPointsForLetterNumbers(),
						passwordStrengthChecker.getPointsForLetterNumberSpecials(),
						passwordStrengthChecker.getPointsForUpperLowerLetterNumberSpecials()
				},
				getUserAccount().getPerson().getDomainLocale().getLocale());
	}
	

	/**
	 * Calculate score and strength.
	 *
	 * @return the int
	 */
	public int calculateScoreAndStrength(){
		passwordStrengthLog.setLength(0);
		score = passwordStrengthChecker.checkPasswordStrength(
				passwordCandidate.getPassword1(), passwordStrengthLog);
		
		strength = passwordStrengthChecker.toStrength(score);
		
		
		return score;
	}

	/**
	 * Gets the score.
	 * 
	 * @return the score
	 */
	public final int getScore() {
		return score;
	}

	/**
	 * Gets the password score log.
	 * 
	 * @return the password score log
	 */
	public final StringBuffer getPasswordStrengthLog() {
		return passwordStrengthLog;
	}

	/**
	 * Gets the strength.
	 *
	 * @return the strength
	 */
	public Strength getStrength() {
		return strength;
	}

	/**
	 * Sets the strength.
	 *
	 * @param strength the new strength
	 */
	public void setStrength(Strength strength) {
		this.strength = strength;
	}

	/**
	 * Sets the score.
	 *
	 * @param score the new score
	 */
	public void setScore(int score) {
		this.score = score;
	}
	
	public boolean isPasswordExpired(){
		return userAccount.isChangePasswordRequired();
	}

	public PasswordStrengthChecker getPasswordStrengthChecker() {
		return passwordStrengthChecker;
	}

	public void setPasswordStrengthChecker(
			PasswordStrengthChecker passwordStrengthChecker) {
		this.passwordStrengthChecker = passwordStrengthChecker;
	}

	public String getPasswordScoreStyle() {
		if (strength != null){
			return "Strength." + strength.name();
		}
		
		return null;
	}

	public String getScoreAsString(){
		return score > 0 ? Integer.toString(score) : "";
	}

	
	public EventSource<Event<PasswordModel>> getSaved() {
		return saved;
	}
}
