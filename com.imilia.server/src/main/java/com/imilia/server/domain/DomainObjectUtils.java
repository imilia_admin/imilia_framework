/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2008 - all rights reserved
 * Created on: Jan 23, 2009
 * Created by: emcgreal
 */
package com.imilia.server.domain;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.imilia.server.domain.filter.RestrictionCandidate;

// TODO: Auto-generated Javadoc
/**
 * The Class DomainObjectUtils.
 */
public final class DomainObjectUtils{

	/**
	 * A Domain Object can mark properties as a
	 * RestrictionCandidat using the @RestrictionCandidate
	 * annotation.
	 *
	 * @param clazz the clazz
	 *
	 * @return all properties that are restriction candidates
	 */
	public static List<Field> getRestrictionCandidates(Class<?> clazz) {
		// Build the hierarchy first as we want to
		// present a list starting with the most basic
		// restrictions e.g. OID
		ArrayList<Class<?>> hierarchy = new ArrayList<Class<?>>();
		Class<?> cl = clazz;
		while (cl != null){
			hierarchy.add(0, cl);
			cl = cl.getSuperclass();
		}

		// Look for all fields with @ResttrictionCandidate
		ArrayList<Field> candidates = new ArrayList<Field>();
		for (Class<?> c:hierarchy){
			final Field[] fields = c.getDeclaredFields();
			if (fields != null){
				for(Field f:fields){
					if (f.isAnnotationPresent(RestrictionCandidate.class)){
						candidates.add(f);
					}
				}
			}
		}
		return candidates;
	}

	/**
	 * Find field.
	 *
	 * @param clazz the clazz
	 * @param fieldName the field name
	 *
	 * @return the field
	 */
	public static Field findField(Class<?> clazz, String fieldName){
		while (clazz != null){
			final Field[] fields = clazz.getDeclaredFields();
			if (fields != null){
				for(Field f:fields){
					if (f.getName().equals(fieldName)){
						return f;
					}
				}
			}
		}
		return null;
	}
	
	/**
	 * Convert oid list.
	 *
	 * @param domainObjects the domain objects
	 * @return the list
	 */
	public static List<Long> convertOIDList(Collection<? extends DomainObject> domainObjects){
		final ArrayList<Long> oids = new ArrayList<Long>();
		
		for (DomainObject d:domainObjects){
			oids.add(d.getOid());
		}
		return oids;
	}
}