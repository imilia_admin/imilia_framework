/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2014 - all rights reserved
 * Created on: Feb 27, 2014
 * Created by: emcgreal
 */

package com.imilia.server.domain.query;

import java.util.List;

import com.imilia.utils.observer.ObservableArrayList;

/**
 * The Class QueryResult.
 *
 * @param <T> the generic type
 */
public class QueryResult<T> extends	ObservableArrayList<T> {

	/** The limit exceeded. */
	private boolean limitExceeded;

	/**
	 * Instantiates a new query result.
	 */
	public QueryResult() {
		super();
	}

	/**
	 * Instantiates a new query result.
	 *
	 * @param result the result
	 * @param limitExceeded the limit exceeded
	 */
	public QueryResult(List<T> result, boolean limitExceeded) {
		super();
		addAll(result);
		this.limitExceeded = limitExceeded;
	}

	/**
	 * Checks if is limit exceeded.
	 *
	 * @return true, if is limit exceeded
	 */
	public boolean isLimitExceeded() {
		return limitExceeded;
	}
	
	/**
	 * Sets the result.
	 *
	 * @param result the result
	 * @param limitExceeded the limit exceeded
	 */
	public void setResult(List<T> result, boolean limitExceeded) {
		clear();
		addAll(result);
		this.limitExceeded = limitExceeded;
	}

	/**
	 * Sets the limit exceeded.
	 *
	 * @param limitExceeded the new limit exceeded
	 */
	public void setLimitExceeded(boolean limitExceeded) {
		this.limitExceeded = limitExceeded;
	}

	/* (non-Javadoc)
	 * @see com.imilia.utils.observer.ObservableArrayList#isNotEmpty()
	 */
	public boolean isNotEmpty(){
		return !isEmpty();
	}
}
