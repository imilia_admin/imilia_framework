/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 *
 * Created on: Jun 8, 2009
 * Created by: emcgreal
 */
package com.imilia.server.domain.common;

import org.springframework.core.io.InputStreamSource;

/**
 * The Interface EmailAttachment.
 */
public interface EmailAttachment {

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName();

	/**
	 * Gets the input source.
	 *
	 * @return the contents
	 */
	public InputStreamSource getInputStreamSource();
}
