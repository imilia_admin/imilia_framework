package com.imilia.server.domain.model.event.impl;

import java.util.HashSet;
import java.util.Set;

import com.imilia.server.domain.model.event.IEvent;
import com.imilia.server.domain.model.event.IEventSource;

public class EventSource<T extends IEvent> implements IEventSource<T>{

	private String name;
	
	private final Set<Listener<T>> listeners = new HashSet<Listener<T>>();
	
	public EventSource(){
	}

	public void register(Listener<T> l) {
		listeners.add(l);
	}

	public void unregister(Listener<T> l) {
		listeners.remove(l);
	}

	public void clearListeners() {
		listeners.clear();
	}

	public void fire(T event) {
		for (Listener<T> l:listeners){
			l.fired(event);
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
