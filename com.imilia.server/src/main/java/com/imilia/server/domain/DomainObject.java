/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 * Created on: Jan 23, 2009
 * Created by: emcgreal
 */

package com.imilia.server.domain;

import javax.persistence.Basic;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.joda.time.DateTime;

import com.imilia.server.domain.common.UserAccount;
import com.imilia.server.domain.enums.ObjectStatus;
import com.imilia.server.domain.filter.RestrictionCandidate;

/**
 * The Class DomainObject servers as the base class of all domain objects.
 * 
 * <p>
 * The domain object class is the root class of all entities in an Imilia based
 * system.
 * </p>
 */
@MappedSuperclass
public abstract class DomainObject implements ObjectIdentity, CrudAware {

	/** The object ID. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@RestrictionCandidate(advanced=true)
	private long oid;

	/** The version. */
	@Version
	private int version;

	/** The created on. */
	@Basic(optional = false)
	@Temporal(value = TemporalType.TIMESTAMP)
	@RestrictionCandidate(advanced=true)
	private DateTime createdOn;

	/** The created by. */
	@ManyToOne(optional = false)
	@RestrictionCandidate(advanced=true, path="createdBy.username")
	private UserAccount createdBy;

	/** The changed on. */
	@Basic(optional = false)
	@Temporal(value = TemporalType.TIMESTAMP)
	@RestrictionCandidate(advanced=true)
	private DateTime changedOn;

	/** The changed by. */
	@ManyToOne(optional = false)
	@RestrictionCandidate(advanced=true, path="changedBy.username")
	private UserAccount changedBy;

	/** The object status. */
	@RestrictionCandidate(advanced=true)
	@Enumerated(value = EnumType.ORDINAL)
	private ObjectStatus objectStatus = ObjectStatus.Active;

	// Transient properties ---------------------------------------------------
	/** The crud. */
	@Transient
	private Crud crud;

	@Transient
	private boolean ignoreAuditor;
	// -------------------------------------------------------------------------

	/**
	 * Instantiates a new domain object.
	 */
	public DomainObject() {
	}

	// Object Identity Interface ----------------------------------------------

	/**
	 * Gets the oid.
	 * 
	 * @return the oid
	 */
	public long getOid() {
		return oid;
	}

	/**
	 * Sets the oid.
	 * 
	 * @param oid the new oid
	 */
	public void setOid(long oid) {
		this.oid = oid;
	}

	/**
	 * Gets the changed by.
	 * 
	 * @return the changed by
	 */
	public UserAccount getChangedBy() {
		return changedBy;
	}

	/**
	 * Sets the changed by.
	 * 
	 * @param changedBy the new changed by
	 */
	public void setChangedBy(UserAccount changedBy) {
		this.changedBy = changedBy;
	}

	/**
	 * Gets the changed on.
	 * 
	 * @return the changed on
	 */
	public DateTime getChangedOn() {
		return changedOn;
	}

	/**
	 * Sets the changed on.
	 * 
	 * @param changedOn the new changed on
	 */
	public void setChangedOn(DateTime changedOn) {
		this.changedOn = changedOn;
	}

	/**
	 * Gets the created by.
	 * 
	 * @return the created by
	 */
	public UserAccount getCreatedBy() {
		return createdBy;
	}

	/**
	 * Sets the created by.
	 * 
	 * @param createdBy the new created by
	 */
	public void setCreatedBy(UserAccount createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * Gets the created on.
	 * 
	 * @return the created on
	 */
	public DateTime getCreatedOn() {
		return createdOn;
	}

	/**
	 * Sets the created on.
	 * 
	 * @param createdOn the new created on
	 */
	public void setCreatedOn(DateTime createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * Gets the version.
	 * 
	 * @return the version
	 */
	public int getVersion() {
		return version;
	}

	/**
	 * Sets the version.
	 * 
	 * @param version the version to set
	 */
	public void setVersion(int version) {
		this.version = version;
	}

	/**
	 * Sets the version.
	 * 
	 * @param version the new version
	 */
	public void setVersion(Integer version) {
		this.version = version;
	}

	/**
	 * Gets the object status.
	 * 
	 * @return the object status
	 */
	public ObjectStatus getObjectStatus() {
		return objectStatus;
	}

	/**
	 * Sets the object status.
	 * 
	 * @param objectStatus the new object status
	 */
	public void setObjectStatus(ObjectStatus objectStatus) {
		this.objectStatus = objectStatus;
	}


	/**
	 * Checks if is new.
	 * 
	 * @return true, if is new
	 */
	public boolean isNew() {
		return getOid() == 0L;
	}

	/**
	 * Checks if is stored.
	 * 
	 * @return true, if is stored
	 */
	public boolean isStored() {
		return !isNew();
	}

	/**
	 * Checks if is deleted.
	 * 
	 * @return true, if is deleted
	 */
	public boolean isDeleted(){
		return ObjectStatus.Deleted.equals(getObjectStatus());
	}
	
	/**
	 * Checks if is active.
	 * 
	 * @return true, if is deleted
	 */
	public boolean isActive(){
		return ObjectStatus.Active.equals(getObjectStatus());
	}
	
	public boolean isDisabled(){
		return ObjectStatus.Disabled.equals(getObjectStatus());
	}
	
	
	/**
	 * Touch change data.
	 * 
	 * @param userAccount the user account
	 */
	public void touchChangeData(UserAccount userAccount) {
		setChangedBy(userAccount);
		setChangedOn(new DateTime());
	}

	/**
	 * Touch create data.
	 * 
	 * @param userAccount the user account
	 */
	public void touchCreateData(UserAccount userAccount) {
		setCreatedBy(userAccount);
		setCreatedOn(new DateTime());
		setObjectStatus(ObjectStatus.Active);
	}

	/**
	 * Dump.
	 * 
	 * @return the string
	 */
	public String dump() {
		StringBuffer buffer = new StringBuffer();
		buffer.append(toString());
		buffer.append("{");
		buffer.append(getOid());
		buffer.append("}");
		buffer.append(" isNew=");
		buffer.append(isNew());
		return buffer.toString();
	}

	/**
	 * Gets the crud.
	 * 
	 * @return the crud
	 */
	public Crud getCrud() {
		return crud;
	}

	/**
	 * Sets the crud.
	 * 
	 * @param crud the crud to set
	 */
	public void setCrud(Crud crud) {
		this.crud = crud;
	}
	
	/**
	 * Gets the audit summary.
	 * 
	 * @return the audit summary
	 */
	public String getAuditSummary() {
		StringBuffer buffer = new StringBuffer();
		buffer.append(" Object: " + getClass().getCanonicalName());
		buffer.append(" OID: " + getOid());
		
		if ( getCreatedBy() != null){
			buffer.append(" Created By: " + getCreatedBy().getUsername());
		}
		buffer.append(" Created On: " + getCreatedOn());
		
		if ( getChangedBy() != null){
			buffer.append(" Changed By: " + getChangedBy().getUsername());
		}
		
		buffer.append(" Changed On: " + getChangedOn());

		return buffer.toString();
	}

	public final boolean isIgnoreAuditor() {
		return ignoreAuditor;
	}

	public final void setIgnoreAuditor(boolean ignoreAuditor) {
		this.ignoreAuditor = ignoreAuditor;
	}

	@Override
	public boolean equals(Object other) {
		if (this == other){
			return true;
		}

		// Make sure other is not null and has the same class as this
		if (other != null && getClass().equals(other.getClass())){
			final DomainObject domainObject = (DomainObject)other; 
			if ( oid == domainObject.oid && version == domainObject.version){
				if (oid == 0){
					return false;
				}else{
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public int hashCode() {
		if (oid != 0){
			long l = 37 * version;
			return (int)l +(int)( oid ^ (oid >>> 32) );
		}
		return super.hashCode();
	}


}
