package com.imilia.server.domain.model.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface Grp {
	/**
	 * Name of model
	 *
	 * @return the string
	 */
	String name();
	
	String path() default "";
	
	String attributes() default "";
	String cruds() default "";
	
	/**
	 * Prps. Properties exposed by the model
	 *
	 * @return the prp[]
	 */
	Prp[] prps() default {};
	
	String[] prpNames() default {};
}
