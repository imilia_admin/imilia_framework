package com.imilia.server.domain.query.execution;

public enum Comparison {
	Like,
	LikeCaseSensitive,
	Equals,
	NotEquals,
	LessThan,
	LessThanEquals,
	GreaterThan,
	GreaterThanEquals,
	Between,
	In, 
	IsNull,
	NotNull,
	Any;	// e.g. for booleans true, false or not set
	
	public String getLabel(){
		switch(this){
		case Like:
			return "*";
		
		case LikeCaseSensitive:
			return "Aa*";
			
		case Equals:
			return "=";
		
		case NotEquals:
			return "!>";
			
		case LessThan:
			return "<";
			
		case LessThanEquals:
			return "<=";
			
		case GreaterThan:
			return ">";
			
		case GreaterThanEquals:
			return ">=";
			
		case Between:
			return "|><|";
			
		case In:
			return "|...|";

		case IsNull:
			return "Null";

		case NotNull:
			return "!Null";
			
		default:
			return name();
		}
	}
	
	public String toPredicate(String property, String paramMarker){
		switch(this){
		case Like:
			return property + " like " + paramMarker;
		case LikeCaseSensitive:
			return "UPPER("+ property + ") like UPPER(" + paramMarker +")";
		case Equals:
			return property + " = " + paramMarker;
		
		case NotEquals:
			return property + " != " + paramMarker;
			
		case LessThan:
			return property + " < " + paramMarker;
			
		case LessThanEquals:
			return property + " <= " + paramMarker;
			
		case GreaterThan:
			return property + " > " + paramMarker;
			
		case GreaterThanEquals:
			return property + " >= " + paramMarker;
			
		case Between:
			return property + " between(" + paramMarker + ")";
			
		case In:
			return property + " in (" + paramMarker + ")";
			
		case IsNull:
			return "property is null";

		case NotNull:
			return "property is not null";

			default:
				return null;
		}
	}
}
