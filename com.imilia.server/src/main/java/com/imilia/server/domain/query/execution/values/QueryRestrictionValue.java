package com.imilia.server.domain.query.execution.values;

import com.imilia.server.domain.query.entities.QueryRestriction;
import com.imilia.server.domain.query.execution.LogicalOperator;
import com.imilia.server.domain.query.execution.QueryCrudExecution;
import com.imilia.utils.strings.StringUtils;

public class QueryRestrictionValue {
	private final QueryRestriction queryRestriction;

	public QueryRestrictionValue(QueryRestriction queryRestriction) {
		super();
		this.queryRestriction = queryRestriction;
	}

	public QueryRestriction getQueryRestriction() {
		return queryRestriction;
	}
	
	public void addToExecution(QueryCrudExecution<?> execution) {
		if (queryRestriction.getRestriction() != null){
			String s = 
					StringUtils.replaceVariables(queryRestriction.getRestriction(), execution);
			
			if (s != null){
				execution.addRestrictionValue(s, LogicalOperator.And);
			}
		}
	}

}
