package com.imilia.server.domain.query.entities;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;

import com.imilia.server.domain.DomainObject;
import com.imilia.server.domain.access.Visibility;

@Entity
public class QueryJoin extends DomainObject{
	private String path;
	
	/** The visibility. */
	@Enumerated(EnumType.ORDINAL)
	private Visibility visibility;
	
	@ManyToOne
	private QueryView queryView;

	public String getPath() {
		return path;
	}

	public void setPath(String joinPath) {
		this.path = joinPath;
	}

	public QueryView getQueryView() {
		return queryView;
	}

	public void setQueryView(QueryView queryView) {
		this.queryView = queryView;
	}

	public Visibility getVisibility() {
		return visibility;
	}

	public void setVisibility(Visibility visibility) {
		this.visibility = visibility;
	}
}
