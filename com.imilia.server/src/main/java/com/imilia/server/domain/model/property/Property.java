/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2014 - all rights reserved
 *
 * Created on: Jan 22, 2014
 * Created by: emcgreal
 */
package com.imilia.server.domain.model.property;

import com.imilia.server.domain.model.IModel;

/**
 * The Interface IProperty defines a property exposed by a model.
 */
public interface Property {
	
	/**
	 * Gets the model.
	 *
	 * @return the model
	 */
	public IModel getModel();
	
	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName();
	
	/**
	 * Gets the group.
	 *
	 * @return the group
	 */
	public String getGroup();

	/**
	 * Gets the qualified name.
	 *
	 * @return the qualified name
	 */
	public String getQName(); 

	/**
	 * Gets the path.
	 *
	 * @return the path
	 */
	public String getPath();
	
	
	/**
	 * Gets the crud provider.
	 *
	 * @return the crud provider
	 */
	public PropertyCrudProvider getPropertyCrudProvider();
	
	/**
	 * Gets the attributes provider.
	 *
	 * @return the attributes provider
	 */
	public PropertyAttributeProvider getAttributesProvider();
}
