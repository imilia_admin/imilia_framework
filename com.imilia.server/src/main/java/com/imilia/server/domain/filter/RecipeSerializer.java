/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2010 - all rights reserved
 * Created on: Oct 6, 2010
 * Created by: emcgreal
 */
package com.imilia.server.domain.filter;

import java.io.StringReader;
import java.util.List;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.XMLEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.joda.time.format.ISODateTimeFormat;

import com.imilia.server.xml.DefaultStaxHandler;
import com.imilia.server.xml.DefaultStaxObjectFactory;
import com.imilia.server.xml.StaxHandler;
import com.imilia.server.xml.StaxHandlerFactory;
import com.imilia.server.xml.StaxObjectFactory;
import com.imilia.utils.i18n.DomainLocale;

/**
 * The Class RecipeSerializer.
 */
public class RecipeSerializer {
	// The logger
	/** The Constant logger. */
	private static final Logger logger = LoggerFactory.getLogger(RecipeSerializer.class);
	
	/** The buffer. */
	private final StringBuffer buffer = new StringBuffer();
	
	/** The property filter visitor. */
	private PropertyFilterVisitor<Object> propertyFilterVisitor;
	
	/**
	 * Instantiates a new recipe serializer.
	 */
	public RecipeSerializer(){
		createPropertyFilterVisitor();
	}
	
	/**
	 * Open tag.
	 * 
	 * @param t the t
	 */
	private void openTag(String t){
		buffer.append("<");
		buffer.append(t);
	}
	
	/**
	 * Open tag close.
	 * 
	 * @param bodyFollowing the body following
	 */
	private void openTagClose(boolean bodyFollowing){
		if (bodyFollowing){
			buffer.append(">\n");
		}else{
			buffer.append("/>\n");
		}
	}
	
	/**
	 * Close tag.
	 * 
	 * @param t the t
	 */
	private void closeTag(String t){
		buffer.append("</");
		buffer.append(t);
		buffer.append(">\n");
	}
	
	/**
	 * Adds the attribute.
	 * 
	 * @param name the name
	 * @param value the value
	 */
	private void addAttribute(String name, String value){
		if (value != null){
			buffer.append(" ");
			buffer.append(name);
			buffer.append("=\"");
			buffer.append(value);
			buffer.append("\"");
		}
	}
	
	/**
	 * Serialize.
	 * 
	 * @param recipe the recipe
	 */
	public void serialize(Recipe<?> recipe){
		openTag(Recipe.TAG_RECIPE);
		openTagClose(true);
		
		openTag(Recipe.TAG_CRUDFILTERS);
		openTagClose(true);
		
		for (CrudFilter<?> crudFilter:recipe.getCrudFilters()){
			serialize(crudFilter);
		}
		closeTag(Recipe.TAG_CRUDFILTERS);
		closeTag(Recipe.TAG_RECIPE);
	}

	/**
	 * Serialize.
	 * 
	 * @param crudFilter the crud filter
	 */
	private void serialize(CrudFilter<?> crudFilter){
		openTag(CrudFilter.TAG_FILTER);
		
		serializeAttributes(crudFilter);
		addAttribute(CrudFilter.TAG_CRUD, Long.toString(crudFilter.getCruds()));
		
		openTagClose(true);
		
		serializeProperties(crudFilter.getPropertyFilters());

		
		serialize(crudFilter.getClassJoins());
		
		closeTag(CrudFilter.TAG_FILTER);
	}
	
	/**
	 * Serialize.
	 * 
	 * @param joins the joins
	 */
	private void serialize(List<ClassJoin> joins){
		if (!joins.isEmpty()){
			openTag(ClassFilter.TAG_CLASS_JOINS);
			openTagClose(true);
			for (ClassJoin j:joins){
				serialize(j);
			}
			closeTag(ClassFilter.TAG_CLASS_JOINS);
		}
	}
	
	/**
	 * Serialize.
	 * 
	 * @param j the j
	 */
	private void serialize(ClassJoin j){
		openTag(ClassJoin.TAG_FILTER);
		addAttribute(ClassJoin.TAG_JOIN_PROPERTY, j.getJoinProperty());
		openTagClose(true);
		
		serialize(j.getClassFilter());
		
		closeTag(ClassJoin.TAG_FILTER);
	}
	
	/**
	 * Serialize attributes.
	 * 
	 * @param classFilter the class filter
	 */
	private void serializeAttributes(ClassFilter<?> classFilter){
		addAttribute(ClassFilter.TAG_CLAZZ, classFilter.getDomainClassName());
		addAttribute(ClassFilter.TAG_PATH, classFilter.getPath());
		addAttribute(ClassFilter.TAG_FLAGS, Long.toString(classFilter.getFlags()));
		addAttribute(ClassFilter.TAG_RESTRICTION, classFilter.getRestriction());		
	}
		
	/**
	 * Serialize.
	 * 
	 * @param classFilter the class filter
	 */
	private void serialize(ClassFilter<?> classFilter){
		openTag(ClassFilter.TAG_FILTER);
		serializeAttributes(classFilter);
		openTagClose(true);
		
		serializeProperties(classFilter.getPropertyFilters());

		serialize(classFilter.getClassJoins());

		closeTag(ClassFilter.TAG_FILTER);
	}
	
	/**
	 * Serialize properties.
	 * 
	 * @param propertyFilters the property filters
	 */
	private void serializeProperties(List<PropertyFilter> propertyFilters){
		if (!propertyFilters.isEmpty()){
			openTag(PropertyFilter.TAG_PROPERTY_FILTERS);
			openTagClose(true);
			
			for (PropertyFilter propertyFilter:propertyFilters){
				propertyFilter.visit(propertyFilterVisitor);
			}
			
			closeTag(PropertyFilter.TAG_PROPERTY_FILTERS);
		}
	}
	
	/**
	 * Gets the buffer.
	 * 
	 * @return the buffer
	 */
	public final StringBuffer getBuffer() {
		return buffer;
	}
	
	/**
	 * Gets the property filter visitor.
	 * 
	 * @return the property filter visitor
	 */
	private void createPropertyFilterVisitor(){
		propertyFilterVisitor = new PropertyFilterVisitor<Object>(){
			
			private void serialize(String name, PropertyFilter filter){
				openTag(name);
				addAttribute(PropertyFilter.TAG_PROPERTY_NAME, filter.getPropertyName());
				addAttribute(PropertyFilter.TAG_PROPERTY_MANDATORY, filter.isMandatory() ? Boolean.TRUE.toString() : Boolean.FALSE.toString());
				addAttribute(PropertyFilter.TAG_PROPERTY_ADVANCED, filter.isAdvanced() ? Boolean.TRUE.toString() : Boolean.FALSE.toString());
				
			}

			public Object accept(BitMaskPropertyFilter filter) {
				serialize(BitMaskPropertyFilter.TAG_BITMASK_FILTER, filter);
				addAttribute(BitMaskPropertyFilter.TAG_FILTERS, filter.getFiltersAsString());
				addAttribute(BitMaskPropertyFilter.TAG_BITMASK_OPERATOR, filter.getBitMaskOperator().name());
				openTagClose(false);
				return null;
			}

			public Object accept(DatePropertyFilter filter) {
				serialize(DatePropertyFilter.TAG_FILTER, filter);
				if (filter.getLowerBound() != null){
					addAttribute(DatePropertyFilter.TAG_LOWERBOUND,  
							filter.getLowerBound().toString(ISODateTimeFormat.basicDate()));
				}
				if (filter.getUpperBound() != null){
					addAttribute(DatePropertyFilter.TAG_UPPERBOUND,  
							filter.getUpperBound().toString(ISODateTimeFormat.basicDate()));
				}
				openTagClose(false);
				return null;
			}

			public Object accept(DateTimePropertyFilter filter) {
				serialize(DateTimePropertyFilter.TAG_FILTER, filter);
				if (filter.getLowerBound() != null){
					addAttribute(DateTimePropertyFilter.TAG_LOWERBOUND,  
							filter.getLowerBound().toString(ISODateTimeFormat.basicDateTime()));
				}
				if (filter.getUpperBound() != null){
					addAttribute(DateTimePropertyFilter.TAG_UPPERBOUND,  
							filter.getUpperBound().toString(ISODateTimeFormat.basicDateTime()));
				}
				openTagClose(false);
				return null;
			}

			public Object accept(DoublePropertyFilter filter) {
				serialize(DoublePropertyFilter.TAG_FILTER, filter);
				addAttribute(DoublePropertyFilter.TAG_PREDICATES, filter.getDoublePredicates());
				openTagClose(false);
				return null;
			}

			public Object accept(EnumPropertyFilter filter) {
				serialize(EnumPropertyFilter.TAG_FILTER, filter);
				
				if (filter.isValid()){
					addAttribute(EnumPropertyFilter.TAG_ORDINALS, filter.getOrdinalsAsString());
				}
				addAttribute(EnumPropertyFilter.TAG_ENUM_CLASS, filter.getClazz().getName());
				
				openTagClose(false);
				return null;
			}

			public Object accept(IntegerPropertyFilter filter) {
				serialize(IntegerPropertyFilter.TAG_FILTER, filter);
				if (filter.isValid()){
					addAttribute(IntegerPropertyFilter.TAG_PREDICATES, filter.getIntPredicates());
				}
				openTagClose(false);
				return null;
			}

			public Object accept(LocaleTextFilter filter) {
				serialize(LocaleTextFilter.TAG_FILTER, filter);
				if (filter.isValid()){
					addAttribute(LocaleTextFilter.TAG_PREDICATES, filter.getStringPredicates());
				}
				openTagClose(false);
				return null;
			}

			public Object accept(LongPropertyFilter filter) {
				serialize(LongPropertyFilter.TAG_FILTER, filter);
				if (filter.isValid()){
					addAttribute(LongPropertyFilter.TAG_PREDICATES, filter.getLongPredicates());
				}
				openTagClose(false);
				return null;
			}

			public Object accept(OIDPropertyFilter filter) {
				serialize(OIDPropertyFilter.TAG_FILTER, filter);
				if (filter.isValid()){
					addAttribute(OIDPropertyFilter.TAG_PREDICATES, Long.toString(filter.getOid()));
				}
				openTagClose(false);
				return null;
			}

			public Object accept(StringListPropertyFilter filter) {
				serialize(StringListPropertyFilter.TAG_FILTER, filter);
				if (filter.isValid()){
					addAttribute(StringListPropertyFilter.TAG_FILTERS, filter.getFiltersAsString());
				}
				openTagClose(false);
				return null;
			}

			public Object accept(StringPropertyFilter filter) {
				serialize(StringPropertyFilter.TAG_FILTER, filter);
				if (filter.isValid()){
					addAttribute(StringPropertyFilter.TAG_PREDICATES, filter.getStringPredicates());
				}
				openTagClose(false);
				return null;
			}

			@Override
			public Object accept(FlagListPropertyFilter filter) {
				serialize(FlagListPropertyFilter.TAG_FLAGLIST_FILTER, filter);
				addAttribute(FlagListPropertyFilter.TAG_FILTERS, filter.getFiltersAsString());
				openTagClose(false);
				return null;
			}

			public Object accept(BooleanPropertyFilter filter) {
				serialize(BooleanPropertyFilter.TAG_FILTER, filter);
				if (filter.isValid()){
					addAttribute(BooleanPropertyFilter.TAG_FILTERS, filter.isBooleanValue() ? Boolean.TRUE.toString() : Boolean.FALSE.toString());
				}
				openTagClose(false);
				return null;
			}
			
			@Override
			public Object accept(EntityRefPropertyFilter entityRefPropertyFilter) {
				serialize(EntityRefPropertyFilter.TAG_FILTER, entityRefPropertyFilter);
				if (entityRefPropertyFilter.isValid()){
					addAttribute(BooleanPropertyFilter.TAG_FILTERS, entityRefPropertyFilter.isBooleanValue() ? Boolean.TRUE.toString() : Boolean.FALSE.toString());
				}
				openTagClose(false);
				return null;
			}};
	}
	
	/**
	 * Deserialize.
	 * 
	 * @param recipe the recipe
	 * @param str the str
	 */
	public <T> void deserialize(final Recipe<T> recipe, String str){
		recipe.getCrudFilters().clear();
		
		try {
			XMLStreamReader xmlStreamReader =
			        XMLInputFactory.newInstance().
			            createXMLStreamReader(new StringReader(str));
			
			StaxHandlerFactory factory = new StaxHandlerFactory(){

				public StaxHandler<?> create(QName elementName, XMLStreamReader xmlStreamReader, StaxObjectFactory objectFactory) {
					final String className = elementName.getLocalPart();
					if (Recipe.TAG_RECIPE.equals(className)){
						return new DefaultStaxHandler<Recipe<?>>(this, recipe, objectFactory);
					}else if (CrudFilter.TAG_FILTER.equals(className)){
						return new DefaultStaxHandler<CrudFilter<T>>(this, new CrudFilter<T>(), objectFactory);
					}else if (ClassFilter.TAG_FILTER.equals(className)){
						return new DefaultStaxHandler<ClassFilter<T>>(this, new ClassFilter<T>(), objectFactory);
					}else if (ClassJoin.TAG_FILTER.equals(className)){
						return new DefaultStaxHandler<ClassJoin>(this, new ClassJoin(), objectFactory);
					}else if (BooleanPropertyFilter.TAG_FILTER.equals(className)){
						return new DefaultStaxHandler<BooleanPropertyFilter>(this, new BooleanPropertyFilter(), objectFactory);
					}else if (EntityRefPropertyFilter.TAG_FILTER.equals(className)){
						return new DefaultStaxHandler<EntityRefPropertyFilter>(this, new EntityRefPropertyFilter(), objectFactory);
					}else if (BitMaskPropertyFilter.TAG_BITMASK_FILTER.equals(className)){
						return new DefaultStaxHandler<BitMaskPropertyFilter>(this, new BitMaskPropertyFilter(), objectFactory);
					}else if (DatePropertyFilter.TAG_FILTER.equals(className)){
						return new DefaultStaxHandler<DatePropertyFilter>(this, new DatePropertyFilter(), objectFactory);
					}else if (DateTimePropertyFilter.TAG_FILTER.equals(className)){
						return new DefaultStaxHandler<DateTimePropertyFilter>(this, new DateTimePropertyFilter(), objectFactory);
					}else if (DoublePropertyFilter.TAG_FILTER.equals(className)){
						return new DefaultStaxHandler<DoublePropertyFilter>(this, new DoublePropertyFilter(), objectFactory);
					}else if (EnumPropertyFilter.TAG_FILTER.equals(className)){
						return new DefaultStaxHandler<EnumPropertyFilter>(this, new EnumPropertyFilter(), objectFactory);
					}else if (IntegerPropertyFilter.TAG_FILTER.equals(className)){
						return new DefaultStaxHandler<IntegerPropertyFilter>(this, new IntegerPropertyFilter(), objectFactory);
					}else if (LocaleTextFilter.TAG_FILTER.equals(className)){
						return new DefaultStaxHandler<LocaleTextFilter>(this, new LocaleTextFilter(), objectFactory);
					}else if (LongPropertyFilter.TAG_FILTER.equals(className)){
						return new DefaultStaxHandler<LongPropertyFilter>(this, new LongPropertyFilter(), objectFactory);
					}else if (OIDPropertyFilter.TAG_FILTER.equals(className)){
						return new DefaultStaxHandler<OIDPropertyFilter>(this, new OIDPropertyFilter(), objectFactory);
					}else if (StringListPropertyFilter.TAG_FILTER.equals(className)){
						return new DefaultStaxHandler<StringListPropertyFilter>(this, new StringListPropertyFilter(), objectFactory);
					}else if (StringPropertyFilter.TAG_FILTER.equals(className)){
						return new DefaultStaxHandler<StringPropertyFilter>(this, new StringPropertyFilter(), objectFactory);
					}else
						logger.debug("No handler found for: " +  className);
						return null;
					}

				@Override
				public void register(String elementName, Class<?> clazz) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void register(Class<?>... clazzs) {
					// TODO Auto-generated method stub
					
				}

				
				};
			
			while (xmlStreamReader.hasNext()){
				switch(xmlStreamReader.next()){
				case XMLEvent.START_ELEMENT:
					QName qn = xmlStreamReader.getName();
					
					final DefaultStaxObjectFactory objectFactory = new DefaultStaxObjectFactory(DomainLocale.en_US);
					
					final StaxHandler<?> handler = factory.create(qn, xmlStreamReader, objectFactory);
					
					if (handler != null){
						handler.handle(xmlStreamReader);
					}
				}
			}
			
			for (CrudFilter<?> f: recipe.getCrudFilters()){
				f.setRecipe(recipe);
				fixReferences(recipe, f);
			}
		} catch (Exception ex){
			logger.error("Serialization failed", ex);
		}	
	}
	
	/**
	 * Fix references.
	 * 
	 * @param r the r
	 * @param filter the filter
	 */
	private void fixReferences(Recipe<?> r, ClassFilter<?> filter){
		for (PropertyFilter p:filter.getPropertyFilters()){
			p.setClassFilter(filter);
		}
		
		for (ClassJoin j:filter.getClassJoins()){
			j.setParent(filter);
			fixReferences(r, j.getClassFilter());
		}
	}
	
}
