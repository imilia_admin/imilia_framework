/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 * 
 * Created on: Jul 22, 2009
 * Created by: emcgreal
 */
package com.imilia.server.domain.filter;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.imilia.server.validation.ValidationException;

/**
 * The Class EnumPropertyFilter.
 */
public class EnumPropertyFilter extends PropertyFilter {
	
	/** The Constant TAG_FILTER. */
	public final static String TAG_FILTER = "enumPropertyFilter";
	
	/** The Constant TAG_ORDINALS. */
	public final static String TAG_ORDINALS = "ordinals";
	
	/** The Constant TAG_ENUM_CLASS. */
	public final static String TAG_ENUM_CLASS = "clazz";

	
	/** The Constant logger. */
	private final static Logger logger = LoggerFactory.getLogger(EnumPropertyFilter.class);
	
	/** The ordinals. */
	private List<Enum<?>> enums;
	
	/** The clazz. */
	private Class<? extends Enum<?>> clazz;

	/**
	 * Instantiates a new enum property filter.
	 */
	public EnumPropertyFilter() {
		super();
	}

	/**
	 * Instantiates a new enum property filter.
	 *
	 * @param other the other
	 */
	public EnumPropertyFilter(EnumPropertyFilter other) {
		super(other);
		setEnums(other.getEnums());
		setClazz(other.getClazz());
	}

	/**
	 * Instantiates a new enum property filter.
	 * 
	 * @param propertyName the property name
	 */
	public EnumPropertyFilter(String propertyName) {
		super(propertyName);
	}

	/**
	 * Instantiates a new enum property filter.
	 *
	 * @param classFilter the class filter
	 * @param propertyName the property name
	 */
	public EnumPropertyFilter(ClassFilter<?> classFilter, String propertyName, Class<? extends Enum<?>> clazz) {
		this(classFilter, propertyName, clazz, null);
	}
	
	/**
	 * Instantiates a new enum property filter.
	 *
	 * @param classFilter the class filter
	 * @param propertyName the property name
	 * @param clazz the clazz
	 * @param flags the flags
	 */
	public EnumPropertyFilter(ClassFilter<?> classFilter, String propertyName, Class<? extends Enum<?>> clazz, long flags) {
		this(classFilter, propertyName, clazz, flags, null);
	}

	/**
	 * Instantiates a new enum property filter.
	 *
	 * @param classFilter the class filter
	 * @param propertyName the property name
	 * @param clazz the clazz
	 * @param propertyNameLabelMessageID the property name label message id
	 */
	public EnumPropertyFilter(ClassFilter<?> classFilter, String propertyName, Class<? extends Enum<?>> clazz, String propertyNameLabelMessageID) {
		this(classFilter, propertyName, clazz, 0, propertyNameLabelMessageID);
	}


	/**
	 * Instantiates a new enum property filter.
	 *
	 * @param classFilter the class filter
	 * @param propertyName the property name
	 * @param clazz the clazz
	 * @param readOnly the read only
	 * @param mandatory the mandatory
	 * @param advanced the advanced
	 * @param propertyNameLabelMessageID the property name label message id
	 */
	public EnumPropertyFilter(ClassFilter<?> classFilter, String propertyName, Class<? extends Enum<?>> clazz, long flags, String propertyNameLabelMessageID) {
		super(classFilter, propertyName, flags, propertyNameLabelMessageID);
		this.clazz = clazz;
	}

	
	/* (non-Javadoc)
	 * @see com.imilia.server.domain.filter.PropertyFilter#buildQuery(com.imilia.server.domain.filter.QueryBuilder)
	 */
	@Override
	public void buildQuery(QueryBuilder q) throws ValidationException {
		if (isValid()) {

			final StringBuffer buffer = new StringBuffer();
			for (Enum<?> o : enums ) {
				if (buffer.length() > 0) {
					buffer.append(" OR ");
				}

				buffer.append(getQualifiedName());
				buffer.append(" = ");

				buffer.append(q.addParam(o));
			}

			// Add a parenthesis if required 
			if (enums.size() > 1){
				buffer.insert(0, "(");
				buffer.append(")");
			}
			final String query = buffer.toString();
			logger.debug(query);
			q.addRestriction(query);
		}
	}
	
	
	/* (non-Javadoc)
	 * @see com.imilia.server.domain.filter.PropertyFilter#isValid()
	 */
	@Override
	public boolean isValid() {
		return (enums != null && !enums.isEmpty());
	}

	/**
	 * Gets the ordinals.
	 * 
	 * @return the ordinals
	 */
	public List<Enum<?>> getEnums() {
		return enums;
	}

	/**
	 * Gets the ordinals as string.
	 *
	 * @return the ordinals as string
	 */
	public String getOrdinalsAsString(){
		
		if (isValid()){
			StringBuffer buffer = new StringBuffer();
			
			for (Enum<?> i:enums){
				if (buffer.length() > 0){
					buffer.append(";");
				}
				buffer.append(i.ordinal());
			}
			
		}
		
		return null;
	}
	
	/**
	 * Sets the ordinals.
	 * 
	 * @param ordinals the ordinals to set
	 */
	public void setEnums(List<Enum<?>> ordinals) {
		this.enums = ordinals;
	}

	/**
	 * Configure.
	 *
	 * @param ordinals the ordinals
	 * @return the enum property filter
	 */
	public EnumPropertyFilter configure(List<Enum<?>> ordinals) {
		setEnums(ordinals);
		return this;
	}

	/**
	 * Gets the clazz.
	 * 
	 * @return the clazz
	 */
	public Class<? extends Enum<?>> getClazz() {
		return clazz;
	}

	/**
	 * Sets the clazz.
	 * 
	 * @param clazz the clazz to set
	 */
	public void setClazz(Class<? extends Enum<?>> clazz) {
		this.clazz = clazz;
	}

	/* (non-Javadoc)
	 * @see com.imilia.server.domain.filter.PropertyFilter#visit(com.imilia.server.domain.filter.PropertyFilterVisitor)
	 */
	@Override
	public <T> T visit(PropertyFilterVisitor<T> propertyFilterVisitor) {
		return propertyFilterVisitor.accept(this);		
	}

	/* (non-Javadoc)
	 * @see com.imilia.server.domain.filter.PropertyFilter#duplicate()
	 */
	@Override
	public PropertyFilter duplicate() {
		return new EnumPropertyFilter(this);
	}

	@Override
	public void clear() {
		if (!isReadOnly()){
			enums = null;
		}
	}

	@Override
	public String getFilterPropertyName() {
		return "enums";
	}
}
