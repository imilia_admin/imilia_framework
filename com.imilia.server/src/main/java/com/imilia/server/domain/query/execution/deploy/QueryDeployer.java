package com.imilia.server.domain.query.execution.deploy;

import java.util.List;

import javax.management.relation.RoleResult;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.imilia.server.domain.access.Role;
import com.imilia.server.domain.access.Visibility;
import com.imilia.server.domain.query.Query;
import com.imilia.server.domain.query.entities.QueryJoin;
import com.imilia.server.domain.query.entities.QueryProperty;
import com.imilia.server.domain.query.entities.QueryRestriction;
import com.imilia.server.domain.query.entities.QueryRoleRestriction;
import com.imilia.server.domain.query.entities.QueryTemplate;
import com.imilia.server.domain.query.entities.QueryView;
import com.imilia.server.domain.query.execution.values.QueryPropertyValue;
import com.imilia.server.domain.query.execution.values.QueryRestrictionValue;

public class QueryDeployer {

	private final static Logger logger = LoggerFactory.getLogger(QueryDeployer.class);
	
	private final Query<?> query;
	
	private List<Role> roles;

	private QueryPropertyDeployer queryPropertyDeployer;
	
	private Visibility roleVisibility;
	
	public QueryDeployer(Query<?> query, List<Role> roles) {
		super();
		this.query = query;
		setRoles(roles);
		this.queryPropertyDeployer = new QueryPropertyDeployer(this );
	}

	public void deploy(){
		deployViews();
		deployJoins();
		deployRestrictions();
	}
	
	private void deployRestrictions() {
		for (QueryRestriction r:query.getQueryTemplate().getRestrictions()){
			if (r.isApplicable(getRoles())){
				query.getQueryRestrictionValues().add(new QueryRestrictionValue(r));
			}
		}
	}

	protected void deployViews(){
		query.getQueryPropertyValues().clear();
		
		final QueryTemplate template = query.getQueryTemplate();
		
		deploy(template.getQueryView(), null);
	}

	private void deploy(QueryView queryView, String path) {
		for (QueryProperty p:queryView.getProperties()){
			deploy(p, path);
		}
		
		if (queryView.getParent() != null){
			deploy(queryView.getParent(), path);
		}
	}

	private void deploy(QueryProperty p, String path) {
		if (roleVisibility.ordinal() <= p.getVisibility().ordinal()){
			QueryPropertyValue v =  queryPropertyDeployer.deploy(p);
			
			if ( v != null){
				if (path != null){
					v.setPath(path + "." + v.getPath());
				}
				query.getQueryPropertyValues().add(v);
			}else{
				logger.error("Could not deploy query property: " + p);
			}
		}else{
			if (logger.isDebugEnabled()){
				logger.debug("Ignoring property: " + p.getPropertyName() + 
						" visibility: " + p.getVisibility() + 
						" role visibility: " + roleVisibility );
			}
		}
	}
	
	protected void deployJoins(){
		final QueryTemplate template = query.getQueryTemplate();
		
		for (QueryJoin j:template.getQueryView().getJoins()){
			deploy(j.getQueryView(), j.getPath());
		}
	}

	public List<Role> getRoles() {
		return roles;
	}
	
	public void setRoles(List<Role> roles){
		this.roles = roles;
		this.roleVisibility = null;
		
		for (Role r:roles){
			if (this.roleVisibility == null){
				this.roleVisibility = r.getVisibility();
			}else{
				// Ordinal 0 i.e. Sudo has the highest visibility
				if (this.roleVisibility.ordinal() > r.getVisibility().ordinal()){
					this.roleVisibility = r.getVisibility();
				}
			}
		}
	}
}
