/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 * Created on: Dec 31, 2009
 * Created by: emcgreal
 */
package com.imilia.server.domain.user;



/**
 * The Interface UserPropertyManager.
 */
public interface UserPropertyManager {
	
	/**
	 * Gets the.
	 * 
	 * @param instanceId the instance id
	 * @param path the path
	 * 
	 * @return the user property
	 */
	public UserProperty get(String instanceId, String path);
	
	/**
	 * Save.
	 * 
	 * @param userProperty the user property
	 */
	public void save(UserProperty userProperty);
}
