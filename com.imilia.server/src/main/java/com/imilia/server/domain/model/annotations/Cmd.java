/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2014 - all rights reserved
 *
 * Created on: Jan 22, 2014
 * Created by: emcgreal
 */
package com.imilia.server.domain.model.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * The Interface Cmd defines a model command.
 *
 * @author emcgreal
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE,ElementType.METHOD})
public @interface Cmd {
	
	/**
	 * Name of the command.
	 *
	 * @return the string
	 */
	String value();
	
	/**
	 * Method name .
	 *
	 * @return the string
	 */
	String method() default "";
	
	/**
	 * Attributes method.
	 *
	 * @return the string
	 */
	String attributesMethod() default "";
	
	/**
	 * Crud method.
	 *
	 * @return the string
	 */
	String crudMethod() default "";
}
