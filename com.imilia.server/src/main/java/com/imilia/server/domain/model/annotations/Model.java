/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2014 - all rights reserved
 *
 * Created on: Jan 22, 2014
 * Created by: emcgreal
 */
package com.imilia.server.domain.model.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * The Interface Model.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Model {
	
	/**
	 * Name of model.
	 *
	 * @return the string
	 */
	String name() default "";
	
	/**
	 * Path.
	 *
	 * @return the string
	 */
	String path() default "";
	
	/**
	 * Attributes.
	 *
	 * @return the string
	 */
	String attributes() default "";
	
	/**
	 * Cruds.
	 *
	 * @return the string
	 */
	String cruds() default "";
	
	/**
	 * Prps. Properties exposed by the model
	 *
	 * @return the prp[]
	 */
	Prp[] prps() default {};
	
	/**
	 * Grps.
	 *
	 * @return the grp[]
	 */
	Grp[] grps() default {};
	
	/**
	 * Evts. Events supported by the model
	 *
	 * @return the evt[]
	 */
	Evt[] evts() default {};
	
	/**
	 * Cmds. Commands supported by the model
	 *
	 * @return the cmd[]
	 */
	Cmd[] cmds() default {};
}
