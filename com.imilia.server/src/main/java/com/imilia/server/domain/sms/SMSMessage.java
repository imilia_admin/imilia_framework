/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2010 - all rights reserved
 * Created on: May 19, 2010
 * Created by: emcgreal
 */
package com.imilia.server.domain.sms;

import java.util.HashMap;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.imilia.server.domain.DomainObject;

/**
 * The Class SMSMessage.
 */
@Entity
public class SMSMessage extends DomainObject {
	
	/** The Constant NUMBER. */
	public static final String NUMBER 	= "number";
	
	/** The Constant TEXT. */
	public static final String TEXT 	= "text";
	
	/** The Constant SENDER. */
	public static final String SENDER 	= "sender";
	
	/** The mobile number. */
	@NotNull
	@NotBlank
	private String mobileNumber;
	
	/** The text. */
	@NotNull
	@NotBlank
	private String text;
	
	/** The sender. */
	@NotNull
	@NotBlank
	private String sender;
	
	/** The sms routing. */
	@ManyToOne
	@NotNull
	private SMSRouting smsRouting;

	/**
	 * Instantiates a new sMS message.
	 */
	public SMSMessage(){
		super();
	}
	
	/**
	 * Instantiates a new sMS message.
	 * 
	 * @param mobileNumber the mobile number
	 * @param text the text
	 * @param sender the sender
	 * @param smsRouting the sms routing
	 */
	public SMSMessage(String mobileNumber, String text, String sender,
			SMSRouting smsRouting) {
		super();
		this.mobileNumber = mobileNumber;
		this.text = text;
		this.sender = sender;
		this.smsRouting = smsRouting;
	}

	/**
	 * Gets the mobile number.
	 * 
	 * @return the mobile number
	 */
	public final String getMobileNumber() {
		return mobileNumber;
	}

	/**
	 * Sets the mobile number.
	 * 
	 * @param mobileNumber the new mobile number
	 */
	public final void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	/**
	 * Gets the text.
	 * 
	 * @return the text
	 */
	public final String getText() {
		return text;
	}

	/**
	 * Sets the text.
	 * 
	 * @param text the new text
	 */
	public final void setText(String text) {
		this.text = text;
	}

	/**
	 * Gets the sender.
	 * 
	 * @return the sender
	 */
	public final String getSender() {
		return sender;
	}

	/**
	 * Sets the sender.
	 * 
	 * @param absender the absender
	 */
	public final void setSender(String absender) {
		this.sender = absender;
	}

	/**
	 * Gets the sms routing.
	 * 
	 * @return the sms routing
	 */
	public final SMSRouting getSmsRouting() {
		return smsRouting;
	}

	/**
	 * Sets the sms routing.
	 * 
	 * @param smsRouting the new sms routing
	 */
	public final void setSmsRouting(SMSRouting smsRouting) {
		this.smsRouting = smsRouting;
	}
	
	/**
	 * To map.
	 * 
	 * @return the hash map< string, string>
	 */
	public HashMap<String, String> toMap(){
		final HashMap<String, String> map = new HashMap<String, String>();
		
		map.put(NUMBER, getMobileNumber());
		map.put(TEXT, getText());
		map.put(SENDER, getSender());
		map.put(SMSRouting.ACCOUNT, getSmsRouting().getAccount());
		map.put(SMSRouting.PASSWORD, getSmsRouting().getPassword());
		map.put(SMSRouting.GATEWAY, getSmsRouting().getGateway());
		
		return map;
	}
}
