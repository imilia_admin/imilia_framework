/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2014 - all rights reserved
 * Created on: Feb 19, 2014
 * Created by: emcgreal
 */
package com.imilia.server.domain.model.property;

import com.imilia.server.domain.Crud;

/**
 * The Interface Property Permission.
 */
public interface PropertyCrudProvider {
	/**
	 * Gets the permission.
	 *
	 * @param property the property
	 * @return the permission
	 */
	public Crud getCrud(Property property);
}
