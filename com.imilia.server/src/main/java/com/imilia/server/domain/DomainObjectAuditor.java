/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 *
 * Created on: Jul 13, 2009
 * Created by: emcgreal
 */
package com.imilia.server.domain;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.joda.time.DateTime;
import org.springframework.security.core.context.SecurityContextHolder;

import com.avaje.ebean.event.BeanPersistController;
import com.avaje.ebean.event.BeanPersistRequest;
import com.imilia.server.domain.common.UserAccount;


/**
 * The Class DomainObjectAuditor.
 */
/**
 * @author emcgreal
 *
 */
public class DomainObjectAuditor implements BeanPersistController{

	/** The Constant logger. */
	private final static Logger logger = LoggerFactory.getLogger(DomainObjectAuditor.class);


	public DomainObjectAuditor(){
	}


	/* (non-Javadoc)
	 * @see com.avaje.ebean.event.BeanPersistController#getExecutionOrder()
	 */
	public int getExecutionOrder() {
		return 0;
	}

	/* (non-Javadoc)
	 * @see com.avaje.ebean.event.BeanPersistController#isRegisterFor(java.lang.Class)
	 */
	public boolean isRegisterFor(Class<?> arg0) {
		return true;
	}

	/* (non-Javadoc)
	 * @see com.avaje.ebean.event.BeanPersistController#postDelete(com.avaje.ebean.event.BeanPersistRequest)
	 */
	public void postDelete(BeanPersistRequest<?> arg0) {
	}

	/* (non-Javadoc)
	 * @see com.avaje.ebean.event.BeanPersistController#postInsert(com.avaje.ebean.event.BeanPersistRequest)
	 */
	public void postInsert(BeanPersistRequest<?> arg0) {
	}

	/* (non-Javadoc)
	 * @see com.avaje.ebean.event.BeanPersistController#postLoad(java.lang.Object, java.util.Set)
	 */
	public void postLoad(Object arg0, Set<String> arg1) {
	}

	/* (non-Javadoc)
	 * @see com.avaje.ebean.event.BeanPersistController#postUpdate(com.avaje.ebean.event.BeanPersistRequest)
	 */
	public void postUpdate(BeanPersistRequest<?> arg0) {

	}

	/* (non-Javadoc)
	 * @see com.avaje.ebean.event.BeanPersistController#preDelete(com.avaje.ebean.event.BeanPersistRequest)
	 */
	public boolean preDelete(BeanPersistRequest<?> arg0) {
		return true;
	}

	/* (non-Javadoc)
	 * @see com.avaje.ebean.event.BeanPersistController#preInsert(com.avaje.ebean.event.BeanPersistRequest)
	 */
	public boolean preInsert(BeanPersistRequest<?> request) {
		final DomainObject domainObject = (DomainObject) request.getBean();
		
		if (!domainObject.isIgnoreAuditor()){
			final DateTime now = new DateTime();
	
			// Created by/on
			domainObject.setCreatedBy(getAuthenticatedUser());
			domainObject.setCreatedOn(now);
	
			// Changed by/on
			domainObject.setChangedBy(getAuthenticatedUser());
			domainObject.setChangedOn(now);
		}else{
			logger.debug("Ignoring auditor for insert");
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see com.avaje.ebean.event.BeanPersistController#preUpdate(com.avaje.ebean.event.BeanPersistRequest)
	 */
	public boolean preUpdate(BeanPersistRequest<?> request) {
		final DomainObject domainObject = (DomainObject) request.getBean();
		// Changed by/on
		
		if (!domainObject.isIgnoreAuditor()){
			domainObject.setChangedBy(getAuthenticatedUser());
			domainObject.setChangedOn(new DateTime());
		}else{
			logger.debug("Ignoring auditor for update");
		}
		return true;
	}

	
	/**
	 * Gets the authenticated user.
	 * 
	 * @return the authenticated user
	 */
	private UserAccount getAuthenticatedUser(){
		try{
			final Object o = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			
			if (o instanceof UserAccount){
				return (UserAccount) o;
			}
			logger.error("Expecting a UserAccout and found:" + o);
		}catch(NullPointerException ex){
			logger.warn("No authenticated user", ex);
		}
		
		return null;
	}
}
