package com.imilia.server.domain.model.list;

import java.util.ArrayList;

import org.springframework.context.MessageSource;

import com.imilia.server.domain.i18n.DomainLocaleVariant;

public class EnumGenericObjectListModel extends DefaultGenericObjectListModel<EnumWrapper>{

	public EnumGenericObjectListModel(Class<? extends Enum<?>> enumType, DomainLocaleVariant domainLocale, MessageSource messageSource){
		super(domainLocale, messageSource);
		buildList(enumType);
		setStoreProperty("enumValue");
	}

	private void buildList(Class<? extends Enum<?>> enumType) {
		
		ArrayList<EnumWrapper> wrapperList = new ArrayList<EnumWrapper>();
		
		for (Enum<?> e:enumType.getEnumConstants()){
			wrapperList.add(new EnumWrapper(e));
		}
		
		setList(wrapperList);
	}
}
