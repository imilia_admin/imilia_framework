package com.imilia.server.domain.validation.properties;

public interface PropertyValidationModel {
	public PropertyValidationSpecification getPropertyValidationSpecification(String path);
}
