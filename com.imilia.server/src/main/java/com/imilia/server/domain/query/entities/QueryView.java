/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2014 - all rights reserved
 * Created on: Mar 14, 2014
 * Created by: emcgreal
 */
package com.imilia.server.domain.query.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.imilia.server.domain.DomainObject;

/**
 * The Class QueryView.
 */
@Entity
public class QueryView extends DomainObject{
	
	/** The name. */
	private String name;

	/** The flags. */
	private long flags;
	
	/** The clazz. */
	private Class<?> clazz;

	/** The parent. */
	@ManyToOne
	private QueryView parent;
	
	/** The properties. */
	@OneToMany(mappedBy="queryView", cascade=CascadeType.ALL)
	private List<QueryProperty> properties;
	
	/** The joins. */
	@OneToMany(mappedBy="queryView", cascade=CascadeType.ALL)
	private List<QueryJoin> joins;

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the properties.
	 *
	 * @return the properties
	 */
	public List<QueryProperty> getProperties() {
		return properties;
	}

	/**
	 * Sets the properties.
	 *
	 * @param properties the new properties
	 */
	public void setProperties(List<QueryProperty> properties) {
		this.properties = properties;
	}

	/**
	 * Gets the flags.
	 *
	 * @return the flags
	 */
	public long getFlags() {
		return flags;
	}

	/**
	 * Sets the flags.
	 *
	 * @param flags the new flags
	 */
	public void setFlags(long flags) {
		this.flags = flags;
	}

	/**
	 * Gets the joins.
	 *
	 * @return the joins
	 */
	public List<QueryJoin> getJoins() {
		return joins;
	}

	/**
	 * Sets the joins.
	 *
	 * @param joins the new joins
	 */
	public void setJoins(List<QueryJoin> joins) {
		this.joins = joins;
	}

	/**
	 * Gets the clazz.
	 *
	 * @return the clazz
	 */
	public Class<?> getClazz() {
		return clazz;
	}

	/**
	 * Sets the clazz.
	 *
	 * @param clazz the new clazz
	 */
	public void setClazz(Class<?> clazz) {
		this.clazz = clazz;
	}

	/**
	 * Gets the parent.
	 *
	 * @return the parent
	 */
	public QueryView getParent() {
		return parent;
	}

	/**
	 * Sets the parent.
	 *
	 * @param parent the new parent
	 */
	public void setParent(QueryView parent) {
		this.parent = parent;
	}

	/**
	 * Adds the property
	 *
	 * @param queryProperty the query property
	 */
	public void add(QueryProperty queryProperty) {
		queryProperty.setQueryView(this);
		getProperties().add(queryProperty);
	}

	/**
	 * Adds the join
	 *
	 * @param queryJoin the query join
	 */
	public void add(QueryJoin queryJoin) {
		queryJoin.setQueryView(this);
		getJoins().add(queryJoin);
	}

	@Override
	public String toString() {
		return "QueryView [name=" + name + ", clazz=" + clazz + "]";
	}
	
}
