/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2013 - all rights reserved
 *
 * Created on: Aug 23, 2013
 * Created by: emcgreal
 */
package com.imilia.server.domain.i18n;

import org.springframework.context.MessageSource;

import com.imilia.utils.i18n.DomainLocale;


/**
 * The Interface DomainLocaleComparator.
 * Implementations of this interface should be able to intelligently sort instances of 
 * type T and have access to a message source e.g. for Localized etc
 *
 * @param <T> the generic type
 */
public interface DomainLocaleComparator<T> {
	
	/**
	 * Compare.
	 *
	 * @param lhs the lhs
	 * @param rhs the rhs
	 * @param locale the locale
	 * @return the int
	 */
	public int compare(T lhs, T rhs, DomainLocale locale);	
	
	/**
	 * Gets the message source.
	 *
	 * @return the message source
	 */
	public MessageSource getMessageSource();
	
	/**
	 * Sets the message source
	 *
	 * @param messageSource the new message source
	 */
	public void setMessageSource(MessageSource messageSource);

}
