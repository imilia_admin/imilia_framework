/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2014 - all rights reserved
 *
 * Created on: Jan 22, 2014
 * Created by: emcgreal
 */
package com.imilia.server.domain.model.manager;

import java.util.List;

import com.imilia.server.domain.model.command.ICommand;
import com.imilia.server.domain.model.event.IEvent;
import com.imilia.server.domain.model.event.IEventSource;
import com.imilia.server.domain.model.property.Property;

/**
 * The Interface IModelManager.
 */
public interface IModelManager {
	
	public void init();
	
	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName();
	
	/**
	 * Gets the properties.
	 *
	 * @return the properties
	 */
	public List<Property> getProperties();

	/**
	 * Gets the property.
	 *
	 * @param name the name
	 * @return the property
	 */
	public Property getProperty(String name);

	/**
	 * Gets the event sources.
	 *
	 * @return the event sources
	 */
	public List<IEventSource<IEvent>> getEventSources();
	
	/**
	 * Gets the commands.
	 *
	 * @return the commands
	 */
	public List<ICommand> getCommands();

}
