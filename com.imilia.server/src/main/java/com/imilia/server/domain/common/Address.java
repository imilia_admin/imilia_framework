/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 * Created on: Jan 23, 2009
 * Created by: emcgreal
 */

package com.imilia.server.domain.common;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.hibernate.validator.constraints.Length;

import com.imilia.server.domain.DomainObject;
import com.imilia.server.domain.ObjectStatusIterator;
import com.imilia.server.domain.enums.PhoneNumberType;
import com.imilia.server.domain.filter.RestrictionCandidate;
import com.imilia.server.domain.validation.ConditionalValidation;
import com.imilia.server.domain.validation.ValidationClassSpec;
import com.imilia.utils.Flags;
import com.imilia.utils.strings.StringUtils;
/**
 * The Class Address holds address, e-mail and phone contacts details.
 * 
 * @author emcg
 */
@Entity
public class Address extends DomainObject implements ConditionalValidation{

	/** The Constant PrimaryPhoneContact. */
	public static final String PrimaryPhoneContact = "primaryPhoneContact";

	/** The street. */
//	@NotNullOn(method = "isMandatory('street')")
//	@RestrictionCandidate
	@Length(max = 255)
	@Column(length = 255)
	private String street;

	/** The house number. */
//	@NotNullOn(method = "isMandatory('houseNumber')")
//	@Max(value = 20)
	@Column(length = 20)
	private String houseNumber;

	/** The zip. */
	@RestrictionCandidate
//	@NotNullOn(method = "isMandatory('zip')")
//	@Max(value = 30)
	@Column(length = 30)
	private String zip;

	/** The area. */
//	@NotNullOn(method = "isMandatory('area')")
//	@Max(value = 255)
	@Column(length = 255)
	private String area;

	/** The city. */
//	@RestrictionCandidate
//	@NotNullOn(method = "isMandatory('city')")
//	@Max(value = 255)
	@Column(length = 255)
	private String city;

	/** The country. */
//	@RestrictionCandidate
//	@NotNullOn(method = "isMandatory('country')")
//	@Max(value = 255)
	@Column(length = 255)
	private String country;

	/** The Constant EMAIL_IS_MANDATORY. */
	public final static long EMAIL_IS_MANDATORY = 0x00000001;

	/** The email. */
//	@RestrictionCandidate
//	@Email
//	@NotNullOn(method = "isMandatory('email')")
	private String email;

	/** The url. */
	private String url;

	/** The comment. */
	private String comment;

	/** The primary phone contact. */
//	@NotNullOn(method = "isMandatory('primaryPhoneContact')")
//	@ManyToOne(optional = true, cascade = { CascadeType.ALL })
	private PhoneContact primaryPhoneContact;

	/** The phone contacts. */
	@OneToMany(mappedBy = "address", cascade = { CascadeType.ALL })
	private List<PhoneContact> phoneContacts;

	/** The Constant SHARE_ADDRESS. */
	private final static long SHARE_ADDRESS = 0x00000001;

	/** The Constant SHARE_EMAIL. */
	private final static long SHARE_EMAIL = 0x00000002;

	/** The Constant SHARE_URL. */
	private final static long SHARE_URL = 0x00000004;

	/** The flags. */
	private long flags;

	@ManyToOne(optional=true)
	private ValidationClassSpec validationClassSpec;
	
	public ValidationClassSpec getValidationClassSpec() {
		return validationClassSpec;
	}

	public void setValidationClassSpec(ValidationClassSpec validationClassSpec) {
		this.validationClassSpec = validationClassSpec;
	}

	/**
	 * Instantiates a new address.
	 */
	public Address() {
	}

	/**
	 * Instantiates a new address.
	 * 
	 * @param street
	 *            the street
	 * @param houseNumber
	 *            the house number
	 * @param zip
	 *            the zip
	 * @param area
	 *            the area
	 * @param city
	 *            the city
	 * @param email
	 *            the email
	 * @param url
	 *            the url
	 * @param comment
	 *            the comment
	 */
	public Address(String street, String houseNumber, String zip, String area,
			String city, String email, String url, String comment) {
		super();
		this.street = street;
		this.houseNumber = houseNumber;
		this.zip = zip;
		this.area = area;
		this.city = city;
		this.email = email;
		this.url = url;
		this.comment = comment;
	}

	/**
	 * Gets the street.
	 * 
	 * @return the street
	 */
	public String getStreet() {
		return street;
	}

	/**
	 * Sets the street.
	 * 
	 * @param street
	 *            the new street
	 */
	public void setStreet(String street) {
		if (street != null) {
			final String[] parts = street.split(" ");
			final int len = parts.length;
			if (len > 1) {
				final String number = parts[len - 1];

				if (StringUtils.containsNumber(number)) {
					setHouseNumber(number.trim());
					final StringBuffer buffer = new StringBuffer();
					for (int i = 0; i < len - 1; i++) {
						if (buffer.length() > 0) {
							buffer.append(" ");
						}
						buffer.append(parts[i].trim());
					}
					street = buffer.toString();
				}
			}
		}
		this.street = street;
	}

	/**
	 * Gets the house number.
	 * 
	 * @return the house number
	 */
	public String getHouseNumber() {
		return houseNumber;
	}

	/**
	 * Sets the house number.
	 * 
	 * @param houseNumber
	 *            the new house number
	 */
	public void setHouseNumber(String houseNumber) {
		this.houseNumber = houseNumber;
	}

	/**
	 * Gets the zip.
	 * 
	 * @return the zip
	 */
	public String getZip() {
		return zip;
	}

	/**
	 * Sets the zip.
	 * 
	 * @param zip
	 *            the new zip
	 */
	public void setZip(String zip) {
		this.zip = zip;
	}

	/**
	 * Gets the area.
	 * 
	 * @return the area
	 */
	public String getArea() {
		return area;
	}

	/**
	 * Sets the area.
	 * 
	 * @param area
	 *            the new area
	 */
	public void setArea(String area) {
		this.area = area;
	}

	/**
	 * Gets the city.
	 * 
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * Sets the city.
	 * 
	 * @param city
	 *            the new city
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * Gets the email.
	 * 
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Sets the email.
	 * 
	 * @param email
	 *            the new email
	 */
	public void setEmail(String email) {
		this.email = email != null ? email.trim() : null;
	}

	/**
	 * Gets the url.
	 * 
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * Sets the url.
	 * 
	 * @param url
	 *            the new url
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * Gets the comment.
	 * 
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * Sets the comment.
	 * 
	 * @param comment
	 *            the new comment
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}

	/**
	 * Gets the primary phone contact.
	 * 
	 * @return the primary phone contact
	 */
	public PhoneContact getPrimaryPhoneContact() {
		return primaryPhoneContact;
	}

	/**
	 * Sets the primary phone contact.
	 * 
	 * @param phoneContact
	 *            the new primary phone contact
	 */
	public void setPrimaryPhoneContact(PhoneContact phoneContact) {
		this.primaryPhoneContact = phoneContact;

		if (this.primaryPhoneContact != null
				&& !this.equals(primaryPhoneContact.getAddress())) {
			this.primaryPhoneContact.setAddress(this);
		}

		addPhoneContact(phoneContact);
	}

	/**
	 * Gets the phone contacts.
	 * 
	 * @return the phoneContacts
	 */
	public List<PhoneContact> getPhoneContacts() {
		return phoneContacts;
	}

	/**
	 * Gets the active phone contacts.
	 * 
	 * @return the active phone contacts
	 */
	public List<PhoneContact> getActivePhoneContacts() {
		return new ObjectStatusIterator<PhoneContact>(phoneContacts.iterator())
				.toList();
	}

	/**
	 * Sets the phone contacts.
	 * 
	 * @param phoneContacts
	 *            the phoneContacts to set
	 */
	public void setPhoneContacts(List<PhoneContact> phoneContacts) {
		this.phoneContacts = phoneContacts;
	}

	/**
	 * Adds the phone contact.
	 * 
	 * @param phoneContact
	 *            the phone contact
	 */
	public void addPhoneContact(PhoneContact phoneContact) {
		if (phoneContact != null) {
			if (!getPhoneContacts().contains(phoneContact)) {
				getPhoneContacts().add(phoneContact);
			}
			phoneContact.setAddress(this);
		}
	}

	/**
	 * Adds the phone contact.
	 * 
	 * @param number
	 *            the number
	 * @param type
	 *            the type
	 */
	public void addPhoneContact(String number, PhoneNumberType type) {
		if (number != null && type != null) {
			PhoneContact pc = findPhoneContact(type);

			if (pc == null) {
				pc = new PhoneContact(number, type);
				getPhoneContacts().add(pc);
				pc.setAddress(this);
			} else {
				pc.setNumber(number);
			}
		}
	}

	/**
	 * Gets the country.
	 * 
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * Sets the country.
	 * 
	 * @param country
	 *            the new country
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * Find phone contact.
	 * 
	 * @param type
	 *            the type
	 * 
	 * @return the phone contact
	 */
	public PhoneContact findPhoneContact(PhoneNumberType type) {
		for (PhoneContact pc : getPhoneContacts()) {
			if (pc.getPhoneNumberType().equals(type)) {
				return pc;
			}
		}

		return null;
	}

	/**
	 * Sets the phone home.
	 * 
	 * @param number
	 *            the new phone home
	 */
	public void setPhoneHome(String number) {
		addPhoneContact(number, PhoneNumberType.Home);
	}

	/**
	 * Gets the phone home.
	 * 
	 * @return the phone home
	 */
	public PhoneContact getPhoneHome() {
		return findPhoneContact(PhoneNumberType.Home);
	}

	/**
	 * Sets the phone work.
	 * 
	 * @param number
	 *            the new phone work
	 */
	public void setPhoneWork(String number) {
		addPhoneContact(number, PhoneNumberType.Work);
	}

	/**
	 * Gets the phone work.
	 * 
	 * @return the phone work
	 */
	public String getPhoneWork() {
		final PhoneContact pc = findPhoneContact(PhoneNumberType.Work);

		if (pc != null) {
			return pc.getNumber();
		}
		return null;
	}

	/**
	 * Sets the phone home fax.
	 * 
	 * @param number
	 *            the new phone home fax
	 */
	public void setPhoneHomeFax(String number) {
		addPhoneContact(number, PhoneNumberType.HomeFax);
	}

	/**
	 * Gets the phone home fax.
	 * 
	 * @return the phone home fax
	 */
	public String getPhoneHomeFax() {
		final PhoneContact pc = findPhoneContact(PhoneNumberType.HomeFax);

		if (pc != null) {
			return pc.getNumber();
		}
		return null;
	}

	/**
	 * Sets the phone work fax.
	 * 
	 * @param number
	 *            the new phone work fax
	 */
	public void setPhoneWorkFax(String number) {
		addPhoneContact(number, PhoneNumberType.WorkFax);
	}

	/**
	 * Gets the phone work fax.
	 * 
	 * @return the phone work fax
	 */
	public String getPhoneWorkFax() {
		final PhoneContact pc = findPhoneContact(PhoneNumberType.WorkFax);

		if (pc != null) {
			return pc.getNumber();
		}
		return null;
	}

	/**
	 * Sets the phone mobile.
	 * 
	 * @param number
	 *            the new phone mobile
	 */
	public void setPhoneMobile(String number) {
		addPhoneContact(number, PhoneNumberType.Mobile);
	}

	/**
	 * Gets the phone mobile.
	 * 
	 * @return the phone mobile
	 */
	public String getPhoneMobile() {
		final PhoneContact pc = findPhoneContact(PhoneNumberType.Mobile);

		if (pc != null) {
			return pc.getNumber();
		}
		return null;
	}

	/**
	 * Sets the phone emergency.
	 * 
	 * @param number
	 *            the new phone emergency
	 */
	public void setPhoneEmergency(String number) {
		addPhoneContact(number, PhoneNumberType.Emergency);
	}

	/**
	 * Gets the phone emergency.
	 * 
	 * @return the phone emergency
	 */
	public String getPhoneEmergency() {
		final PhoneContact pc = findPhoneContact(PhoneNumberType.Emergency);

		if (pc != null) {
			return pc.getNumber();
		}
		return null;
	}

	/**
	 * Sets the phone other.
	 * 
	 * @param number
	 *            the new phone other
	 */
	public void setPhoneOther(String number) {
		addPhoneContact(number, PhoneNumberType.Other);
	}

	/**
	 * Gets the phone other.
	 * 
	 * @return the phone other
	 */
	public String getPhoneOther() {
		final PhoneContact pc = findPhoneContact(PhoneNumberType.Other);

		if (pc != null) {
			return pc.getNumber();
		}
		return null;
	}

	/**
	 * Gets the flags.
	 * 
	 * @return the flags
	 */
	public long getFlags() {
		return flags;
	}

	/**
	 * Sets the flags.
	 * 
	 * @param flags
	 *            the new flags
	 */
	public void setFlags(long flags) {
		this.flags = flags;
	}

	/**
	 * Sets the share address.
	 * 
	 * @param share
	 *            the new share address
	 */
	public void setShareAddress(boolean share) {
		flags = Flags.set(getFlags(), SHARE_ADDRESS, share);
	}

	/**
	 * Checks if is share address.
	 * 
	 * @return true, if is share address
	 */
	public boolean isShareAddress() {
		return Flags.isSet(getFlags(), SHARE_ADDRESS);
	}

	/**
	 * Sets the share email.
	 * 
	 * @param share
	 *            the new share email
	 */
	public void setShareEmail(boolean share) {
		flags = Flags.set(getFlags(), SHARE_EMAIL, share);
	}

	/**
	 * Checks if is share email.
	 * 
	 * @return true, if is share email
	 */
	public boolean isShareEmail() {
		return Flags.isSet(getFlags(), SHARE_EMAIL);
	}

	/**
	 * Sets the share url.
	 * 
	 * @param share
	 *            the new share url
	 */
	public void setShareUrl(boolean share) {
		flags = Flags.set(getFlags(), SHARE_URL, share);
	}

	/**
	 * Checks if is share url.
	 * 
	 * @return true, if is share url
	 */
	public boolean isShareUrl() {
		return Flags.isSet(getFlags(), SHARE_URL);
	}

	/**
	 * Gets the area shared.
	 * 
	 * @return the area shared
	 */
	public String getAreaShared() {
		if (isShareAddress()) {
			return getArea();
		}
		return null;
	}

	/**
	 * Gets the city shared.
	 * 
	 * @return the city shared
	 */
	public String getCityShared() {
		if (isShareAddress()) {
			return getCity();
		}
		return null;
	}

	/**
	 * Gets the country shared.
	 * 
	 * @return the country shared
	 */
	public String getCountryShared() {
		if (isShareAddress()) {
			return getCountry();
		}
		return null;
	}

	/**
	 * Gets the email shared.
	 * 
	 * @return the email shared
	 */
	public String getEmailShared() {
		if (isShareEmail()) {
			return getEmail();
		}
		return null;
	}

	/**
	 * Gets the house nr shared.
	 * 
	 * @return the house nr shared
	 */
	public String getHouseNrShared() {
		if (isShareAddress()) {
			return getHouseNumber();
		}
		return null;
	}

	/**
	 * Gets the street shared.
	 * 
	 * @return the street shared
	 */
	public String getStreetShared() {
		if (isShareAddress()) {
			return getStreet();
		}
		return null;
	}

	/**
	 * Gets the url shared.
	 * 
	 * @return the url shared
	 */
	public String getUrlShared() {
		if (isShareUrl()) {
			return getUrl();
		}
		return null;
	}

	/**
	 * Gets the zip shared.
	 * 
	 * @return the zip shared
	 */
	public String getZipShared() {
		if (isShareAddress()) {
			return getZip();
		}
		return null;
	}
	
}
