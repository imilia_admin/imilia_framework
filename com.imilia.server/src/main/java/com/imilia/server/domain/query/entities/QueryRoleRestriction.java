/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2014 - all rights reserved
 * Created on: Feb 26, 2014
 * Created by: emcgreal
 */

package com.imilia.server.domain.query.entities;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.imilia.server.domain.DomainObject;
import com.imilia.server.domain.access.Role;

@Entity
public class QueryRoleRestriction extends DomainObject {
	
	@ManyToOne
	private Role role;
	
	@ManyToOne
	private QueryRestriction restriction;
	
	public QueryRoleRestriction(){
		super();
	}
	
	public QueryRoleRestriction(Role role, QueryRestriction restriction) {
		super();
		this.role = role;
		this.restriction = restriction;
		restriction.getRestrictionRoles().add(this);
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public QueryRestriction getRestriction() {
		return restriction;
	}

	public void setRestriction(QueryRestriction restriction) {
		this.restriction = restriction;
	}

	@Override
	public String toString() {
		return "RoleRestriction [role=" + role.getAuthority() + "]";
	}

	public boolean isApplicable(List<Role> roles){
		for (Role r:roles){
			if (r.equals(role)){
				return true;
			}
		}
		
		return false;
		
	}
}
