/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2010 - all rights reserved
 * Created on: Aug 10, 2010
 * Created by: emcgreal
 */

package com.imilia.server.domain.filter;

import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.imilia.utils.Flags;
import com.imilia.server.validation.ValidationException;

/**
 * The Class BitMaskPropertyFilter.
 */
public class BitMaskPropertyFilter extends PropertyFilter {

	/** The Constant logger. */
	private final static Logger logger = LoggerFactory
			.getLogger(BitMaskPropertyFilter.class);

	public final static String TAG_BITMASK_FILTER = "BitMaskPropertyFilter";
	public final static String TAG_FILTERS = "filters";
	public final static String TAG_BITMASK_OPERATOR = "bitMaskOperator";
	
	/** The filters. */
	private Set<Long> filters = new HashSet<Long>();
	
	private BitMaskOperator bitMaskOperator = BitMaskOperator.OR;
	/**
	 * Instantiates a new string list property filter.
	 */
	public BitMaskPropertyFilter() {
		super();
	}

	public BitMaskPropertyFilter(BitMaskPropertyFilter other) {
		super(other);
		getFilters().addAll(other.getFilters());
	}

	
	/**
	 * The Constructor.
	 * 
	 * @param classFilter the class filter
	 * @param propertyName the property name
	 */
	public BitMaskPropertyFilter(ClassFilter<?> classFilter, String propertyName) {
		super(classFilter, propertyName);
	}
	
	/**
	 * The Constructor.
	 * 
	 * @param classFilter the class filter
	 * @param propertyName the property name
	 * @param propertyNameLabelMessageID the property name label message id
	 */
	public BitMaskPropertyFilter(ClassFilter<?> classFilter, String propertyName, String propertyNameLabelMessageID) {
		super(classFilter, propertyName, propertyNameLabelMessageID);
	}

	/**
	 * Instantiates a new bit mask property filter.
	 * 
	 * @param classFilter the class filter
	 * @param propertyName the property name
	 * @param operator the operator
	 */
	public BitMaskPropertyFilter(ClassFilter<?> classFilter, String propertyName, BitMaskOperator operator) {
		super(classFilter, propertyName);
		setBitMaskOperator(operator);
	}
	/**
	 * Instantiates a new bit mask property filter.
	 * 
	 * @param classFilter the class filter
	 * @param propertyName the property name
	 * @param operator the operator
	 */
	public BitMaskPropertyFilter(ClassFilter<?> classFilter, String propertyName, String propertyNameLabelMessageID, BitMaskOperator operator) {
		super(classFilter, propertyName, propertyNameLabelMessageID);
		setBitMaskOperator(operator);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.imilia.bkf.api.domain.filter.PropertyFilter#clear()
	 */
	@Override
	public void clear() {
		this.filters.clear();
	}

	/* (non-Javadoc)
	 * @see com.imilia.server.domain.filter.PropertyFilter#buildQuery(com.imilia.server.domain.filter.QueryBuilder)
	 */
	@Override
	public void buildQuery(QueryBuilder q) throws ValidationException {
		if (isValid()) {
			String query = null;
			switch(bitMaskOperator){
			case AND:
				query = bitwiseAnd(q);
				break;
			case OR:
				query = bitwiseOr(q);
				break;
			}
			logger.debug(query);
			q.addRestriction(query);
		}
	}
	
	private String bitwiseAnd(QueryBuilder q){
		long flags = Flags.build(filters);
		StringBuffer buffer = new StringBuffer();
		
		if (q.getDatabasePlatform().getName().equals("oracle")){
			buffer.append("bitand(");
			buffer.append(getQualifiedName());
			buffer.append(", ");
			buffer.append(flags);
			buffer.append(")");
		}else{
			buffer.append(getQualifiedName());
			buffer.append(" & ");
			buffer.append(flags);
		}
		buffer.append( " = ");
		buffer.append("flags");
		return buffer.toString();
	}
	
	private String bitwiseOr(QueryBuilder q){
		long flags = Flags.build(filters);
		StringBuffer buffer = new StringBuffer();
		if (q.getDatabasePlatform().getName().equals("oracle")){
			buffer.append("bitand(");
			buffer.append(getQualifiedName());
			buffer.append(", ");
			buffer.append(flags);
			buffer.append(")");
		}else{
			buffer.append(getQualifiedName());
			buffer.append(" & ");
			buffer.append(flags);
		}
		buffer.append( " != 0");
		return buffer.toString();
	}

	public String getFilterPropertyName() {
		return "filters";
	}
	/*
	 * (non-Javadoc)
	 *
	 * @see com.imilia.bkf.api.domain.filter.PropertyFilter#isValid()
	 */
	@Override
	public boolean isValid() {
		return !this.filters.isEmpty();
	}

	/**
	 * Gets the filters.
	 *
	 * @return the filters
	 */
	public Set<Long> getFilters() {
		return filters;
	}
	
	/**
	 * Gets the filters.
	 *
	 * @return the filters
	 */
	public String getFiltersAsString() {
		StringBuffer buffer = new StringBuffer();
		
		for (Long l:filters){
			if (buffer.length() > 0){
				buffer.append(";");
			}
			
			buffer.append(l);
		}
		
		return buffer.toString();
	}


	/**
	 * Sets the filters.
	 *
	 * @param filters
	 *            the filters to set
	 */
	public void setFilters(Set<Long> filters) {
		this.filters = filters;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.imilia.api.domain.filter.PropertyFilter#duplicate()
	 */
	@Override
	public PropertyFilter duplicate() {
		return new BitMaskPropertyFilter(this);
	}

	public final BitMaskOperator getBitMaskOperator() {
		return bitMaskOperator;
	}

	public final void setBitMaskOperator(BitMaskOperator bitMaskOperator) {
		this.bitMaskOperator = bitMaskOperator;
	}

	@Override
	public <T> T visit(PropertyFilterVisitor<T> propertyFilterVisitor) {
		return propertyFilterVisitor.accept(this);		
	}
}
