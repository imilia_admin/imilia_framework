package com.imilia.server.domain.model.property;

import com.imilia.server.domain.model.IModel;

public class PropertyImpl implements Property{

	private IModel model;
	
	private String group;
	
	private String name;
	
	private String path;
	
	private PropertyCrudProvider propertyCrudProvider;
	
	private PropertyAttributeProvider attributes;

	public PropertyImpl(IModel model){
		this.model = model;
		setPropertyCrudProvider(new DefaultCrudProvider());
		setAttributes(new DefaultAttributes());
	}
	
	public PropertyImpl(String nameAndPath){
		this((IModel)null);
		setName(nameAndPath);
		setPath(nameAndPath);
	}
	
	public PropertyImpl(IModel model, String nameAndPath){
		this(model);
		setName(nameAndPath);
		setPath(nameAndPath);
	}
	
	public PropertyImpl(IModel model, String name, String path){
		this(model);
		setName(name);
		setPath(path);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getQName() {
		return model.getName() +"."+ getName();
	}



	public PropertyCrudProvider getPropertyCrudProvider() {
		return propertyCrudProvider;
	}
	
	public void setPropertyCrudProvider(PropertyCrudProvider propertyCrudProvider) {
		this.propertyCrudProvider = propertyCrudProvider;
	}
	
	public PropertyAttributeProvider getAttributesProvider() {
		return attributes;
	}
	
	public void setAttributes(PropertyAttributeProvider attributes) {
		this.attributes = attributes;
	}

	public IModel getModel() {
		return model;
	}

	public void setModel(IModel model) {
		this.model = model;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	@Override
	public String toString() {
		return getQName();
	}
}
