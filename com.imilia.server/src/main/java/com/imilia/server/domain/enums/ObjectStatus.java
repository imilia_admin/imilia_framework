/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 * 
 * Created on: Mar 3, 2009
 * Created by: emcgreal
 */
package com.imilia.server.domain.enums;

import java.util.Locale;

import org.springframework.context.MessageSource;

import com.imilia.utils.i18n.Localized;

/**
 * The Enum ObjectStatus.
 */
public enum ObjectStatus implements Localized {

	/** The Active. */
	Active,

	/** The Deleted. */
	Deleted,

	/** The Disabled. */
	Disabled;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.imilia.server.domain.common.Localeized#getMessageId()
	 */
	public String getMessageId() {
		return ObjectStatus.class.getSimpleName() + "." + name();
	}
	
	/* (non-Javadoc)
	 * @see com.imilia.server.domain.Localized#getLocalizedMessage(org.springframework.context.MessageSource, java.util.Locale)
	 */
	public String getLocalizedMessage(MessageSource messageSource, Locale locale) {
		return messageSource.getMessage(getMessageId(), null, locale);
	}
};
