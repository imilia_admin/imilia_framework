/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 * Created on: Jan 23, 2009
 * Created by: emcgreal
 */

package com.imilia.server.domain.enums;

import java.util.Locale;

import org.springframework.context.MessageSource;

import com.imilia.utils.i18n.Localized;

/**
 * The Enum Flavor.
 */
public enum Flavor implements Localized {
		
	
	/** The Medical. */
	Medical,
	
	/** The Sports. */
	Sports,
	
	/** The Education. */
	Education,
	
	/** The Other. */
	Other;
	
	/* (non-Javadoc)
	 * @see com.imilia.server.domain.common.Localeized#getMessageId()
	 */
	/**
	 * Gets the message id.
	 * 
	 * @return the message id
	 */
	public String getMessageId() {
		return Flavor.class.getSimpleName() + "." + name();
	}

	/* (non-Javadoc)
	 * @see com.imilia.server.domain.Localized#getLocalizedMessage(org.springframework.context.MessageSource, java.util.Locale)
	 */
	public String getLocalizedMessage(MessageSource messageSource, Locale locale) {
		return messageSource.getMessage(getMessageId(), null, locale);
	}
}
