/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2014 - all rights reserved
 *
 * Created on: Jan 22, 2014
 * Created by: emcgreal
 */
package com.imilia.server.domain.model.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE,ElementType.FIELD})
public @interface Prp {
	String value();
	String path() default "";
	
	String attributes() default "";
	String cruds() default "";
	
	String list() default "";
}
