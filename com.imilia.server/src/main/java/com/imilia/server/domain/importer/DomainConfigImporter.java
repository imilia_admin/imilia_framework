package com.imilia.server.domain.importer;


public interface DomainConfigImporter {
	public void importDomainConfig() throws Exception;
}
