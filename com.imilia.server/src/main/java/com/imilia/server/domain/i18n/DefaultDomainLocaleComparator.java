/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2013 - all rights reserved
 *
 * Created on: Aug 23, 2013
 * Created by: emcgreal
 */
package com.imilia.server.domain.i18n;

import org.joda.time.Interval;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;

import com.imilia.utils.i18n.DomainLocale;
import com.imilia.utils.i18n.Localized;
import com.imilia.utils.i18n.LocalizedProperty;

/**
 * The Class DefaultDomainLocaleComparator.
 * Can handle comparing LocalizedProperty, Localized, Interval and Comparable
 * If all else fails then the default is to compare toString()
 * 
 * @author emcgreal
 */
public class DefaultDomainLocaleComparator implements DomainLocaleComparator<Object>, MessageSourceAware{

	/** The message source. */
	private MessageSource messageSource;
	
	/* (non-Javadoc)
	 * @see com.imilia.server.domain.i18n.DomainLocaleComparator#compare(java.lang.Object, java.lang.Object, com.imilia.server.domain.i18n.DomainLocale)
	 */
	@SuppressWarnings("unchecked")
	public int compare(Object lhs, Object rhs, DomainLocale domainLocale) {
		// Check for nulls - a null value is placed at the beginning
		if (lhs == null) {
			if (rhs != null) {
				return -1;
			}
			return 0;
		} else if (rhs == null) {
			if (lhs != null) {
				return 1;
			}
		}

		
		if (lhs instanceof LocalizedProperty && rhs instanceof LocalizedProperty){
			// Localized Property
			return compareAsStrings(((LocalizedProperty)lhs).getText(domainLocale),
					((LocalizedProperty)rhs).getText(domainLocale));
			
		} else if (lhs instanceof Localized && rhs instanceof Localized){
			// Localized
			final MessageSource messageSource = getMessageSource();
			return compareAsStrings(
					((Localized)lhs).getLocalizedMessage(messageSource, domainLocale.getLocale()),
					((Localized)rhs).getLocalizedMessage(messageSource, domainLocale.getLocale())
					);
		} else if (lhs instanceof Interval && rhs instanceof Interval){

			// Interval Sort intervals by begin and then duration
			Interval lhsInterval = (Interval) lhs;
			Interval rhsInterval = (Interval) rhs;
			
			int comp = lhsInterval.getStart().compareTo(rhsInterval.getStart());
			
			if (comp == 0){
				return lhsInterval.toDuration().compareTo(rhsInterval.toDuration());
			}
			return 0;
		}else if (lhs instanceof Comparable && rhs != null) {
			// Comparable
			return ((Comparable<Object>) lhs).compareTo(rhs);
		}else{
			// Default to toString as last resort
			return compareAsStrings(lhs, rhs);
		}
	}
	
	/**
	 * Compare as strings.
	 * 
	 * @param lhs the lhs
	 * @param rhs the rhs
	 * 
	 * @return the int
	 */
	protected int compareAsStrings(Object lhs, Object rhs) {
		return lhs.toString().toLowerCase().compareTo(rhs.toString().toLowerCase());
	}

	/**
	 * Gets the message source.
	 *
	 * @return the message source
	 */
	public MessageSource getMessageSource() {
		return messageSource;
	}

	/**
	 * Sets the message source.
	 *
	 * @param messageSource the new message source
	 */
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
}
