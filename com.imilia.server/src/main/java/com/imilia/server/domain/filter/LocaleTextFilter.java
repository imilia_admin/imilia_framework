/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2010 - all rights reserved
 * Created on: Jan 4, 2010
 * Created by: emcgreal
 */
package com.imilia.server.domain.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.imilia.server.validation.ValidationException;

/**
 * The Class LocaleTextFilter.
 */
public class LocaleTextFilter extends PropertyFilter{
	
	/** The Constant TAG_FILTER. */
	public final static String TAG_FILTER = "localeTextFilter";
	
	/** The Constant TAG_PREDICATES. */
	public final static String TAG_PREDICATES = "stringPredicates";
	
	/** The Constant logger. */
	private final static Logger logger = LoggerFactory
			.getLogger(StringPropertyFilter.class);

	/** The string predicates. */
	private String stringPredicates;

	/**
	 * Instantiates a new locale text filter.
	 */
	public LocaleTextFilter() {
		super();
	}

	/**
	 * Instantiates a new locale text filter.
	 *
	 * @param other the other
	 */
	public LocaleTextFilter(LocaleTextFilter other) {
		super(other);
		setStringPredicates(other.stringPredicates);
	}


	/**
	 * Instantiates a new locale text filter.
	 * 
	 * @param propertyName the property name
	 */
	public LocaleTextFilter(String propertyName) {
		super(propertyName);
	}

	/**
	 * Instantiates a new locale text filter.
	 *
	 * @param classFilter the class filter
	 * @param propertyName the property name
	 */
	public LocaleTextFilter(ClassFilter<?> classFilter, String propertyName) {
		super(classFilter, propertyName);
	}

	/**
	 * Instantiates a new locale text filter.
	 * 
	 * @param classFilter the class filter
	 * @param propertyName the property name
	 * @param propertyNameLabelMessageID the property name label message id
	 */
	public LocaleTextFilter(ClassFilter<?> classFilter, String propertyName, String propertyNameLabelMessageID) {
		super(classFilter, propertyName, propertyNameLabelMessageID);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.imilia.server.domain.filter.PropertyFilter#isValid()
	 */
	public boolean isValid() {
		return stringPredicates != null && !"".equals(stringPredicates);
	}


	/* (non-Javadoc)
	 * @see com.imilia.server.domain.filter.PropertyFilter#buildQuery(com.imilia.server.domain.filter.QueryBuilder)
	 */
	@Override
	public void buildQuery(QueryBuilder q) throws ValidationException {
		if (isValid()) {
			final StringBuffer buffer = new StringBuffer();

			// Check that its not just a simple null check
			if (!isNullOrNotNull(buffer, stringPredicates)){
				// Split the predicates up using the delimiter ;
				final String[] tokens = stringPredicates.split(";");

				for (String t : tokens) {
					if (buffer.length() > 0) {
						buffer.append(" OR ");
					}
					// Trim any empty characters
					t = t.trim();

					// Negate if necessary
					if ('!' == t.charAt(0)){
						buffer.append(" not ");
						t = t.substring(1);			// Cut out the '!'
					}

					String[] range = t.split(":");

					if (range.length == 2) {
						buffer.append(getQualifiedName());
						buffer.append(" BETWEEN ");
						buffer.append(q.addParam(range[0]));
						buffer.append(" AND ");
						buffer.append(q.addParam(range[1]));
					} else {
						buffer.append(getQualifiedName());
						buffer.append(" like '");
						buffer.append(t.replace("*", "%"));
						buffer.append("'");
					}

				}
				
				buffer.insert(0, "(");
				buffer.append( " AND ");
				buffer.append(super.getQualifiedName());
				buffer.append(".localeTexts.domainLocale = ");
				buffer.append(q.addParam(q.getDomainLocale()));
				buffer.append(")");
			}
			
			final String query = buffer.toString();
			logger.debug(query);
			q.addRestriction(query);
		}
	}
	
	/* (non-Javadoc)
	 * @see com.imilia.server.domain.filter.PropertyFilter#getQualifiedName()
	 */
	@Override
	public String getQualifiedName() {
		StringBuffer buffer = new StringBuffer();
		buffer.append(super.getQualifiedName());
		buffer.append(".localeTexts.text");
		
		return buffer.toString();
	}

	/**
	 * Gets the string predicates.
	 * 
	 * @return the stringPredicates
	 */
	public String getStringPredicates() {
		return stringPredicates;
	}

	/**
	 * Sets the string predicates.
	 * 
	 * @param stringPredicates the stringPredicates to set
	 */
	public void setStringPredicates(String stringPredicates) {
		this.stringPredicates = stringPredicates;
	}

	/**
	 * Configure.
	 *
	 * @param stringPredicates the string predicates
	 * @return the locale text filter
	 */
	public LocaleTextFilter configure(String stringPredicates) {
		setStringPredicates(stringPredicates);
		return this;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.imilia.bkf.api.domain.filter.PropertyFilter#clear()
	 */
	@Override
	public void clear() {
		this.stringPredicates = null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.imilia.api.domain.filter.PropertyFilter#duplicate()
	 */
	@Override
	public PropertyFilter duplicate() {
		return new LocaleTextFilter(this);
	}


	/* (non-Javadoc)
	 * @see com.imilia.server.domain.filter.PropertyFilter#visit(com.imilia.server.domain.filter.PropertyFilterVisitor)
	 */
	@Override
	public <T> T visit(PropertyFilterVisitor<T> propertyFilterVisitor) {
		return propertyFilterVisitor.accept(this);		
	}
}
