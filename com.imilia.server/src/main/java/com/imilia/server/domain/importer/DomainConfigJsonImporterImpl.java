/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2014 - all rights reserved
 * Created on: Mar 18, 2014
 * Created by: emcgreal
 */
package com.imilia.server.domain.importer;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.imilia.server.domain.Crud;
import com.imilia.server.domain.access.Role;
import com.imilia.server.domain.access.Visibility;
import com.imilia.server.domain.i18n.LocaleTextHolder;
import com.imilia.server.domain.query.entities.QueryJoin;
import com.imilia.server.domain.query.entities.QueryProperty;
import com.imilia.server.domain.query.entities.QueryRestriction;
import com.imilia.server.domain.query.entities.QueryTemplate;
import com.imilia.server.domain.query.entities.QueryView;
import com.imilia.server.service.impl.EbeanAwareServiceImpl;
import com.imilia.utils.ResourceBean;
import com.imilia.utils.i18n.DomainLocale;
import com.imilia.utils.strings.StringUtils;

/**
 * The Class DomainConfigJsonImporterImpl imports the roles, views and queries for the domain
 */
public class DomainConfigJsonImporterImpl extends EbeanAwareServiceImpl implements DomainConfigImporter{
	
	/** The Constant logger. */
	private final static Logger logger = LoggerFactory.getLogger(DomainConfigJsonImporterImpl.class);
	
	/** The domain locale. */
	private DomainLocale domainLocale;
	
	/** The domain config json resource bean. */
	@Autowired
	@Qualifier("domainConfigJson")
	private ResourceBean domainConfigJsonResourceBean;
	
	/** The jason factory. */
	@Autowired
	private JsonFactory jasonFactory;
	
	/** The jp. */
	JsonParser parser; 

	/** The roles. */
	final Map<String, Role> roles = new HashMap<>();

	/** The views. */
	final Map<String, QueryView> views = new HashMap<>();
	
	/** The parents. */
	final Map<String, String> parents = new HashMap<>();
	
	/** The joins. */
	final Map<QueryJoin, String> joins = new HashMap<>();

	/** The queries. */
	final Map<String, QueryTemplate> queries = new HashMap<>();
	
	/* (non-Javadoc)
	 * @see com.imilia.server.domain.importer.DomainConfigImporter#importDomainConfig()
	 */
	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public void importDomainConfig() throws Exception {
		logger.debug("Opening json config...");
		parser = jasonFactory.createJsonParser(domainConfigJsonResourceBean.getResource().getInputStream());

		logger.debug("Opened json config");

		JsonToken token = null;
		while ((token = parser.nextToken()) != null) {
			if (token == JsonToken.FIELD_NAME) {
				String name = parser.getCurrentName();
				switch(name){
				case "roles":
					handleRoles();
					break;
				case "views":
					handleViews();
					break;
				case "queries":
					handleQueries();
					break;
					default:
						logger.error("Unexpected field encountered: " + name);
				}
			}
		}
	}
	
	/**
	 * Handle roles.
	 *
	 * @throws Exception the exception
	 */
	private void handleRoles() throws Exception{
		logger.debug("Parsing roles...");
		JsonToken token = null;
		if ((token = parser.nextToken()) == JsonToken.START_ARRAY) {
			while((token = parser.nextToken()) == JsonToken.START_OBJECT){
				// Handle view
				handleRole();
			}
		}
		getEbeanServer().save(roles.values());
		logger.debug("Parsed and created "+ roles.size() + " roles");
	}

	/**
	 * Handle role.
	 *
	 * @throws Exception the exception
	 */
	private void handleRole() throws Exception {
		logger.debug("Parse role...");
		JsonToken token = null;
		Role role = new Role();
		while((token = parser.nextToken()) == JsonToken.FIELD_NAME){
			String field = parser.getCurrentName();
			String value = parser.nextTextValue();
			switch(field){
			case "name":
				logger.debug("Name: " + value);
				role.setName(LocaleTextHolder.create(getDomainLocale(), value));
				break;
			case "authority":
				role.setAuthority(value);
				logger.debug("Authority: " + value);
				break;
			case "crud":
				role.setCrud(new Crud(value));
				logger.debug("Crud: " + value);
				break;
			case "visibility":
				role.setVisibility(Visibility.valueOf(StringUtils.capitalizeFirstLetter(value)));
				logger.debug("Visibility: " + role.getVisibility());
				break;
			default:
				logger.error("Unexpected field encountered: " + field);
			}
		}	
		roles.put(role.getAuthority(), role);
		logger.debug("End role");
	}
	
	/**
	 * Handle views.
	 *
	 * @throws Exception the exception
	 */
	private void handleViews() throws Exception {
		logger.debug("Parsing views...");
		JsonToken token = null;
		if ((token = parser.nextToken()) == JsonToken.START_ARRAY) {
			while((token = parser.nextToken()) == JsonToken.START_OBJECT){
				// Handle view
				handleView();
			}
		}
		
		// Save all views (parents are not yet set)
		getEbeanServer().save(views.values());
		
		// Link parents
		for (String v: parents.keySet()){
			String parent = parents.get(v);
			
			QueryView parentView = views.get(parent);
			
			if (parentView != null){
				QueryView view = views.get(v);
				view.setParent(parentView);
			}
		}
		
		getEbeanServer().save(views.values());

		// Create the joins
		for (QueryJoin j:joins.keySet()){
			String s = joins.get(j);
			
			
			QueryView view = views.get(s);
			
			if (view == null){
				logger.error("View not found:" + s);
			}else{
				j.setQueryView(view);
			}
		}
		
		getEbeanServer().save(joins.keySet());
		logger.debug("Parsed and saved " + views.size()  +" views");
	}
	/**
	 * Handle view.
	 *
	 * @throws Exception the exception
	 */
	private void handleView() throws Exception {
		logger.debug("Parsing view...");

		JsonToken token = null;
		
		String parent = null;
		QueryView queryView = new QueryView();;
		
		while((token = parser.nextToken()) != JsonToken.END_OBJECT){
			if (token == JsonToken.FIELD_NAME){
				String field = parser.getCurrentName();
				
				switch(field){
				
				case "class":
					String className = parser.nextTextValue();
					logger.info("class: "  + className);
					queryView.setClazz(Class.forName(className));
					break;
					
				case "name":
					String name = parser.nextTextValue();
					logger.info("name: "  + name);
					queryView.setName(name);
					break;
					
				case "properties":
					if ((token = parser.nextToken()) == JsonToken.START_ARRAY) {
						while((token = parser.nextToken()) == JsonToken.START_OBJECT){
							// Handle queryViews
							handleProperty(queryView);
						}
					}
					break;
					
				case "parent":
					parent = parser.nextTextValue();
					logger.info("parent: "  + parent);
					break;
					
				case "joins":
					if ((token = parser.nextToken()) == JsonToken.START_ARRAY) {
						while((token = parser.nextToken()) == JsonToken.START_OBJECT){
							// Handle joins
							handleJoins(queryView);
						}
					}
					break;
				default:
					logger.error("Unexpected field encountered: " + field);					
				}
			}
		}
		
		// set the view name to the class name
		if (queryView.getName() == null && queryView.getClazz() != null){
			queryView.setName(queryView.getClazz().getName());
		}

		// Add the parent if present
		if (parent != null){
			parents.put(queryView.getName(), parent);
		}

		views.put(queryView.getName(), queryView);
		
		
		logger.debug("Parsed view: " + queryView.getName());
	}

	/**
	 * Handle properties.
	 *
	 * @param queryView the query view
	 * @throws Exception the exception
	 */
	private void handleProperty(QueryView queryView) throws Exception {
		logger.debug("Parsing property...");
		JsonToken token = null;
		QueryProperty queryProperty = new QueryProperty();
		while((token = parser.nextToken()) == JsonToken.FIELD_NAME){
			String field = parser.getCurrentName();
			String value = parser.nextTextValue();
			switch(field){
			case "name":
				logger.debug("Found property: " + field +  " Value: " + value);
				queryProperty.setPropertyName(value);
				break;
			case "visibility":
				queryProperty.setVisibility(Visibility.valueOf(StringUtils.capitalizeFirstLetter(value)));
				logger.debug("Property visibility: " + value);
				break;
				
			default:
				logger.error("Unexpected field encountered: " + field);
			}
		}	
		
		// No visibility defined? Then set to Public as default
		if (queryProperty.getVisibility() == null){
			queryProperty.setVisibility(Visibility.Public);
		}
		queryView.add(queryProperty);
		logger.debug("Parsed property: " + queryProperty.getPropertyName());
	}

	/**
	 * Handle joins.
	 *
	 * @param queryView the query view
	 * @throws Exception the exception
	 */
	private void handleJoins(QueryView queryView) throws Exception {
		logger.debug("Parsing joins...");
		QueryJoin queryJoin = new QueryJoin();
		JsonToken token = null;
		while((token = parser.nextToken()) == JsonToken.FIELD_NAME){
			String field = parser.getCurrentName();
			String value = parser.nextTextValue();
			switch(field){
			case "path":
				logger.debug("Path: " + value);
				queryJoin.setPath(value);
				break;
				
			case "visibility":
				queryJoin.setVisibility(Visibility.valueOf(StringUtils.capitalizeFirstLetter(value)));
				logger.debug("Visibility: " + value);
				break;
				
			case "view":
				logger.debug("View: " + value);
				joins.put(queryJoin, value);
				break;

			default:
				logger.error("Unexpected field encountered: " + field);
			}
		}		
		// set the view but don't add as the joins are saved later
		queryJoin.setQueryView(queryView);
		logger.debug("Parsed join: " + queryJoin.getPath());
	}


	/**
	 * Handle queries.
	 *
	 * @throws Exception the exception
	 */
	private void handleQueries() throws Exception {
		logger.debug("Parsing queries...");
		JsonToken token = null;
		if ((token = parser.nextToken()) == JsonToken.START_ARRAY) {
			while((token = parser.nextToken()) == JsonToken.START_OBJECT){
				// Handle view
				handleQuery();
			}
		}
		getEbeanServer().save(queries.values());
		logger.debug("Parsed " + queries.size() +" queries");
	}
	
	/**
	 * Handle query.
	 *
	 * @throws Exception the exception
	 */
	private void handleQuery() throws Exception {
		logger.debug("Parsing query...");
		QueryTemplate queryTemplate = new QueryTemplate();
		JsonToken token = null;
		while((token = parser.nextToken()) == JsonToken.FIELD_NAME){
			String field = parser.getCurrentName();
			String value = null;
			switch(field){
			case "name":
				value = parser.nextTextValue();
				logger.debug("name: " + value);
				queryTemplate.setName(value);
				break;
				
			case "queryText":
				value = parser.nextTextValue();
				logger.debug("queryText: " + value);
				queryTemplate.setQueryText(value);
				break;
				
			case "view":
				value = parser.nextTextValue();
				logger.debug("view: " + value);
				QueryView view = views.get(value);
				queryTemplate.setQueryView(view);
				
				if (view == null){
					logger.error("View not found:" + value);
				}
				break;
			case "restrictions":
				getEbeanServer().save(queryTemplate);
				handleRestrictions(queryTemplate);
				break;
				
			default:
				logger.error("Unexpected field encountered: " + field);
			}
		}	
		getEbeanServer().save(queryTemplate);
		queries.put(queryTemplate.getName(), queryTemplate);
		logger.debug("Parsed query");
	}

	/**
	 * Handle restrictions.
	 *
	 * @param queryTemplate the query template
	 * @throws Exception the exception
	 */
	private void handleRestrictions(QueryTemplate queryTemplate) throws Exception {
		logger.debug("Parsing roles...");
		JsonToken token = null;
		if ((token = parser.nextToken()) == JsonToken.START_ARRAY) {
			while((token = parser.nextToken()) == JsonToken.START_OBJECT){
				// Handle view
				handleRestriction(queryTemplate);
			}
		}
		getEbeanServer().save(roles.values());
		logger.debug("Parsed and created "+ roles.size() + " roles");
	}

	/**
	 * Handle restriction.
	 *
	 * @param queryTemplate the query template
	 * @throws Exception the exception
	 */
	private void handleRestriction(QueryTemplate queryTemplate) throws Exception {
		logger.debug("Parsing restrictions...");

		final QueryRestriction restriction = new QueryRestriction();
		queryTemplate.add(restriction);
		
		JsonToken token = null;
		while((token = parser.nextToken()) == JsonToken.FIELD_NAME){
			String field = parser.getCurrentName();
			String value = null;
			switch(field){
			case "restriction":
				value = parser.nextTextValue();
				logger.debug("restriction:" + value);
				restriction.setRestriction(value);
				break;
			case "crud":
				value = parser.nextTextValue();
				logger.debug("crud:" + value);
				restriction.setRestrictionCrud(new Crud(value));
				break;
			case "roles":
				handleRestrictionRoles(restriction);
				break;
			default:
				logger.error("Unexpected field encountered: " + field);				
			}
		}
		logger.debug("Parsed restrictions");

	}
	
	/**
	 * Handle restriction roles.
	 *
	 * @param restriction the restriction
	 * @throws Exception the exception
	 */
	private void handleRestrictionRoles(QueryRestriction restriction) throws Exception {
		logger.debug("Parsing restriction roles...");
		JsonToken token = null;
		if ((token = parser.nextToken()) == JsonToken.START_ARRAY) {
			while((token = parser.nextToken()) == JsonToken.VALUE_STRING){
				String roleName = parser.getValueAsString();
				
				Role role = roles.get(roleName);
				
				if (role != null){
					logger.debug("Adding role: " + roleName);
					restriction.addRole(role);
				}else{
					logger.error("Failed to find role:" + roleName);
				}
			}
		}
		logger.debug("Parsed and created "+ roles.size() + " restriction roles");
	}
	

	// Getter and setter methods ----------------------------------------------
	/**
	 * Gets the jason factory.
	 *
	 * @return the jason factory
	 */
	public JsonFactory getJasonFactory() {
		return jasonFactory;
	}

	/**
	 * Sets the jason factory.
	 *
	 * @param jasonFactory the new jason factory
	 */
	public void setJasonFactory(JsonFactory jasonFactory) {
		this.jasonFactory = jasonFactory;
	}

	/**
	 * Gets the domain locale.
	 *
	 * @return the domain locale
	 */
	public DomainLocale getDomainLocale() {
		return domainLocale;
	}

	/**
	 * Sets the domain locale.
	 *
	 * @param domainLocale the new domain locale
	 */
	public void setDomainLocale(DomainLocale domainLocale) {
		this.domainLocale = domainLocale;
	}

	/**
	 * Gets the domain config json resource bean.
	 *
	 * @return the domain config json resource bean
	 */
	public ResourceBean getDomainConfigJsonResourceBean() {
		return domainConfigJsonResourceBean;
	}

	/**
	 * Sets the domain config json resource bean.
	 *
	 * @param domainConfigJsonResourceBean the new domain config json resource bean
	 */
	public void setDomainConfigJsonResourceBean(
			ResourceBean domainConfigJsonResourceBean) {
		this.domainConfigJsonResourceBean = domainConfigJsonResourceBean;
	}
}
