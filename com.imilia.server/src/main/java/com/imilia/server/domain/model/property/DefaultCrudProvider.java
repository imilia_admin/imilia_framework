package com.imilia.server.domain.model.property;

import com.imilia.server.domain.Crud;

public class DefaultCrudProvider implements PropertyCrudProvider {

	private Crud crud;
	
	public DefaultCrudProvider(){
		crud = new Crud(Crud.READABLE);
	}

	public Crud getCrud(Property property) {
		return crud;
	}

	public void setPermission(Crud crud) {
		this.crud = crud;
	}

	
}
