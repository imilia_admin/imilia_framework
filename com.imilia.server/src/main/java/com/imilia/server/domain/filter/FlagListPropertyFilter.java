/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2010 - all rights reserved
 * Created on: Aug 10, 2010
 * Created by: emcgreal
 */

package com.imilia.server.domain.filter;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.imilia.server.validation.ValidationException;

/**
 * The Class FlagListPropertyFilter.
 */
public class FlagListPropertyFilter extends PropertyFilter {

	/** The Constant logger. */
	private final static Logger logger = LoggerFactory
			.getLogger(FlagListPropertyFilter.class);

	/** The Constant TAG_FLAGLIST_FILTER. */
	public final static String TAG_FLAGLIST_FILTER = "FlagListPropertyFilter";
	
	/** The Constant TAG_FILTERS. */
	public final static String TAG_FILTERS = "filters";
	
	/** The filters. */
	private HashMap<Long, Boolean> filters = new HashMap<Long, Boolean>();
	
	/**
	 * Instantiates a new flag list property filter.
	 */
	public FlagListPropertyFilter() {
		super();
	}

	/**
	 * Instantiates a new flag list property filter.
	 *
	 * @param other the other
	 */
	public FlagListPropertyFilter(FlagListPropertyFilter other) {
		super(other);
		getFilters().putAll(other.getFilters());
	}

	
	/**
	 * The Constructor.
	 * 
	 * @param classFilter the class filter
	 * @param propertyName the property name
	 */
	public FlagListPropertyFilter(ClassFilter<?> classFilter, String propertyName) {
		super(classFilter, propertyName);
	}
	
	/**
	 * The Constructor.
	 * 
	 * @param classFilter the class filter
	 * @param propertyName the property name
	 * @param propertyNameLabelMessageID the property name label message id
	 */
	public FlagListPropertyFilter(ClassFilter<?> classFilter, String propertyName, String propertyNameLabelMessageID) {
		super(classFilter, propertyName, propertyNameLabelMessageID);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.imilia.bkf.api.domain.filter.PropertyFilter#clear()
	 */
	@Override
	public void clear() {
		this.filters.clear();
	}

	/* (non-Javadoc)
	 * @see com.imilia.server.domain.filter.PropertyFilter#buildQuery(com.imilia.server.domain.filter.QueryBuilder)
	 */
	@Override
	public void buildQuery(QueryBuilder q) throws ValidationException {
		if (isValid()) {
			StringBuffer buffer = new StringBuffer();
		
			for (long flag:filters.keySet()){	
				if (buffer.length() > 0) {
					buffer.append(" AND ");
				}
				if (q.getDatabasePlatform().getName().equals("oracle")){
					buffer.append("bitand(");
					buffer.append(getQualifiedName());
					buffer.append(", ");
					buffer.append(flag);
					buffer.append(")");
				}else{
					buffer.append(getQualifiedName());
					buffer.append(" & ");
					buffer.append(flag);
				}
				if (filters.get(flag)){
					buffer.append( " != 0");
				}else{
					buffer.append( " = 0");
				}
				
			}
			// Add a parenthesis if required 
			if (filters.size() > 1){
				buffer.insert(0, "(");
				buffer.append(")");
			}
			final String query = buffer.toString();
			logger.debug(query);
			q.addRestriction(query);
		}
	}
	
	/* (non-Javadoc)
	 * @see com.imilia.server.domain.filter.PropertyFilter#getFilterPropertyName()
	 */
	public String getFilterPropertyName() {
		return "filters";
	}
	/*
	 * (non-Javadoc)
	 *
	 * @see com.imilia.bkf.api.domain.filter.PropertyFilter#isValid()
	 */
	@Override
	public boolean isValid() {
		return !this.filters.isEmpty();
	}

	/**
	 * Gets the filters.
	 *
	 * @return the filters
	 */
	public String getFiltersAsString() {
		StringBuffer buffer = new StringBuffer();
		
		for (Long l:filters.keySet()){
			if (buffer.length() > 0){
				buffer.append(";");
			}
			
			buffer.append(l);
		}
		
		return buffer.toString();
	}


	/*
	 * (non-Javadoc)
	 *
	 * @see com.imilia.api.domain.filter.PropertyFilter#duplicate()
	 */
	@Override
	public PropertyFilter duplicate() {
		return new FlagListPropertyFilter(this);
	}

	/* (non-Javadoc)
	 * @see com.imilia.server.domain.filter.PropertyFilter#visit(com.imilia.server.domain.filter.PropertyFilterVisitor)
	 */
	@Override
	public <T> T visit(PropertyFilterVisitor<T> propertyFilterVisitor) {
		return propertyFilterVisitor.accept(this);		
	}

	/**
	 * Gets the filters.
	 *
	 * @return the filters
	 */
	public HashMap<Long, Boolean> getFilters() {
		return filters;
	}

	/**
	 * Sets the filters.
	 *
	 * @param filters the filters
	 */
	public void setFilters(HashMap<Long, Boolean> filters) {
		this.filters = filters;
	}
}
