/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2014 - all rights reserved
 * Created on: Feb 19, 2014
 * Created by: emcgreal
 */

package com.imilia.server.domain.access;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

import org.springframework.security.core.GrantedAuthority;

import com.imilia.server.domain.Crud;
import com.imilia.server.domain.DomainObject;
import com.imilia.server.domain.filter.RestrictionCandidate;
import com.imilia.server.domain.i18n.LocaleTextHolder;

/**
 * The Class Role represents a role within an application.
 */
@Entity
public class Role extends DomainObject implements GrantedAuthority{
	
	/** The authority. */
	@Column(unique=true)
	private String authority;
	
	/** The name. */
	@ManyToOne(optional=false, cascade={CascadeType.ALL})
	@RestrictionCandidate
	@NotNull
	private LocaleTextHolder name;

	/** The subscriptions. */
	@OneToMany
	private List<SubscriptionRole> subscriptions;
	
	/** The model cruds. */
	@OneToMany(mappedBy="role", cascade=CascadeType.ALL)
	private List<ModelCrud> modelCruds;

	/** The default crud for this role. */
	@Embedded
	private Crud crud;
	
	@Embedded
	private Visibility visibility;
	
	public Role(){
		super();
	}
	/**
	 * Instantiates a new role.
	 *
	 * @param authority the authority
	 * @param name the name
	 * @param crud the default crud
	 */
	public Role(String authority, LocaleTextHolder name, Crud crud) {
		setAuthority(authority);
		setCrud(crud);
		setName(name);
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public LocaleTextHolder getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(LocaleTextHolder name) {
		this.name = name;
	}
	
	/* (non-Javadoc)
	 * @see org.springframework.security.core.GrantedAuthority#getAuthority()
	 */
	public String getAuthority() {
		return authority;
	}
	
	/**
	 * Sets the authority.
	 *
	 * @param authority the new authority
	 */
	public void setAuthority(String authority){
		this.authority = authority;
	}

	/**
	 * Gets the default crud.
	 *
	 * @return the default crud
	 */
	public Crud getCrud() {
		return crud;
	}

	/**
	 * Sets the default crud.
	 *
	 * @param crud the new default crud
	 */
	public void setCrud(Crud crud) {
		this.crud = crud;
	}

	/**
	 * Gets the subscriptions.
	 *
	 * @return the subscriptions
	 */
	public List<SubscriptionRole> getSubscriptions() {
		return subscriptions;
	}

	/**
	 * Sets the subscriptions.
	 *
	 * @param subscriptions the new subscriptions
	 */
	public void setSubscriptions(List<SubscriptionRole> subscriptions) {
		this.subscriptions = subscriptions;
	}

	/**
	 * Gets the model cruds.
	 *
	 * @return the model cruds
	 */
	public List<ModelCrud> getModelCruds() {
		return modelCruds;
	}

	/**
	 * Sets the model cruds.
	 *
	 * @param modelCruds the new model cruds
	 */
	public void setModelCruds(List<ModelCrud> modelCruds) {
		this.modelCruds = modelCruds;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return getAuthority();
	}
	
	/**
	 * Gets the model crud.
	 *
	 * @param modelName the model name
	 * @return the model crud
	 */
	public ModelCrud getModelCrud(String modelName){
		for (ModelCrud mp:getModelCruds()){
			if (mp.getName().equals(modelName)){
				return mp;
			}
		}
		return null;
	}
	public Visibility getVisibility() {
		return visibility;
	}
	public void setVisibility(Visibility visibility) {
		this.visibility = visibility;
	}
}
