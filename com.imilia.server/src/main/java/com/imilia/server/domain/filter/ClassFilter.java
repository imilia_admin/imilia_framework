/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 *
 * Created on: Mar 31, 2009
 * Created by: eddiemcgreal
 */

package com.imilia.server.domain.filter;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import org.joda.time.DateMidnight;
import org.joda.time.DateTime;

import com.imilia.server.domain.DomainObjectUtils;
import com.imilia.server.domain.i18n.LocaleTextHolder;
import com.imilia.server.domain.model.list.GenericObjectListModel;
import com.imilia.utils.Flags;
import com.imilia.utils.classes.ClassUtils;
import com.imilia.server.validation.ValidationException;

/**
 * A ClassFilter is used to generate restriction candidates and joins for
 * queries
 * 
 * <p>
 * Applies restriction criteria to a domain object in association with a crud
 * filter and a recipe
 * </p>.
 *
 * @param <T> the generic type
 */
public class ClassFilter<T> {

	/** The Constant TAG_FILTER. */
	public final static String TAG_FILTER = "classFilter";
	
	/** The Constant TAG_CLAZZ. */
	public final static String TAG_CLAZZ = "clazz";
	
	/** The Constant TAG_PATH. */
	public final static String TAG_PATH = "path";
	
	/** The Constant TAG_FLAGS. */
	public final static String TAG_FLAGS = "flags";
	
	/** The Constant TAG_RESTRICTION. */
	public final static String TAG_RESTRICTION = "restriction";
	
	/** The Constant TAG_CLASS_JOINS. */
	public final static String TAG_CLASS_JOINS = "classJoins";

	/** The clazz. */
	private Class<T> clazz;

	/** The path. */
	private String path;

	/** The Constant HIDDEN. */
	public final static int FILTER_VISIBLE 	= 0x00000001;
	
	/** The Constant RESULT_VISIBLE. */
	public final static int RESULT_VISIBLE 	= 0x00000002;

	/** The flags. */
	private long flags = FILTER_VISIBLE | RESULT_VISIBLE;

	/** The property filters. */
	private List<PropertyFilter> propertyFilters = new ArrayList<PropertyFilter>(0);

	/** The class joins. */
	private List<ClassJoin> classJoins = new ArrayList<ClassJoin>(0);

	/** The restriction. */
	private String restriction;
	
	/** The prepared. */
	private boolean prepared;
	
	/** The order by. */
	private String orderBy;
	
	/**
	 * The Constructor.
	 */
	public ClassFilter() {
	}

	/**
	 * The Constructor.
	 * 
	 * @param clazz the clazz
	 */
	public ClassFilter(Class<T> clazz) {
		this.clazz = clazz;
	}

	/**
	 * The Constructor.
	 * 
	 * @param clazz the clazz
	 * @param path the path
	 */
	public ClassFilter(Class<T> clazz, String path) {
		this.clazz = clazz;
		this.path = path;
	}

	/**
	 * The Constructor.
	 *
	 * @param clazz the clazz
	 * @param path the path
	 * @param flags the flags
	 */
	public ClassFilter(Class<T> clazz, String path, long flags) {
		this(clazz, path);
		this.flags = flags;
	}

	/**
	 * Creates the property filters from annotations.
	 * 
	 * @param filterModel the filter model
	 */
	public void createPropertyFiltersFromAnnotations(final FilterModel<?,?> filterModel){
		if (!prepared){
			createPropertyFiltersFromAnnotations(this, filterModel);
			prepared = true;
		}
	}
	
	/**
	 * Creates the property filters from annotations.
	 * 
	 * @param classFilter the class filter
	 * @param filterModel the filter model
	 */
	private void createPropertyFiltersFromAnnotations(final ClassFilter<?> classFilter, 
			final FilterModel<?,?> filterModel){
		final List<Field> fields = 
			DomainObjectUtils.getRestrictionCandidates(getClazz());
		
		for (Field field : fields) {
			
			RestrictionCandidate rc = field
			.getAnnotation(RestrictionCandidate.class);
			
			PropertyFilter propertyFilter = findPropertyFilter(rc, field, filterModel);

			if (propertyFilter == null){
				final Class<?> type = field.getType();
				if (rc.list()) {
					propertyFilter = addListRestriction(field, filterModel);
				}else if (rc.flagList()){
					propertyFilter = addFlagListRestriction(field.getName(), field.getType(), rc.minSelected() > 0, filterModel);
				}else if (Long.class.isAssignableFrom(type)
						|| long.class.isAssignableFrom(type)) {
					propertyFilter = addLongRestriction(field);
				} else if (int.class.isAssignableFrom(type)
						|| Integer.class.isAssignableFrom(type)) {
					propertyFilter = addIntegerRestriction(field);
				} else if (Double.class.isAssignableFrom(type)
						|| double.class.isAssignableFrom(type)) {
					propertyFilter = addDoubleRestriction(field);
				} else if (Boolean.class.isAssignableFrom(type)
						|| boolean.class.isAssignableFrom(type)) {
					propertyFilter = addBooleanRestriction(field);
				} else if (DateMidnight.class.isAssignableFrom(type)) {
					DatePropertyFilter f = addDateRestriction(field);
					propertyFilter = f;
				} else if (DateTime.class.isAssignableFrom(type)) {
					propertyFilter = addDateTimeRestriction(field);
				}  else if (LocaleTextHolder.class.isAssignableFrom(type)) {
					propertyFilter = addLocaleTextHolder(field);
				} else if (type.isEnum()){
					propertyFilter = addEnumRestriction(field);
				}else {
					propertyFilter = addStringRestriction(field);
				}
				
				// Set the label name and mandatory 
				if (propertyFilter != null){
					
					if (!"".equals(rc.name())){
						propertyFilter.setPropertyNameLabelMessageID(rc.name());
					}else if (propertyFilter.getPropertyNameLabelMessageID() == null){
						propertyFilter.setPropertyNameLabelMessageID(getQualifiedFieldName(field));
					}
					
					if (!"".equals(rc.path())){
						propertyFilter.setPropertyName(rc.path());
					}
					
					propertyFilter.setMandatory(rc.mandatory());
					propertyFilter.setAdvanced(rc.advanced());
				}
			}
		}
		
		// add any join filters
		for (ClassJoin cj : classFilter.getClassJoinsSorted()) {
			addJoins(cj, filterModel);
		}
	}

	/**
	 * Find property filter.
	 *
	 * @param rc the rc
	 * @param field the field
	 * @param filterModel the filter model
	 * @return the property filter
	 */
	private PropertyFilter findPropertyFilter(RestrictionCandidate rc, Field field, FilterModel<?,?> filterModel) {
		if (rc.list()){
			return findPropertyFilter(getListPropertyName(field, filterModel));
		}else{
			return findPropertyFilter(field.getName());
		}
		
	}

	/**
	 * Find property filter.
	 *
	 * @param propertyName the property name
	 * @return the property filter
	 */
	private PropertyFilter findPropertyFilter(String propertyName) {
		for (PropertyFilter f:propertyFilters){
			if (f.getPropertyName().equals(propertyName)){
				return f;
			}
		}
		
		return null;
	}

	/**
	 * Adds the joins.
	 * 
	 * @param cj the cj
	 * @param filterModel the filter model
	 */
	private void addJoins(final ClassJoin cj, final FilterModel<?,?> filterModel) {
		final ClassFilter<?> classFilter = cj.getClassFilter();
		classFilter.createPropertyFiltersFromAnnotations(filterModel);
	}

	
	/**
	 * Adds the locale text holder.
	 *
	 * @param field the field
	 * @return the locale text filter
	 */
	private LocaleTextFilter addLocaleTextHolder(Field field) {
		return new LocaleTextFilter(this, field.getName(), getQualifiedFieldName(field));
	}

	/**
	 * Adds the date time restriction.
	 *
	 * @param field the field
	 * @return the date time property filter
	 */
	private DateTimePropertyFilter addDateTimeRestriction(Field field) {
		DateTimePropertyFilter f = new DateTimePropertyFilter(this, field.getName(), getQualifiedFieldName(field) );
		RestrictionCandidate rc = field.getAnnotation(RestrictionCandidate.class);
		f.setTimeRestricted(rc.timeRestricted());
		return f;
	}
	
	/**
	 * Adds the date restriction.
	 *
	 * @param field the field
	 * @return the date property filter
	 */
	private DatePropertyFilter addDateRestriction(Field field) {
		return new DatePropertyFilter(this, field.getName(), getQualifiedFieldName(field) );
	}

	/**
	 * Adds the boolean restriction.
	 *
	 * @param field the field
	 * @return the boolean property filter
	 */
	private BooleanPropertyFilter addBooleanRestriction(Field field) {
		return new BooleanPropertyFilter(this, field.getName(), getQualifiedFieldName(field) );
	}

	/**
	 * Adds the double restriction.
	 *
	 * @param field the field
	 * @return the double property filter
	 */
	private DoublePropertyFilter addDoubleRestriction(Field field) {
		return new DoublePropertyFilter(this, field.getName(), getQualifiedFieldName(field));
	}

	/**
	 * Adds the integer restriction.
	 *
	 * @param field the field
	 * @return the integer property filter
	 */
	private IntegerPropertyFilter addIntegerRestriction(Field field) {
		return new IntegerPropertyFilter(this, field.getName(), getQualifiedFieldName(field));
	}

	/**
	 * Adds the long restriction.
	 *
	 * @param field the field
	 * @return the long property filter
	 */
	private LongPropertyFilter addLongRestriction(Field field) {
		return new LongPropertyFilter(this, field.getName(), 0, getQualifiedFieldName(field));
	}
	
	/**
	 * Adds the enum restriction.
	 *
	 * @param field the field
	 * @return the enum property filter
	 */
	@SuppressWarnings("unchecked")
	private EnumPropertyFilter addEnumRestriction(Field field) {
		return new EnumPropertyFilter(this, field.getName(), 
				(Class<? extends Enum<?>>) field.getType(), 
				getQualifiedFieldName(field));
	}
	/**
	 * Adds the string restriction.
	 * 
	 * @param field the field
	 * 
	 * @return the string property filter
	 */
	private StringPropertyFilter addStringRestriction(Field field) {
		return new StringPropertyFilter(this, field.getName(), getQualifiedFieldName(field));
	}
	
	/**
	 * Adds the list restriction.
	 *
	 * @param field the field
	 * @param filterModel the filter model
	 * @return the property filter
	 */
	private PropertyFilter addListRestriction(Field field, FilterModel<?,?> filterModel) {
		final RestrictionCandidate r = 
			field.getAnnotation(RestrictionCandidate.class);

		BitMaskOperator bitMaskOperator = null;
		if (r.bitmask()){
			bitMaskOperator = r.bitMaskOperator();
		}
		return addListRestriction(field.getName(), field.getType(), bitMaskOperator, r.minSelected() > 0, filterModel);
	}
	
	/**
	 * Adds the list restriction.
	 *
	 * @param fieldName the field name
	 * @param fieldType the field type
	 * @param bitMaskOperator the bit mask operator
	 * @param mandatory the mandatory
	 * @param filterModel the filter model
	 * @return the property filter
	 */
	public PropertyFilter addListRestriction(String fieldName, Class<?> fieldType, 
			BitMaskOperator bitMaskOperator, boolean mandatory,
			FilterModel<?,?> filterModel) {

		PropertyFilter propertyFilter = null;

		final String listModelName = getClazz().getName() + "." + fieldName;
		
		GenericObjectListModel<?> listModel = filterModel.getListModel(listModelName);
		
		if (listModel != null){
			String propertyName = fieldName;
			Class<?> propertyType = fieldType;
			final String storeProperty = listModel.getStoreProperty(); 
			if (!".".equals(storeProperty)) {
				if (!propertyType.isPrimitive()){
					Field storeField = ClassUtils.findField(propertyType, storeProperty);
				
					if (storeField != null){
						propertyType = storeField.getType();
						propertyName = fieldName + "." + storeField.getName();
					}
				}
			}
			
			if (Long.class.isAssignableFrom(propertyType)
					|| long.class.isAssignableFrom(propertyType)) {
				if (bitMaskOperator != null){
					propertyFilter = new BitMaskPropertyFilter(this, propertyName, bitMaskOperator);
				}else{
					propertyFilter = new LongPropertyFilter(this, propertyName);
				}
			} else if (int.class.isAssignableFrom(propertyType)
					|| Integer.class.isAssignableFrom(propertyType)) {
					propertyFilter = new IntegerPropertyFilter(this, propertyName);
			} else if (Double.class.isAssignableFrom(propertyType)
					|| double.class.isAssignableFrom(propertyType)) {
			} else if (DateMidnight.class.isAssignableFrom(propertyType)) {
			} else {
				propertyFilter = new StringListPropertyFilter(this, propertyName);
			}
	
			if (propertyFilter != null){
				propertyFilter.setList(true);
				propertyFilter.setListModelName(listModelName);
				propertyFilter.setMandatory(mandatory);
				propertyFilter.setPropertyNameLabelMessageID(listModelName);
				
			}
			
		}
		return propertyFilter;
	}
	
	public PropertyFilter addFlagListRestriction(String fieldName, Class<?> fieldType, 
			boolean mandatory,
			FilterModel<?,?> filterModel) {

		PropertyFilter propertyFilter = null;

		final String listModelName = getClazz().getName() + "." + fieldName;
		
		GenericObjectListModel<?> listModel = filterModel.getListModel(listModelName);
		
		if (listModel != null){
			String propertyName = fieldName;
			Class<?> propertyType = fieldType;
			final String storeProperty = listModel.getStoreProperty(); 
			if (!".".equals(storeProperty)) {
				if (!propertyType.isPrimitive()){
					Field storeField = ClassUtils.findField(propertyType, storeProperty);
				
					if (storeField != null){
						propertyType = storeField.getType();
						propertyName = fieldName + "." + storeField.getName();
					}
				}
			}
			
			if (Long.class.isAssignableFrom(propertyType)
					|| long.class.isAssignableFrom(propertyType)) {
				
				propertyFilter = new FlagListPropertyFilter(this, propertyName);
				
			} else if (int.class.isAssignableFrom(propertyType)
					|| Integer.class.isAssignableFrom(propertyType)) {
					propertyFilter = new IntegerPropertyFilter(this, propertyName);
			} else if (Double.class.isAssignableFrom(propertyType)
					|| double.class.isAssignableFrom(propertyType)) {
			} else if (DateMidnight.class.isAssignableFrom(propertyType)) {
			} else {
				propertyFilter = new StringListPropertyFilter(this, propertyName);
			}
	
			if (propertyFilter != null){
				propertyFilter.setListModelName(listModelName);
				propertyFilter.setMandatory(mandatory);
				propertyFilter.setPropertyNameLabelMessageID(listModelName);
				
			}
			
		}
		return propertyFilter;
	}
	
	/**
	 * Gets the list property name.
	 * 
	 * @param field the field
	 * @param filterModel the filter model
	 * 
	 * @return the list property name
	 */
	private String getListPropertyName(Field field, FilterModel<?,?> filterModel){
		final String listModelName = getClazz().getName() + "." + field.getName();
		
		GenericObjectListModel<?> listModel = filterModel.getListModel(listModelName);

		String propertyName = field.getName();

		if (listModel != null){
			Class<?> propertyType = field.getType();
			final String storeProperty = listModel.getStoreProperty(); 
			if (!".".equals(storeProperty)) {
				if (!propertyType.isPrimitive()){
					Field storeField = ClassUtils.findField(propertyType, storeProperty);
					
					if (storeField != null){
						propertyType = storeField.getType();
						propertyName = field.getName() + "." + storeField.getName();
					}
				}
			}
			
		}
		
		return propertyName;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.spiconsult.vera.api.domain.DomainObjectFilter#getpath()
	 */
	/**
	 * Gets the path.
	 * 
	 * @return the path
	 */
	public String getPath() {
		return path;
	}

	/**
	 * Sets the path.
	 * 
	 * @param path the new path
	 */
	public void setPath(String path) {
		this.path = path;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.spiconsult.vera.api.domain.DomainObjectFilter#getClazz()
	 */
	/**
	 * Gets the clazz.
	 * 
	 * @return the clazz
	 */
	public Class<T> getClazz() {
		return clazz;
	}

	/**
	 * Sets the clazz.
	 * 
	 * @param clazz the new clazz
	 */
	public void setClazz(Class<T> clazz) {
		this.clazz = clazz;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.spiconsult.vera.api.domain.DomainObjectFilter#getDomainClassName()
	 */
	/**
	 * Gets the domain class name.
	 * 
	 * @return the domain class name
	 */
	public String getDomainClassName() {
		return clazz.getName();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.spiconsult.vera.api.domain.DomainObjectFilter#qualifyName(java.lang
	 * .String)
	 */
	/**
	 * Qualify name.
	 * 
	 * @param propertyName the property name
	 * 
	 * @return the string
	 */
	public String qualifyName(String propertyName) {
		return path + "." + propertyName;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.spiconsult.vera.api.domain.DomainObjectFilter#duplicate()
	 */
	/**
	 * Duplicate.
	 * 
	 * @param recipe the recipe
	 * 
	 * @return the class filter
	 */
	public ClassFilter<T> duplicate(Recipe<T> recipe) {
		final ClassFilter<T> lhs = new ClassFilter<T>(clazz, path);
		lhs.flags = flags;

		for (PropertyFilter pf : getPropertyFilters()) {
			PropertyFilter duplicate = pf.duplicate();
			
			lhs.add(duplicate);
		}

		for (ClassJoin cj : getClassJoins()) {
			// Add the duplicate automatically
			cj.duplicate(lhs);
		}
		return lhs;
	}

	/**
	 * Gets the property filters.
	 * 
	 * @return the propertyFilters
	 */
	public List<PropertyFilter> getPropertyFilters() {
		return propertyFilters;
	}

	/**
	 * Sets the property filters.
	 * 
	 * @param propertyFilters the propertyFilters to set
	 */
	public void setPropertyFilters(List<PropertyFilter> propertyFilters) {
		this.propertyFilters = propertyFilters;
	}

	/**
	 * Adds the.
	 * 
	 * @param propertyFilter the property filter
	 */
	public void add(PropertyFilter propertyFilter) {
		getPropertyFilters().add(propertyFilter);
		propertyFilter.setClassFilter(this);
	}
	
	/**
	 * Adds the property filters.
	 *
	 * @param classFilter the class filter
	 * @param propertyFilters the property filters
	 */
	public void addPropertyFilters(Class<?> classFilter, PropertyFilter...propertyFilters){
		if (getClazz().equals(classFilter)){
			for (PropertyFilter f:propertyFilters){
				add(f.duplicate());
			}
		}
			
		// Recurse into joined classes
		for (ClassJoin cj:getClassJoins()){
			cj.getClassFilter().addPropertyFilters(classFilter, propertyFilters);
		}
	}

	/**
	 * Adds the.
	 * 
	 * @param classJoin the class join
	 */
	public void add(ClassJoin classJoin) {
		this.classJoins.add(classJoin);
	}

	/**
	 * Gets the class joins.
	 * 
	 * @return the joins
	 */
	public List<ClassJoin> getClassJoins() {
		return classJoins;
	}

	/**
	 * Sets the class joins.
	 * 
	 * @param classJoins the joins to set
	 */
	public void setClassJoins(List<ClassJoin> classJoins) {
		this.classJoins = classJoins;
	}

	/**
	 * Gets the property filter.
	 * 
	 * @param propertyName the property name
	 * 
	 * @return the property filter
	 */
	public PropertyFilter getPropertyFilter(String propertyName) {
		for (PropertyFilter filter : getPropertyFilters()) {
			if (filter.getPropertyName().equals(propertyName)) {
				return filter;
			}
		}
		return null;
	}

	/**
	 * Join.
	 * 
	 * @param path the path
	 * @param query the query
	 */
	public void join(String path, QueryBuilder query) {
		if (!getClassJoins().isEmpty()) {
			for (ClassJoin j : getClassJoins()) {
				j.toOql(query);

				if (!j.getClassFilter().getClassJoins().isEmpty()) {
					j.getClassFilter()
							.join(j.getClassFilter().getPath(), query);
				}
			}
		}
	}

	/**
	 * Gets the all mandatory criterion.
	 * 
	 * @return a list of all mandatory criteria
	 */
	public List<PropertyFilter> getAllMandatoryCriterion() {
		ArrayList<PropertyFilter> allMandatoryCriterion = new ArrayList<PropertyFilter>();

		for (ClassFilter<?> classFilter : getClassFilters()) {
			for (PropertyFilter c : classFilter.getPropertyFilters()) {
				if (c.isMandatory()) {
					allMandatoryCriterion.add(c);
				}
			}
		}
		return allMandatoryCriterion;
	}

	/**
	 * Gets the all optional criterion.
	 * 
	 * @return a list of all optional criteria
	 */
	public List<PropertyFilter> getAllOptionalCriterion() {
		ArrayList<PropertyFilter> allOptionalCriterion = new ArrayList<PropertyFilter>();

		for (ClassFilter<?> classFilter : getClassFilters()) {
			for (PropertyFilter c : classFilter.getPropertyFilters()) {
				if (!c.isMandatory()) {
					allOptionalCriterion.add(c);
				}
			}
		}
		return allOptionalCriterion;
	}

	/**
	 * Gets the class filters.
	 * 
	 * @return the class filters
	 */
	public List<ClassFilter<?>> getClassFilters() {
		List<ClassFilter<?>> filters = new ArrayList<ClassFilter<?>>();
		filters.add(this);

		for (ClassJoin cj : getClassJoinsSorted()) {
			filters.add(cj.getClassFilter());
		}

		return filters;
	}

	/**
	 * Gets the class joins sorted.
	 * 
	 * @return the class joins sorted
	 */
	public List<ClassJoin> getClassJoinsSorted() {
		ArrayList<ClassJoin> classJoins = new ArrayList<ClassJoin>(
				getClassJoins());

		Collections.sort(classJoins, new Comparator<ClassJoin>() {

			public int compare(ClassJoin lhs, ClassJoin rhs) {
				if (lhs.getPosition() > rhs.getPosition()) {
					return 1;
				} else if (lhs.getPosition() < rhs.getPosition()) {
					return -1;
				}
				return 0;
			}
		});
		return classJoins;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.spiconsult.vera.api.domain.DomainObjectCrudFilter#clearOptionalCriteria
	 * ()
	 */
	/**
	 * Clear optional criteria.
	 */
	public void clearOptionalCriteria() {

		for (ClassFilter<?> classFilter : getClassFilters()) {
			Iterator<PropertyFilter> iter = classFilter.getPropertyFilters()
					.iterator();

			while (iter.hasNext()) {
				PropertyFilter c = iter.next();

				if (!c.isMandatory()) {
					iter.remove();
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.spiconsult.vera.api.domain.DomainObjectCrudFilter#hasOptionalCriteria
	 * ()
	 */
	/**
	 * Checks for optional criteria.
	 * 
	 * @return true, if successful
	 */
	public boolean hasOptionalCriteria() {
		for (ClassFilter<?> classFilter : getClassFilters()) {
			Iterator<PropertyFilter> iter = classFilter.getPropertyFilters()
					.iterator();

			while (iter.hasNext()) {
				PropertyFilter c = iter.next();

				if (!c.isMandatory()) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Builds the oql query.
	 *
	 * @param q the q
	 * @return the query builder
	 * @throws ValidationException the validation exception
	 */
	public QueryBuilder buildOqlQuery(QueryBuilder q) throws ValidationException {
		final StringBuffer buffer = q.getProjection();

		buffer.append("find ");
		buffer.append(getDomainClassName());

		join(getPath(), q);
		addResctriction(q);
		addPropertyFilter(q, this);

		for (ClassJoin classJoin : getClassJoins()) {
			final ClassFilter<?> filter = classJoin.getClassFilter();
			filter.addResctriction(q);
			
			// add property filters
			if (!filter.getPropertyFilters().isEmpty()) {
				addPropertyFilter(q, filter);
			}
		}
		
		if (orderBy != null){
			q.addOrderBy(orderBy);
		}

		return q;
	}

	/**
	 * Adds the property filter.
	 * 
	 * @param q the q
	 * @param classFilter the class filter
	 * 
	 * @throws ValidationException the validation exception
	 */
	private void addPropertyFilter(QueryBuilder q, ClassFilter<?> classFilter)
			throws ValidationException {
		for (PropertyFilter propertyFilter : classFilter.getPropertyFilters()) {
			propertyFilter.buildQuery(q);
		}
	}

	/**
	 * Adds the resctriction.
	 * 
	 * @param queryBuilder the query builder
	 */
	public void addResctriction(QueryBuilder queryBuilder){
		if (restriction != null && restriction.length() > 0){
			queryBuilder.addRestriction(restriction);
		}
	}
	
	/**
	 * Checks if is filter is visible.
	 * 
	 * @return true, if is visible
	 */
	public boolean isFilterVisible() {
		return Flags.isSet(this.flags, FILTER_VISIBLE);
	}

	/**
	 * Visibility of the criteria in the filter dialog.
	 * 
	 * @param hide the filter
	 */
	public void setFilterVisible(boolean hide) {
		if (hide) {
			flags = Flags.set(this.flags, FILTER_VISIBLE);
		} else {
			flags = Flags.clear(this.flags, FILTER_VISIBLE);
		}
	}

	/**
	 * Checks if is result is visible.
	 * 
	 * @return true, if is visible
	 */
	public boolean isResultVisible() {
		return Flags.isSet(this.flags, RESULT_VISIBLE);
	}

	/**
	 * Visibility of the result in the filter dialog.
	 * 
	 * @param hide the results for this class
	 */
	public void setResultVisible(boolean hide) {
		if (hide) {
			flags = Flags.set(this.flags, RESULT_VISIBLE);
		} else {
			flags = Flags.clear(this.flags, RESULT_VISIBLE);
		}
	}

	/**
	 * Gets the restriction.
	 * 
	 * @return the restriction
	 */
	public String getRestriction() {
		return restriction;
	}

	/**
	 * Sets the restriction.
	 * 
	 * @param restriction the new restriction
	 */
	public void setRestriction(String restriction) {
		this.restriction = restriction;
	}

	/**
	 * Gets the flags.
	 * 
	 * @return the flags
	 */
	public final long getFlags() {
		return flags;
	}

	/**
	 * Sets the flags.
	 * 
	 * @param flags the new flags
	 */
	public final void setFlags(long flags) {
		this.flags = flags;
	}
	
	/**
	 * Gets the qualified field name.
	 *
	 * @param field the field
	 * @return the qualified field name
	 */
	private String getQualifiedFieldName(Field field){
		return field.getDeclaringClass().getCanonicalName() + "." + field.getName();
	}

	/**
	 * Gets the order by.
	 *
	 * @return the order by
	 */
	public String getOrderBy() {
		return orderBy;
	}

	/**
	 * Sets the order by.
	 *
	 * @param orderBy the new order by
	 */
	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}
	
	/**
	 * Adds the fetch.
	 *
	 * @param path the path
	 * @return the class filter
	 */
	public ClassFilter<T> addFetch(String path){
		return addFetch(path, FILTER_VISIBLE|RESULT_VISIBLE, null);
	}
	
	/**
	 * Adds the fetch.
	 *
	 * @param path the path
	 * @param flags the flags
	 * @return the class filter
	 */
	public ClassFilter<T> addFetch(String path, long flags){
		return addFetch(path, flags, null);
	}
	
	/**
	 * Adds the fetch.
	 *
	 * @param path the path
	 * @param flags the flags
	 * @param fetchConfig the fetch config
	 * @return the class filter
	 */
	public ClassFilter<T> addFetch(String path, long flags, String fetchConfig){
		createClassJoin(path, FILTER_VISIBLE|RESULT_VISIBLE, fetchConfig);
		return this;
	}
	
	/**
	 * Creates the class join.
	 *
	 * @param path the path
	 * @param flags the flags
	 * @param fetchConfig the fetch config
	 * @return the class join
	 */
	public ClassJoin createClassJoin(String path, long flags, String fetchConfig){
		final Field field = ClassUtils.findFieldFromPath(getClazz(), path);
		
		final Class<?> fetchClass = field.getType();
		
		final ClassFilter<?> cf = new ClassFilter(fetchClass, path, flags);
		return new ClassJoin(this, path, cf, fetchConfig);
	}
}
