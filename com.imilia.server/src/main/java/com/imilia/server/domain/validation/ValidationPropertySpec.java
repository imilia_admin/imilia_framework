/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2010 - all rights reserved
 * Created on: May 13, 2010
 * Created by: emcgreal
 */
package com.imilia.server.domain.validation;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.imilia.server.domain.DomainObject;
import com.imilia.server.domain.validation.properties.PropertyValidationSpecification;
import com.imilia.utils.Flags;

/**
 * The Class ValidationPropertySpec defines the validation rules for 
 * a specific class property
 */
@Entity
public class ValidationPropertySpec extends DomainObject implements PropertyValidationSpecification{

	/** The property name. */
	@NotNull
	@NotBlank
	private String propertyName;
	
	/** The Constant MANDATORY. */
	private final static long MANDATORY = 0x00000001;
	
	/** The flags. */
	private long flags;
	
	/** The validation class spec. */
	@NotNull
	@ManyToOne(optional=false)
	private ValidationClassSpec validationClassSpec;

	/**
	 * Instantiates a new validation property spec.
	 */
	public ValidationPropertySpec() {
		super();
	}

	/**
	 * Instantiates a new validation property spec.
	 * 
	 * @param name the name
	 */
	public ValidationPropertySpec(String name) {
		super();
		this.propertyName = name;
	}

	/**
	 * Sets the mandatory.
	 * 
	 * @param mandatory the new mandatory
	 */
	public void setMandatory(boolean mandatory){
		this.flags = Flags.set(flags, MANDATORY, mandatory);
	}
	
	/**
	 * Checks if is mandatory.
	 * 
	 * @return true, if is mandatory
	 */
	public boolean isMandatory(){
		return Flags.isSet(flags, MANDATORY);
	}
	
	/**
	 * Gets the property name.
	 * 
	 * @return the property name
	 */
	public final String getPropertyName() {
		return propertyName;
	}

	/**
	 * Sets the property name.
	 * 
	 * @param propertyName the new property name
	 */
	public final void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	/**
	 * Gets the flags.
	 * 
	 * @return the flags
	 */
	public final long getFlags() {
		return flags;
	}

	/**
	 * Sets the flags.
	 * 
	 * @param flags the new flags
	 */
	public final void setFlags(long flags) {
		this.flags = flags;
	}

	/**
	 * Gets the validation class spec.
	 * 
	 * @return the validation class spec
	 */
	public final ValidationClassSpec getValidationClassSpec() {
		return validationClassSpec;
	}

	/**
	 * Sets the validation class spec.
	 * 
	 * @param validationClassSpec the new validation class spec
	 */
	public final void setValidationClassSpec(ValidationClassSpec validationClassSpec) {
		this.validationClassSpec = validationClassSpec;
	}

	public String getQualifiedPropertyName() {
		return getValidationClassSpec().getClazz().getName() + "." + getPropertyName();
	}
	
	@Override
	public String toString() {
		return getQualifiedPropertyName() + "[" + isMandatory() +"]";
	}

	public String getModelName() {
		return getPropertyName();
	}

	
}
