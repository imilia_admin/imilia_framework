/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2014 - all rights reserved
 * Created on: Feb 23, 2014
 * Created by: emcgreal
 */
package com.imilia.server.domain.model.property.impl;

import java.util.List;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;

import com.imilia.server.domain.Crud;
import com.imilia.server.domain.access.ModelCrud;
import com.imilia.server.domain.access.CrudTarget;
import com.imilia.server.domain.access.Role;
import com.imilia.server.domain.model.property.ApplicationPropertyCrudProvider;
import com.imilia.server.domain.model.property.Property;
import com.imilia.server.service.RoleService;

/**
 * The Class ApplicationPropertyCrudProviderImpl.
 */
public class ApplicationPropertyCrudProviderImpl implements
		ApplicationPropertyCrudProvider, InitializingBean {

	/** The roles. */
	private List<Role> roles;
	
	/** The role service. */
	@Autowired
	private RoleService roleService;
	
	/* (non-Javadoc)
	 * @see org.springframework.beans.factory.InitializingBean#afterPropertiesSet()
	 */
	@Override
	public void afterPropertiesSet() throws Exception {
		loadRoles();
	}

	/**
	 * Load roles.
	 */
	public void loadRoles(){
		roles = roleService.loadRoles();
	}
	
	/**
	 * Gets the combined crud.
	 *
	 * @param roles the roles
	 * @return the combined crud
	 */
	public Crud getCombinedCruds(List<Role> roles){
		final Crud crud = new Crud();
		
		for (Role r:roles){
			crud.combine(r.getCrud());
		}
		return crud;
	}
	
	/* (non-Javadoc)
	 * @see com.imilia.server.domain.model.property.ApplicationPropertyCrudProvider#getCrud(com.imilia.server.domain.model.property.Property, java.util.List)
	 */
	public Crud getCrud(Property property, List<Role> roles){
		Crud crud = null;
		for (Role r:roles){
			Crud p = getCrud(property, r);
			
			if (p != null){
				if (crud == null){
					crud = new Crud(p.getCrudFlags());
				}else{
					crud.combine(p);
				}
			}
		}
		
		if (crud !=  null){
			return crud;
		}
		
		return getCombinedCruds(roles);
	}
	
	/**
	 * Gets the crud.
	 *
	 * @param property the property
	 * @param role the role
	 * @return the crud
	 */
	Crud getCrud(Property property, Role role){
		ModelCrud mp = getModelCrud(property, role);
		
		if (mp != null){
			CrudTarget pp = mp.getCrudTarget(property.getName());
			
			if (pp == null && property.getGroup() != null){
				pp = mp.getCrudTarget(property.getGroup());
			}
			
			if (pp != null){
				return pp.getCrud();
			}

			return mp.getCrud();
		}
		
		return null;
	}

	/**
	 * Gets the model crud.
	 *
	 * @param property the property
	 * @param role the role
	 * @return the model crud
	 */
	private ModelCrud getModelCrud(Property property, Role role){
		role = getRole(role);
		
		return role.getModelCrud(property.getModel().getName());
	}
	
	/**
	 * Gets the role.
	 *
	 * @param role the role
	 * @return the role
	 */
	private Role getRole(Role role){
		for (Role r:roles){
			if (r.equals(role)){
				return r;
			}
		}
		
		return null;
	}

	// Getter and setters --------------------------------------
	/**
	 * Gets the role service.
	 *
	 * @return the role service
	 */
	public RoleService getRoleService() {
		return roleService;
	}

	/**
	 * Sets the role service.
	 *
	 * @param roleService the new role service
	 */
	public void setRoleService(RoleService roleService) {
		this.roleService = roleService;
	}
	
}
