/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 * 
 * Created on: Mar 15, 2009
 * Created by: emcgreal
 */
package com.imilia.server.domain.validation;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.OptimisticLockException;

import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

import com.imilia.utils.observer.DefaultObservable;
import com.imilia.server.validation.ValidationException;

/**
 * The Class ValidationModel holds a list of ObjectErrors and Observers
 * 
 * <p>
 * The class allows observers to register and be informed upon changes to the
 * errors. This class is used to decouple the gathering of errors from the
 * processing of these errors. Various observers can be implemented to process
 * the errors e.g. display to the user or log the errors. The concept of a
 * validation transaction is used to make it easier for an application
 * programmer to just use code that throws validation exceptions e.g. storing
 * binding contexts or calling a service method and not have to worry about how
 * validation exceptions are handled.
 * </p>
 */
public class ValidationModel extends DefaultObservable {

	/** The object errors. */
	private final List<ObjectError> objectErrors = new ArrayList<ObjectError>();

	
	/**
	 * Instantiates a new validation model.
	 */
	public ValidationModel() {
		super();
	}

	/**
	 * Clear errors.
	 */
	public void clearErrors() {
		objectErrors.clear();
		fireEvent(this);
	}

	/**
	 * Adds the errors.
	 * 
	 * @param errors the errors
	 */
	public void addErrors(List<ObjectError> errors) {
		addErrors(errors, true);
	}

	/**
	 * Adds the errors.
	 * 
	 * @param errors the errors
	 * @param notifyListeners the notify listeners
	 */
	public void addErrors(List<ObjectError> errors, boolean notifyListeners) {
		this.objectErrors.addAll(errors);

		if (notifyListeners) {
			fireEvent(null);
		}
	}

	/**
	 * Do in validation transaction clears the errors, calls execute and catches
	 * any validation exception.
	 * 
	 * @param validationTransaction the validation transaction
	 * 
	 * @return true, if no validation exceptions caught
	 */
	public boolean doInValidationTransaction(ValidationTransaction validationTransaction) {
		objectErrors.clear();

		try {
			validationTransaction.execute();
			fireEvent(null);
		} catch (ValidationException e) {
			addErrors(e.getErrors().getAllErrors());
			return false;	// Validation exceptions were encountered
		}catch(OptimisticLockException e){
			ArrayList<ObjectError> list = new ArrayList<ObjectError>();
			list.add(new ObjectError(e.getEntity().getClass().getName(), 
					new String[]{"javax.persistence.OptimisticLockException"},
					null, 
					e.getMessage()));
			
			addErrors(list);
			return false;	// Validation exceptions were encountered
		}

		return true;		// No validation exception
	}

	/**
	 * Gets the object errors.
	 * 
	 * @return the objectErrors
	 */
	public List<ObjectError> getObjectErrors() {
		return objectErrors;
	}

	/**
	 * Checks for errors.
	 * 
	 * @return true, if there are errors
	 */
	public boolean hasErrors(){
		return !this.objectErrors.isEmpty();
	}
	
	/**
	 * Gets the field errors.
	 * 
	 * @return the field errors
	 */
	public List<FieldError> getFieldErrors(){
		List<FieldError> list = new ArrayList<FieldError>();
		
		for (ObjectError e:objectErrors){
			if (e instanceof FieldError){
				list.add((FieldError) e);
			}
		}
		
		return list;
	}
}
