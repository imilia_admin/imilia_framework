/**
 * Imilia - Interactive Mobile Applications  GmbH.
 * Copyright (c) 2008
 *
 * @author:		emcgreal
 * @created:	Mar 27, 2008
 * Last modified by:   $Author:$
 * Last modfication:   $Date:$
 * Version:            $Revision:$
 */
package com.imilia.server.domain.filter;

import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.imilia.server.validation.ValidationException;

/**
 * The Class StringListPropertyFilter.
 */
public class StringListPropertyFilter extends PropertyFilter {
	
	/** The Constant TAG_FILTER. */
	public final static String TAG_FILTER = "stringListPropertyFilter";
	
	/** The Constant TAG_FILTERS. */
	public final static String TAG_FILTERS = "filters";
	
	/** The Constant logger. */
	private final static Logger logger = LoggerFactory
			.getLogger(StringListPropertyFilter.class);

	/** The filters. */
	private Set<String> filters = new HashSet<String>();

	/**
	 * Instantiates a new string list property filter.
	 */
	public StringListPropertyFilter() {
		super();
	}

	/**
	 * Instantiates a new string list property filter.
	 *
	 * @param other the other
	 */
	public StringListPropertyFilter(StringListPropertyFilter other) {
		super(other);
		getFilters().addAll(other.getFilters());
	}

	/**
	 * The Constructor.
	 * 
	 * @param propertyName the property name
	 */
	public StringListPropertyFilter(String propertyName) {
		super(propertyName);
	}

	/**
	 * Instantiates a new string list property filter.
	 *
	 * @param classFilter the class filter
	 * @param propertyName the property name
	 */
	public StringListPropertyFilter(ClassFilter<?> classFilter, String propertyName) {
		super(classFilter, propertyName);
	}

	/**
	 * The Constructor.
	 * 
	 * @param classFilter the class filter
	 * @param propertyName the property name
	 * @param propertyNameLabelMessageID the property name label message id
	 */
	public StringListPropertyFilter(ClassFilter<?> classFilter, String propertyName, String propertyNameLabelMessageID) {
		super(classFilter, propertyName, propertyNameLabelMessageID);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.imilia.bkf.api.domain.filter.PropertyFilter#clear()
	 */
	@Override
	public void clear() {
		this.filters.clear();
	}

	/* (non-Javadoc)
	 * @see com.imilia.server.domain.filter.PropertyFilter#buildQuery(com.imilia.server.domain.filter.QueryBuilder)
	 */
	@Override
	public void buildQuery(QueryBuilder q) throws ValidationException {
		if (isValid()) {
			StringBuffer oidBuffer = new StringBuffer();
			for (String s : filters) {
				if (oidBuffer.length() > 0) {
					oidBuffer.append(", ");
				}
				oidBuffer.append("'");
				oidBuffer.append(s);
				oidBuffer.append("'");
			}

			StringBuffer buffer = new StringBuffer();
			buffer.append(getQualifiedName());
			buffer.append(" IN ");
			buffer.append("(");
			buffer.append(oidBuffer.toString());
			buffer.append(")");

			final String query = buffer.toString();
			logger.debug(query);
			q.addRestriction(query);
		}
	}

	/* (non-Javadoc)
	 * @see com.imilia.server.domain.filter.PropertyFilter#getFilterPropertyName()
	 */
	public String getFilterPropertyName() {
		return "filters";
	}
	/*
	 * (non-Javadoc)
	 *
	 * @see com.imilia.bkf.api.domain.filter.PropertyFilter#isValid()
	 */
	@Override
	public boolean isValid() {
		return !this.filters.isEmpty();
	}

	/**
	 * Gets the filters.
	 *
	 * @return the filters
	 */
	public Set<String> getFilters() {
		return filters;
	}
	
	/**
	 * Gets the filters as string.
	 * 
	 * @return the filters as string
	 */
	public String getFiltersAsString(){
		if (isValid()){
			StringBuffer buffer = new StringBuffer();
			
			for (String s:filters){
				if (buffer.length() > 0){
					buffer.append(";");
				}
				
				buffer.append(s);
			}
			return buffer.toString();
		}
		
		return null;
	}

	/**
	 * Sets the filters.
	 *
	 * @param filters
	 *            the filters to set
	 */
	public void setFilters(Set<String> filters) {
		this.filters = filters;
	}

	/**
	 * Configure.
	 *
	 * @param filters the filters
	 * @return the string list property filter
	 */
	public StringListPropertyFilter configure(Set<String> filters) {
		setFilters(filters);
		return this;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.imilia.api.domain.filter.PropertyFilter#duplicate()
	 */
	@Override
	public PropertyFilter duplicate() {
		return new StringListPropertyFilter(this);
	}
	
	/* (non-Javadoc)
	 * @see com.imilia.server.domain.filter.PropertyFilter#visit(com.imilia.server.domain.filter.PropertyFilterVisitor)
	 */
	@Override
	public <T> T visit(PropertyFilterVisitor<T> propertyFilterVisitor) {
		return propertyFilterVisitor.accept(this);		
	}
}
