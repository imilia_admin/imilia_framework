package com.imilia.server.domain.query.execution.deploy;

import java.lang.reflect.Field;

import org.jboss.logging.Logger;

import com.imilia.server.domain.query.entities.QueryProperty;
import com.imilia.server.domain.query.execution.Comparison;
import com.imilia.server.domain.query.execution.values.BooleanQueryPropertyValue;
import com.imilia.server.domain.query.execution.values.DateTimeQueryPropertyValue;
import com.imilia.server.domain.query.execution.values.EnumQueryPropertyValue;
import com.imilia.server.domain.query.execution.values.LongQueryPropertyValue;
import com.imilia.server.domain.query.execution.values.QueryPropertyValue;
import com.imilia.server.domain.query.execution.values.StringQueryPropertyValue;
import com.imilia.utils.classes.ClassUtils;

public class QueryPropertyDeployer {
	
	private final static Logger logger = Logger.getLogger(QueryPropertyDeployer.class);
	
	private final QueryDeployer queryDeployer;
	
	public QueryPropertyDeployer(QueryDeployer queryDeployer) {
		super();
		this.queryDeployer = queryDeployer;
	}

	public QueryPropertyValue deploy(QueryProperty p){
		QueryPropertyValue restrictionValue = null;
		Field field = ClassUtils.findFieldFromPath(p.getQueryView().getClazz(), p.getPropertyName());
		
		if (field != null){
			Class<?> type = field.getType();
			switch(type.getName()){
			case "java.lang.String":
				restrictionValue = createStringValue(p);
				break;
			case "long":
				restrictionValue = createLongValue(p);
				break;
			case "org.joda.time.DateTime":
				restrictionValue = createDateTimeValue(p);
				break;
			case "boolean":
				restrictionValue = createBooleanValue(p);
				break;
				default:
					if (type.isEnum()){
						restrictionValue = createEnumValue(p);
					}
			}
		}else{
			logger.error("No field found for query property " + p);
		}
		
		return restrictionValue;
	}

	private QueryPropertyValue createEnumValue(QueryProperty p) {
		return new EnumQueryPropertyValue(p);
	}

	private QueryPropertyValue createBooleanValue(QueryProperty p) {
		return new BooleanQueryPropertyValue(p);
	}

	private StringQueryPropertyValue createStringValue(QueryProperty p) {
		return new StringQueryPropertyValue(p);
	}

	private DateTimeQueryPropertyValue createDateTimeValue(QueryProperty p){
		return new DateTimeQueryPropertyValue(p);
	}



	
	private LongQueryPropertyValue createLongValue(QueryProperty p) {
		return new LongQueryPropertyValue(p, Comparison.Equals);
	}

	public QueryDeployer getQueryDeployer() {
		return queryDeployer;
	}
}
