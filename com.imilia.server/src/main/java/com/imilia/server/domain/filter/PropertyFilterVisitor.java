package com.imilia.server.domain.filter;

public interface PropertyFilterVisitor<T> {
	public T accept(BooleanPropertyFilter filter);
	public T accept(BitMaskPropertyFilter filter);
	public T accept(DatePropertyFilter filter);
	public T accept(DateTimePropertyFilter filter);
	public T accept(DoublePropertyFilter filter);
	public T accept(EnumPropertyFilter filter);
	public T accept(IntegerPropertyFilter filter);
	public T accept(LocaleTextFilter filter);
	public T accept(LongPropertyFilter filter);
	public T accept(OIDPropertyFilter filter);
	public T accept(StringListPropertyFilter filter);
	public T accept(StringPropertyFilter filter);
	public T accept(FlagListPropertyFilter filter);
	public T accept(EntityRefPropertyFilter entityRefPropertyFilter);
}
