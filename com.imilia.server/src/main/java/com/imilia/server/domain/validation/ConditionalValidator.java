package com.imilia.server.domain.validation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class ConditionalValidator implements Validator {
	private final Logger logger = LoggerFactory.getLogger(ConditionalValidator.class);

	@Override
	public boolean supports(Class<?> clazz) {
		return ConditionalValidation.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		ConditionalValidation c = (ConditionalValidation)target;
		
		if (c.getValidationClassSpec() != null){
			for (ValidationPropertySpec p:c.getValidationClassSpec().getPropertySpecs()){
				ValidationUtils.rejectIfEmpty(errors, p.getPropertyName(), p.getPropertyName()+ ".empty");
			}
		}
	}
}
