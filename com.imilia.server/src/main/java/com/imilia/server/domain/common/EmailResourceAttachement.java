/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 *
 * Created on: Jun 8, 2009
 * Created by: emcgreal
 */
package com.imilia.server.domain.common;

import org.springframework.core.io.InputStreamSource;
import org.springframework.core.io.Resource;

/**
 * The Class EmailResourceAttachement wraps a Resource as attachment.
 *
 * <p>Generally used as for attaching static content e.g. through a
 * spring resource.
 * </p>
 */
public class EmailResourceAttachement implements EmailAttachment{

	/** The name. */
	private String name;

	/** The resource. */
	private Resource resource;
	/* (non-Javadoc)
	 * @see com.imilia.server.domain.common.EMailAttachment#getInputStreamSource()
	 */
	public InputStreamSource getInputStreamSource() {
		return resource;
	}
	/* (non-Javadoc)
	 * @see com.imilia.server.domain.common.EMailAttachment#getName()
	 */
	public String getName() {
		return name;
	}

	/**
	 * Gets the resource.
	 *
	 * @return the resource
	 */
	public Resource getResource() {
		return resource;
	}

	/**
	 * Sets the resource.
	 *
	 * @param resource the resource to set
	 */
	public void setResource(Resource resource) {
		this.resource = resource;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
}
