package com.imilia.server.domain.filter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.imilia.server.domain.CurrentUser;
import com.imilia.server.domain.model.annotations.Model;
import com.imilia.server.domain.model.annotations.Prp;
import com.imilia.server.domain.model.impl.DefaultModel;
import com.imilia.server.domain.model.list.DefaultGenericObjectListModel;
import com.imilia.server.domain.model.list.GenericObjectListModel;
import com.imilia.server.service.RecipeService;
import com.imilia.utils.observer.ObservableArrayList;
import com.imilia.server.validation.ValidationException;

/**
 * The Class FilterModel is the default model for filters
 * 
 * <p>The FilterModel manages a <code>Recipe</code> and a <code>RecipeResult</code>.
 * The recipeTemplate is manipulated by the <code>FilterController</code> and the <code>FilterDialog</code>.
 * The recipeResult is observable and thus allows observers react to the recipeTemplate being executed
 * </p>
 *
 * @param <T> the generic type
 * @param <C> the generic type
 * @see FilterController
 * @see FilterDialog
 * @see Recipe
 */
@Model(name="Filter")
public class FilterModel<T, C extends CurrentUser> extends DefaultModel{

	/** The logger. */
	private final static Logger logger = LoggerFactory.getLogger(FilterModel.class);

	/** The recipe Template. */
	private Recipe<T> recipeTemplate;

	/** The recipe. */
	private Recipe<T> recipe;
	

	/** The recipeTemplate result. */
	private RecipeResult<T> recipeResult = new RecipeResult<T>();

	/** The recipeTemplate service. */
	private RecipeService recipeService;

	/** The current user. */
	private C currentUser;
	
	/** The list models. */
	protected HashMap<String, GenericObjectListModel<?>> listModels =
		new HashMap<String, GenericObjectListModel<?>>();

	/** The multiple select mode. */
	private boolean multipleSelectMode;
	
	/** The selected rows. */
	private List<T> selectedRowsList;
	
	/** The selected recipeTemplate holder. */
	private RecipeHolder selectedRecipeHolder;
	
	/** The recipeTemplate holders. */
	private ObservableArrayList<RecipeHolder> recipeHolders = new ObservableArrayList<RecipeHolder>();
	
	
	/** Allow the user to manage recipes. */
	private boolean manageRecipes;

	/** The prepared. */
	protected boolean prepared;
	
	
	/** The last execution. */
	protected DateTime lastExecution;

	/** The show advanced property filters. */
	protected boolean showAdvancedPropertyFilters = true;
	
	
	/** The landscape export. */
	private boolean landscapeExport = false;
	
	/** The title. */
	private String title;
	
	/** The sub title. */
	private String subTitle;
	
	private List<String> exclusions;
	
	/**
	 * Instantiates a new filter model.
	 */
	public FilterModel(){
	}

	/**
	 * Instantiates a new filter model.
	 *
	 * @param recipe the recipe
	 */
	public FilterModel(Recipe<T> recipe) {
		super();
		this.recipeTemplate = recipe;
	}
	
	/**
	 * Update recipeTemplate holders.
	 */
	public void updateRecipeHolders(){
		recipeHolders.clear();
		if (recipeTemplate != null && isManageRecipes()){
			recipeHolders.addAll(recipeService.findRecipeHolders(currentUser.getUserAccount().getPerson(), getRecipeTemplate().getClazz()));
		}
		recipeHolders.fireEvent();
		fireEvent();
	}

	/**
	 * Execute.
	 * 
	 * <p>Executes the recipeTemplate (This fires update events)</p>
	 * 
	 * @throws ValidationException the validation exception
	 */
	public void execute() throws ValidationException{
		logger.debug("Begin execute recipeTemplate: " + recipeTemplate.getName());
		recipeService.execute(recipe, currentUser.getDomainLocaleVariant().getDomainLocale(), recipeResult);

		// Update the timestamp
		lastExecution = new DateTime();
		fireEvent();

		logger.debug("End execute recipeTemplate: " + recipe.getName() +  " Results: " + recipeResult.size());
	}

	/**
	 * Gets the list model.
	 * 
	 * @param fieldName the field name
	 * 
	 * @return the list model
	 */
	public GenericObjectListModel<?> getListModel(String fieldName) {
		final GenericObjectListModel<?> m = listModels.get(fieldName);

		if (m == null){
			logger.debug("Cannot find listModel for field: " + fieldName);
		}
		return m;
	}

	/**
	 * Adds the list model.
	 * 
	 * @param fieldName the field name
	 * @param listModel the list model
	 */
	public void addListModel(String fieldName, GenericObjectListModel<?> listModel) {
		listModels.put(fieldName, listModel);
	}
	
	/**
	 * Adds the enum list.
	 *
	 * @param <E> the element type
	 * @param fieldName the field name
	 * @param displayProperty the display property
	 * @param storeProperty the store property
	 * @param e the e
	 */
	public <E extends Enum<?>> void addListModelEnums(String fieldName, String displayProperty, String storeProperty, Class<E> e){
		final ArrayList<Object> entries = new ArrayList<Object>(); 
		for (Object o:e.getEnumConstants()){
			entries.add(o);
		}
		addListModel(fieldName, displayProperty, storeProperty, entries);
	}
	
	/**
	 * Adds the list model values.
	 *
	 * @param fieldName the field name
	 * @param displayProperty the display property
	 * @param storeProperty the store property
	 * @param values the values
	 */
	public void addListModelValues(String fieldName, String displayProperty, String storeProperty, Object...values){
		final ArrayList<Object> entries = new ArrayList<Object>(); 
		for (Object o:values){
			entries.add(o);
		}
		addListModel(fieldName, displayProperty, storeProperty, entries);
	}
	
	/**
	 * Adds the list model.
	 *
	 * @param fieldName the field name
	 * @param displayProperty the display property
	 * @param storeProperty the store property
	 * @param entries the entries
	 */
	public void addListModel(String fieldName, String displayProperty, String storeProperty, List<Object> entries){

		final GenericObjectListModel<?> listModel = 
				new DefaultGenericObjectListModel<Object>(entries, displayProperty, storeProperty, getCurrentUser().getDomainLocaleVariant());
		listModel.setMessageSource(getMessageSource());
		addListModel(fieldName, listModel);
	}
	
	/**
	 * Reset.
	 */
	public void reset(){
		recipeResult.clear();
		recipeResult.fireEvent();
		lastExecution = null;
		fireEvent();
	}

	// Getter and setters -----------------------------------------------------
	/**
	 * Gets the recipeTemplate.
	 * 
	 * @return the recipeTemplate
	 */
	public Recipe<T> getRecipeTemplate() {
		return recipeTemplate;
	}

	/**
	 * Sets the recipeTemplate.
	 *
	 * @param recipe the new recipe template
	 */
	public void setRecipeTemplate(Recipe<T> recipe) {
		this.recipeTemplate = recipe;
	}
	
	
	/**
	 * Prepares the filter model by creating the property filters and default recipe.
	 */
	public void prepare(){
		if (!prepared){
			createPropertyFiltersFromAnnotations();
			
			createDefaultFromTemplate();
			
			prepared = true;
		}
	}

	/**
	 * Checks if is prepared.
	 *
	 * @return true, if is prepared
	 */
	public boolean isPrepared() {
		return prepared;
	}

	/**
	 * Creates the property filters from annotations.
	 */
	public void createPropertyFiltersFromAnnotations(){
		if (this.recipeTemplate != null){
			for (CrudFilter<?> crudFilter: recipeTemplate.getCrudFilters()){
				crudFilter.createPropertyFiltersFromAnnotations(this);
			}
		}
	}
	
	/**
	 * Creates the default from template.
	 */
	public void createDefaultFromTemplate(){
		this.recipe = this.recipeTemplate.duplicate();
	}

	/**
	 * Gets the recipeTemplate result.
	 * 
	 * @return the recipeTemplate result
	 */
	public RecipeResult<T> getRecipeResult() {
		return recipeResult;
	}

	/**
	 * Sets the recipeTemplate result.
	 * 
	 * @param recipeResult the new recipeTemplate result
	 */
	public void setRecipeResult(RecipeResult<T> recipeResult) {
		this.recipeResult = recipeResult;
	}

	/**
	 * Gets the recipeTemplate service.
	 * 
	 * @return the recipeTemplate service
	 */
	public RecipeService getRecipeService() {
		return recipeService;
	}

	/**
	 * Sets the recipeTemplate service.
	 * 
	 * @param recipeService the new recipeTemplate service
	 */
	public void setRecipeService(RecipeService recipeService) {
		this.recipeService = recipeService;
	}

	/**
	 * Gets the list models.
	 * 
	 * @return the list models
	 */
	public HashMap<String, GenericObjectListModel<?>> getListModels() {
		return listModels;
	}

	/**
	 * Sets the list models.
	 * 
	 * @param listModels the list models
	 */
	public void setListModels(HashMap<String, GenericObjectListModel<?>> listModels) {
		this.listModels = listModels;
	}

	/**
	 * Gets the limit exceeded message.
	 * 
	 * @return the limit exceeded message
	 */
	public String getLimitExceededMessage(){
		return getMessageSource().getMessage("com.imilia.echosupport.filter.FilterDialog.limit.exceeded",
				new Object[]{recipeResult.getLimitExceeded()}, currentUser.getDomainLocaleVariant().getLocale());
	}

	/**
	 * Checks if is multiple select mode.
	 * 
	 * @return the multipleSelectMode
	 */
	public boolean isMultipleSelectMode() {
		return multipleSelectMode;
	}

	/**
	 * Sets the multiple select mode.
	 * 
	 * @param multipleSelectMode the multipleSelectMode to set
	 */
	public void setMultipleSelectMode(boolean multipleSelectMode) {
		this.multipleSelectMode = multipleSelectMode;
	}

	/**
	 * Gets the selected rows.
	 * 
	 * @return the selectedRows
	 */
	public List<T> getSelectedRows() {
		return selectedRowsList;
	}

	/**
	 * Sets the selected rows.
	 * 
	 * @param selectedRows the selectedRows to set
	 */
	public void setSelectedRows(List<T> selectedRows) {
		this.selectedRowsList = selectedRows;
	}

	/**
	 * Checks if is rows selected.
	 * 
	 * @return the rowsSelected
	 */
	public boolean isRowsSelected() {
		return (this.selectedRowsList != null ? !this.selectedRowsList.isEmpty() : false);
	}

	/**
	 * Gets the current user.
	 * 
	 * @return the current user
	 */
	public C getCurrentUser() {
		return currentUser;
	}

	/**
	 * Sets the current user.
	 * 
	 * @param currentUser the new current user
	 */
	public void setCurrentUser(C currentUser) {
		this.currentUser = currentUser;
	}

	/**
	 * Gets the selected recipeTemplate holder.
	 * 
	 * @return the selected recipeTemplate holder
	 */
	public RecipeHolder getSelectedRecipeHolder() {
		return selectedRecipeHolder;
	}

	/**
	 * Sets the selected recipeTemplate holder.
	 * 
	 * @param selectedRecipeHolder the new selected recipeTemplate holder
	 */
	public void setSelectedRecipeHolder(RecipeHolder selectedRecipeHolder) {
		this.selectedRecipeHolder = selectedRecipeHolder;
	}

	/**
	 * Gets the recipeTemplate holders.
	 * 
	 * @return the recipeTemplate holders
	 */
	public ObservableArrayList<RecipeHolder> getRecipeHolders() {
		return recipeHolders;
	}

	/**
	 * Sets the recipeTemplate holders.
	 * 
	 * @param recipeHolders the new recipeTemplate holders
	 */
	public void setRecipeHolders(ObservableArrayList<RecipeHolder> recipeHolders) {
		this.recipeHolders = recipeHolders;
	}

	/**
	 * Checks if is show recipeTemplate holder list.
	 * 
	 * @return true, if is show recipeTemplate holder list
	 */
	public boolean isShowRecipeHolderList() {
		return !recipeHolders.isEmpty();
	}

	/**
	 * Checks if is landscape export.
	 * 
	 * @return true, if is landscape export
	 */
	public boolean isLandscapeExport() {
		return landscapeExport;
	}

	/**
	 * Sets the landscape export.
	 * 
	 * @param landscapeExport the new landscape export
	 */
	public void setLandscapeExport(boolean landscapeExport) {
		this.landscapeExport = landscapeExport;
	}

	/**
	 * Gets the recipe.
	 *
	 * @return the recipe
	 */
	public final Recipe<T> getRecipe() {
		return recipe;
	}

	/**
	 * Sets the recipe.
	 *
	 * @param recipe the new recipe
	 */
	public final void setRecipe(Recipe<T> recipe) {
		this.recipe = recipe;
	}

	/**
	 * Checks if is manage recipes.
	 *
	 * @return true, if is manage recipes
	 */
	public final boolean isManageRecipes() {
		return manageRecipes;
	}

	/**
	 * Sets the manage recipes.
	 *
	 * @param manageRecipes the new manage recipes
	 */
	public final void setManageRecipes(boolean manageRecipes) {
		this.manageRecipes = manageRecipes;
	}

	/**
	 * Checks if is select recipe holder possible.
	 *
	 * @return true, if is select recipe holder possible
	 */
	public boolean isSelectRecipeHolderPossible(){
		return isManageRecipes() && !this.recipeHolders.isEmpty();
	}

	/**
	 * Gets the last execution.
	 *
	 * @return the last execution
	 */
	public DateTime getLastExecution() {
		return lastExecution;
	}

	/**
	 * Sets the last execution.
	 *
	 * @param lastExecution the new last execution
	 */
	public void setLastExecution(DateTime lastExecution) {
		this.lastExecution = lastExecution;
	}
	
	/**
	 * Checks if is select all possible.
	 *
	 * @return true, if is select all possible
	 */
	public boolean isSelectAllPossible(){
		return multipleSelectMode && recipeResult.isNotEmpty();
	}

	/**
	 * Checks if is show advanced property filters.
	 *
	 * @return true, if is show advanced property filters
	 */
	public boolean isShowAdvancedPropertyFilters() {
		return showAdvancedPropertyFilters;
	}

	/**
	 * Sets the show advanced property filters.
	 *
	 * @param showAdvancedPropertyFilters the new show advanced property filters
	 */
	public void setShowAdvancedPropertyFilters(boolean showAdvancedPropertyFilters) {
		this.showAdvancedPropertyFilters = showAdvancedPropertyFilters;
	}
	
	
	/**
	 * Checks if is show no data found message.
	 *
	 * @return true, if is show no data found message
	 */
	public boolean isShowNoDataFoundMessage(){
		return lastExecution != null && recipeResult.isEmpty();
	}
	
	/**
	 * Gets the no data found message.
	 *
	 * @return the no data found message
	 */
	public String getNoDataFoundMessage(){
		final Locale locale = getCurrentUser().getDomainLocaleVariant().getLocale();
		final DateTimeFormatter df = DateTimeFormat.forPattern(getCurrentUser().getDomainLocaleVariant().getDomainLocale().getDateTimeFormat()).withLocale(locale);
		return getMessageSource().getMessage(
				"com.imilia.server.domain.filter.FilterModel.noDataFoundMessage", 
				new Object[]{ df.print(lastExecution)}, 
				locale);
	}

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Gets the safe title.
	 *
	 * @return the safe title
	 */
	public String getSafeTitle(){
		if (title == null){
			String t =  getRecipe().getName();
			
			if (t != null){
				return t;
			}
			
			t = getMessageSource().getMessage(getRecipe().getClazz().getCanonicalName(), null, getCurrentUser().getDomainLocaleVariant().getLocale());
			
			if (t == null){
				return getRecipe().getClazz().getSimpleName();
			}
			return t;
		}
		
		return title;
	}
	
	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the sub title.
	 *
	 * @return the sub title
	 */
	public String getSubTitle() {
		return subTitle;
	}

	/**
	 * Sets the sub title.
	 *
	 * @param subTitle the new sub title
	 */
	public void setSubTitle(String subTitle) {
		this.subTitle = subTitle;
	}

	public List<String> getExclusions() {
		return exclusions;
	}

	public void setExclusions(List<String> exclusions) {
		this.exclusions = exclusions;
	}
	
	public void setExclusions(String...exclusions) {
		this.exclusions = Arrays.asList(exclusions);
	}
}
