package com.imilia.server.domain.model.event;

public interface IEventSource<T extends IEvent> {
	
	
	public interface Listener<T> {
		public void fired(T event);
	}

	public String getName();
	public void setName(String name);
	
	public void register(Listener<T> l);
	public void unregister(Listener<T> l);
	public void clearListeners();
	public void fire(T event);
}
