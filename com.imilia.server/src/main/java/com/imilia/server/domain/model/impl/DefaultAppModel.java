/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2014 - all rights reserved
 * Created on: Feb 23, 2014
 * Created by: emcgreal
 */
package com.imilia.server.domain.model.impl;

import javax.validation.Validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

import com.imilia.server.domain.Crud;
import com.imilia.server.domain.CurrentUser;
import com.imilia.server.domain.model.IAppModel;
import com.imilia.server.domain.model.property.ApplicationPropertyCrudProvider;
import com.imilia.server.domain.model.property.Property;
import com.imilia.server.server.Profile;

/**
 * The Class DefaultAppModel.
 *
 * @param <C> the generic type
 */
public class DefaultAppModel<C extends CurrentUser> implements IAppModel<C>{

	/** The application property crud provider. */
	@Autowired
	protected ApplicationPropertyCrudProvider applicationPropertyCrudProvider;
	
	/** The bean validator. */
	@Autowired
	protected Validator beanValidator;
	
	/** The message source. */
	@Autowired
	protected MessageSource messageSource;
	
	/** The current user. */
	@Autowired
	protected C currentUser;
	
	/** The profile. */
	@Autowired
	private Profile profile;
	
	/* (non-Javadoc)
	 * @see com.imilia.server.domain.model.IAppModel#getBeanValidator()
	 */
	public Validator getBeanValidator() {
		return beanValidator;
	}
	
	/**
	 * Sets the bean validator.
	 *
	 * @param beanValidator the new bean validator
	 */
	public void setBeanValidator(Validator beanValidator) {
		this.beanValidator = beanValidator;
	}
	
	/* (non-Javadoc)
	 * @see com.imilia.server.domain.model.IAppModel#getMessageSource()
	 */
	public MessageSource getMessageSource() {
		return messageSource;
	}
	
	/**
	 * Sets the message source.
	 *
	 * @param messageSource the new message source
	 */
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
	
	/* (non-Javadoc)
	 * @see com.imilia.server.domain.model.IAppModel#getCurrentUser()
	 */
	public C getCurrentUser() {
		return currentUser;
	}
	
	/**
	 * Sets the current user.
	 *
	 * @param currentUser the new current user
	 */
	public void setCurrentUser(C currentUser) {
		this.currentUser = currentUser;
	}
	
	/**
	 * Gets the profile.
	 *
	 * @return the profile
	 */
	public Profile getProfile() {
		return profile;
	}
	
	/**
	 * Sets the profile.
	 *
	 * @param profile the new profile
	 */
	public void setProfile(Profile profile) {
		this.profile = profile;
	}

	@Override
	public Crud getCrud(Property property) {
		return applicationPropertyCrudProvider.getCrud(property, currentUser.getRoles());
	}
	
	/**
	 * Gets the application property crud provider.
	 *
	 * @return the application property crud provider
	 */
	public ApplicationPropertyCrudProvider getApplicationPropertyCrudProvider() {
		return applicationPropertyCrudProvider;
	}
	
	/**
	 * Sets the application property crud provider.
	 *
	 * @param applicationPropertycrudProvider the new application property crud provider
	 */
	public void setApplicationPropertyCrudProvider(
			ApplicationPropertyCrudProvider applicationPropertyCrudProvider) {
		this.applicationPropertyCrudProvider = applicationPropertyCrudProvider;
	}
}
