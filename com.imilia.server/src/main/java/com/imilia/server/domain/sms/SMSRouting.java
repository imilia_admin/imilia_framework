/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2010 - all rights reserved
 * Created on: May 19, 2010
 * Created by: emcgreal
 */
package com.imilia.server.domain.sms;

import javax.persistence.Entity;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.imilia.server.domain.DomainObject;
import com.imilia.utils.Flags;

/**
 * The Class SMSRouting.
 */
@Entity
public class SMSRouting extends DomainObject{
	
	/** The Constant IMILIA_SMS_PROVIDER. */
	public final static String IMILIA_SMS_PROVIDER = "ImiliaSMSProvider";
	
	/** The Constant ACCOUNT. */
	public static final String ACCOUNT 		= "account";
	
	/** The Constant PASSWORD. */
	public static final String PASSWORD 	= "password";
	
	/** The Constant GATEWAY. */
	public static final String GATEWAY 		= "gateway";

	/** The provider. */
	@NotNull
	@NotBlank
	private String provider; 
	
	/** The account id. */
	@NotNull
	@NotBlank
	private String account;
	
	/** The password. */
	@NotNull
	@NotBlank
	private String password;
	
	/** The gateway. */
	@NotNull
	@NotBlank
	private String gateway;
	
	/** The Constant FLAG_ENABLED. */
	final static long FLAG_ENABLED = 0x00000001;
	
	/** The flags. */
	private long flags;
	
	/** The default sender. */
	@Max(11)
	private String defaultSender;
	
	/**
	 * Instantiates a new SMS routing.
	 */
	public SMSRouting(){
		super();
	}
	
	/**
	 * Instantiates a new SMS routing.
	 * 
	 * @param provider the provider
	 * @param account the account
	 * @param password the password
	 * @param gateway the gateway
	 * @param sender 
	 */
	public SMSRouting(String provider, String account, String password,
			String gateway, String sender) {
		super();
		this.provider = provider;
		this.account = account;
		this.password = password;
		this.gateway = gateway;
		this.defaultSender = sender;
	}

	/**
	 * Gets the account.
	 * 
	 * @return the account
	 */
	public final String getAccount() {
		return account;
	}
	
	/**
	 * Sets the account.
	 * 
	 * @param account the new account
	 */
	public final void setAccount(String account) {
		this.account = account;
	}
	
	/**
	 * Gets the password.
	 * 
	 * @return the password
	 */
	public final String getPassword() {
		return password;
	}
	
	/**
	 * Sets the password.
	 * 
	 * @param password the new password
	 */
	public final void setPassword(String password) {
		this.password = password;
	}
	
	/**
	 * Gets the gateway.
	 * 
	 * @return the gateway
	 */
	public final String getGateway() {
		return gateway;
	}
	
	/**
	 * Sets the gateway.
	 * 
	 * @param gateway the new gateway
	 */
	public final void setGateway(String gateway) {
		this.gateway = gateway;
	}

	/**
	 * Gets the provider.
	 * 
	 * @return the provider
	 */
	public final String getProvider() {
		return provider;
	}

	/**
	 * Sets the provider.
	 * 
	 * @param provider the new provider
	 */
	public final void setProvider(String provider) {
		this.provider = provider;
	}

	/**
	 * Gets the flags.
	 * 
	 * @return the flags
	 */
	public final long getFlags() {
		return flags;
	}

	/**
	 * Sets the flags.
	 * 
	 * @param flags the new flags
	 */
	public final void setFlags(long flags) {
		this.flags = flags;
	}
	
	/**
	 * Sets the enabled.
	 * 
	 * @param enabled the new enabled
	 */
	public void setEnabled(boolean enabled){
		this.flags = Flags.set(this.flags, FLAG_ENABLED, enabled);
	}
	
	/**
	 * Checks if is enabled.
	 * 
	 * @return true, if is enabled
	 */
	public boolean isEnabled(){
		return Flags.isSet(this.flags, FLAG_ENABLED);
	}

	public final String getDefaultSender() {
		return defaultSender;
	}

	public final void setDefaultSender(String defaultSender) {
		this.defaultSender = defaultSender;
	}
}
