/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2014 - all rights reserved
 * Created on: Feb 27, 2014
 * Created by: emcgreal
 */
package com.imilia.server.domain.query.execution;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;

import com.imilia.server.domain.Crud;
import com.imilia.server.domain.CrudAware;
import com.imilia.server.domain.query.Query;
import com.imilia.server.domain.query.execution.values.QueryPropertyValue;
import com.imilia.server.domain.query.execution.values.QueryRestrictionValue;

/**
 * The Class QueryExection.
 *
 * @param <T> the generic type
 */
public class QueryExecution<T extends CrudAware>{
	
	/** The Constant logger. */
	final static Logger logger = LoggerFactory.getLogger(QueryExecution.class);
	
	/** The query template. */
	private Query<T> query;
	
	/** The parser. */
	private ExpressionParser parser = new SpelExpressionParser();

	/** The context. */
	private final StandardEvaluationContext context;
	
	/**
	 * Instantiates a new query execution context.
	 *
	 * @param query the template
	 * @param roles the roles
	 * @param model the model
	 */
	public QueryExecution(Query<T> query, Object model){
		super();
		this.query = query;
		context = new StandardEvaluationContext(model);
	}
	

	/**
	 * Gets the crud queries.
	 *
	 * @return the crud queries
	 */
	public List<QueryCrudExecution<T>> getCrudQueries(){
		final HashMap<Crud, List<QueryRestrictionValue>> crudGroups = new HashMap<>();
		
		//-----------------------------------------------------------
		// Group the restrictions by CRUD
		for (QueryRestrictionValue r:query.getQueryRestrictionValues()){
			final Crud crud = r.getQueryRestriction().getRestrictionCrud();
			
			List<QueryRestrictionValue> list = crudGroups.get(crud);
			
			if (list == null){
				// new CRUD group
				if (logger.isDebugEnabled()){
					logger.debug("Create CRUD group:" + crud.toString());
				}
				list = new ArrayList<>();
				crudGroups.put(crud, list);
			}
			
			// Add to the list
			list.add(r);
		}

		
		final List<QueryCrudExecution<T>> executions = new ArrayList<>();
		
		for (Crud c:crudGroups.keySet()){
			List<QueryRestrictionValue> list = crudGroups.get(c);
			
			final QueryCrudExecution<T> crudExecution = new QueryCrudExecution<T>(this, c);
			
			crudExecution.getQueryRestrictionValues().addAll(list);
			
			executions.add(crudExecution);
		}
		

		return executions;
	}
	
	
	
	
	
	/**
	 * Gets the query template.
	 *
	 * @return the query template
	 */
	public Query<T> getQuery() {
		return query;
	}
	
	/**
	 * Sets the query template.
	 *
	 * @param queryTemplate the new query template
	 */
	public void setQuery(Query<T> queryTemplate) {
		this.query = queryTemplate;
	}

	/**
	 * Gets the variable value.
	 *
	 * @param variableName the variable name
	 * @return the variable value
	 */
	public Object getVariableValue(String variableName) {
		if (context != null){
			return parser.parseExpression(variableName).getValue(context);
		}
		
		return null;
	}
}
