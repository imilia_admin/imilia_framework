package com.imilia.server.domain.model.event.impl;

import com.imilia.server.domain.model.event.IInvoker;

public class Invoker<T> implements IInvoker{
	private final T source;

	public Invoker(T source) {
		super();
		this.source = source;
	}

	public T getSource() {
		return source;
	}
	
}
