/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2014 - all rights reserved
 * Created on: Feb 19, 2014
 * Created by: emcgreal
 */
package com.imilia.server.domain.access;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

import com.imilia.server.domain.Crud;
import com.imilia.server.domain.DomainObject;
import com.imilia.server.domain.filter.RestrictionCandidate;

/**
 * The Class ModelEntity.
 */
@Entity
public class ModelCrud extends DomainObject {

	@ManyToOne
	private Role role;

	/** The model name. */
	@RestrictionCandidate
	private String name;
	
	@OneToMany(mappedBy="model", cascade=CascadeType.ALL)
	private List<CrudTarget> crudTargets;
	
	
	/** The default crud. */
	@Embedded
	@NotNull
	private Crud crud;

	public ModelCrud(){
		super();
	}

	public ModelCrud(Role role, String model, Crud crud){
		super();
		this.role = role;
		role.getModelCruds().add(this);
		this.name = model;
		this.crud = crud;
	}
	
	/**
	 * Gets the model name.
	 *
	 * @return the model name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the model name.
	 *
	 * @param modelName the new model name
	 */
	public void setName(String modelName) {
		this.name = modelName;
	}

	/**
	 * Gets the property crud.
	 *
	 * @return the property crud
	 */
	public List<CrudTarget> getCrudTargets() {
		return crudTargets;
	}

	/**
	 * Sets the property cruds.
	 *
	 * @param crudTargets the new property cruds
	 */
	public void setCrudTargets(List<CrudTarget> crudTargets) {
		this.crudTargets = crudTargets;
	}

	public Crud getCrud() {
		return crud;
	}

	public void setCrud(Crud crud) {
		this.crud = crud;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public CrudTarget getCrudTarget(String property) {
		for (CrudTarget p:getCrudTargets()){
			if (p.getName().equals(property)){
				return p;
			}
		}
		return null;
	}

}
