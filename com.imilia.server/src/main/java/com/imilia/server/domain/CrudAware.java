/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2011 - all rights reserved
 *
 * Created on: Aug 22, 2011
 * Created by: emcgreal
 */
package com.imilia.server.domain;

public interface CrudAware {
	
	/**
	 * Sets the crud.
	 *
	 * @param crud the new crud
	 */
	public void setCrud(Crud crud);
	
	/**
	 * Gets the crud.
	 *
	 * @return the crud
	 */
	public Crud getCrud();
}
