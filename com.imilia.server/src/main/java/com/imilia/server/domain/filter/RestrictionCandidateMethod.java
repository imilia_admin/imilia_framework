/**
 * Imilia Interactive Mobile Applications  GmbH.
 * Copyright (c) 2008
 * @author emcgreal
 * @created 12.03.2008
 * Last modified by:   	$Author:$
 * Last modified on:	$Date:$
 * Version:				$Revision:$
 */
package com.imilia.server.domain.filter;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * The Interface RestrictionCandidateMethod.
 * 
 * @author emcg
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface RestrictionCandidateMethod {

}
