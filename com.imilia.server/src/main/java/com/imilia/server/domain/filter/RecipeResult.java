/**
 * Imilia - Interactive Mobile Applications  GmbH.
 * Copyright (c) 2008
 *
 * @author:		emcgreal
 * @created:	Apr 18, 2008
 * Last modified by:   $Author:$
 * Last modfication:   $Date:$
 * Version:            $Revision:$
 */
package com.imilia.server.domain.filter;

import java.util.List;

import com.imilia.utils.observer.ObservableArrayList;

/**
 * The Class RecipeResult.
 */
public class RecipeResult<T> extends ObservableArrayList<T> {

	/** The limit exceeded. */
	private int limitExceeded;

	/**
	 * Instantiates a new recipe result.
	 */
	public RecipeResult() {
		super();
	}

	/**
	 * Instantiates a new recipe result.
	 *
	 * @param result
	 *            the result
	 * @param limitExceeded
	 *            the limit exceeded
	 */
	public RecipeResult(List<T> result, int limitExceeded) {
		super();
		addAll(result);
		this.limitExceeded = limitExceeded;
	}

	/**
	 * Checks if is limit exceeded.
	 *
	 * @return the limitExceeded
	 */
	public boolean isLimitExceeded() {
		return limitExceeded > 0;
	}

	/**
	 * Checks if is limit exceeded.
	 *
	 * @return the limitExceeded
	 */
	public int getLimitExceeded() {
		return limitExceeded;
	}
	/**
	 * Sets the result.
	 *
	 * @param result
	 *            the result to set
	 * @param limitExceeded
	 *            the limit exceeded
	 */
	public void setResult(List<T> result, int limitExceeded) {
		clear();
		addAll(result);
		this.limitExceeded = limitExceeded;
	}

	/**
	 * Sets the limit exceeded.
	 *
	 * @param limitExceeded
	 *            the limitExceeded to set
	 */
	public void setLimitExceeded(int limitExceeded) {
		this.limitExceeded = limitExceeded;
	}

	/**
	 * Checks if is not empty.
	 *
	 * @return true, if is not empty
	 */
	public boolean isNotEmpty(){
		return !isEmpty();
	}
}
