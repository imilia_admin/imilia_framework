/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 * 
 * Created on: Mar 10, 2009
 * Created by: emcgreal
 */
package com.imilia.server.domain.validation;

import org.springframework.validation.Validator;

import com.imilia.server.validation.ValidationException;

/**
 * The Interface Validational.
 */
public interface Validational {
	
	/**
	 * Gets the bean validator.
	 * 
	 * @return the bean validator
	 */
	public Validator getBeanValidator();

	/**
	 * Sets the bean validator.
	 * 
	 * @param beanValidator the new bean validator
	 */
	public void setBeanValidator(Validator beanValidator) ;
	
	/**
	 * Validate.
	 * 
	 * @throws ValidationException the validation exception
	 */
	public void validate() throws ValidationException;
	
	/**
	 * Validate.
	 * 
	 * @param targetName the target name
	 * 
	 * @throws ValidationException the validation exception
	 */
	public void validate(String targetName) throws ValidationException;
}
