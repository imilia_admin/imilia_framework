/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 * Created on: Jan 23, 2009
 * Created by: emcgreal
 */

package com.imilia.server.domain.i18n;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

import com.imilia.server.domain.DomainObject;
import com.imilia.utils.i18n.DomainLocale;

/**
 * The Class LocaleText.
 * 
 * @author emcg
 */
@Entity
public class LocaleText extends DomainObject implements java.io.Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -8020743498553597653L;
	
	/** The Constant MAXIMUMLENGTH. */
	private static final int MAXIMUMLENGTH = 16384; // 2^14

	/** The domain locale. */
	@Enumerated(value = EnumType.ORDINAL)
	private DomainLocale domainLocale;

	/** The locale text holder. */
	@ManyToOne(optional=false, fetch=FetchType.EAGER)
	private LocaleTextHolder localeTextHolder;

	/** The text. */
	@Lob
	@Column(length=MAXIMUMLENGTH)		
	private String text;

	/** The maxlength. */
	private int maxlength = MAXIMUMLENGTH;

	/**
	 * Instantiates a new locale text.
	 */
	public LocaleText(){
	}

	/**
	 * Instantiates a new locale text.
	 * 
	 * @param domainLocale the domain locale
	 * @param localeTextHolder the locale text holder
	 * @param text the text
	 */
	public LocaleText(DomainLocale domainLocale,
			LocaleTextHolder localeTextHolder, String text) {
		super();
		this.domainLocale = domainLocale;
		localeTextHolder.addLocaleText(this);
		this.text = text;
	}

	/**
	 * Gets the domain locale.
	 * 
	 * @return the domain locale
	 */
	public DomainLocale getDomainLocale() {
		return domainLocale;
	}

	/**
	 * Sets the domain locale.
	 * 
	 * @param domainLocale the new domain locale
	 */
	public void setDomainLocale(DomainLocale domainLocale) {
		this.domainLocale = domainLocale;
	}

	/**
	 * Gets the text.
	 * 
	 * @return the text
	 */
	public String getText() {
		return text;
	}

	/**
	 * Sets the text.
	 * 
	 * @param text the text to set
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * Gets the locale text holder.
	 * 
	 * @return the localeTextHolder
	 */
	public LocaleTextHolder getLocaleTextHolder() {
		return localeTextHolder;
	}

	/**
	 * Sets the locale text holder.
	 * 
	 * @param localeTextHolder the localeTextHolder to set
	 */
	public void setLocaleTextHolder(LocaleTextHolder localeTextHolder) {
		this.localeTextHolder = localeTextHolder;
	}

	/**
	 * Gets the maxlength.
	 * 
	 * @return the maxlength
	 */
	public int getMaxlength() {
		return maxlength;
	}

	/**
	 * Sets the maxlength.
	 * 
	 * @param maxlength the maxlength to set
	 */
	public void setMaxlength(int maxlength) {
		this.maxlength = maxlength;
	}
}
