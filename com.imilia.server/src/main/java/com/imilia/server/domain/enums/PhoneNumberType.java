/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 * 
 * Created on: Mar 3, 2009
 * Created by: emcgreal
 */
package com.imilia.server.domain.enums;

import java.util.Locale;

import org.springframework.context.MessageSource;

import com.imilia.utils.i18n.Localized;

/**
 * The Enum PhoneNumberType is used to classify Phone numbers
 */
public enum PhoneNumberType implements Localized{

	/** The Home. */
	Home,
	/** The Work. */
	Work,
	/** The Home fax. */
	HomeFax,
	/** The Work fax. */
	WorkFax,

	/** The Mobile. */
	Mobile,
	/** The Emergency. */
	Emergency,
	/** The Other. */
	Other;

	/* (non-Javadoc)
	 * @see com.imilia.server.domain.common.Localeized#getMessageId()
	 */
	public String getMessageId() {
		return PhoneNumberType.class.getSimpleName() + "." + name();
	}
	
	/* (non-Javadoc)
	 * @see com.imilia.server.domain.Localized#getLocalizedMessage(org.springframework.context.MessageSource, java.util.Locale)
	 */
	public String getLocalizedMessage(MessageSource messageSource, Locale locale) {
		return messageSource.getMessage(getMessageId(), null, locale);
	}
}
