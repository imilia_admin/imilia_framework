/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 *
 * Created on: Jun 8, 2009
 * Created by: emcgreal
 */
package com.imilia.server.domain.common;

import java.util.Iterator;
import java.util.List;

/**
 * The Interface Email abstracts an email to it bare properties.
 */
/**
 * @author emcgreal
 *
 */
public interface Email {

	/**
	 * Gets the sender.
	 *
	 * @return the sender
	 */
	public String getSender();

	/**
	 * Gets the to recipients.
	 *
	 * @return the to recipients
	 */
	public List<String> getToRecipients();

	/**
	 * Gets the cc recipients.
	 *
	 * @return the cc recipients
	 */
	public List<String> getCcRecipients();

	/**
	 * Gets the bcc recipients.
	 *
	 * @return the bcc recipients
	 */
	public List<String> getBccRecipients();

	/**
	 * Gets the subject.
	 *
	 * @return the subject
	 */
	public String getSubject();

	/**
	 * Gets the body.
	 *
	 * @return the body
	 */
	public String getBody();

	/**
	 * Checks if is html.
	 *
	 * @return true, if is html
	 */
	public boolean isHtml();

	/**
	 * Checks for attachments.
	 *
	 * @return true, if successful
	 */
	public boolean hasAttachments();
	
	/**
	 * Gets the attachments.
	 *
	 * @return the attachments
	 */
	public Iterator<? extends EmailAttachment> getAttachmentsIterator();

	/**
	 * Gets the summary - used for logging.
	 *
	 * @return the summary
	 */
	public String getSummary();
	
	
	/**
	 * Sets the mulitpart mode.
	 *
	 * @param mode the new mulitpart mode
	 */
	public void setMulitpartMode(int mode);
	
	/**
	 * Gets the mulitpart mode.
	 *
	 * @return the mulitpart mode
	 */
	public int getMulitpartMode();
}
