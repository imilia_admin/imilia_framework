/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 *
 * Created on: Apr 30, 2009
 * Created by: eddiemcgreal
 */
package com.imilia.server.domain;

import java.util.List;

import com.imilia.server.domain.access.Role;
import com.imilia.server.domain.access.Subscription;
import com.imilia.server.domain.common.UserAccount;
import com.imilia.server.domain.i18n.DomainLocaleVariant;
import com.imilia.server.domain.user.UserPropertyManager;
import com.imilia.utils.observer.Observable;

/**
 * The Interface CurrentUser is used throughout the system to transport the currently
 * authenticated user.
 *
 * <p>Concrete implementations of this class are usually created for a specific application. The
 * concrete implementation may then carry additional properties specific to the application and user.
 * </p>
 */
public interface CurrentUser extends Observable {

	/**
	 * Sets the user account.
	 *
	 * @param userAccount the new user account
	 */
	public void setUserAccount(UserAccount userAccount);

	public void setUserAccountOnly(UserAccount userAccount);
	
	/**
	 * Gets the user account.
	 *
	 * @return the user account
	 */
	public UserAccount getUserAccount();

	public enum CurrentUserEvent{
		DomainLocaleVariantChanged;
	};
	
	/**
	 * Gets the domain locale.
	 *
	 * @return the domain locale
	 */
	public DomainLocaleVariant getDomainLocaleVariant();
	
	public UserPropertyManager getUserPropertyManager();
	
	public Subscription getSubscription();
	
	public List<Role> getRoles();
}
