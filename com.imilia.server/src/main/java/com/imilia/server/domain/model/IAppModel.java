/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2014 - all rights reserved
 * Created on: Feb 23, 2014
 * Created by: emcgreal
 */
package com.imilia.server.domain.model;

import javax.validation.Validator;

import org.springframework.context.MessageSource;

import com.imilia.server.domain.CurrentUser;
import com.imilia.server.domain.model.property.PropertyCrudProvider;

/**
 * The Interface IAppModel.
 *
 * @param <U> the generic type
 */
public interface IAppModel<U extends CurrentUser> extends PropertyCrudProvider {
	
	/**
	 * Gets the bean validator.
	 *
	 * @return the bean validator
	 */
	public Validator getBeanValidator();
	
	/**
	 * Gets the message source.
	 *
	 * @return the message source
	 */
	public MessageSource getMessageSource();
	
	/**
	 * Gets the current user.
	 *
	 * @return the current user
	 */
	public U getCurrentUser();
}
