package com.imilia.server.domain.filter;

import java.lang.reflect.Field;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.imilia.server.domain.DomainObject;
import com.imilia.utils.classes.ClassUtils;

/**
 * The Class ClassJoin joins two class filters by a property
 */
public class ClassJoin extends DomainObject {

	/** The logger. */
	private static Logger logger = LoggerFactory.getLogger(ClassJoin.class);
	public final static String TAG_FILTER = "classJoin";
	public final static String TAG_JOIN_PROPERTY = "joinProperty";

	/** The parent. */
	@ManyToOne(optional = false)
	private ClassFilter<?> parent;

	/** The join property. */
	@Column(length = 255)
	private String joinProperty;

	/** The class filter. */
	@ManyToOne(optional = false, cascade = { CascadeType.ALL })
	private ClassFilter<?> classFilter;

	private String fetchConfig;


	/** Position of Join in parent - used to for display only. */
	int position;
	
	private String orderBy;

	/**
	 * Instantiates a new class join.
	 *
	 * @param parent
	 *            the parent
	 * @param joinProperty
	 *            the join property
	 * @param classFilter
	 *            the class filter
	 */
	public ClassJoin(ClassFilter<?> parent, 
			String joinProperty,
			ClassFilter<?> classFilter) {
		this.parent = parent;
		this.joinProperty = joinProperty;
		this.classFilter = classFilter;
		parent.add(this);
	}

	public ClassJoin() {
		super();
	}

	/**
	 * Instantiates a new class join.
	 *
	 * @param parent the parent
	 * @param joinProperty the join property
	 * @param classFilter the class filter
	 * @param fetchConfig the fetch config
	 */
	public ClassJoin(ClassFilter<?> parent, String joinProperty,
			ClassFilter<?> classFilter, String fetchConfig) {
		this.parent = parent;
		this.joinProperty = joinProperty;
		this.classFilter = classFilter;
		this.fetchConfig = fetchConfig;
		parent.add(this);
	}

	/**
	 * Gets the class filter.
	 *
	 * @return the classFilter
	 */
	public ClassFilter<?> getClassFilter() {
		return classFilter;
	}

	/**
	 * Gets the join property.
	 *
	 * @return the joinProperty
	 */
	public String getJoinProperty() {
		return joinProperty;
	}

	/**
	 * To oql.
	 *
	 * @param alias
	 *            the alias
	 * @param query
	 *            the query
	 */
	public void toOql(QueryBuilder query) {
		StringBuffer buffer = query.getProjection();
		
		buffer.append(" fetch ");
		buffer.append(joinProperty);
		
		if (fetchConfig != null){
			buffer.append(fetchConfig);
		}
	}

	/**
	 * Gets the position.
	 *
	 * @return the position
	 */
	public int getPosition() {
		return position;
	}

	/**
	 * Sets the position.
	 *
	 * @param position
	 *            the position to set
	 */
	public void setPosition(int position) {
		this.position = position;
	}

	/**
	 * Duplicate.
	 *
	 * @param parent
	 *            the parent
	 *
	 * @return the class join
	 */
	public ClassJoin duplicate(ClassFilter<?> parent) {
		ClassJoin cj = new ClassJoin(parent, joinProperty, classFilter, fetchConfig);
		cj.setPosition(position);
		return cj;
	}

	/**
	 * Gets the parent.
	 *
	 * @return the parent
	 */
	public ClassFilter<?> getParent() {
		return parent;
	}

	/**
	 * Gets the parent join field.
	 *
	 * @return the parent join field
	 */
	public Field getParentJoinField() {
		try {
			return ClassUtils.findField(getParent().getClazz(), joinProperty);
		} catch (SecurityException e) {
			logger.error("Failed to find join property: " + joinProperty, e);
		} 

		return null;
	}

	/**
	 * Checks if is join on not null field.
	 *
	 * @return true, if is join on not null field
	 */
	public boolean isJoinOnNotNullField() {
		Field parentJoinField = getParentJoinField();

		if (parentJoinField != null
				&& parentJoinField.isAnnotationPresent(NotNull.class)) {
			return true;
		}
		return false;
	}

	/**
	 * Gets the qualified join property name.
	 *
	 * @return the qualified join property name
	 */
	public String getQualifiedJoinPropertyName() {
		return getParent().getDomainClassName() + "." + joinProperty;
	}

	public final void setJoinProperty(String joinProperty) {
		this.joinProperty = joinProperty;
	}

	public final void setParent(ClassFilter<?> parent) {
		this.parent = parent;
	}

	public final void setClassFilter(ClassFilter<?> classFilter) {
		this.classFilter = classFilter;
	}

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}
}
