/**
 * Imilia - Interactive Mobile Applications  GmbH.
 * Copyright (c) 2008
 *
 * @author:		emcgreal
 * @created:	Mar 13, 2008
 * Last modified by:   $Author:$
 * Last modfication:   $Date:$
 * Version:            $Revision:$
 */
package com.imilia.server.domain.filter;

import org.joda.time.DateTime;

import com.imilia.server.validation.ValidationException;

/**
 * The Class Date Time PropertyFilter.
 */
public class DateTimePropertyFilter extends PropertyFilter {

	/** The Constant TAG_FILTER. */
	public final static String TAG_FILTER = "dateTimePropertyFilter";
	
	/** The Constant TAG_LOWERBOUND. */
	public final static String TAG_LOWERBOUND = "lowerBound";
	
	/** The Constant TAG_UPPERBOUND. */
	public final static String TAG_UPPERBOUND = "upperBound";
	
	/** The lower bound. */
	private DateTime lowerBound;

	/** The upper bound. */
	private DateTime upperBound;
	
	/** The time restricted. */
	private boolean timeRestricted;

	/**
	 * Checks if is time restricted.
	 *
	 * @return true, if is time restricted
	 */
	public boolean isTimeRestricted() {
		return timeRestricted;
	}

	/**
	 * Sets the time restricted.
	 *
	 * @param timeRestricted the new time restricted
	 */
	public void setTimeRestricted(boolean timeRestricted) {
		this.timeRestricted = timeRestricted;
	}

	/**
	 * Instantiates a new date property filter.
	 */
	public DateTimePropertyFilter() {
		super();
	}

	/**
	 * Instantiates a new date time property filter.
	 *
	 * @param other the other
	 */
	public DateTimePropertyFilter(DateTimePropertyFilter other) {
		super(other);
		setLowerBound(other.getLowerBound());
		setUpperBound(other.getUpperBound());
	}

	/**
	 * Instantiates a new date property filter.
	 * 
	 * @param propertyName the property name
	 */
	public DateTimePropertyFilter(String propertyName) {
		super(propertyName);
	}

	/**
	 * Instantiates a new date time property filter.
	 *
	 * @param classFilter the class filter
	 * @param propertyName the property name
	 */
	public DateTimePropertyFilter(ClassFilter<?> classFilter, String propertyName) {
		super(classFilter, propertyName);
	}
	
	/**
	 * Instantiates a new date property filter.
	 * 
	 * @param classFilter the class filter
	 * @param propertyName the property name
	 * @param propertyNameLabelMessageID the property name label message id
	 */
	public DateTimePropertyFilter(ClassFilter<?> classFilter, String propertyName, String propertyNameLabelMessageID) {
		super(classFilter, propertyName, propertyNameLabelMessageID);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.imilia.server.domain.filter.PropertyFilter#isValid()
	 */
	public boolean isValid() {
		return lowerBound != null || upperBound != null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.imilia.bkf.api.domain.filter.PropertyFilter#createJPQL(com.imilia
	 * .bkf.api.domain.filter.JPQLQuery)
	 */
	@Override
	public void buildQuery(QueryBuilder q)	throws ValidationException {

		if (isValid()) {
			if (lowerBound != null && upperBound != null) {
				q.addRestriction(addBetwen(q, lowerBound, upperBound));
			} else if (lowerBound != null) {
				q.addRestriction(addComparison(q, ">=", lowerBound));
			} else {
				q.addRestriction(addComparison(q, "<=", upperBound));
			}
		}
	}

	/**
	 * Gets the lower bound.
	 *
	 * @return the lowerBound
	 */
	public DateTime getLowerBound() {
		return lowerBound;
	}

	/**
	 * Sets the lower bound.
	 *
	 * @param lowerBound
	 *            the lowerBound to set
	 */
	public void setLowerBound(DateTime lowerBound) {
		this.lowerBound = lowerBound;
	}

	/**
	 * Gets the upper bound.
	 *
	 * @return the upperBound
	 */
	public DateTime getUpperBound() {
		return upperBound;
	}

	/**
	 * Sets the upper bound.
	 *
	 * @param upperBound
	 *            the upperBound to set
	 */
	public void setUpperBound(DateTime upperBound) {
		this.upperBound = upperBound;
	}

	/**
	 * configure.
	 *
	 * @param lowerBound the lower bound
	 * @param upperBound the upper bound
	 * @return the date time property filter
	 */
	public DateTimePropertyFilter configure(DateTime lowerBound, DateTime upperBound){
		setLowerBound(lowerBound);
		setUpperBound(upperBound);
		return this;
	}
	/*
	 * (non-Javadoc)
	 *
	 * @see com.imilia.bkf.api.domain.filter.PropertyFilter#clear()
	 */
	@Override
	public void clear() {
		lowerBound = null;
		upperBound = null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.imilia.api.domain.filter.PropertyFilter#duplicate()
	 */
	@Override
	public PropertyFilter duplicate() {
		return new DateTimePropertyFilter(this);
	}
	
	/* (non-Javadoc)
	 * @see com.imilia.server.domain.filter.PropertyFilter#visit(com.imilia.server.domain.filter.PropertyFilterVisitor)
	 */
	@Override
	public <T> T visit(PropertyFilterVisitor<T> propertyFilterVisitor) {
		return propertyFilterVisitor.accept(this);		
	}

}
