package com.imilia.server.domain.enums;

/**
 * The Enum MimeType.
 */
public enum MimeType{
	
	/** The Pdf mime type. */
	Pdf("application/pdf"),
	
	/** The Excel mime type. */
	Excel("application/vnd.ms-excel"),
	
	/** The Txt mime type. */
	Txt("text/plain"),

	ICal("text/calendar"),
	
	OctetStream("application/octet-stream");
	
	/** The mime type code. */
	private String code;
	
	/**
	 * Instantiates a new mime type.
	 * 
	 * @param code the code
	 */
	MimeType(String code){
		this.code = code;
	}
	
	/**
	 * Gets the code.
	 * 
	 * @return the code
	 */
	public String getCode(){
		return code;
	}
}