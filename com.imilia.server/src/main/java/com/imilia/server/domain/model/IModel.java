package com.imilia.server.domain.model;

import com.imilia.server.domain.model.manager.IModelManager;
import com.imilia.server.domain.model.property.PropertyAttributeProvider;
import com.imilia.server.domain.model.property.PropertyCrudProvider;

public interface IModel extends PropertyCrudProvider, PropertyAttributeProvider {
	public IModelManager getModelManager();
	
	public IAppModel<?> getAppModel();
	

	public String getName();
	public void setName(String modelName);
}
