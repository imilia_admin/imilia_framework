/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2010 - all rights reserved
 * Created on: May 11, 2010
 * Created by: emcgreal
 */
package com.imilia.server.domain.validation.properties;

/**
 * The Interface ValidationPropertyModel.
 */
public interface PropertyValidationSpecification {
	
	/**
	 * Checks if is mandatory.
	 * 
	 * @param propertyName the property name
	 * 
	 * @return true, if is mandatory
	 */
	public boolean isMandatory();
}
