/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 * Created on: Jan 23, 2009
 * Created by: emcgreal
 */
package com.imilia.server.domain.i18n;

import java.util.Locale;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.imilia.server.domain.DomainObject;
import com.imilia.utils.i18n.DomainLocale;
import com.imilia.utils.i18n.LocalizedProperty;

/**
 * The Class LocaleTextHolder.
 */
@Entity
public class LocaleTextHolder extends DomainObject implements LocalizedProperty {
	// The logger
	private static Logger logger = LoggerFactory.getLogger(LocaleTextHolder.class);

	/** The locale text. */
	@OneToMany(mappedBy = "localeTextHolder", cascade = { CascadeType.ALL }, fetch = FetchType.EAGER)
	private Set<LocaleText> localeTexts; 

	/** The position. */
	private int position;

	/**
	 * Instantiates a new locale text holder.
	 */
	public LocaleTextHolder() {
	}

	/**
	 * Instantiates a new locale text holder.
	 *
	 * @param domainLocale the domain locale
	 * @param text the text
	 */
	public LocaleTextHolder(DomainLocale domainLocale, String text){
		setLocaleText(domainLocale, text);
	}

	/**
	 * Adds the locale text.
	 *
	 * @param localeText the locale text
	 */
	public void addLocaleText(LocaleText localeText) {
		localeText.setLocaleTextHolder(this);
		getLocaleTexts().add(localeText);
	}

	/**
	 * Gets the locale texte.
	 *
	 * @return the locale texte
	 */
	public Set<LocaleText> getLocaleTexts() {
		return localeTexts;
	}

	/**
	 * Sets the locale texte.
	 *
	 * @param localeTexte the new locale texte
	 */
	public void setLocaleTexts(Set<LocaleText> localeTexte) {
		this.localeTexts = localeTexte;
	}

	/**
	 * Gets the position.
	 *
	 * @return the position
	 */
	public int getPosition() {
		return position;
	}

	/**
	 * Sets the position.
	 *
	 * @param position the new position
	 */
	public void setPosition(int position) {
		this.position = position;
	}

	/**
	 * Sets the locale text.
	 *
	 * @param domainLocale the domain locale
	 * @param text the text
	 */
	public void setLocaleText(DomainLocale domainLocale, String text) {
		LocaleText lt = findLocaleText(domainLocale.getLocale());

		if (lt == null) {
			addLocaleText(new LocaleText(domainLocale, this, text));
		} else {
			lt.setText(text);
		}
	}

	/**
	 * Gets the locale text.
	 *
	 * @param aLocale the a locale
	 *
	 * @return the locale text
	 */
	public String getLocaleText(Locale aLocale) {
		LocaleText lt = findLocaleText(aLocale);

		if (lt != null){
			String  text = lt.getText();
			
			if (text != null){
				return text;
			}
		}
		
		if (!localeTexts.isEmpty()) {
			return "{" + localeTexts.iterator().next().getText() + "}";
		} else {
			return "{" + getOid() + "}";
		}
	}

	/**
	 * Find locale text.
	 *
	 * @param aLocale the a locale
	 *
	 * @return the locale text
	 */
	public LocaleText findLocaleText(Locale aLocale) {
		Set<LocaleText> set = getLocaleTexts();
		
		if (!set.isEmpty()){
			for (int i = 3; i >= 0; i--) {
				
				for (LocaleText lt : set ) {
					if (((i < 3 ? "" : aLocale.getVariant()).equals(lt
							.getDomainLocale().getLocale().getVariant()))
							&& ((i < 2 ? "" : aLocale.getCountry()).equals(lt
									.getDomainLocale().getLocale().getCountry()))
							&& ((i < 1 ? "" : aLocale.getLanguage()).equals(lt
									.getDomainLocale().getLocale().getLanguage()))) {
						return lt;
					}
				}
			}
		}else{
			logger.debug("No locale texts");
		}
		return null;
	}

	/**
	 * Creates the.
	 *
	 * @param domainLocale the domain locale
	 * @param text the text
	 *
	 * @return the locale text holder
	 */
	public static LocaleTextHolder create(DomainLocale domainLocale, String text) {
		LocaleTextHolder holder = new LocaleTextHolder();
		new LocaleText(domainLocale, holder, text);
		return holder;
	}

	/* (non-Javadoc)
	 * @see com.imilia.server.domain.i18n.LocalizedTextProperty#getText(com.imilia.server.domain.i18n.DomainLocale)
	 */
	public String getText(DomainLocale locale) {
		return getLocaleText(locale.getLocale());
	}

	/* (non-Javadoc)
	 * @see com.imilia.server.domain.i18n.LocalizedTextProperty#setText(com.imilia.server.domain.i18n.DomainLocale, java.lang.String)
	 */
	public void setText(DomainLocale domainLocale, String text) {
		LocaleText t = findLocaleText(domainLocale.getLocale());

		if (t == null){
			t = new LocaleText(domainLocale, this, text);
		}else{
			t.setText(text);
		}
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString(){
		final StringBuilder builder = new StringBuilder();
		
		for (LocaleText l:getLocaleTexts()){
			builder.append("[");
			builder.append(l.getOid());
			builder.append("]");
			builder.append(l.getText());
		}
		return builder.toString();
	}
}
