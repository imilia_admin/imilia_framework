/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2014 - all rights reserved
 * Created on: Feb 21, 2014
 * Created by: emcgreal
 */
package com.imilia.server.domain.model.property;

import java.util.HashMap;

/**
 * The Class DefaultAttributes.
 */
public class DefaultAttributes implements PropertyAttributeProvider{

	/** The attributes. */
	private final HashMap<String, Object> attributes = new HashMap<String, Object>();
	
	/* (non-Javadoc)
	 * @see com.imilia.server.domain.model.PropertyAttributes#get(java.lang.String, java.lang.String)
	 */
	@Override
	public Object getAttribute(Property property, String name) {
		return attributes.get(property.getQName() + "." + name);
	}

	/* (non-Javadoc)
	 * @see com.imilia.server.domain.model.PropertyAttributes#set(java.lang.String, java.lang.String, java.lang.Object)
	 */
	public PropertyAttributeProvider setAttribute(Property property, String name, Object value) {
		attributes.put(property.getQName() + "." + name, value);
		return this;
	}
	
	/**
	 * Gets the.
	 *
	 * @return the hash map
	 */
	public HashMap<String, Object> get() {
		return attributes;
	}
}
