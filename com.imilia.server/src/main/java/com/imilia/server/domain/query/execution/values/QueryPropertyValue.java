/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2014 - all rights reserved
 * Created on: Feb 27, 2014
 * Created by: emcgreal
 */
package com.imilia.server.domain.query.execution.values;

import com.imilia.server.domain.query.entities.QueryProperty;
import com.imilia.server.domain.query.execution.QueryCrudExecution;

/**
 * The Class RestrictionValue.
 */
public class QueryPropertyValue {
	
	/** The query property. */
	protected final QueryProperty queryProperty;

	private String path;
	
	/**
	 * Instantiates a new restriction value.
	 *
	 * @param restriction the restriction
	 */
	public QueryPropertyValue(QueryProperty queryProperty) {
		super();
		this.queryProperty = queryProperty;
		this.path = queryProperty.getPropertyName();
	}


	/**
	 * Checks if is valid.
	 *
	 * @return true, if is valid
	 */
	public boolean isValid(){
		return true;
	}

	/**
	 * Adds the to execution.
	 *
	 * @param execution the execution
	 */
	public void addToExecution(QueryCrudExecution<?> execution){
		
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}


	public QueryProperty getQueryProperty() {
		return queryProperty;
	}
}
