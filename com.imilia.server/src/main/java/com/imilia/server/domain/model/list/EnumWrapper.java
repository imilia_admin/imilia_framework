package com.imilia.server.domain.model.list;

import java.util.Locale;

import org.springframework.context.MessageSource;

import com.imilia.utils.i18n.Localized;

class EnumWrapper implements Localized{
	private Enum<?> enumValue;

	public EnumWrapper(Enum<?> e){
		this.enumValue = e;
	}
	public String getLocalizedMessage(MessageSource messageSource,
			Locale locale) {
		return messageSource.getMessage(enumValue.getClass().getName() + "." + enumValue.name(), null, locale);
	}
	public Enum<?> getEnumValue() {
		return enumValue;
	}
	public void setEnumValue(Enum<?> enumValue) {
		this.enumValue = enumValue;
	}
}