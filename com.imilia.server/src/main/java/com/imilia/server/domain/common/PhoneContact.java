/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 * Created on: Jan 23, 2009
 * Created by: emcgreal
 */

package com.imilia.server.domain.common;

import java.util.Locale;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Pattern;

import org.springframework.context.MessageSource;

import com.imilia.server.domain.DomainObject;
import com.imilia.server.domain.enums.PhoneNumberType;
import com.imilia.server.domain.validation.properties.PropertyValidationSpecification;
import com.imilia.utils.Flags;
import com.imilia.utils.i18n.Localized;

/**
 * The Class PhoneContact holds phone number and type details.
 * 
 * @author emcg
 */
@Entity
public class PhoneContact extends DomainObject implements Localized {
	
	/** The Constant PhoneContactNumber. */
	public final static String PhoneContactNumber = "number";

	/** The phone number. */
	@Pattern(regexp = "[\\+]?[0-9( )*-]*")
	private String number;
	
	
	/** The comment. */
	private String comment;

	/** The Constant SHARE_NUMBER. */
	private final static long SHARE_NUMBER = 0x00000001;
	
	/** The Constant SHARE_COMMENT. */
	private final static long SHARE_COMMENT = 0x00000002;

	/** The flags. */
	private long flags;

	/** The phone number type. */
	@Enumerated(EnumType.ORDINAL)
	private PhoneNumberType phoneNumberType = PhoneNumberType.Home;

	/** The address. */
	@ManyToOne(optional = true)
	private Address address;

	/**
	 * Default constructor.
	 */
	public PhoneContact() {
	}

	/**
	 * Instantiates a new phone contact.
	 *
	 * @param number the number
	 */
	public PhoneContact(String number) {
		this.number = number;
	}
	
	/**
	 * Instantiates a new phone contact.
	 * 
	 * @param number the number
	 * @param phoneNumberType the phone number type
	 */
	public PhoneContact(String number, PhoneNumberType phoneNumberType) {
		this.number = number;
		this.phoneNumberType = phoneNumberType;
	}

	/**
	 * Gets the number.
	 * 
	 * @return the number
	 */
	public String getNumber() {
		return number;
	}

	/**
	 * Sets the number.
	 * 
	 * @param number the number to set
	 */
	public void setNumber(String number) {
		this.number = number;
	}

	/**
	 * Gets the phone number type.
	 * 
	 * @return the phoneNumberType
	 */
	public PhoneNumberType getPhoneNumberType() {
		return phoneNumberType;
	}

	/**
	 * Sets the phone number type.
	 * 
	 * @param phoneNumberType the phoneNumberType to set
	 */
	public void setPhoneNumberType(PhoneNumberType phoneNumberType) {
		this.phoneNumberType = phoneNumberType;
	}

	/**
	 * Gets the address.
	 * 
	 * @return the address
	 */
	public Address getAddress() {
		return address;
	}

	/**
	 * Sets the address.
	 * 
	 * @param address the new address
	 */
	public void setAddress(Address address) {
		this.address = address;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.imilia.server.domain.Localized#getLocalizedMessage(org.springframework
	 * .context.MessageSource, java.util.Locale)
	 */
	public String getLocalizedMessage(MessageSource messageSource, Locale locale) {
		return messageSource.getMessage(
				"com.imilia.server.domain.common.PhoneContact.localized",
				new Object[] {
						getNumber(),
						getPhoneNumberType().getLocalizedMessage(messageSource,
								locale) }, locale);
	}
	
	/**
	 * Gets the full information.
	 * 
	 * @return the full information
	 */
	public Localized getFullInformation(){
		return new Localized() {
			public String getLocalizedMessage(MessageSource messageSource,
					Locale locale) {
				
				StringBuilder builder = new StringBuilder();
				if (number != null){
					builder.append(number);
					builder.append( " (");
					builder.append(phoneNumberType.getLocalizedMessage(messageSource, locale));
				
					final String s = getComment();
					if (s != null){
						builder.append(" | ");
						builder.append(s);
					}
				
					builder.append( " )");
				}
				return builder.toString();
			}
		};
	}
	
	/**
	 * Gets the full information shared.
	 *
	 * @return the full information shared
	 */
	public Localized getFullInformationShared(){
		if (isShareNumber()){
			return new Localized() {
				public String getLocalizedMessage(MessageSource messageSource,
						Locale locale) {
					
					StringBuilder builder = new StringBuilder();
					if (number != null){
						builder.append(number);
						builder.append( " (");
						builder.append(phoneNumberType.getLocalizedMessage(messageSource, locale));
					
						if (isShareComment()){
							final String s = getComment();
							if (s != null){
								builder.append(" | ");
								builder.append(s);
							}
						}
					
						builder.append( " )");
					}
					return builder.toString();
				}
			};		}
		return null;
	}

	/**
	 * Gets the comment.
	 *
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * Sets the comment.
	 *
	 * @param comment the new comment
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}

	/**
	 * Gets the flags.
	 *
	 * @return the flags
	 */
	public long getFlags() {
		return flags;
	}

	/**
	 * Sets the flags.
	 *
	 * @param flags the new flags
	 */
	public void setFlags(long flags) {
		this.flags = flags;
	}
	
	/**
	 * Sets the share number.
	 *
	 * @param share the new share number
	 */
	public void setShareNumber(boolean share) {
		flags = Flags.set(getFlags(), SHARE_NUMBER, share);
	}

	/**
	 * Checks if is share number.
	 *
	 * @return true, if is share number
	 */
	public boolean isShareNumber() {
		return Flags.isSet(getFlags(), SHARE_NUMBER);
	}
	
	/**
	 * Gets the number shared.
	 *
	 * @return the number shared
	 */
	public String getNumberShared(){
		if (isShareNumber()){
			return getNumber();
		}
		return null;
	}
	
	/**
	 * Sets the share comment.
	 *
	 * @param share the new share comment
	 */
	public void setShareComment(boolean share) {
		flags = Flags.set(getFlags(), SHARE_COMMENT, share);
	}

	/**
	 * Checks if is share comment.
	 *
	 * @return true, if is share comment
	 */
	public boolean isShareComment() {
		return Flags.isSet(getFlags(), SHARE_COMMENT);
	}
	
	/**
	 * Gets the comment shared.
	 *
	 * @return the comment shared
	 */
	public String getCommentShared(){
		if (isShareComment()){
			return getComment();
		}
		return null;
	}
}