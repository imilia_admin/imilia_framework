/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2014 - all rights reserved
 * Created on: Feb 27, 2014
 * Created by: emcgreal
 */
package com.imilia.server.domain.query.execution.values;

import com.imilia.server.domain.query.entities.QueryProperty;
import com.imilia.server.domain.query.entities.QueryRestriction;
import com.imilia.server.domain.query.execution.Comparison;
import com.imilia.server.domain.query.execution.LogicalOperator;
import com.imilia.server.domain.query.execution.QueryCrudExecution;
import com.imilia.utils.strings.StringUtils;

/**
 * The Class RestrictionStringValue.
 */
public class LongQueryPropertyValue extends ComparisonValue {

	/** The predicates. */
	private String predicates;


	public LongQueryPropertyValue(QueryProperty queryProperty, Comparison comparison) {
		super(queryProperty, comparison);
		addPossibleComparisons(
				Comparison.IsNull,
				Comparison.NotNull,
				Comparison.Equals,
				Comparison.NotEquals,
				Comparison.LessThan,
				Comparison.LessThanEquals,
				Comparison.GreaterThan,
				Comparison.GreaterThanEquals,
				Comparison.Between,
				Comparison.In);
	}
	
	/**
	 * Gets the predicates.
	 *
	 * @return the predicates
	 */
	public String getPredicates() {
		return predicates;
	}

	/**
	 * Sets the predicates.
	 *
	 * @param predicates the new predicates
	 */
	public void setPredicates(String predicates) {
		this.predicates = predicates;
	}

	/* (non-Javadoc)
	 * @see com.imilia.server.domain.query.execution.values.RestrictionValue#isValid()
	 */
	@Override
	public boolean isValid() {
		return StringUtils.isNonEmpty(predicates);
	}

	/* (non-Javadoc)
	 * @see com.imilia.server.domain.query.execution.values.RestrictionValue#addToExecution(com.imilia.server.domain.query.execution.QueryExecution)
	 */
	@Override
	public void addToExecution(QueryCrudExecution<?> execution) {
		if (predicates == null){
			return;
		}
		
		switch(comparison){
		case Like:
		case LikeCaseSensitive:
		case Equals:
		case NotEquals:
		case LessThan:
		case LessThanEquals:
		case GreaterThan:
		case GreaterThanEquals:
		case IsNull:
		case NotNull:
			execution.addRestrictionValue(
					getComparison().toPredicate(getPath(), execution.addParameter(predicates)), 
					LogicalOperator.And);
			break;
			
		case Between:
		case In:
			execution.addRestrictionValue(
					getPath() + 
					getComparison().toPredicate(getPath(), execution.addParameters(getMulitpleValues())), 
					LogicalOperator.And);
		}
		
	}
	
	Long getSingleValue(){
		return Long.parseLong(predicates);
	}
	
	Long[] getMulitpleValues(){
		String[] strings = predicates.split("\\|");
		
		Long[] longs = new Long[strings.length];
		
		for (int i = 0;i < strings.length;i++){
			longs[i] = Long.parseLong(strings[i]);
		}
		
		return longs;
	}

	@Override
	public String toString() {
		return "LongQueryPropertyValue [path="+ getPath() + " predicates=" + predicates + "]";
	}
	
	
}
