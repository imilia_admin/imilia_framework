/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2014 - all rights reserved
 * Created on: Mar 4, 2014
 * Created by: emcgreal
 */
package com.imilia.server.domain.query.execution.values;

import java.util.ArrayList;
import java.util.List;

import com.imilia.server.domain.query.entities.QueryProperty;
import com.imilia.server.domain.query.entities.QueryRestriction;
import com.imilia.server.domain.query.execution.Comparison;

/**
 * The Class ComparisonRestrictionValue.
 */
public class ComparisonValue extends QueryPropertyValue {

	/** The comparison. */
	protected Comparison comparison;
	
	/** The possible comparisons. */
	protected final List<Comparison> possibleComparisons = new ArrayList<>();
	
	/** The hidden. */
	private boolean hidden;
	
	
	/**
	 * Instantiates a new comparison restriction value.
	 *
	 * @param property the restriction
	 */
	public ComparisonValue(QueryProperty property) {
		super(property);
	}

	/**
	 * Instantiates a new comparison restriction value.
	 *
	 * @param property the property
	 * @param comparison the comparison
	 */
	public ComparisonValue(QueryProperty property,
			Comparison comparison) {
		super(property);
		this.comparison = comparison;
	}

	/**
	 * Gets the comparison.
	 *
	 * @return the comparison
	 */
	public Comparison getComparison() {
		return comparison;
	}

	/**
	 * Sets the comparison.
	 *
	 * @param comparison the new comparison
	 */
	public void setComparison(Comparison comparison) {
		this.comparison = comparison;
	}

	/**
	 * Checks if is hidden.
	 *
	 * @return true, if is hidden
	 */
	public boolean isHidden() {
		return hidden;
	}

	/**
	 * Sets the hidden.
	 *
	 * @param hidden the new hidden
	 */
	public void setHidden(boolean hidden) {
		this.hidden = hidden;
	}

	public List<Comparison> getPossibleComparisons() {
		return possibleComparisons;
	}

	public void addPossibleComparisons(Comparison...comparisons){
		for (Comparison c:comparisons){
			if (!possibleComparisons.contains(c)){
				possibleComparisons.add(c);
			}
		}
	}
}
