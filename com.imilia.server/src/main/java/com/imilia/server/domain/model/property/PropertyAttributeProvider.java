package com.imilia.server.domain.model.property;

public interface PropertyAttributeProvider {
	
	// Common attribute paths
	public final static String TITLE = "title";
	public final static String HINT = "hint";
	public final static String STYLE = "style";
	public final static String LIST = "list";
	public static final String DATEFORMAT = "dateFormat";
	public static final String MAXLEN = "maxlen";
	public static final String MANDATORY = "mandatory";
	
	
	public Object getAttribute(Property property, String attributeName);


	public PropertyAttributeProvider setAttribute(Property property, String attributeName, Object value);
}
