/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2014 - all rights reserved
 * Created on: Feb 27, 2014
 * Created by: emcgreal
 */
package com.imilia.server.domain.query.execution.values;

import com.imilia.server.domain.query.entities.QueryProperty;
import com.imilia.server.domain.query.entities.QueryRestriction;
import com.imilia.server.domain.query.execution.Comparison;
import com.imilia.server.domain.query.execution.LogicalOperator;
import com.imilia.server.domain.query.execution.QueryCrudExecution;
import com.imilia.utils.strings.StringUtils;

/**
 * The Class RestrictionStringValue.
 */
public class StringQueryPropertyValue extends ComparisonValue {

	/** The predicates. */
	private String predicates;


	public StringQueryPropertyValue(QueryProperty property, Comparison comparison) {
		super(property, comparison);
		addPossibleComparisons(
				Comparison.Like,
				Comparison.LikeCaseSensitive,
				Comparison.Equals,
				Comparison.NotEquals,
				Comparison.LessThan,
				Comparison.LessThanEquals,
				Comparison.GreaterThan,
				Comparison.GreaterThanEquals,
				Comparison.Between,
				Comparison.In);
	}
	

	/**
	 * Instantiates a new restriction string value.
	 *
	 * @param p the restriction
	 */
	public StringQueryPropertyValue(QueryProperty p) {
		this(p, Comparison.Like);
		
	}

	public StringQueryPropertyValue(QueryProperty p, Comparison comparison, String predicates) {
		this(p, comparison);
		this.predicates = predicates;
	}

	/**
	 * Gets the predicates.
	 *
	 * @return the predicates
	 */
	public String getPredicates() {
		return predicates;
	}

	/**
	 * Sets the predicates.
	 *
	 * @param predicates the new predicates
	 */
	public void setPredicates(String predicates) {
		this.predicates = predicates;
	}

	/* (non-Javadoc)
	 * @see com.imilia.server.domain.query.execution.values.RestrictionValue#isValid()
	 */
	@Override
	public boolean isValid() {
		return StringUtils.isNonEmpty(predicates);
	}

	/* (non-Javadoc)
	 * @see com.imilia.server.domain.query.execution.values.RestrictionValue#addToExecution(com.imilia.server.domain.query.execution.QueryExecution)
	 */
	@Override
	public void addToExecution(QueryCrudExecution<?> execution) {
		if (predicates == null){
			return;
		}
		
		String s = predicates;
		switch(comparison){
		case Like:
		case LikeCaseSensitive:
			s = s.replace('*', '%');
		case IsNull:
		case NotNull:
		case Equals:
		case NotEquals:
		case LessThan:
		case LessThanEquals:
		case GreaterThan:
		case GreaterThanEquals:
			execution.addRestrictionValue(
					getComparison().toPredicate(getQueryProperty().getPropertyName(), execution.addParameter(s)), 
					LogicalOperator.And);
			break;
			
		case Between:
		case In:
			execution.addRestrictionValue(
					getQueryProperty().getPropertyName() + 
					getComparison().toPredicate(getQueryProperty().getPropertyName(), execution.addParameters(s.split("\\|"))), 
					LogicalOperator.And);
		}
		
	}


	@Override
	public String toString() {
		return "StringQueryProperty [path=" + getPath() + " predicates=" + predicates + "]";
	}
}
