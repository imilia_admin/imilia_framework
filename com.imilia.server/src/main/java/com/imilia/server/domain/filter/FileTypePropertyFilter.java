/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 *
 * Created on: Jan 05, 2010
 * Created by: mtemm
 */
package com.imilia.server.domain.filter;

import java.util.ArrayList;
import java.util.List;

import com.imilia.server.validation.ValidationException;

/**
 * The Class FileTypePropertyFilter.
 */
public class FileTypePropertyFilter extends StringPropertyFilter {
	
	/** The file types. */
	private List<String> fileTypes = new ArrayList<String>();

	/**
	 * Instantiates a new file type property filter.
	 */
	public FileTypePropertyFilter() {
	}
	
	/**
	 * Instantiates a new file type property filter.
	 * 
	 * @param classFilter the class filter
	 * @param propertyName the property name
	 * @param fileTypes the file types
	 */
	public FileTypePropertyFilter(ClassFilter<?> classFilter, String propertyName,
			List<String> fileTypes) {
		super(classFilter, propertyName);
		this.fileTypes = fileTypes;
	}

	/* (non-Javadoc)
	 * @see com.imilia.server.domain.filter.PropertyFilter#buildQuery(com.imilia.server.domain.filter.QueryBuilder)
	 */
	@Override
	public void buildQuery(QueryBuilder q) throws ValidationException {
		if (isValid()) {
			StringBuffer oidBuffer = new StringBuffer();
			for (String s : fileTypes) {
				if (oidBuffer.length() > 0) {
					oidBuffer.append(", ");
				}
				oidBuffer.append("'");
				oidBuffer.append(s);
				oidBuffer.append("'");
			}

			StringBuffer buffer = new StringBuffer();
			buffer.append("SUBSTRING_INDEX (");
			buffer.append(getQualifiedName());
			buffer.append(",'.',-1)");
			buffer.append(" IN ");
			buffer.append("(");
			buffer.append(oidBuffer.toString());
			buffer.append(")");

			final String query = buffer.toString();
		
			q.addRestriction(query);
		}
	
	}
	
	/*
	 * (non-Javadoc)
	 *
	 * @see com.imilia.bkf.api.domain.filter.PropertyFilter#isValid()
	 */
	@Override
	public boolean isValid() {
		return !this.fileTypes.isEmpty();
	}

	public List<String> getFileTypes() {
		return fileTypes;
	}

	public void setFileTypes(List<String> fileTypes) {
		this.fileTypes = fileTypes;
	}


}
