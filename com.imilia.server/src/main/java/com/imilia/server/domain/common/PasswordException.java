/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 * Created on: Jan 23, 2009
 * Created by: emcgreal
 */

package com.imilia.server.domain.common;

/**
 * The Class PasswordException.
 */
public class PasswordException extends Exception {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -4793965910528571179L;

	/**
	 * The Enum Problem.
	 */
	public enum Problem{
		
		/** The Password same as old. */
		PasswordSameAsOld
	};
	
	/** The problem. */
	private Problem problem;
	
	/**
	 * Instantiates a new password exception.
	 */
	public PasswordException() {
	}

	/**
	 * Instantiates a new password exception.
	 * 
	 * @param p the p
	 */
	public PasswordException(Problem p) {
		this.problem = p;
	}
	
	/**
	 * The Constructor.
	 * 
	 * @param arg0 the arg0
	 */
	public PasswordException(String arg0) {
		super(arg0);
	}

	/**
	 * The Constructor.
	 * 
	 * @param arg0 the arg0
	 */
	public PasswordException(Throwable arg0) {
		super(arg0);
	}

	/**
	 * The Constructor.
	 * 
	 * @param arg0 the arg0
	 * @param arg1 the arg1
	 */
	public PasswordException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	/**
	 * Gets the problem.
	 * 
	 * @return the problem
	 */
	public Problem getProblem() {
		return problem;
	}

	/**
	 * Sets the problem.
	 * 
	 * @param problem the new problem
	 */
	public void setProblem(Problem problem) {
		this.problem = problem;
	}

}
