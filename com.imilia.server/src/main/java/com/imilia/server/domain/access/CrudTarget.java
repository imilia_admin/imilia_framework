/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2014 - all rights reserved
 * Created on: Feb 19, 2014
 * Created by: emcgreal
 */

package com.imilia.server.domain.access;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.imilia.server.domain.Crud;
import com.imilia.server.domain.DomainObject;
import com.imilia.server.domain.filter.RestrictionCandidate;

/**
 * The Class AttributesEntity.
 */
@Entity
public class CrudTarget extends DomainObject{
	
	/** The property name as exposed by the model. */
	@RestrictionCandidate
	private String name;
	
	/** The owning model. */
	@ManyToOne(optional=false)
	private ModelCrud model;
	
	/** The crud. */
	@Embedded
	private Crud crud;

	public CrudTarget(ModelCrud mp, String target, Crud crud) {
		this.model = mp;
		this.model.getCrudTargets().add(this);
		this.name = target;
		this.crud = crud;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ModelCrud getModel() {
		return model;
	}

	public void setModel(ModelCrud model) {
		this.model = model;
	}

	public Crud getCrud() {
		return crud;
	}

	public void setCrud(Crud crud) {
		this.crud = crud;
	}
}
