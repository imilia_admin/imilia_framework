/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2008 - all rights reserved
 * Created on: Jan 06, 2010
 * Created by: emcgreal
 */
package com.imilia.server.domain;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import com.imilia.server.domain.enums.ObjectStatus;


/**
 * The Class ObjectStatusIterator.
 * 
 * @author emcg
 * A ObjectStatusIterator is used to  only iteratr over objects with the
 * given status
 */
public class ObjectStatusIterator<T extends DomainObject> {
	
	/** The iter. */
	private Iterator<T> iter;
	
	/** The status. */
	private ObjectStatus status;
	
	/** The current. */
	private T current;
	
	
	/**
	 * Constructor.
	 * 
	 * @param iter the iter
	 * @param status the status
	 */
	public ObjectStatusIterator(Iterator<T> iter, ObjectStatus status){
		this.iter = iter;
		this.status = status;
	}
	
	/**
	 * Constructor - using Active as status.
	 * 
	 * @param iter the iter
	 */
	public ObjectStatusIterator(Iterator<T> iter){
		this(iter, ObjectStatus.Active);
	}
	
	/**
	 * Checks for next.
	 * 
	 * @return true if an object with the given status is available
	 */
	boolean hasNext(){
		while(iter.hasNext()){
			current = iter.next();
			if (current.getObjectStatus().equals(status)){
				return true;
			}
		}
		current = null;
		return false;
	}
	
	/**
	 * Next.
	 * 
	 * @return the next object with the given status
	 */
	public T next(){
		return current;
	}
	
	/**
	 * To list.
	 * 
	 * @return a list of objects with the given status
	 */
	public List<T> toList(){
		ArrayList<T> filtered = new ArrayList<T>();
		
		while (hasNext()){
			filtered.add(next());
		}
		return filtered;
	}
	
	/**
	 * To set.
	 * 
	 * @return a set with the given status
	 */
	public HashSet<T> toSet(){
		HashSet<T> filtered = new HashSet<T>();
		
		while (hasNext()){
			filtered.add(next());
		}
		return filtered;
	}
}
