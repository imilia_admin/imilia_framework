/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 *
 * Created on: Apr 16, 2009
 * Created by: eddiemcgreal
 */

package com.imilia.server.domain.filter;

import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.FieldError;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.imilia.server.validation.ValidationException;

import java.util.ArrayList;
import java.util.List;

/**
 * The Class IntegerPropertyFilter.
 */
public class IntegerPropertyFilter extends PropertyFilter {
	
	/** The Constant TAG_FILTER. */
	public final static String TAG_FILTER = "integerPropertyFilter";
	
	/** The Constant TAG_PREDICATES. */
	public final static String TAG_PREDICATES = "predicates";

	/** The Constant INTEGER_FORMAT_ERROR. */
	private static final String INTEGER_FORMAT_ERROR = "typeMismatch.java.lang.Integer";

	/** The Constant logger. */
	private final static Logger logger = LoggerFactory.getLogger(IntegerPropertyFilter.class);

	/** The int predicates. */
	private String intPredicates;

	/** The longs. */
	private List<Integer> ints = new ArrayList<Integer>(); 
	/**
	 * Instantiates a new integer property filter.
	 */
	public IntegerPropertyFilter() {
		super();
	}

	/**
	 * Instantiates a new integer property filter.
	 *
	 * @param other the other
	 */
	public IntegerPropertyFilter(IntegerPropertyFilter other) {
		super(other);
		setIntPredicates(other.intPredicates);
	}


	/**
	 * Instantiates a new integer property filter.
	 *
	 * @param propertyName the property name
	 */
	public IntegerPropertyFilter(String propertyName) {
		super(propertyName);
	}

	/**
	 * Instantiates a new integer property filter.
	 *
	 * @param classFilter the class filter
	 * @param propertyName the property name
	 */
	public IntegerPropertyFilter(ClassFilter<?> classFilter, String propertyName) {
		super(classFilter, propertyName);
	}
	
	/**
	 * Instantiates a new integer property filter.
	 * 
	 * @param classFilter the class filter
	 * @param propertyName the property name
	 * @param propertyNameLabelMessageID the property name label message id
	 */
	public IntegerPropertyFilter(ClassFilter<?> classFilter, String propertyName, String propertyNameLabelMessageID) {
		super(classFilter, propertyName, propertyNameLabelMessageID);
	}

	/**
	 * Parses the range.
	 *
	 * @param q the q
	 * @param token the token
	 *
	 * @return the string
	 *
	 * @throws ValidationException the validation exception
	 */
	private String parseRange(QueryBuilder q, String token) throws ValidationException {
		String[] tokens = token.split(rangeDelimiter);

		if (tokens != null) {
			if (tokens.length == 2) {
				int lhs = parseInt(tokens[0]);
				int rhs = parseInt(tokens[1]);

				return addBetwen(q, lhs, rhs);
			} else {
				return getQualifiedName() + " = " + q.addParam(parseInt(tokens[0]));
			}
		}
		return null;
	}

	/**
	 * Parses the int.
	 *
	 * @param s the s
	 * @return the int
	 * @throws ValidationException the validation exception
	 */
	private int parseInt(String s) throws ValidationException {
		try {
			return Integer.parseInt(s.trim());
		} catch (NumberFormatException ex) {
			logger.debug("Invalid long filter: " + s);

			final String clazzName = getClassFilter().getClazz().getCanonicalName();
			FieldError o = new FieldError(clazzName,
					getQualifiedName(),
					s,
					true,
					new String[] { INTEGER_FORMAT_ERROR },
					new Object[] {
						new DefaultMessageSourceResolvable(	INTEGER_FORMAT_ERROR)
					}, INTEGER_FORMAT_ERROR);
			BeanPropertyBindingResult errors =
				new BeanPropertyBindingResult(this, clazzName);
			errors.addError(o);
			throw new ValidationException(errors, this);
		}
	}

	/* (non-Javadoc)
	 * @see com.imilia.server.domain.filter.PropertyFilter#isValid()
	 */
	public boolean isValid() {
		return intPredicates != null && !"".equals(intPredicates);
	}

	/**
	 * Gets the int predicates.
	 *
	 * @return the int predicates
	 */
	public String getIntPredicates() {
		return intPredicates;
	}

	/**
	 * Sets the int predicates.
	 *
	 * @param longPredicates the new int predicates
	 */
	public void setIntPredicates(String longPredicates) {
		this.intPredicates = longPredicates;
	}

	/**
	 * Configure.
	 *
	 * @param longPredicates the long predicates
	 * @return the integer property filter
	 */
	public IntegerPropertyFilter configure(String longPredicates) {
		setIntPredicates(longPredicates);
		return this;
	}

	/* (non-Javadoc)
	 * @see com.imilia.server.domain.filter.PropertyFilter#buildQuery(com.imilia.server.domain.filter.QueryBuilder)
	 */
	@Override
	public void buildQuery(QueryBuilder q) throws ValidationException{
		if (isValid()) {
			final StringBuffer buffer = new StringBuffer();
			if (!isNullOrNotNull(buffer, intPredicates)){
				// Split the longPredicates up using the delimiter ;
				final String[] tokens = intPredicates.split(";");

				for (String t : tokens) {
					String c = parseRange(q, t);

					if (c != null){
						if (buffer.length() > 0) {
							buffer.append( " OR ");
						}
						buffer.append(c);
					}
				}
			}
			final String query = buffer.toString();
			logger.debug(query);
			q.addRestriction(query);
		}
	}


	/* (non-Javadoc)
	 * @see com.imilia.bkf.api.domain.filter.PropertyFilter#clear()
	 */
	@Override
	public void clear() {
		intPredicates = null;
		ints.clear();
	}


	/* (non-Javadoc)
	 * @see com.imilia.api.domain.filter.PropertyFilter#duplicate()
	 */
	@Override
	public PropertyFilter duplicate() {
		return new IntegerPropertyFilter(this); 
	}

	/* (non-Javadoc)
	 * @see com.imilia.server.domain.filter.PropertyFilter#visit(com.imilia.server.domain.filter.PropertyFilterVisitor)
	 */
	@Override
	public <T> T visit(PropertyFilterVisitor<T> propertyFilterVisitor) {
		return propertyFilterVisitor.accept(this);		
	}

	public String getFilterPropertyName() {
		return "ints";
	}
	
	public void setInts(List<Integer> intPredicates) {
		this.ints = intPredicates;
		
		StringBuffer buffer = new StringBuffer();
		
		for (Integer i:intPredicates){
			if (buffer.length() > 0){
				buffer.append(";");
			}
			buffer.append(i);
		}
		
		this.intPredicates = buffer.toString();
	}
	
	/**
	 * Gets the ints.
	 *
	 * @return the ints
	 */
	public List<Integer> getInts(){
		return ints;
	}
}
