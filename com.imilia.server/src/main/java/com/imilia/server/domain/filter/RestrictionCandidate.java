/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 * Created on: Jan 23, 2009
 * Created by: emcgreal
 */

package com.imilia.server.domain.filter;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * The Interface RestrictionCandidate.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface RestrictionCandidate {

	/**
	 * Name.
	 * 
	 * @return the string
	 */
	String name() default "";

	/**
	 * List.
	 * 
	 * @return true, if it is a list
	 */
	boolean list() default false;

	/**
	 * Min selected.
	 * 
	 * @return the int
	 */
	int minSelected() default 0;

	/**
	 * Mandatory.
	 * 
	 * @return true, if successful
	 */
	boolean mandatory() default false;
	
	/**
	 * Bitmask.
	 * 
	 * @return true, if successful
	 */
	boolean bitmask() default false;
	
	/**
	 * Bit mask operator use to mask a list of longs.
	 *
	 * @return the bit mask operator
	 */
	BitMaskOperator bitMaskOperator() default BitMaskOperator.OR; 
	
	/**
	 * Time restricted.
	 *
	 * @return true, if successful
	 */
	boolean timeRestricted() default false;
	
	/**
	 * Advanced.
	 *
	 * @return true, if successful
	 */
	boolean advanced() default false;
	
	/**
	 * Flag list.
	 *
	 * @return true, if successful
	 */
	boolean flagList() default false;
	
	/**
	 * Path.
	 *
	 * @return the string
	 */
	String path() default "";
}
