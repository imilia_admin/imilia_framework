/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2010 - all rights reserved
 * Created on: May 11, 2010
 * Created by: emcgreal
 */
package com.imilia.server.domain.validation;

import java.util.HashMap;

import com.imilia.server.domain.validation.properties.PropertyValidationModel;
import com.imilia.server.domain.validation.properties.PropertyValidationSpecification;

/**
 * The Class ClassValidationPropertyModel holds a hash map of
 * ValidationClassSpec that are associated with a specific path.
 */
public class ClassValidationPropertyModel implements PropertyValidationModel{

	/** The class specs. */
	private final HashMap<String, ValidationClassSpec> classSpecs = new HashMap<String, ValidationClassSpec>();
	
	
	/* (non-Javadoc)
	 * @see com.imilia.server.domain.validation.ValidationPropertyModel#isMandatory(java.lang.String)
	 */
	public PropertyValidationSpecification getPropertyValidationSpecification(String propertyName) {
		int indexOf = propertyName.lastIndexOf('.');
		
		if (indexOf > 0){
			String path = propertyName.substring(0, indexOf);
			ValidationClassSpec spec = classSpecs.get(path);
			
			if (spec != null){
				String name = propertyName.substring(indexOf+1);
				return spec.getPropertyValidationSpecification(name);
			}
		}else{
			// Try all specs - hopefully all prop specs have a unique name
			for (ValidationClassSpec spec:classSpecs.values()){
				return spec.getPropertyValidationSpecification(propertyName);
			}
		}
		return null;
	}
	
	/**
	 * Find the class spec.
	 * 
	 * @param clazz the clazz
	 * 
	 * @return the validation class spec
	 */
	public ValidationClassSpec findClassSpec(Class<?> clazz){
		final String clazzName = clazz.getCanonicalName();
		for (ValidationClassSpec spec:classSpecs.values()){
			if (spec.getClazz().equals(clazzName)){
				return spec;
			}
		}
		return null;
	}

	/**
	 * Gets the class specs.
	 *
	 * @return the class specs
	 */
	public final HashMap<String, ValidationClassSpec> getClassSpecs() {
		return classSpecs;
	}

	/**
	 * Put class spec.
	 *
	 * @param path the path
	 * @param spec the spec
	 */
	public void putClassSpec(String path, ValidationClassSpec spec){
		classSpecs.put(path, spec);
	}
}
