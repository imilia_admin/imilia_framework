/**
 * Imilia - Interactive Mobile Applications  GmbH.
 * Copyright (c) 2008
 *
 * @author:		emcgreal
 * @created:	Mar 13, 2008
 * Last modified by:   $Author:$
 * Last modfication:   $Date:$
 * Version:            $Revision:$
 */
package com.imilia.server.domain.filter;

import java.lang.reflect.Field;

import com.imilia.utils.Flags;
import com.imilia.utils.classes.ClassUtils;
import com.imilia.server.validation.ValidationException;

/**
 * The Class PropertyFilter.
 */
public abstract class PropertyFilter{

	/** The Constant TAG_PROPERTY_FILTERS. */
	public final static String TAG_PROPERTY_FILTERS = "propertyFilters";
	
	/** The Constant TAG_PROPERTY_NAME. */
	public final static String TAG_PROPERTY_NAME = "propertyName";
	
	/** The Constant TAG_PROPERTY_MANDATORY. */
	public final static String TAG_PROPERTY_MANDATORY = "mandatory";

	public final static String TAG_PROPERTY_ADVANCED = "advanced";
	
	/** The Constant TAG_PROPERTY_LIST. */
	public final static String TAG_PROPERTY_LIST = "list";
	
	/** The Constant rangeDelimiter is used to delimit ranges e.g. 1:100 is a range 1 to 100 */
	public static final String rangeDelimiter = ":";

	/** The class filter. */
	private ClassFilter<?> classFilter;

	/** The property name. */
	private String propertyName;
	
	/** The property name label message id. */
	private String propertyNameLabelMessageID;

	/** The list. */
	private boolean list;
	
	/** The list model name. */
	private String listModelName;

	public final static long READ_ONLY = 	0x00000001;
	public final static long MANDATORY = 	0x00000002;
	public final static long ADVANCED = 	0x00000004;

	private long flags;
	

	/**
	 * Checks if is list.
	 * 
	 * @return true, if is list
	 */
	public boolean isList() {
		return list;
	}

	/**
	 * Sets the list.
	 * 
	 * @param list the new list
	 */
	public void setList(boolean list) {
		this.list = list;
	}

	/** The Constant IS_NULL. */
	public static final String IS_NULL = "null";
	
	/** The Constant IS_NOT_NULL. */
	public static final String IS_NOT_NULL = "!null";

	/**
	 * Instantiates a new property filter.
	 */
	public PropertyFilter() {
		super();
	}
	
	/**
	 * Instantiates a new property filter.
	 * 
	 * @param other the property filter
	 */
	public PropertyFilter(PropertyFilter other) {
		super();
		this.classFilter = other.classFilter;
		this.list = other.list;
		this.propertyName = other.propertyName;
		this.propertyNameLabelMessageID = other.propertyNameLabelMessageID;
		this.listModelName = other.listModelName;
		this.flags = other.flags;
	}

	/**
	 * Instantiates a new property filter.
	 * 
	 * @param propertyName the property name
	 */
	public PropertyFilter(String propertyName) {
		super();
		setPropertyName(propertyName);
	}
	
	/**
	 * Instantiates a new property filter.
	 * 
	 * @param classFilter the class filter
	 * @param propertyName the property name
	 */
	public PropertyFilter(ClassFilter<?> classFilter, String propertyName) {
		super();
		setPropertyName(propertyName);
		setClassFilter(classFilter);
		classFilter.add(this);
	}
	
	/**
	 * Instantiates a new property filter.
	 * 
	 * @param classFilter the class filter
	 * @param propertyName the property name
	 */
	public PropertyFilter(ClassFilter<?> classFilter, String propertyName, long flags) {
		super();
		setPropertyName(propertyName);
		setClassFilter(classFilter);
		setFlags(flags);
		classFilter.add(this);
	}
	
	/**
	 * Instantiates a new property filter.
	 * 
	 * @param classFilter the class filter
	 * @param propertyName the property name
	 * @param propertyNameLabelMessageID the property name label message id
	 */
	public PropertyFilter(ClassFilter<?> classFilter, String propertyName, String propertyNameLabelMessageID) {
		this(classFilter,  propertyName);
		this.propertyNameLabelMessageID = propertyNameLabelMessageID;
	}

	/**
	 * Instantiates a new property filter.
	 * 
	 * @param classFilter the class filter
	 * @param propertyName the property name
	 * @param propertyNameLabelMessageID the property name label message id
	 */
	public PropertyFilter(ClassFilter<?> classFilter, String propertyName, long flags, String propertyNameLabelMessageID) {
		this(classFilter,  propertyName, flags);
		this.propertyNameLabelMessageID = propertyNameLabelMessageID;
	}

	/**
	 * Sets the class filter.
	 * 
	 * @param classFilter the classFilter to set
	 */
	public void setClassFilter(ClassFilter<?> classFilter) {
		this.classFilter = classFilter;
	}
	

	/**
	 * Gets the class filter.
	 * 
	 * @return the classFilter
	 */
	public ClassFilter<?> getClassFilter() {
		return classFilter;
	}


	/**
	 * Clear.
	 */
	public void clear() {
	}

	/**
	 * Gets the property name.
	 * 
	 * @return the propertyName
	 */
	public String getPropertyName() {
		return propertyName;
	}

	/**
	 * Sets the property name.
	 * 
	 * @param propertyName the propertyName to set
	 */
	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	/**
	 * Gets the qualified name using the parent class path and the property name.
	 * 
	 * @return the qualified name
	 */
	public String getQualifiedName() {
		final String path = getClassFilter().getPath();

		if (path != null) {
			return path + "." + propertyName;
		}

		return propertyName;
	}


	/**
	 * Checks if is valid.
	 * 
	 * @return true, if is valid
	 */
	public boolean isValid() {
		return false;
	}

	/**
	 * Builds the query.
	 * 
	 * @param q the q
	 * 
	 * @throws ValidationException the validation exception
	 */
	public void buildQuery(QueryBuilder q)
			throws ValidationException {
	}

	/**
	 * Checks if is null or not null.
	 * 
	 * @param stringBuffer the string buffer
	 * @param predicate the predicate
	 * 
	 * @return true, if predicate equals "null" or "!null"
	 */
	public boolean isNullOrNotNull(StringBuffer stringBuffer, String predicate){
		if (IS_NULL.equals(predicate)){
			stringBuffer.append(getQualifiedName());
			stringBuffer.append(" is null");
			return true;
		}else if (IS_NOT_NULL.equals(predicate)){
			stringBuffer.append(getQualifiedName());
			stringBuffer.append(" is not null");
			return true;
		}
		return false;
	}
	
	/**
	 * Adds the betwen.
	 * 
	 * @param q the q
	 * @param lhs the lhs
	 * @param rhs the rhs
	 * 
	 * @return the string
	 */
	public String addBetwen(QueryBuilder q, Object lhs, Object rhs) {
		final StringBuffer buffer = new StringBuffer();
		buffer.append(" ");
		buffer.append(getQualifiedName());
		buffer.append(" BETWEEN ");
		buffer.append(q.addParam(lhs));
		buffer.append(" AND ");
		buffer.append(q.addParam(rhs));
		return buffer.toString();
	}

	/**
	 * Adds the comparison.
	 * 
	 * @param q the q
	 * @param op the op
	 * @param param the param
	 * 
	 * @return the string
	 */
	public String addComparison(QueryBuilder q, String op,
			Object param) {
		final StringBuffer buffer = new StringBuffer();
		buffer.append(" ");
		buffer.append(getQualifiedName());
		buffer.append(" ");
		buffer.append(op);
		buffer.append(" ");
		buffer.append(q.addParam(param));
		return buffer.toString();
	}

	/**
	 * Duplicate.
	 * 
	 * @return the property filter
	 */
	abstract public PropertyFilter duplicate();

	/**
	 * Gets the filter property name.
	 * 
	 * @return the filter property name
	 */
	public String getFilterPropertyName() {
		return null;
	}
	
	/**
	 * Visit.
	 * 
	 * @param propertyFilterVisitor the property filter visitor
	 * 
	 * @return the T
	 */
	public abstract <T> T visit(PropertyFilterVisitor<T> propertyFilterVisitor);

	/**
	 * Gets the list model name.
	 * 
	 * @return the list model name
	 */
	public String getListModelName() {
		return listModelName;
	}

	/**
	 * Sets the list model name.
	 * 
	 * @param listModelName the new list model name
	 */
	public void setListModelName(String listModelName) {
		this.listModelName = listModelName;
	}
	
	/**
	 * Gets the property name label message id.
	 * 
	 * @return the property name label message id
	 */
	public String getPropertyNameLabelMessageID() {
		if (propertyNameLabelMessageID == null){
			propertyNameLabelMessageID = getPropertyNameLabelDefaultMessageID();
		}
		return propertyNameLabelMessageID;
	}

	private String getPropertyNameLabelDefaultMessageID(){
		if (classFilter != null){
			Field field = ClassUtils.findFieldFromPath(classFilter.getClazz(), propertyName);
			
			if (field != null){
				RestrictionCandidate rc = field.getAnnotation(RestrictionCandidate.class);
			
				if (rc != null && !"".equals(rc.path())){
					field = ClassUtils.findFieldFromPath(field.getType(), rc.path());
				}
				return field.getDeclaringClass().getName() + "." + field.getName();
			}
			return classFilter.getDomainClassName() + "." + propertyName;
		}
		return propertyName;
	}
	/**
	 * Sets the property name label message id.
	 * 
	 * @param propertyNameLabelMessageID the new property name label message id
	 */
	public void setPropertyNameLabelMessageID(String propertyNameLabelMessageID) {
		this.propertyNameLabelMessageID = propertyNameLabelMessageID;
	}
	
	/**
	 * Checks if is mandatory.
	 * 
	 * @return the mandatory
	 */
	public boolean isMandatory() {
		return Flags.isSet(flags, MANDATORY);
	}

	/**
	 * Sets the mandatory.
	 * 
	 * @param mandatory the mandatory to set
	 */
	public void setMandatory(boolean mandatory) {
		this.flags = Flags.set(flags, MANDATORY, mandatory);
	}


	public boolean isAdvanced() {
		return Flags.isSet(flags, ADVANCED);
	}

	public void setAdvanced(boolean advanced) {
		this.flags = Flags.set(flags, ADVANCED, advanced);
	}
	
	public boolean isReadOnly() {
		return Flags.isSet(flags, READ_ONLY);
	}

	public void setReadOnly(boolean readOnly) {
		this.flags = Flags.set(flags, READ_ONLY, readOnly);
	}

	public long getFlags() {
		return flags;
	}

	public void setFlags(long flags) {
		this.flags = flags;
	}
	
	public String getModelName(){
		return propertyName;
	}
}
