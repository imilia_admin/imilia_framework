/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2014 - all rights reserved
 * Created on: Feb 24, 2014
 * Created by: emcgreal
 */
package com.imilia.server.domain.model.command.impl;

import com.imilia.server.domain.model.IModel;
import com.imilia.server.domain.model.command.ICommand;
import com.imilia.server.domain.model.property.PropertyImpl;

/**
 * The Class Command.
 */
public class Command extends PropertyImpl implements ICommand {

	/** The method. */
	private String method;
	
	/**
	 * Instantiates a new command.
	 *
	 * @param model the model
	 * @param name the name
	 * @param path the path
	 */
	public Command(IModel model, String name, String path) {
		super(model, name, path);
	}
	
	/**
	 * Instantiates a new command.
	 *
	 * @param model the model
	 * @param name the name
	 */
	public Command(IModel model, String name) {
		super(model, name, null);
	}
	
	/**
	 * Instantiates a new command.
	 *
	 * @param model the model
	 */
	public Command(IModel model) {
		super(model);
	}
	
	/* (non-Javadoc)
	 * @see com.imilia.server.domain.model.command.ICommand#getMethod()
	 */
	public String getMethod() {
		return method;
	}
	
	/**
	 * Sets the method.
	 *
	 * @param method the new method
	 */
	public void setMethod(String method) {
		this.method = method;
	}
}
