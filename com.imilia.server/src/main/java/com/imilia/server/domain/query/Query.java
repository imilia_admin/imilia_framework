/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2014 - all rights reserved
 * Created on: Feb 26, 2014
 * Created by: emcgreal
 */

package com.imilia.server.domain.query;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.imilia.server.domain.access.Role;
import com.imilia.server.domain.query.entities.QueryTemplate;
import com.imilia.server.domain.query.execution.deploy.QueryDeployer;
import com.imilia.server.domain.query.execution.values.QueryPropertyValue;
import com.imilia.server.domain.query.execution.values.QueryRestrictionValue;
import com.imilia.server.service.QueryRestrictionService;
import com.imilia.server.service.QueryService;

/**
 * The Class Query.
 *
 * @param <T> the generic type
 */
public class Query<T> {
	
	/** The Constant logger. */
	private final static Logger logger = LoggerFactory.getLogger(Query.class);
	
	/** The clazz. */
	private Class<?> clazz;
	
	/** The name. */
	private String name;
	
	/** The default result count limit. */
	public static int DEFAULT_RESULT_COUNT_LIMIT = 2000;
	
	/** The result count limit. */
	private int resultCountLimit; 
	
	/** The query service. */
	@Autowired
	private QueryService queryService;
	
	/** The restriction service. */
	@Autowired
	private QueryRestrictionService restrictionService; 
	
	
	/** The query template. */
	private QueryTemplate queryTemplate;
	
	
	private final List<QueryPropertyValue> queryPropertyValues = new ArrayList<>();
	
	private final List<QueryRestrictionValue> queryRestrictionValues = new ArrayList<>();

	//---------------------------------------------------------------
	/**
	 * Instantiates a new query.
	 */
	public Query() {
		super();
	}
	
	/**
	 * Instantiates a new query.
	 *
	 * @param clazz the clazz
	 * @param name the name
	 */
	public Query(Class<T> clazz, String name) {
		this(clazz, name, DEFAULT_RESULT_COUNT_LIMIT);
	}
	
	/**
	 * Instantiates a new query.
	 *
	 * @param clazz the clazz
	 * @param name the name
	 * @param resultCountLimit the result count limit
	 */
	public Query(Class<T> clazz, String name, int resultCountLimit) {
		super();
		this.clazz = clazz;
		this.name = name;
		this.resultCountLimit = resultCountLimit;
	}
	
	
	/**
	 * Post construct.
	 */
	@PostConstruct
	public void postConstruct() {
		queryTemplate = queryService.getQueryTemplate(name);
		
		if (queryTemplate == null){
			logger.error("QueryTemplate not found: " + name);
		}
	}
	
	public void deploy(List<Role> roles){
		QueryDeployer deployer = new QueryDeployer(this, roles);
		deployer.deploy();
	}

	
	// Getter & setters
	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the query service.
	 *
	 * @return the query service
	 */
	public QueryService getQueryService() {
		return queryService;
	}

	/**
	 * Sets the query service.
	 *
	 * @param queryService the new query service
	 */
	public void setQueryService(QueryService queryService) {
		this.queryService = queryService;
	}
	
	/**
	 * Gets the clazz.
	 *
	 * @return the clazz
	 */
	public Class<?> getClazz() {
		return clazz;
	}

	/**
	 * Sets the clazz.
	 *
	 * @param clazz the new clazz
	 */
	public void setClazz(Class<?> clazz) {
		this.clazz = clazz;
	}

	/**
	 * Gets the result count limit.
	 *
	 * @return the result count limit
	 */
	public int getResultCountLimit() {
		return resultCountLimit;
	}

	/**
	 * Sets the result count limit.
	 *
	 * @param resultCountLimit the new result count limit
	 */
	public void setResultCountLimit(int resultCountLimit) {
		this.resultCountLimit = resultCountLimit;
	}

	/**
	 * Gets the restriction service.
	 *
	 * @return the restriction service
	 */
	public QueryRestrictionService getRestrictionService() {
		return restrictionService;
	}

	/**
	 * Sets the restriction service.
	 *
	 * @param restrictionService the new restriction service
	 */
	public void setRestrictionService(QueryRestrictionService restrictionService) {
		this.restrictionService = restrictionService;
	}

	/**
	 * Gets the query template.
	 *
	 * @return the query template
	 */
	public QueryTemplate getQueryTemplate() {
		return queryTemplate;
	}

	public List<QueryPropertyValue> getQueryPropertyValues() {
		return queryPropertyValues;
	}

	public List<QueryRestrictionValue> getQueryRestrictionValues() {
		return queryRestrictionValues;
	}

	public void setQueryTemplate(QueryTemplate queryTemplate) {
		this.queryTemplate = queryTemplate;
	}

	public QueryPropertyValue getQueryPropertyValue(String path) {

		for (QueryPropertyValue v:getQueryPropertyValues()){
			if (v.getPath().equals(path)){
				return v;
			}
		}
		
		return null;
	}
}
