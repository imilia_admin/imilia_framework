/**
 * Imilia - Interactive Mobile Applications  GmbH.
 * Copyright (c) 2008
 *
 * @author:		emcgreal
 * @created:	Apr 25, 2008
 * Last modified by:   $Author:$
 * Last modfication:   $Date:$
 * Version:            $Revision:$
 */
package com.imilia.server.domain.filter;

import com.imilia.server.validation.ValidationException;

/**
 * The Class OIDPropertyFilter.
 */
public class OIDPropertyFilter extends PropertyFilter {
	
	/** The Constant TAG_FILTER. */
	public final static String TAG_FILTER = "oidPropertyFilter";
	
	/** The Constant TAG_PREDICATES. */
	public final static String TAG_PREDICATES = "oid";
	
	/** The oid. */
	private long oid;

	/**
	 * Instantiates a new oID property filter.
	 */
	public OIDPropertyFilter() {
		super();
	}

	/**
	 * Instantiates a new oID property filter.
	 *
	 * @param other the other
	 */
	public OIDPropertyFilter(OIDPropertyFilter other) {
		super(other);
		setOid(other.getOid());
	}

	/**
	 * Instantiates a new oID property filter.
	 *
	 * @param propertyName the property name
	 */
	public OIDPropertyFilter(String propertyName) {
		super(propertyName);
	}

	/**
	 * Instantiates a new oID property filter.
	 *
	 * @param classFilter the class filter
	 * @param propertyName the property name
	 * @param oid the oid
	 */
	public OIDPropertyFilter(ClassFilter<?> classFilter, String propertyName,
			long oid) {
		super(classFilter, propertyName);
		this.oid = oid;
	}

	/**
	 * The Constructor.
	 *
	 * @param classFilter the class filter
	 * @param propertyName the property name
	 * @param propertyNameLabelMessageID the property name label message id
	 * @param oid the oid
	 */
	public OIDPropertyFilter(ClassFilter<?> classFilter, String propertyName, String propertyNameLabelMessageID,
			long oid) {
		super(classFilter, propertyName, propertyNameLabelMessageID);
		this.oid = oid;
	}

	/* (non-Javadoc)
	 * @see com.imilia.server.domain.filter.PropertyFilter#buildQuery(com.imilia.server.domain.filter.QueryBuilder)
	 */
	@Override
	public void buildQuery(QueryBuilder q) throws ValidationException {
		q.addRestriction(getQualifiedName() + " = " + q.addParam(oid));
	}
	
	/* (non-Javadoc)
	 * @see com.imilia.server.domain.filter.PropertyFilter#visit(com.imilia.server.domain.filter.PropertyFilterVisitor)
	 */
	@Override
	public <T> T visit(PropertyFilterVisitor<T> propertyFilterVisitor) {
		return propertyFilterVisitor.accept(this);		
	}

	/* (non-Javadoc)
	 * @see com.imilia.server.domain.filter.PropertyFilter#isValid()
	 */
	@Override
	public boolean isValid() {
		return true;		// OID filters are always valid
	}

	/**
	 * Gets the oid.
	 *
	 * @return the oid
	 */
	public final long getOid() {
		return oid;
	}

	/**
	 * Sets the oid.
	 *
	 * @param oid the new oid
	 */
	public final void setOid(long oid) {
		this.oid = oid;
	}

	/**
	 * Configure.
	 *
	 * @param oid the oid
	 * @return the oID property filter
	 */
	public final OIDPropertyFilter configure(long oid) {
		setOid(oid);
		return this;
	}

	/* (non-Javadoc)
	 * @see com.imilia.server.domain.filter.PropertyFilter#duplicate()
	 */
	@Override
	public PropertyFilter duplicate() {
		return new OIDPropertyFilter(this);
	}
}
