/**
 * Imilia - Interactive Mobile Applications  GmbH.
 * Copyright (c) 2008
 *
 * @author:		emcgreal
 * @created:	Mar 13, 2008
 * Last modified by:   $Author:$
 * Last modfication:   $Date:$
 * Version:            $Revision:$
 */
package com.imilia.server.domain.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.FieldError;

import com.imilia.server.validation.ValidationException;

/**
 * The Class DoublePropertyFilter.
 * 
 * @author emcgreal
 */
public class DoublePropertyFilter extends PropertyFilter {
	
	/** The Constant TAG_FILTER. */
	public final static String TAG_FILTER = "doublePropertyFilter";
	
	/** The Constant TAG_PREDICATES. */
	public final static String TAG_PREDICATES = "predicates";

	/** The Constant DOUBLE_FORMAT_ERROR. */
	private static final String DOUBLE_FORMAT_ERROR = "typeMismatch.java.lang.Double";

	/** The Constant logger. */
	private final static Logger logger = LoggerFactory
			.getLogger(DoublePropertyFilter.class);

	/** The double predicates. */
	private String doublePredicates;

	/**
	 * Instantiates a new double property filter.
	 */
	public DoublePropertyFilter() {
		super();
	}

	/**
	 * Instantiates a new double property filter.
	 * 
	 * @param other the other
	 */
	public DoublePropertyFilter(DoublePropertyFilter other) {
		super(other);
		setDoublePredicates(other.doublePredicates);
	}

	/**
	 * Instantiates a new double property filter.
	 * 
	 * @param propertyName the property name
	 */
	public DoublePropertyFilter(String propertyName) {
		super(propertyName);
	}
	/**
	 * Instantiates a new double property filter.
	 * 
	 * @param classFilter the class filter
	 * @param propertyName the property name
	 */
	public DoublePropertyFilter(ClassFilter<?> classFilter, String propertyName) {
		super(classFilter, propertyName);
	}
	
	/**
	 * Instantiates a new double property filter.
	 * 
	 * @param classFilter the class filter
	 * @param propertyName the property name
	 * @param propertyNameLabelMessageID the property name label message id
	 */
	public DoublePropertyFilter(ClassFilter<?> classFilter, String propertyName, String propertyNameLabelMessageID) {
		super(classFilter, propertyName, propertyNameLabelMessageID);
	}

	/**
	 * Parses the range.
	 *
	 * @param q the q
	 * @param token the token
	 * @return between Criteria if a range was present
	 * @throws ValidationException the validation exception
	 */
	private String parseRange(QueryBuilder q, String token)
			throws ValidationException {
		String[] tokens = token.split(rangeDelimiter);

		if (tokens != null) {
			if (tokens.length == 2) {
				double lhs = parseDouble(tokens[0]);
				double rhs = parseDouble(tokens[1]);

				return addBetwen(q, lhs, rhs);
			} else {
				return getQualifiedName() + " = "
						+ q.addParam(parseDouble(tokens[0]));
			}
		}
		return null;
	}

	/**
	 * Tries to convert the token to an integer.
	 * 
	 * @param s the s
	 * 
	 * @return long value
	 * 
	 * @throws ValidationException the validation exception
	 */
	private double parseDouble(String s)
			throws ValidationException {
		try {
			return Double.parseDouble(s.trim());
		} catch (NumberFormatException ex) {
			logger.debug("Invalid double filter: " + s);

			final String clazzName = getClassFilter().getClazz()
					.getCanonicalName();
			FieldError o = new FieldError(clazzName, getQualifiedName(), s,
					true, new String[] { DOUBLE_FORMAT_ERROR },
					new Object[] { new DefaultMessageSourceResolvable(
							DOUBLE_FORMAT_ERROR) }, DOUBLE_FORMAT_ERROR);
			BeanPropertyBindingResult errors = new BeanPropertyBindingResult(
					this, clazzName);
			errors.addError(o);
			throw new ValidationException(errors, s);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.imilia.server.domain.filter.PropertyFilter#isValid()
	 */
	public boolean isValid() {
		return doublePredicates != null && !"".equals(doublePredicates);
	}

	/**
	 * Gets the double predicates.
	 * 
	 * @return the longPredicates
	 */
	public String getDoublePredicates() {
		return doublePredicates;
	}

	/**
	 * Sets the double predicates.
	 * 
	 * @param doublePredicates the double predicates
	 */
	public void setDoublePredicates(String doublePredicates) {
		this.doublePredicates = doublePredicates;
	}

	/**
	 * Configure.
	 *
	 * @param doublePredicates the double predicates
	 * @return the double property filter
	 */
	public DoublePropertyFilter configure(String doublePredicates) {
		setDoublePredicates(doublePredicates);
		return this;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.imilia.bkf.api.domain.filter.PropertyFilter#createJPQL(com.imilia
	 * .bkf.api.domain.filter.JPQLQuery)
	 */
	@Override
	public void buildQuery(QueryBuilder q) throws ValidationException {
		if (isValid()) {
			final StringBuffer buffer = new StringBuffer();
			if (!isNullOrNotNull(buffer, doublePredicates)){
				// Split the longPredicates up using the delimiter ;
				String[] tokens = doublePredicates.split(";");

				for (String t : tokens) {
					String c = parseRange(q, t);

					if (c != null) {
						if (buffer.length() > 0) {
							buffer.append(" OR ");
						}
						buffer.append(c);
					}
				}
			}
			final String query = buffer.toString();
			logger.debug(query);
			q.getRestriction().append(query);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.imilia.bkf.api.domain.filter.PropertyFilter#clear()
	 */
	@Override
	public void clear() {
		doublePredicates = null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.imilia.api.domain.filter.PropertyFilter#duplicate()
	 */
	@Override
	public PropertyFilter duplicate() {
		return new DoublePropertyFilter(this);
	}
	
	/* (non-Javadoc)
	 * @see com.imilia.server.domain.filter.PropertyFilter#visit(com.imilia.server.domain.filter.PropertyFilterVisitor)
	 */
	@Override
	public <T> T visit(PropertyFilterVisitor<T> propertyFilterVisitor) {
		return propertyFilterVisitor.accept(this);		
	}
}
