/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2010 - all rights reserved
 * Created on: Jun 22, 2010
 * Created by: emcgreal
 */
package com.imilia.server.domain.access;

/**
 * The Class AuthenticationRequest.
 */
public final class AuthenticationRequest {
	
	/** The username. */
	private String username;
	
	/** The password. */
	private String password;
	
	/** The source ip. */
	private String sourceIP;
	
	/** The max login attempts allowed. */
	private int maxLoginAttemptsAllowed = 3;
	
	/** The remaining attempts. */
	private int remainingAttempts;
	
	public AuthenticationRequest() {
		super();
	}
	/**
	 * Instantiates a new authentication request.
	 * 
	 * @param username the username
	 * @param password the password
	 */
	public AuthenticationRequest(String username, String password) {
		super();
		this.username = username;
		this.password = password;
	}
	
	/**
	 * Instantiates a new authentication request.
	 * 
	 * @param username the username
	 * @param password the password
	 * @param sourceIP the source ip
	 */
	public AuthenticationRequest(String username, String password,
			String sourceIP) {
		super();
		this.username = username;
		this.password = password;
		this.sourceIP = sourceIP;
	}
	
	

	
	/**
	 * Gets the username.
	 * 
	 * @return the username
	 */
	public final String getUsername() {
		return username;
	}
	
	/**
	 * Sets the username.
	 * 
	 * @param username the new username
	 */
	public final void setUsername(String username) {
		this.username = username;
	}
	
	/**
	 * Gets the password.
	 * 
	 * @return the password
	 */
	public final String getPassword() {
		return password;
	}
	
	/**
	 * Sets the password.
	 * 
	 * @param password the new password
	 */
	public final void setPassword(String password) {
		this.password = password;
	}
	
	/**
	 * Gets the source ip.
	 * 
	 * @return the source ip
	 */
	public final String getSourceIP() {
		return sourceIP;
	}
	
	/**
	 * Sets the source ip.
	 * 
	 * @param sourceIP the new source ip
	 */
	public final void setSourceIP(String sourceIP) {
		this.sourceIP = sourceIP;
	}
	
	/**
	 * Gets the max login attempts allowed.
	 * 
	 * @return the max login attempts allowed
	 */
	public final int getMaxLoginAttemptsAllowed() {
		return maxLoginAttemptsAllowed;
	}
	
	/**
	 * Sets the max login attempts allowed.
	 * 
	 * @param maxLoginAttemptsAllowed the new max login attempts allowed
	 */
	public final void setMaxLoginAttemptsAllowed(int maxLoginAttemptsAllowed) {
		this.maxLoginAttemptsAllowed = maxLoginAttemptsAllowed;
	}

	public final int getRemainingAttempts() {
		return remainingAttempts;
	}

	public final void setRemainingAttempts(int remainingAttempts) {
		this.remainingAttempts = remainingAttempts;
	}
	
	public boolean isUsernameAndPasswordNonEmpty(){
		return username != null && password != null && !username.isEmpty() && !password.isEmpty();
	}
}
