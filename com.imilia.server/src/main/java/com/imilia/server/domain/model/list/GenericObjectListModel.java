/**
 * Imilia - Interactive Mobile Applications  GmbH.
 * Copyright (c) 2008
 *
 * @author:		emcgreal
 * @created:	Mar 25, 2008
 * Last modified by:   $Author:$
 * Last modfication:   $Date:$
 * Version:            $Revision:$
 */
package com.imilia.server.domain.model.list;

import java.util.List;

import org.springframework.context.MessageSource;

import com.imilia.server.domain.i18n.DomainLocaleVariant;
import com.imilia.utils.observer.Observable;

/**
 * The Interface GenericObjectListModel.
 */
public interface GenericObjectListModel<T> extends Observable{

	/**
	 * Gets the.
	 * 
	 * @param row the row
	 * 
	 * @return the object
	 */
	public Object get(int row);
	
	/**
	 * Gets the localized display value.
	 * 
	 * @param v the v
	 * 
	 * @return the localized display value
	 */
	public Object getLocalizedDisplayValue(Object v);

	/**
	 * Gets the object.
	 * 
	 * @param row the row
	 * 
	 * @return the object
	 */
	public T getObject(int row);
	
	/**
	 * Gets the store object.
	 * 
	 * @param row the row
	 * 
	 * @return the store object
	 */
	public Object getStoreObject(int row);

	/**
	 * Gets the display object.
	 *
	 * @param row the row
	 * @return the display object
	 */
	public Object getDisplayObject(int row);
	
	/**
	 * Find object.
	 * 
	 * @param storeValue the store value
	 * 
	 * @return the t
	 */
	public T findObject(Object storeValue);

	/**
	 * Find object index.
	 * 
	 * @param t the t
	 * 
	 * @return the int
	 */
	public int findObjectIndex(Object t);
	
	/*
	 * (non-Javadoc)
	 *
	 * @see nextapp.echo2.app.list.ListModel#size()
	 */
	/**
	 * Size.
	 * 
	 * @return the int
	 */
	public int size();

	/**
	 * Gets the display property.
	 * 
	 * @return the display property
	 */
	public String getDisplayProperty();

	/**
	 * Gets the store property.
	 * 
	 * @return the store property
	 */
	public String getStoreProperty();

	/**
	 * Gets the list.
	 * 
	 * @return the list
	 */
	public List<T> getList();
	
	/**
	 * Gets the locale.
	 * 
	 * @return the locale
	 */
	public DomainLocaleVariant getLocale();

	/**
	 * Sets the locale.
	 * 
	 * @param locale the new locale
	 */
	public void setLocale(DomainLocaleVariant locale);

	/**
	 * Gets the message source.
	 * 
	 * @return the message source
	 */
	public MessageSource getMessageSource();

	/**
	 * Sets the message source.
	 * 
	 * @param messageSource the new message source
	 */
	public void setMessageSource(MessageSource messageSource);
}
