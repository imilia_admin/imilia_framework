/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2013 - all rights reserved
 *
 * Created on: Apr 22, 2013
 * Created by: emcgreal
 */
package com.imilia.server.domain.filter;

import com.imilia.server.validation.ValidationException;

/**
 * The Class EntityRefPropertyFilter.
 */
public class EntityRefPropertyFilter extends BooleanPropertyFilter  {

	/** The Constant TAG_FILTER. */
	public final static String TAG_FILTER = "EntityRefPropertyFilter";
	
	public EntityRefPropertyFilter() {
		super();
	}

	public EntityRefPropertyFilter(BooleanPropertyFilter other) {
		super(other);
	}

	public EntityRefPropertyFilter(ClassFilter<?> classFilter,
			String propertyName, String propertyNameLabelMessageID) {
		super(classFilter, propertyName, propertyNameLabelMessageID);
	}

	public EntityRefPropertyFilter(ClassFilter<?> classFilter,
			String propertyName) {
		super(classFilter, propertyName);
	}

	public EntityRefPropertyFilter(String propertyName) {
		super(propertyName);
	}

	/* (non-Javadoc)
	 * @see com.imilia.server.domain.filter.PropertyFilter#visit(com.imilia.server.domain.filter.PropertyFilterVisitor)
	 */
	@Override
	public <T> T visit(PropertyFilterVisitor<T> propertyFilterVisitor) {
		return propertyFilterVisitor.accept(this);
	}

	/* (non-Javadoc)
	 * @see com.imilia.server.domain.filter.PropertyFilter#buildQuery(com.imilia.server.domain.filter.QueryBuilder)
	 */
	@Override
	public void buildQuery(QueryBuilder q) throws ValidationException {
		if (isValid()) {
			if (getBooleanValue()){
				q.addRestriction(getQualifiedName() + " is not null ");
			}else{
				q.addRestriction(getQualifiedName() + " is null ");
			}
		}
	}
}
