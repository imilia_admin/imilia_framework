package com.imilia.server.domain.model.event.impl;

import java.lang.reflect.Method;
import java.util.HashMap;

import ognl.Ognl;
import ognl.OgnlException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.imilia.server.domain.model.event.IEvent;
import com.imilia.server.domain.model.event.IEventSource.Listener;

public class EventListenerDelegator implements Listener<IEvent>{

	/** The Constant logger. */
	private final static Logger logger = LoggerFactory.getLogger(EventListenerDelegator.class);

	/** The target object. */
	private Object targetObject;
	
	/** The method. */
	private String method;

	/** The params. */
	protected final HashMap<String, Object> params = new HashMap<String, Object>();
	
	/** The  event param. */
	public final String EVENT_PARAM = "event";

	public EventListenerDelegator(){
		super();
	}
	
	public EventListenerDelegator(Object targetObject, String method) {
		super();
		setTargetObject(targetObject);
		setMethod(method);
	}
	
	public EventListenerDelegator(Object targetObject, Method method) {
		super();
		setTargetObject(targetObject);
		setMethod(method);
	}

	

	@Override
	public void fired(IEvent event) {
		params.put(EVENT_PARAM, event);
		delegate();
	}

	/**
	 * Delegate.
	 */
	protected void delegate(){
		try {
			callMethod();
		} catch (OgnlException e) {
			logger.error("Calling method: " + method + " failed with params: " + params, e);
		}
	}
	
	protected void callMethod() throws OgnlException{
		Ognl.getValue(method, params, targetObject);
	}
	
	
	protected IEvent getActionEvent(){
		return (IEvent) params.get(EVENT_PARAM);
	}
	/////////////////////////////////////////////////
	// Getter and setters 
	/**
	 * Gets the target object.
	 *
	 * @return the target object
	 */
	public Object getTargetObject() {
		return targetObject;
	}

	/**
	 * Sets the target object.
	 *
	 * @param targetObject the new target object
	 */
	public void setTargetObject(Object targetObject) {
		this.targetObject = targetObject;
	}

	/**
	 * Gets the method.
	 *
	 * @return the method
	 */
	public String getMethod() {
		return method;
	}

	/**
	 * Sets the method.
	 *
	 * @param method the new method
	 */
	public void setMethod(String method) {
		this.method = method;
	}

	public HashMap<String, Object> getParams() {
		return params;
	}

	public void setMethod(Method m) {
		final Class<?>[] params = m.getParameterTypes();
		
		if (params.length == 0){
			setMethod(m.getName() + "()");
		}else if (params.length == 1){
			if (matchSingleParamMethod(m, params[0])){
				return;
			}
		}
		
		
		StringBuilder builder = new StringBuilder();
		builder.append(m.getName());
		builder.append("(");
		for (int i = 0;i<params.length;i++){
			if (i > 0){
				builder.append(",");
				builder.append("#p");
				builder.append(i);
			}
		}
		builder.append(")");
		setMethod(builder.toString());
	}
	
	protected boolean matchSingleParamMethod(Method m, Class<?> param){
		return setMethod(m, param, Event.class, EVENT_PARAM);
	}
	
	protected boolean setMethod(Method m, Class<?> param, Class<?> test, String paramName){
		if (param.isAssignableFrom(test)){
			setMethod(m.getName() + "(#"+ paramName +")");
			return true;
		}
		
		return false;
	}

}
