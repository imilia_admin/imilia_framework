/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 * 
 * Created on: Mar 10, 2009
 * Created by: emcgreal
 */
package com.imilia.server.domain.password;

import java.util.Locale;
import java.util.Set;

import javax.validation.ConstraintViolation;

import org.springframework.validation.Validator;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.validation.BeanPropertyBindingResult;

import com.imilia.server.domain.validation.Validational;
import com.imilia.server.service.impl.PasswordStrengthChecker;
import com.imilia.server.service.impl.PasswordStrengthChecker.Strength;
import com.imilia.server.validation.ValidationException;

/**
 * The Class PasswordCandidate is used to check the validity of passwords
 * 
 * <p>
 * The following checks are performed:
 * <ol>
 * 	<li>Check both passwords are not null or blank</li>
 * 	<li>Check the minimum length is fulfilled</li>
 * 	<li>Check both passwords are identical</li>
 * </ol>
 * </p>
 */
public class PasswordCandidate implements Validational {
	
	/** The bean validator. */
	@Autowired @Qualifier("jsr303Validator") 
	private Validator beanValidator;

	public final static int MIN_PASSWORD_LENGTH = 8;
	/** The password */
	@NotNull
	@NotBlank
	@Length(max = 255, min=MIN_PASSWORD_LENGTH)
	private String password1;

	/** The password repeated. */
	@NotNull
	@NotBlank
	@Length(max = 255, min=MIN_PASSWORD_LENGTH)
	private String password2;
	
	public PasswordCandidate() {
	}

	/**
	 * Instantiates a new password candidate.
	 * 
	 * @param password1 the password1
	 * @param password2 the password2
	 */
	public PasswordCandidate(String password1, String password2) {
		super();
		this.password1 = password1;
		this.password2 = password2;
	}

	/**
	 * Gets the password1.
	 * 
	 * @return the password1
	 */
	public String getPassword1() {
		return password1;
	}

	/**
	 * Sets the password1.
	 * 
	 * @param password1 the new password1
	 */
	public void setPassword1(String password1) {
		this.password1 = password1;
	}

	/**
	 * Gets the password2.
	 * 
	 * @return the password2
	 */
	public String getPassword2() {
		return password2;
	}

	/**
	 * Sets the password2.
	 * 
	 * @param password2 the new password2
	 */
	public void setPassword2(String password2) {
		this.password2 = password2;
	}

	/* (non-Javadoc)
	 * @see com.imilia.server.domain.Validational#getBeanValidator()
	 */
	public Validator getBeanValidator() {
		return beanValidator;
	}

	/* (non-Javadoc)
	 * @see com.imilia.server.domain.Validational#setBeanValidator(org.springmodules.validation.bean.BeanValidator)
	 */
	public void setBeanValidator(Validator beanValidator) {
		this.beanValidator = beanValidator;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.imilia.server.domain.Validational#validate(java.lang.String)
	 */
	public void validate(String targetName) throws ValidationException {
		final BeanPropertyBindingResult beanPropertyBindingResult = new BeanPropertyBindingResult(
				this, targetName != null ? targetName : this.getClass().getCanonicalName());

		beanValidator.validate(this, beanPropertyBindingResult);

		if (!beanPropertyBindingResult.hasErrors()) {
			
			// Check that both passwords match
			if (!password1.equals(password2)) {
				
				throw ValidationException.createFieldError(this, 
						"PasswordCandiate", "password1", getPassword1(), false, 
						new String[]{"com.imilia.server.domain.password.PasswordCandidate.password[mismatch]"}, 
						null, 
						"PasswordCandiate mismatch");
			}
		}else{
			throw new ValidationException(beanPropertyBindingResult,this);
		}
	}

	/**
	 * Check password strength.
	 * 
	 * @param passwordStrengthChecker the password strength checker
	 * @param messageSource the message source
	 * @param locale the locale
	 * 
	 * @return the strength
	 * 
	 * @throws ValidationException the validation exception
	 */
	public Strength checkPasswordStrength(PasswordStrengthChecker passwordStrengthChecker, MessageSource messageSource, Locale locale) throws ValidationException  {
		final StringBuffer logBuffer = new StringBuffer();
		int score = passwordStrengthChecker.checkPasswordStrength(getPassword1(), logBuffer);
		final Strength strength = passwordStrengthChecker.toStrength(score); 
			
		if (score < passwordStrengthChecker.getMinStrength()){
			throw ValidationException.createFieldError(this, 
					"PasswordCandiate", "password1", getPassword1(), false, 
					new String[]{"com.imilia.server.domain.password.PasswordCandidate.password[tooWeak]"}, 
					new Object[]{strength.getLocalizedMessage(messageSource, locale), score, logBuffer.toString()}, 
					"");
		}
		
		return strength;
	}
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.imilia.server.domain.Validational#validate()
	 */
	public void validate() throws ValidationException {
		validate(this.getClass().getCanonicalName());
	}

}
