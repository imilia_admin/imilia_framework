package com.imilia.server.domain.model.command;

import com.imilia.server.domain.model.property.Property;

public interface ICommand extends Property{
	
	public String getMethod();
}
