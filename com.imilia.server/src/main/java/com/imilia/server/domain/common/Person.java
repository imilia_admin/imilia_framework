/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 * Created on: Jan 23, 2009
 * Created by: emcgreal
 */

package com.imilia.server.domain.common;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import org.joda.time.DateMidnight;
import org.joda.time.DateTime;
import org.joda.time.Years;

import com.imilia.server.domain.DomainObject;
import com.imilia.server.domain.access.Subscription;
import com.imilia.server.domain.enums.Gender;
import com.imilia.server.domain.filter.RestrictionCandidate;
import com.imilia.utils.Flags;
import com.imilia.utils.i18n.DomainLocale;
import com.imilia.utils.strings.StringUtils;

/**
 * The Class Person.
 * 
 * @author emcgreal
 */
@Entity
public class Person extends DomainObject{

	/** The alias. */
	@Column(length = 50)
	private String alias;

	/** The title. */
	@Length(max = 50)
	@Column(length = 50)
	private String title;

	/** The first name. */
	@Length(max = 150)
	@Column(length = 150)
	@RestrictionCandidate
	private String firstName;


	/** The last name. */
	@NotNull
	@NotBlank
	@Length(max = 150)
	@Column(length = 150)
	@RestrictionCandidate
	private String lastName;

	/** The gender. */
	@Enumerated(EnumType.ORDINAL)
	private Gender gender;

	/** The dob. */
	@RestrictionCandidate
	private DateMidnight dob;

	/** The comment. */
	@Column(length = 255)
	private String comment;

	/** The address. */
	@ManyToOne(optional=true, fetch=FetchType.LAZY, cascade={CascadeType.ALL})
	private Address address;

	/** The domain locale. */
	@NotNull
	@Enumerated(value = EnumType.ORDINAL)
	private DomainLocale domainLocale;

	/** The user account. */
	@OneToOne(mappedBy="person")
	private UserAccount userAccount;
	
	/** The Constant SHARE_FIRST_NAME. */
	private final static long SHARE_FIRST_NAME = 0x00000001; 
	
	/** The Constant SHARE_LAST_NAME. */
	private final static long SHARE_LAST_NAME  = 0x00000002; 
	
	/** The Constant SHARE_DOB. */
	private final static long SHARE_DOB  	   = 0x00000004;
	
	/** The Constant SHARE_TITLE. */
	private final static long SHARE_TITLE  	   = 0x00000008;
	
	/** The Constant SHARE_ALIAS. */
	private final static long SHARE_ALIAS  	   = 0x00000010;
	
	/** The Constant SHARE_GENDER. */
	private final static long SHARE_GENDER     = 0x00000020;

	/** The Constant SHARE_NAMES. */
	private final static long SHARE_NAMES     = SHARE_FIRST_NAME|SHARE_LAST_NAME;
	
	/** The flags. */
	private long flags;
	
	@OneToMany(mappedBy="person")
	private List<Subscription> subscriptions;
	
	/**
	 * Instantiates a new person.
	 */
	public Person() {
	}

	/**
	 * Instantiates a new person.
	 * 
	 * @param firstName the first name
	 * @param lastName the last name
	 */
	public Person(String firstName, String lastName) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
	}

	/**
	 * Instantiates a new person.
	 * 
	 * @param alias the alias
	 * @param firstName the first name
	 * @param lastName the last name
	 * @param gender the gender
	 * @param dob the dob
	 */
	public Person(String alias, String firstName, String lastName,
			Gender gender, DateMidnight dob) {
		this.alias = alias;
		this.firstName = firstName;
		this.lastName = lastName;
		this.gender = gender;
		this.dob = dob;
	}

	/**
	 * Gets the address.
	 * 
	 * @return the address
	 */
	public Address getAddress() {
		return address;
	}

	/**
	 * Sets the address.
	 * 
	 * @param address the address to set
	 */
	public void setAddress(Address address) {
		this.address = address;
	}

	/**
	 * Gets the first name.
	 * 
	 * @return the first name
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Gets the gender.
	 * 
	 * @return the gender
	 */
	public Gender getGender() {
		return gender;
	}

	/**
	 * Gets the last name.
	 * 
	 * @return the last name
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Sets the first name.
	 * 
	 * @param firstName the new first name
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * Sets the gender.
	 * 
	 * @param gender the new gender
	 */
	public void setGender(Gender gender) {
		this.gender = gender;
	}

	/**
	 * Sets the last name.
	 * 
	 * @param lastName the new last name
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * Capatalise names.
	 */
	public void capataliseNames(){
		setLastName(StringUtils.capitalizeFirstLetter(getLastName()));
		setFirstName(StringUtils.capitalizeFirstLetter(getFirstName()));
	}
	/**
	 * Gets the alias.
	 * 
	 * @return the alias
	 */
	public String getAlias() {
		return alias;
	}

	/**
	 * Sets the alias.
	 * 
	 * @param displayName the new alias
	 */
	public void setAlias(String displayName) {
		this.alias = displayName;
	}

	/**
	 * Gets the dob.
	 * 
	 * @return the dob
	 */
	public DateMidnight getDob() {
		return dob;
	}

	/**
	 * Sets the dob.
	 * 
	 * @param dob the new dob
	 */
	public void setDob(DateMidnight dob) {
		this.dob = dob;
	}

	/**
	 * Gets the title.
	 * 
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 * 
	 * @param title the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the comment.
	 * 
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * Sets the comment.
	 * 
	 * @param comment the new comment
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}

	/**
	 * Gets the domain locale.
	 * 
	 * @return the domainLocale
	 */
	public DomainLocale getDomainLocale() {
		return domainLocale;
	}

	/**
	 * Sets the domain locale.
	 * 
	 * @param domainLocale the domainLocale to set
	 */
	public void setDomainLocale(DomainLocale domainLocale) {
		this.domainLocale = domainLocale;
	}

	/**
	 * Gets the user account.
	 * 
	 * @return the userAccout
	 */
	public UserAccount getUserAccount() {
		return userAccount;
	}

	/**
	 * Sets the user account.
	 * 
	 * @param userAccout the userAccout to set
	 */
	public void setUserAccount(UserAccount userAccout) {
		this.userAccount = userAccout;
	}
	
	/**
	 * Gets the full name.
	 * 
	 * @return the full name
	 */
	public String getFullName(){
		StringBuffer sb = new StringBuffer();
		if (firstName != null && firstName.length() > 0)
			sb.append(firstName);
		
		if (firstName != null && firstName.length() > 0 && lastName != null && lastName.length() > 0)
			sb.append(" ");
		
		if (lastName != null)
			sb.append(lastName);
		
		return sb.toString();
	}
	/**
	 * Gets the full name.
	 * 
	 * @return the full name
	 */
	public String getLastNameCommaInitial(){
		StringBuffer sb = new StringBuffer();
		if (lastName != null){
			sb.append(lastName);
		}
		
		if (firstName != null && firstName.length() > 0){
			if (sb.length() > 0){
				sb.append(", ");
			}
			sb.append(firstName.charAt(0));
		}
		
		
		
		return sb.toString();
	}
	
	/**
	 * Gets the first name initial and last name e.g.:Tom Jones -> tjones
	 *
	 * @return the first name initial and last name
	 */
	public String getFirstNameInitialAndLastName(){
		StringBuffer sb = new StringBuffer();
		if (firstName != null && firstName.length() > 0){
			sb.append(firstName.charAt(0));
		}
		
		if (lastName != null){
			sb.append(lastName);
		}
		
		return sb.toString();
	}	
	/**
	 * Gets the full name.
	 * 
	 * @return the full name
	 */
	public String getLastNameCommaFirst(){
		StringBuffer sb = new StringBuffer();
		if (lastName != null){
			sb.append(lastName);
		}
		
		if (firstName != null && firstName.length() > 0){
			if (sb.length() > 0){
				sb.append(", ");
			}
			sb.append(firstName);
		}
		
		return sb.toString();
	}
	
	/**
	 * Gets the age.
	 *
	 * @return the age
	 */
	public Years getAge(){
		if (getDob() != null){
			return Years.yearsBetween(getDob(), new DateTime());
		}
		
		return null;
	}

	/**
	 * Gets the flags.
	 *
	 * @return the flags
	 */
	public long getFlags() {
		return flags;
	}

	/**
	 * Sets the flags.
	 *
	 * @param flags the new flags
	 */
	public void setFlags(long flags) {
		this.flags = flags;
	}
	
	/**
	 * Sets the share first name.
	 *
	 * @param share the new share first name
	 */
	public void setShareFirstName(boolean share) {
		flags = Flags.set(getFlags(), SHARE_FIRST_NAME, share);
	}

	/**
	 * Checks if is share first name.
	 *
	 * @return true, if is share first name
	 */
	public boolean isShareFirstName() {
		return Flags.isSet(getFlags(), SHARE_FIRST_NAME);
	}
	
	/**
	 * Gets the first name shared.
	 *
	 * @return the first name shared
	 */
	public String getFirstNameShared(){
		if (isShareFirstName()){
			return getFirstName();
		}
		return null;
	}

	/**
	 * Sets the share last name.
	 *
	 * @param share the new share last name
	 */
	public void setShareLastName(boolean share) {
		flags = Flags.set(getFlags(), SHARE_LAST_NAME, share);
	}
	
	/**
	 * Checks if is share last name.
	 *
	 * @return true, if is share last name
	 */
	public boolean isShareLastName() {
		return Flags.isSet(getFlags(), SHARE_LAST_NAME);
	}
	
	/**
	 * Gets the last name shared.
	 *
	 * @return the last name shared
	 */
	public String getLastNameShared(){
		if (isShareLastName()){
			return getLastName();
		}
		return null;
	}

	/**
	 * Sets the share dob.
	 *
	 * @param share the new share dob
	 */
	public void setShareDob(boolean share) {
		flags = Flags.set(getFlags(), SHARE_DOB, share);
	}
	
	/**
	 * Checks if is share dob.
	 *
	 * @return true, if is share dob
	 */
	public boolean isShareDob() {
		return Flags.isSet(getFlags(), SHARE_DOB);
	}
	
	/**
	 * Gets the dob shared.
	 *
	 * @return the dob shared
	 */
	public DateMidnight getDobShared(){
		if (isShareDob()){
			return getDob();
		}
		return null;
	}

	/**
	 * Sets the share title.
	 *
	 * @param share the new share title
	 */
	public void setShareTitle(boolean share) {
		flags = Flags.set(getFlags(), SHARE_TITLE, share);
	}
	
	/**
	 * Checks if is share title.
	 *
	 * @return true, if is share title
	 */
	public boolean isShareTitle() {
		return Flags.isSet(getFlags(), SHARE_TITLE);
	}
	
	/**
	 * Gets the title shared.
	 *
	 * @return the title shared
	 */
	public String getTitleShared(){
		if (isShareTitle()){
			return getTitle();
		}
		return null;
	}

	/**
	 * Sets the share alias.
	 *
	 * @param share the new share alias
	 */
	public void setShareAlias(boolean share) {
		flags = Flags.set(getFlags(), SHARE_ALIAS, share);
	}
	
	/**
	 * Checks if is share alias.
	 *
	 * @return true, if is share alias
	 */
	public boolean isShareAlias() {
		return Flags.isSet(getFlags(), SHARE_ALIAS);
	}
	
	/**
	 * Gets the alias shared.
	 *
	 * @return the alias shared
	 */
	public String getAliasShared(){
		if (isShareAlias()){
			return getAlias();
		}
		return null;
	}
	
	/**
	 * Sets the share gender.
	 *
	 * @param share the new share gender
	 */
	public void setShareGender(boolean share) {
		flags = Flags.set(getFlags(), SHARE_GENDER, share);
	}
	
	/**
	 * Checks if is share gender.
	 *
	 * @return true, if is share gender
	 */
	public boolean isShareGender() {
		return Flags.isSet(getFlags(), SHARE_GENDER);
	}
	
	/**
	 * Gets the gender shared.
	 *
	 * @return the gender shared
	 */
	public Gender getGenderShared(){
		if (isShareGender()){
			return getGender();
		}
		return null;
	}

	
	/**
	 * Gets the age shared.
	 *
	 * @return the age shared
	 */
	public Years getAgeShared(){
		if (isShareDob()){
			return getAge();
		}
		return null;
	}
	
	/**
	 * Checks if is share names.
	 *
	 * @return true, if is share names
	 */
	public boolean isShareNames(){
		return Flags.isSet(getFlags(), SHARE_NAMES);
	}

	/**
	 * Sets the share names.
	 *
	 * @param share the new share names
	 */
	public void setShareNames(boolean share){
		flags = Flags.set(getFlags(), SHARE_NAMES, share);
	}

	public List<Subscription> getSubscriptions() {
		return subscriptions;
	}

	public void setSubscriptions(List<Subscription> subscriptions) {
		this.subscriptions = subscriptions;
	}
}
