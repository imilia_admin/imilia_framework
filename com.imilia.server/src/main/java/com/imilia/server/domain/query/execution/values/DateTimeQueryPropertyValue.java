package com.imilia.server.domain.query.execution.values;

import org.joda.time.DateTime;

import com.imilia.server.domain.query.entities.QueryProperty;
import com.imilia.server.domain.query.entities.QueryRestriction;
import com.imilia.server.domain.query.execution.Comparison;
import com.imilia.server.domain.query.execution.LogicalOperator;
import com.imilia.server.domain.query.execution.QueryCrudExecution;

public class DateTimeQueryPropertyValue extends ComparisonValue{

	private DateTime lowerBound;
	
	private DateTime upperBound;

	public DateTimeQueryPropertyValue(QueryProperty queryProperty) {
		super(queryProperty);
		addPossibleComparisons(
				Comparison.IsNull,
				Comparison.NotNull,
				Comparison.Equals,
				Comparison.NotEquals,
				Comparison.LessThan,
				Comparison.LessThanEquals,
				Comparison.GreaterThan,
				Comparison.GreaterThanEquals,
				Comparison.Between);

	}

	@Override
	public void addToExecution(QueryCrudExecution<?> execution) {
		if (lowerBound == null && upperBound == null){
			return;
		}
		
		switch(comparison){
		case Equals:
		case NotEquals:
		case LessThan:
		case LessThanEquals:
		case GreaterThan:
		case GreaterThanEquals:
		case IsNull:
		case NotNull:
			execution.addRestrictionValue(
					getComparison().toPredicate(getPath(), 
							execution.addParameter(lowerBound)), 
					LogicalOperator.And);
			break;
			
		case Between:
		case In:
			execution.addRestrictionValue(
					getPath() + 
					getComparison().toPredicate(getPath(), 
							execution.addParameters(new Object[]{lowerBound, upperBound})), 
					LogicalOperator.And);
		}
		
	}

	
	public DateTime getLowerBound() {
		return lowerBound;
	}

	public void setLowerBound(DateTime lowerBound) {
		this.lowerBound = lowerBound;
	}

	public DateTime getUpperBound() {
		return upperBound;
	}

	public void setUpperBound(DateTime upperBound) {
		this.upperBound = upperBound;
	}

	@Override
	public String toString() {
		return "DateTimeQueryPropertyValue [path=" + getPath() +" lowerBound=" + lowerBound
				+ ", upperBound=" + upperBound + " comparison=" + getComparison() +"]";
	}

}
