/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2014 - all rights reserved
 * Created on: Feb 21, 2014
 * Created by: emcgreal
 */
package com.imilia.server.domain.access;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.imilia.server.domain.DomainObject;
import com.imilia.server.domain.common.Person;

/**
 * The Class Subscription.
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "subscription_type", discriminatorType = DiscriminatorType.INTEGER)
@DiscriminatorValue("0")
public class Subscription extends DomainObject{
	
	/** The person. */
	@ManyToOne
	private Person person;
	
	/** The roles. */
	@OneToMany(mappedBy="subscription", cascade=CascadeType.ALL)
	private List<SubscriptionRole> subscriptionRoles;

	/**
	 * Instantiates a new subscription.
	 */
	public Subscription(){
		super();
	}
	
	/**
	 * Instantiates a new subscription.
	 *
	 * @param person the person
	 * @param roles the roles
	 */
	public Subscription(Person person, List<Role> roles) {
		super();
		this.person = person;
		addRoles(roles);
	}

	
	/**
	 * Instantiates a new subscription.
	 *
	 * @param person the person
	 * @param roles the roles
	 */
	public Subscription(Person person, Role ... roles) {
		super();
		this.person = person;
		addRoles(Arrays.asList(roles));
	}
	
	public void addRoles(List<Role> roles){
		for (Role r:roles){
			if (!hasRole(r)){
				subscriptionRoles.add(new SubscriptionRole(this, r));
			}
		}
	}
	public void addRoles(Role...roles ){
		addRoles(Arrays.asList(roles));
	}

	/**
	 * Gets the person.
	 *
	 * @return the person
	 */
	public Person getPerson() {
		return person;
	}

	/**
	 * Sets the person.
	 *
	 * @param person the new person
	 */
	public void setPerson(Person person) {
		this.person = person;
	}
	
	/**
	 * Checks if the subscription has any of the test roles.
	 *
	 * @param roles the test roles
	 * @return true, if any of the roles match
	 */
	public boolean hasRole(Role... roles) {
		for (Role r:roles){
			for(SubscriptionRole sr:subscriptionRoles){
				if (sr.getRole().equals(r)){
					return true;
				}
			}
		}
		return false;
	}

	public List<SubscriptionRole> getSubscriptionRoles() {
		return subscriptionRoles;
	}

	public void setSubscriptionRoles(List<SubscriptionRole> subscriptionRoles) {
		this.subscriptionRoles = subscriptionRoles;
	}
	
	public String getRolesAuthorities(){
		StringBuilder builder = new StringBuilder();
		
		for (SubscriptionRole r:subscriptionRoles){
			if (builder.length() > 0){
				builder.append(", ");
			}
			
			builder.append(r.getRole().getAuthority());
		}
		
		return builder.toString();
	}
	
	public List<Role> getRoles(){
		List<Role> roles = new ArrayList<>();
		
		for (SubscriptionRole r:getSubscriptionRoles()){
			roles.add(r.getRole());
		}
		
		return roles;
	}
}
