/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 * Created on: Jan 23, 2009
 * Created by: emcgreal
 */

package com.imilia.server.domain.enums;

import java.util.Locale;

import org.springframework.context.MessageSource;

import com.imilia.utils.i18n.Localized;

/**
 * The Enum Gender.
 */
public enum Gender implements Localized {
		
	/** The Male. */
	Male,
	
	/** The Female. */
	Female;
	
	/* (non-Javadoc)
	 * @see com.imilia.server.domain.common.Localeized#getMessageId()
	 */
	/**
	 * Gets the message id.
	 * 
	 * @return the message id
	 */
	public String getMessageId() {
		return Gender.class.getSimpleName() + "." + name();
	}

	/* (non-Javadoc)
	 * @see com.imilia.server.domain.Localized#getLocalizedMessage(org.springframework.context.MessageSource, java.util.Locale)
	 */
	public String getLocalizedMessage(MessageSource messageSource, Locale locale) {
		return messageSource.getMessage(getMessageId(), null, locale);
	}
	
	/**
	 * Gets the symbol.
	 * 
	 * @return the symbol
	 */
	public String getSymbol(){
		switch(this){
		case Male:
			return "\u2642";
		default:
			return "\u2640";
		}
	}
}
