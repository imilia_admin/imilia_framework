/**
 * Imilia Interactive Mobile Applications  GmbH.
 * Copyright (c) 2008
 * @author emcgreal
 * @created 12.03.2008
 * Last modified by:   	$Author:$
 * Last modified on:	$Date:$
 * Version:				$Revision:$
 */

package com.imilia.server.domain.filter;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Transient;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import com.avaje.ebean.EbeanServer;
import com.avaje.ebean.Query;
import com.avaje.ebeaninternal.api.SpiEbeanServer;
import com.imilia.server.domain.Crud;
import com.imilia.server.domain.CrudAware;
import com.imilia.server.domain.DomainObject;
import com.imilia.server.domain.enums.ObjectStatus;
import com.imilia.utils.i18n.DomainLocale;
import com.imilia.server.validation.ValidationException;

/**
 * The Class Recipe.
 *
 * @param <T> the generic type
 * @author emcg
 */
@Entity
public class Recipe<T> extends DomainObject {

	/** The Constant MAXIMUMLENGTH. */
	private static final int MAXIMUMLENGTH = 16384; // 2^14

	/** The Constant TAG_RECIPE. */
	public final static String TAG_RECIPE = "recipe";
	
	/** The Constant TAG_CRUDFILTERS. */
	public final static String TAG_CRUDFILTERS = "crudFilters";

	
	/** The clazz. */
//	@NotNull
//	@Basic(optional = false)
	private Class<T> clazz;

	/** The name. */
	@NotNull
	@Min(1)
	@Column(length = 255)
	@Max(value = 255)
	private String name;

	/** The description. */
	@Column(length = 255)
	@Max(value = 255)
	private String description;

	/** The crud filters. */
	@Transient
	private transient List<CrudFilter<T>> crudFilters = new ArrayList<CrudFilter<T>>();

	/** The lob. */
	@Lob
	@Column(length=MAXIMUMLENGTH)		
	private String lob;
	
	
	/** The RECIPE_NO_LIMIT. */
	public static int RECIPE_NO_LIMIT = Integer.MAX_VALUE;

	/** The RECIPE_DEFAULT_LIMIT. */
	public static int RECIPE_DEFAULT_LIMIT = 1000;

	/** The max results. */
	private int maxResults = RECIPE_DEFAULT_LIMIT;

	/**
	 * Instantiates a new recipe.
	 */
	public Recipe() {
		super();
	}

	/**
	 * Instantiates a new recipe.
	 *
	 * @param clazz
	 *            the clazz
	 */
	public Recipe(Class<T> clazz) {
		this.clazz = clazz;

		CrudFilter<T> cf = new CrudFilter<T>(this);
		this.crudFilters.add(cf);
	}

	/**
	 * Instantiates a new recipe.
	 *
	 * @param clazz
	 *            the clazz
	 * @param cf
	 *            the cf
	 */
	public Recipe(Class<T> clazz, CrudFilter<T> cf) {
		this.clazz = clazz;
		this.crudFilters.add(cf);
		cf.setRecipe(this);
	}

	/**
	 * Duplicate.
	 *
	 * @return the recipe< t>
	 */
	public Recipe<T> duplicate() {
		Recipe<T> recipe = new Recipe<T>();
		recipe.setClazz(clazz);
		recipe.setDescription(description);
		recipe.setName(name);
		recipe.setMaxResults(maxResults);

		for (CrudFilter<T> cf : getCrudFilters()) {
			recipe.getCrudFilters().add((CrudFilter<T>) cf.duplicate(recipe));
		}
		return recipe;
	}
	
	/**
	 * Duplicate.
	 *
	 * @param recipe the recipe
	 * @return the recipe< t>
	 */
	public void copy(Recipe<T> recipe) {
		if (recipe != this){
			recipe.getCrudFilters().clear();
			recipe.setLob(null);
	
			recipe.setClazz(clazz);
	
			for (CrudFilter<T> cf : getCrudFilters()) {
				recipe.getCrudFilters().add((CrudFilter<T>) cf.duplicate(recipe));
			}
		}
	}
	/**
	 * Clear optional criteria.
	 */
	public void clearOptionalCriteria() {
		for (CrudFilter<T> classFilter : crudFilters) {
			classFilter.clearOptionalCriteria();
		}
	}

	/**
	 * Gets the clazz.
	 *
	 * @return the clazz
	 */
	public Class<T> getClazz() {
		return clazz;
	}

	/**
	 * Sets the clazz.
	 *
	 * @param clazz
	 *            the clazz to set
	 */
	public void setClazz(Class<T> clazz) {
		this.clazz = clazz;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the max results.
	 *
	 * @return the maxResults
	 */
	public int getMaxResults() {
		return maxResults;
	}

	/**
	 * Sets the max results.
	 *
	 * @param maxResults
	 *            the maxResults to set
	 */
	public void setMaxResults(int maxResults) {
		this.maxResults = maxResults;
	}

	/**
	 * Checks if is result set limited.
	 *
	 * @return true, if is result set limited
	 */
	public boolean isResultSetLimited() {
		return maxResults != RECIPE_NO_LIMIT;
	}

	/**
	 * Execute.
	 *
	 * @param ebeanServer the ebean server
	 * @param domainLocale the domain locale
	 * @param result the result
	 * @throws ValidationException the validation exception
	 */
	public void execute(final EbeanServer ebeanServer, final DomainLocale domainLocale, final RecipeResult<T> result)
			throws ValidationException {
		final List<T> results = new ArrayList<T>(0);

		for (CrudFilter<?> cf : getCrudFilters()) {
			final QueryBuilder oqlQuery 
				= new QueryBuilder(domainLocale, ((SpiEbeanServer)ebeanServer).getDatabasePlatform()); 

			cf.buildOqlQuery(oqlQuery);
			
			final Query<T> q = (Query<T>) ebeanServer.createQuery(getClazz(), oqlQuery.toHQL());

			for (String paramName : oqlQuery.getParams().keySet()) {
				q.setParameter(paramName, oqlQuery.getParams().get(paramName));
			}

			if (isResultSetLimited()) {
				q.setMaxRows(getMaxResults() + 1);
			}
			
			final List<T> list = q.findList();

			if (!list.isEmpty()){
				boolean crudAware = cf.getClazz().isAssignableFrom(CrudAware.class);
				
				if (crudAware){
					for (T t:list){
						((CrudAware)t).setCrud(new Crud(cf.getCruds()));
					}
				}
				
				results.addAll(list);
			}
		}

		final int size = results.size();
		int limitExceeded = 0;
		if (results.size() > getMaxResults()) {
			// Remove the last one
			limitExceeded = getMaxResults();
			results.remove(size - 1);
		}
		result.setResult(results, limitExceeded);

		// Update any observers
		result.fireEvent();
	}

	/**
	 * Gets the crud filters.
	 *
	 * @return the crudFilters
	 */
	public List<CrudFilter<T>> getCrudFilters() {
		return crudFilters;
	}

	/**
	 * Sets the crud filters.
	 *
	 * @param crudFilters
	 *            the crudFilters to set
	 */
	public void setCrudFilters(List<CrudFilter<T>> crudFilters) {
		this.crudFilters = crudFilters;
	}
	
	/**
	 * Filters out deleted objects
	 * 
	 * <p>Filters only the root object as putting filters on joined objects will prevent
	 * rows with missing rhs objects to be omitted</p>.
	 */
	public void addFilterOutDeleted(){
		for (CrudFilter<?> crudFilter:getCrudFilters()){
			EnumPropertyFilter enumPropertyFilter = 
				new EnumPropertyFilter(crudFilter, "objectStatus", ObjectStatus.class,"com.imilia.server.domain.DomainObject.objectStatus");
			enumPropertyFilter.setAdvanced(true);
			enumPropertyFilter.setMandatory(true);
			List<Enum<?>> list = new ArrayList<Enum<?>>(1);
			list.add(ObjectStatus.Active);
			enumPropertyFilter.setEnums(list);
			enumPropertyFilter.setReadOnly(true);
		}
	}

	/**
	 * Gets the lob.
	 *
	 * @return the lob
	 */
	public final String getLob() {
		return lob;
	}

	/**
	 * Sets the lob.
	 *
	 * @param lob the new lob
	 */
	public final void setLob(String lob) {
		this.lob = lob;
	}
	
	/**
	 * Serialize.
	 */
	public void serialize(){
		RecipeSerializer serializer = new RecipeSerializer();
		serializer.serialize(this);
		setLob(serializer.getBuffer().toString());	
	}
	
	/**
	 * Deserialize.
	 */
	public void deserialize(){
		RecipeSerializer serializer = new RecipeSerializer();
		serializer.deserialize(this, getLob());
	}
	
	/**
	 * Adds the property filters to any class filter (also joined).
	 *
	 * @param classFilter the clazz of the target class filter 
	 * @param propertyFilter the property filter
	 */
	public void addPropertyFilters(Class<?> classFilter, PropertyFilter...propertyFilters){
		for (CrudFilter<?> f:getCrudFilters()){
			f.addPropertyFilters(classFilter, propertyFilters);
		}
	}
	
	
	public List<PropertyFilter> findPropertyFilters(Class<?> clazz,String propertyName){
		final List<PropertyFilter> list = new  ArrayList<PropertyFilter>(); 
		for (CrudFilter<?> f:getCrudFilters()){
			
			// Directly on the crud filter
			if (f.getClazz().equals(clazz)){
				final PropertyFilter pf = f.getPropertyFilter(propertyName);
				
				if (pf != null){
					list.add(pf);
				}
			}

			// Check any joins
			final List<ClassFilter<?>> filters = f.findClassFilters(clazz);
			for (ClassFilter<?> cf:filters){
				final PropertyFilter pf = cf.getPropertyFilter(propertyName);
				
				if (pf != null){
					list.add(pf);
				}
			}
		}
		
		return list;
	}
}
