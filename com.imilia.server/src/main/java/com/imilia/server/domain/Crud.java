/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 * 
 * Created on: Mar 3, 2009
 * Created by: emcgreal
 */
package com.imilia.server.domain;

import javax.persistence.Embeddable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.imilia.utils.Flags;

/**
 * The Class Crud is a helper class to manager Domain Object cruds
 * 
 * CRUD = Create, Read, Update and Delete (+ Executable).
 */
@Embeddable
public class Crud {
	private final static Logger logger = LoggerFactory.getLogger(Crud.class);
	
	// CRUD codes - can be combined with boolean operators
	/** The Constant NONE. */
	public final static long NONE 	= 0x00000000;

	/** The Constant CREATABLE. */
	public final static long CREATABLE 	= 0x00000001;

	/** The Constant READABLE. */
	public final static long READABLE 	= 0x00000002;

	/** The Constant UPDATABLE. */
	public final static long UPDATABLE 	= 0x00000004;

	/** The Constant DELETABLE. */
	public final static long DELETABLE 	= 0x00000008;
	
	/** The Constant EXECUTABLE. */
	public final static long EXECUTABLE = 0x00000010;
	

	/** The Constant CRUD. */
	public final static long CRUD = CREATABLE|READABLE|UPDATABLE|DELETABLE;				//0x0F = 15
	
	/** The Constant CRUDE. */
	public final static long CRUDE = CREATABLE|READABLE|UPDATABLE|DELETABLE|EXECUTABLE; //0x1F = 31

	/** The Constant RU. */
	public final static long RU = READABLE|UPDATABLE;

	/** The Constant RUD. */
	public final static long RUD = READABLE|UPDATABLE|DELETABLE;


	/** The crud. */
	private long crudFlags;

	/**
	 * Instantiates a new crud.
	 */
	public Crud() {
		super();
	}

	/**
	 * Instantiates a new crud.
	 *
	 * @param crud the crud
	 */
	public Crud(long crud) {
		super();
		setCrudFlags(crud);
	}

	public Crud(String crud) {
		this(Crud.parse(crud));
	}

	/**
	 * Gets the crud.
	 *
	 * @return the crud
	 */
	public long getCrudFlags() {
		return crudFlags;
	}

	/**
	 * Sets the crud.
	 *
	 * @param crud the crud to set
	 */
	public void setCrudFlags(long crud) {
		this.crudFlags = crud;
	}
	
	/**
	 * Checks if is creates the allowed.
	 *
	 * @return true, if is creates the allowed
	 */
	public boolean isCreatable(){
		return Flags.isSet(crudFlags, CREATABLE);
	}

	/**
	 * Sets the creates the allowed.
	 *
	 * @param allowed the new creates the allowed
	 */
	public void setCreatable(boolean allowed){
		crudFlags = Flags.set(crudFlags, CREATABLE, allowed);
	}

	/**
	 * Checks if is read allowed.
	 *
	 * @return true, if is read allowed
	 */
	public boolean isReadable(){
		return Flags.isSet(crudFlags, READABLE);
	}
	
	/**
	 * Sets the read allowed.
	 *
	 * @param allowed the new read allowed
	 */
	public void setReadable(boolean allowed){
		crudFlags = Flags.set(crudFlags, READABLE, allowed);
	}

	/**
	 * Checks if is update allowed.
	 *
	 * @return true, if is update allowed
	 */
	public boolean isUpdatable(){
		return Flags.isSet(crudFlags, UPDATABLE);
	}

	/**
	 * Sets the update allowed.
	 *
	 * @param allowed the new update allowed
	 */
	public void setUpdatable(boolean allowed){
		crudFlags = Flags.set(crudFlags, UPDATABLE, allowed);
	}

	/**
	 * Checks if is delete allowed.
	 *
	 * @return true, if is delete allowed
	 */
	public boolean isDeletable(){
		return Flags.isSet(crudFlags, DELETABLE);
	}

	/**
	 * Sets the delete allowed.
	 *
	 * @param allowed the new delete allowed
	 */
	public void setDeletable(boolean allowed){
		crudFlags = Flags.set(crudFlags, DELETABLE, allowed);
	}
	
	/**
	 * Checks if is executable.
	 *
	 * @return true, if is executable
	 */
	public boolean isExecutable(){
		return Flags.isSet(crudFlags, EXECUTABLE);
	}

	/**
	 * Sets the delete allowed.
	 *
	 * @param allowed the new delete allowed
	 */
	public void setExecutable(boolean allowed){
		crudFlags = Flags.set(crudFlags, EXECUTABLE, allowed);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (crudFlags ^ (crudFlags >>> 32));
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Crud other = (Crud) obj;
		if (crudFlags != other.crudFlags)
			return false;
		return true;
	}
	
	/**
	 * Combine.
	 *
	 * @param crud the crud
	 */
	public void combine(Crud crud){
		this.crudFlags |= crud.crudFlags;
	}

	/**
	 * Checks if is writable or executable.
	 *
	 * @return true, if is writable or executable
	 */
	public boolean isWritableOrExecutable() {
		return isUpdatable() || isExecutable();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		
		if (isCreatable()){
			builder.append("C");
		}
		if (isReadable()){
			builder.append("R");
		}
		if (isUpdatable()){
			builder.append("U");
		}
		if (isDeletable()){
			builder.append("D");
		}
		if (isExecutable()){
			builder.append("E");
		}
		
		return builder.toString(); 
	}
	
	public static long parse(String crud){
		long result = 0;
		
		for (char c:crud.toUpperCase().toCharArray()){
			switch(c){
			case 'C':
				result |= CREATABLE;
				break;
			case 'R':
				result |= READABLE;
				break;
			case 'U':
				result |= UPDATABLE;
				break;
			case 'D':
				result |= DELETABLE;
				break;
			case 'E':
				result |= EXECUTABLE;
				break;
				default:
					logger.warn("Ignoring crud flag:'" + c + "'");
			}
		}
		
		return result;
	}
}
