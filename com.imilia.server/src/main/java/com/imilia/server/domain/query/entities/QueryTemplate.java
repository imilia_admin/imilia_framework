package com.imilia.server.domain.query.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.imilia.server.domain.DomainObject;

/**
 * The Class QueryTemplate.
 */
@Entity
public class QueryTemplate extends DomainObject {
	
	/** The name. */
	@Column(unique=true)
	private String name;
	
	/** The query text. */
	@Lob
	private String queryText;
	
	/** The query view. */
	@ManyToOne
	private QueryView queryView;
	
	/** The restrictions. */
	@OneToMany(mappedBy="queryTemplate", cascade=CascadeType.ALL)
	private List<QueryRestriction> restrictions;

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the query view.
	 *
	 * @return the query view
	 */
	public QueryView getQueryView() {
		return queryView;
	}

	/**
	 * Sets the query view.
	 *
	 * @param queryView the new query view
	 */
	public void setQueryView(QueryView queryView) {
		this.queryView = queryView;
	}

	/**
	 * Gets the restrictions.
	 *
	 * @return the restrictions
	 */
	public List<QueryRestriction> getRestrictions() {
		return restrictions;
	}

	/**
	 * Sets the restrictions.
	 *
	 * @param restrictions the new restrictions
	 */
	public void setRestrictions(List<QueryRestriction> restrictions) {
		this.restrictions = restrictions;
	}

	/**
	 * Gets the query text.
	 *
	 * @return the query text
	 */
	public String getQueryText() {
		return queryText;
	}

	/**
	 * Sets the query text.
	 *
	 * @param queryText the new query text
	 */
	public void setQueryText(String queryText) {
		this.queryText = queryText;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "QueryTemplate [name=" + name + ", queryText=" + getQueryText()
				+ ", queryView=" + queryView + "]";
	}

	/**
	 * Adds the.
	 *
	 * @param restriction the restriction
	 */
	public void add(QueryRestriction restriction) {
		getRestrictions().add(restriction);
		restriction.setQueryTemplate(this);
	}
	
}
