/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2010 - all rights reserved
 * Created on: Feb 2, 2010
 * Created by: emcgreal
 */
package com.imilia.server.domain.common;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Email;
/**
 * The Class EmailAddress.
 */
public class EmailAddress {
	
	/** The email. */
	@NotNull
	@Min(3)
	@Email
	private String email;
	
	/**
	 * Instantiates a new email address.
	 * 
	 * @param email the email
	 */
	public EmailAddress(String email) {
		super();
		setEmail(email);
	}

	/**
	 * Gets the email.
	 * 
	 * @return the email
	 */
	public final String getEmail() {
		return email;
	}

	/**
	 * Sets the email.
	 * 
	 * @param email the new email
	 */
	public final void setEmail(String email) {
		this.email = email != null ? email.trim() : null;
	}
}
