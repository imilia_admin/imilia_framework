/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2014 - all rights reserved
 *
 * Created on: Jan 22, 2014
 * Created by: emcgreal
 */
package com.imilia.server.domain.model.property;

import java.util.List;

import com.imilia.server.domain.Crud;
import com.imilia.server.domain.access.Role;

/**
 * The Interface ApplicationPropertyCrudProvider.
 */
public interface ApplicationPropertyCrudProvider {
	
	/**
	 * Gets the crud.
	 *
	 * @param property the property
	 * @param roles the roles
	 * @return the crud
	 */
	public Crud getCrud(Property property, List<Role> roles);
}
