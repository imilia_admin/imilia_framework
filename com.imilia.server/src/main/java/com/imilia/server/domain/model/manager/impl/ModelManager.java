/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2014 - all rights reserved
 *
 * Created on: Jan 22, 2014
 * Created by: emcgreal
 */
package com.imilia.server.domain.model.manager.impl;

import java.util.ArrayList;
import java.util.List;

import ognl.Ognl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.imilia.server.domain.model.IModel;
import com.imilia.server.domain.model.annotations.Cmd;
import com.imilia.server.domain.model.annotations.Evt;
import com.imilia.server.domain.model.annotations.Grp;
import com.imilia.server.domain.model.annotations.Model;
import com.imilia.server.domain.model.annotations.Prp;
import com.imilia.server.domain.model.command.ICommand;
import com.imilia.server.domain.model.command.impl.Command;
import com.imilia.server.domain.model.event.IEvent;
import com.imilia.server.domain.model.event.IEventSource;
import com.imilia.server.domain.model.manager.IModelManager;
import com.imilia.server.domain.model.property.Property;
import com.imilia.server.domain.model.property.PropertyAttributeProvider;
import com.imilia.server.domain.model.property.PropertyImpl;
import com.imilia.server.domain.model.property.PropertyCrudProvider;
import com.imilia.utils.strings.StringUtils;

/**
 * The Class ModelManager reads the model's @Model annotation and builds the properties, events and commands supported by the model
 *
 * @param <T> the model type
 */
public class ModelManager<T extends IModel> implements IModelManager {

	/** The logger. */
	private final Logger logger = LoggerFactory.getLogger(ModelManager.class);
	
	/** The model. */
	private T model;
	
	/** The m. */
	private Model m;
	
	/** The properties. */
	private List<Property> properties = new ArrayList<Property>();
	
	/** The event sources. */
	private List<IEventSource<IEvent>> eventSources = new ArrayList<IEventSource<IEvent>>();
	
	/** The commands. */
	private List<ICommand> commands = new ArrayList<ICommand>();
	
	/**
	 * Instantiates a new model manager.
	 *
	 * @param model the model
	 */
	public ModelManager(T model) {
		this.model = model; 
		m = model.getClass().getAnnotation(Model.class);
		
		String modelName = StringUtils.empty2Null(m.name()); 
		if (modelName == null){
			modelName = model.getClass().getName();
		}
		model.setName(modelName);
	}
	
	public void init(){
		
		buildProperties(m.prps(), null, StringUtils.empty2Null(m.path()), 
				StringUtils.empty2Null(m.cruds()),
				StringUtils.empty2Null(m.attributes())
				);
		
		for (Grp g:m.grps()){
			buildProperties(g.prps(), 
					g.name(),
					StringUtils.chooseFirstNonEmpty(g.path(),m.path()), 
					StringUtils.chooseFirstNonEmpty(g.cruds(), m.cruds()),
					StringUtils.chooseFirstNonEmpty(g.attributes(), m.attributes())
					);
		}
		
		buildEvents();
		buildCommands();
	}

	/**
	 * Gets the model.
	 *
	 * @return the model
	 */
	public T getModel(){
		return model;
	}
	
	/* (non-Javadoc)
	 * @see com.imilia.server.domain.model.manager.IModelManager#getName()
	 */
	public String getName() {
		return m.name();
	}


	/**
	 * Builds the properties.
	 */
	protected void buildProperties(Prp[] prps, String group, String pathPrefix, String crudMethod, String attributesMethod){
		for (Prp pd:prps){
			
			String name = pd.value();
				
			// Path is the name by default
			String path = StringUtils.chooseFirstNonEmpty(pd.path(), pd.value());
			
			if (pathPrefix != null){
				path = pathPrefix + "." + path; 
			}
			
			final PropertyImpl p = new PropertyImpl(this.model, name, path);
			p.setGroup(group);
			try{
				// Crud
				String crudMethodName = 
						StringUtils.chooseFirstNonEmpty(pd.cruds(), crudMethod);
				
				if (crudMethodName == null){
					p.setPropertyCrudProvider(model);
				}else{
					p.setPropertyCrudProvider((PropertyCrudProvider) Ognl.getValue(crudMethodName +"()", model));
				}

				// Attributes
				String attributesMethodName = 
						StringUtils.chooseFirstNonEmpty(pd.attributes(), attributesMethod);
				
				if (attributesMethodName != null){
					p.setAttributes((PropertyAttributeProvider) Ognl.getValue(attributesMethodName +"()", model));
				}else{
					p.setAttributes(getModel());
				}

				String list = StringUtils.empty2Null(pd.list());
				
				if (list != null){
					p.getAttributesProvider().setAttribute(p, PropertyAttributeProvider.LIST, list);
				}
				properties.add(p);
			} catch (Exception e) {
				logger.error("Failed to process property:" + p.getQName(), e);
			}
		}
	}
	
	/**
	 * Builds the events.
	 */
	protected void buildEvents(){
		for (Evt ed:m.evts()){
			try {
				String source = ed.source();
				
				if ("".equals(source)){
					source = "get" + StringUtils.capitalizeFirstLetter(ed.name());
				}
				IEventSource<IEvent> eventSource = (IEventSource<IEvent>) Ognl.getValue(source +"()", model);
				eventSource.setName(ed.name());
				eventSources.add(eventSource);
			} catch (Exception e) {
				logger.error("Failed to process event:" + ed.name(), e);
			}
		}
	}
	
	/**
	 * Builds the commands.
	 */
	protected void buildCommands(){
		for (Cmd cd:m.cmds()){
			Command command = new Command(model, cd.value());
			command.setPropertyCrudProvider(getModel());
			command.setAttributes(getModel());
			try {
				if ("".equals(cd.method())){
					command.setMethod(cd.value());
				}else{
					command.setMethod(cd.method());
				}

				// Crud method
				if (!"".equals(cd.crudMethod())){
					command.setPropertyCrudProvider((PropertyCrudProvider) Ognl.getValue(cd.crudMethod() +"()", model));
				}

				// Attributes method
				if (!"".equals(cd.attributesMethod())){
					command.setPropertyCrudProvider((PropertyCrudProvider) Ognl.getValue(cd.attributesMethod() +"()", model));
				}

				commands.add(command);
			} catch (Exception e) {
				logger.error("Failed to process command:" + command.getQName(), e);
			}
		}
	}
	
	//--------------------------------
	
	/* (non-Javadoc)
	 * @see com.imilia.server.domain.model.manager.IModelManager#getProperties()
	 */
	public List<Property> getProperties() {
		return properties;
	}

	/* (non-Javadoc)
	 * @see com.imilia.server.domain.model.manager.IModelManager#getEventSources()
	 */
	public List<IEventSource<IEvent>> getEventSources() {
		return eventSources;
	}

	/* (non-Javadoc)
	 * @see com.imilia.server.domain.model.manager.IModelManager#getCommands()
	 */
	public List<ICommand> getCommands() {
		return commands;
	}

	@Override
	public Property getProperty(String name) {
		for (Property p:properties){
			if (name.equals(p.getName())){
				return p;
			}
		}
		return null;
	}
}
