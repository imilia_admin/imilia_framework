package com.imilia.server.domain.query.execution.values;

import com.imilia.server.domain.query.entities.QueryProperty;
import com.imilia.server.domain.query.execution.Comparison;
import com.imilia.server.domain.query.execution.LogicalOperator;
import com.imilia.server.domain.query.execution.QueryCrudExecution;

public class BooleanQueryPropertyValue extends ComparisonValue{

	private Boolean booleanValue;
	
	public BooleanQueryPropertyValue(QueryProperty queryProperty) {
		super(queryProperty);
		setComparison(Comparison.Any);        
		addPossibleComparisons(
				Comparison.IsNull,
				Comparison.NotNull,
				Comparison.Equals,
				Comparison.NotEquals);
	}

	public void addToExecution(QueryCrudExecution<?> execution) {
		switch(comparison){
		case Any:
			// Don't restrict 
			break;
		case IsNull:
		case NotNull:
		case Equals:
		case NotEquals:
			execution.addRestrictionValue(
					getComparison().toPredicate(getPath(), 
							execution.addParameter(booleanValue)), 
					LogicalOperator.And);
			break;
		}
	}
	public Boolean getBooleanValue() {
		return booleanValue;
	}

	public void setBooleanValue(Boolean booleanValue) {
		this.booleanValue = booleanValue;
	}

	@Override
	public String toString() {
		return "BooleanQueryPropertyValue [path=" + getPath() +" booleanValue=" + booleanValue + "]";
	}

}
