/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2011 - all rights reserved
 *
 * Created on: May 20, 2011
 * Created by: emcgreal
 */
package com.imilia.server.xml;

import javax.xml.stream.XMLStreamReader;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.avaje.ebean.EbeanServer;
import com.imilia.server.domain.DomainObject;
import com.imilia.utils.i18n.DomainLocale;

/**
 * A factory for creating DomainObjectStaxHandler objects.
 */
public class DomainObjectStaxObjectFactory extends DefaultStaxObjectFactory {

	/** The ebean server. */
	private EbeanServer ebeanServer;
	
	/** The classes. */
	private Class<?>[] versionMonitoredClasses;

	/**
	 * Instantiates a new domain object stax object factory.
	 *
	 */
	public DomainObjectStaxObjectFactory() {
		super();
	}
	
	/**
	 * Instantiates a new domain object stax object factory.
	 *
	 * @param domainLocale the domain locale
	 */
	public DomainObjectStaxObjectFactory(DomainLocale domainLocale) {
		super(domainLocale);
	}
	
	/* (non-Javadoc)
	 * @see com.imilia.server.xml.DefaultStaxObjectFactory#create(javax.xml.stream.XMLStreamReader, java.lang.Class)
	 */
	@Override
	public <T> T create(XMLStreamReader xmlReader, Class<T> clazz)
			throws Exception {
		long oid = getOID(xmlReader);
		
		if (oid != 0){
			final T domainObject = load(clazz, oid);
			
			if (domainObject != null){
				checkVersion((DomainObject)domainObject, getVersion(xmlReader));
				return  domainObject;
			}
			throw new IllegalArgumentException("Domain object not found for class:" + clazz + " OID: " + oid);
		}else{
			return super.create(xmlReader, clazz);
		}
	}
	
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public <T> T load(Class<T> clazz, long oid){
		return ebeanServer.find(clazz, oid);
	}
	
	/**
	 * Check version.
	 *
	 * @param domainObject the domain object
	 * @param version the version
	 */
	public void checkVersion(DomainObject domainObject, int version){
		if (version == 0 && isClassMonitored(domainObject.getClass())){
			throw new MissingVersionException("No version found:" + domainObject.getClass() + " OID: " + domainObject.getOid());
		}
	}

	/**
	 * Sets the version monitored classes.
	 *
	 * @param classes the new version monitored classes
	 */
	public void setVersionMonitoredClasses(Class<?>...classes){
		this.versionMonitoredClasses = classes;
	}
	
	/**
	 * Checks if is class monitored.
	 *
	 * @param clazz the clazz
	 * @return true, if is class monitored
	 */
	public boolean isClassMonitored(Class<?> clazz){
		if (versionMonitoredClasses != null){
			for (Class<?> c:versionMonitoredClasses){
				if (c.equals(clazz)){
					return true;
				}
			}
		}
		
		return false;
	}
	/**
	 * Gets the ebean server.
	 *
	 * @return the ebean server
	 */
	public EbeanServer getEbeanServer() {
		return ebeanServer;
	}

	/**
	 * Sets the ebean server.
	 *
	 * @param ebeanServer the new ebean server
	 */
	public void setEbeanServer(EbeanServer ebeanServer) {
		this.ebeanServer = ebeanServer;
	}

	/**
	 * Gets the oID.
	 *
	 * @param xmlStreamReader the xml stream reader
	 * @return the oID
	 */
	private long getOID(XMLStreamReader xmlStreamReader){
		final int attributeCount = xmlStreamReader.getAttributeCount();
		
		for (int i = 0;i < attributeCount;i++){
			if ("oid".equals(xmlStreamReader.getAttributeName(i).getLocalPart())){ 
				return Long.parseLong(xmlStreamReader.getAttributeValue(i));
			}
		}
		
		return 0;
	}
	
	/**
	 * Gets the version.
	 *
	 * @param xmlStreamReader the xml stream reader
	 * @return the version
	 */
	private int getVersion(XMLStreamReader xmlStreamReader){
		final int attributeCount = xmlStreamReader.getAttributeCount();
		for (int i = 0;i < attributeCount;i++){
			if ("version".equals(xmlStreamReader.getAttributeName(i).getLocalPart())){ 
				return Integer.parseInt(xmlStreamReader.getAttributeValue(i));
			}
		}
		
		return 0;
	}

	/**
	 * Gets the version monitored classes.
	 *
	 * @return the version monitored classes
	 */
	public Class<?>[] getVersionMonitoredClasses() {
		return versionMonitoredClasses;
	}
}
