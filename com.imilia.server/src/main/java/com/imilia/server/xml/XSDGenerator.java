package com.imilia.server.xml;

import java.io.PrintWriter;

import com.imilia.utils.classes.ClassMatcher;

public interface XSDGenerator {
	public void openSchema();
	
	public void generate(String basePackage) throws Exception;

	public void closeSchema();

	public void setClassMatcher(ClassMatcher classMatcher);
	public ClassMatcher getClassMatcher();
	
	public void setPrintWriter(PrintWriter printWriter);
	public PrintWriter getPrintWriter();
}
