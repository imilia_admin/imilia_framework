/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2011 - all rights reserved
 *
 * Created on: Jun 15, 2011
 * Created by: emcgreal
 */
package com.imilia.server.xml;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamReader;

/**
 * A factory for creating StaxHandler objects.
 */
public interface StaxHandlerFactory {
	
	/**
	 * Creates the.
	 *
	 * @param elementName the element name
	 * @param xmlReader the xml reader
	 * @param objectFactory the object factory
	 * @return the stax handler
	 * @throws Exception the exception
	 */
	public StaxHandler<?> create(QName elementName, XMLStreamReader xmlReader, StaxObjectFactory objectFactory) throws Exception;
	
	/**
	 * Register.
	 *
	 * @param elementName the element name
	 * @param clazz the clazz
	 */
	public void register(String elementName, Class<?> clazz);
	
	/**
	 * Register.
	 *
	 * @param clazzs the clazzs
	 */
	public void register(Class<?>...clazzs);
}
