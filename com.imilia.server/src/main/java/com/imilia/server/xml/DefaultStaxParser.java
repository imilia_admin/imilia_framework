/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2011 - all rights reserved
 *
 * Created on: May 20, 2011
 * Created by: emcgreal
 */
package com.imilia.server.xml;

import java.io.StringReader;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.XMLEvent;

/**
 * The Class DefaultStaxParser.
 */
public class DefaultStaxParser {

	/** The factory. */
	private final StaxHandlerFactory factory;

	/**
	 * Instantiates a new default stax parser.
	 *
	 * @param factory the factory
	 */
	public DefaultStaxParser(StaxHandlerFactory factory) {
		this.factory = factory;
	}

	/**
	 * Parses the.
	 *
	 * @param <T> the generic type
	 * @param xml the xml
	 * @param clazz the clazz
	 * @return the t
	 * @throws Exception the exception
	 */
	@SuppressWarnings("unchecked")
	public <T> T parse(String xml, Class<T> clazz, StaxObjectFactory staxObjectFactory) throws Exception {
		XMLStreamReader xmlStreamReader = XMLInputFactory.newInstance()
				.createXMLStreamReader(new StringReader(xml));

		T t = null;
		while (xmlStreamReader.hasNext()) {
			switch (xmlStreamReader.next()) {
			case XMLEvent.START_ELEMENT:
				QName qn = xmlStreamReader.getName();

				StaxHandler<?> handler = factory.create(qn, xmlStreamReader, staxObjectFactory);
				
				Object o = handler.handle(xmlStreamReader);
				
				if (t == null){
					t = (T)o;
				}
			}
		}
		
		return t;
	}
}