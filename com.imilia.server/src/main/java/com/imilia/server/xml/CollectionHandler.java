package com.imilia.server.xml;

import java.util.Collection;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.XMLEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class CollectionHandler extends DefaultStaxHandler<Collection<Object>> {
	private static final Logger logger = LoggerFactory.getLogger(CollectionHandler.class);

	public CollectionHandler(StaxHandlerFactory factory, Collection<Object> t, StaxObjectFactory staxObjectFactory) {
		super(factory, t, staxObjectFactory);
	}

	public Collection<Object> handle(XMLStreamReader xmlStreamReader) throws XMLStreamException,
			Exception {
		while (xmlStreamReader.hasNext()){
			switch(xmlStreamReader.next()){
			case XMLEvent.START_ELEMENT:
				StaxHandler<?> handler = getFactory().create(xmlStreamReader.getName(), xmlStreamReader, 
						getStaxObjectFactory());
				
				if (handler != null){
					getT().add((Object)handler.handle(xmlStreamReader));
				}else{
					logger.debug("No handler defined for: " +xmlStreamReader.getName());
				}
				break;
			case XMLEvent.END_ELEMENT:
				return getT();
			}
		}
		
		return getT();
	}

}
