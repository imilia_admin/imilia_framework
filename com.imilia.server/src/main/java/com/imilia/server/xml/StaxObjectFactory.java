/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2011 - all rights reserved
 *
 * Created on: Jun 16, 2011
 * Created by: emcgreal
 */
package com.imilia.server.xml;

import javax.xml.stream.XMLStreamReader;

import com.imilia.utils.i18n.DomainLocale;

/**
 * A factory for creating StaxObject objects.
 */
public interface StaxObjectFactory {
	
	/**
	 * Gets the domain locale.
	 *
	 * @return the domain locale
	 */
	public DomainLocale getDomainLocale();
	
	/**
	 * Creates the.
	 *
	 * @param <T> the generic type
	 * @param xmlReader the xml reader
	 * @param clazz the clazz
	 * @return the t
	 * @throws Exception the exception
	 */
	public <T> T create(XMLStreamReader xmlReader, Class<T> clazz) throws Exception;
}
