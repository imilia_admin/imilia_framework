package com.imilia.server.xml;

public class MissingVersionException extends RuntimeException {

	public MissingVersionException(String msg) {
		super(msg);
	}
}
