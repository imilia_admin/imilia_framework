package com.imilia.server.xml;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

public interface StaxHandler<T> {
	T handle(XMLStreamReader xmlReader) throws XMLStreamException, Exception;
}
