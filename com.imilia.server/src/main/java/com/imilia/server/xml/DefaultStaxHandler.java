/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2010 - all rights reserved
 * Created on: Jul 22, 2010
 * Created by: emcgreal
 */
package com.imilia.server.xml;

import java.lang.reflect.Field;
import java.lang.reflect.Member;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.XMLEvent;

import ognl.Ognl;
import ognl.OgnlException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.joda.time.DateMidnight;
import org.joda.time.DateTime;
import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import com.imilia.server.domain.i18n.LocaleText;
import com.imilia.server.domain.i18n.LocaleTextHolder;
import com.imilia.utils.classes.ClassUtils;

/**
 * The Class DefaultStaxHandler.
 *
 * @param <T> the generic type
 */
public class DefaultStaxHandler<T> implements StaxHandler<T> {
	// The logger
	/** The Constant logger. */
	private static final Logger logger = LoggerFactory.getLogger(DefaultStaxHandler.class);
	
	/** The t. */
	private final T t;
	
	/** The context. */
	@SuppressWarnings("rawtypes")
	private Map context;
	
	/** The factory. */
	private final StaxHandlerFactory factory;
	
	private final StaxObjectFactory staxObjectFactory;
	
	/**
	 * Instantiates a new default stax handler.
	 *
	 * @param factory the factory
	 * @param t the t
	 * @param domainLocale the domain locale
	 */
	public DefaultStaxHandler(StaxHandlerFactory factory, T t, final StaxObjectFactory staxObjectFactory){
		this.factory = factory;
		this.t = t;
		this.staxObjectFactory = staxObjectFactory;
		
		context = Ognl.createDefaultContext(t);

		/* Create an anonymous inner class to handle special conversion */
		Ognl.setTypeConverter(context, new ognl.DefaultTypeConverter() {
			@SuppressWarnings("rawtypes")
		    public Object convertValue( Map context, Object target, Member member, String propertyName, Object value, Class toType)
 		    {
		        if ((toType == DateMidnight.class) && (value instanceof String)) {
					DateTimeFormatter dtf = ISODateTimeFormat.basicDate();
					DateTime dt = dtf.parseDateTime(value.toString());
					
					return dt.toDateMidnight();
		        } else if ((toType == DateTime.class) && (value instanceof String)) {
					DateTimeFormatter dtf = ISODateTimeFormat.basicDateTime();
					DateTime dt = dtf.parseDateTime(value.toString());
					
					return dt;
		        } else if ((toType == LocalTime.class) && (value instanceof String)) {
					DateTimeFormatter dtf = DateTimeFormat.forPattern("hh:mm:ss");
					DateTime dt = dtf.parseDateTime(value.toString());
					
					return dt.toLocalTime();
		        } else if (toType == Class.class && value instanceof String){
		        	try {
						return  Class.forName(value.toString());
					} catch (ClassNotFoundException e) {
						logger.error("Class not found: " + value, e);
					}
					return null;
		        } else if (toType == LocaleTextHolder.class && value instanceof String){
		        	LocaleTextHolder localeTextHolder = new LocaleTextHolder();
		        	new LocaleText(getStaxObjectFactory().getDomainLocale(), localeTextHolder, value.toString());
					return localeTextHolder;
		        } else if (Enum.class.isAssignableFrom(toType) && value instanceof String){
		        	logger.debug("Enum");
		        	Object[] enums = toType.getEnumConstants();
		        	
		        	try{
		        		// Try the index first
		        		int index = Integer.parseInt((String) value);
		        		return enums[index];
		        	}catch(Exception ex){
		        	}
		        	
	        		for (Object e:enums){
	        			Enum<?> en = (Enum<?>) e;
	        		
	        			if (en.name().equals(value)){
	        				return en;
	        			}
	        		}
		        	
		        }else if (Collection.class.isAssignableFrom(toType) && value instanceof String){
		        	
		        	Field f = ClassUtils.findField(target.getClass(), propertyName);
		        	
		        	if (f != null){
		        		Type t = f.getGenericType();
		        	
			        		if( t instanceof ParameterizedType ) {
					        	Collection<Object> collection = null;
					        	if (List.class.isAssignableFrom(toType)){
					        		collection = new ArrayList<Object>();
					        	}else if (Set.class.isAssignableFrom(toType)){
					        		collection = new HashSet<Object>();
					        	}
	
					        	Class<?> genericType = (Class<?>)((ParameterizedType)t).getActualTypeArguments()[0];
	
					        	String input = value.toString().trim();
					        	
					        	if (input.length() > 0){
					 		        String[] vals = input.split(";");
		
				 		        	for (String str:vals){
					 		        	if (Long.class.isAssignableFrom(genericType)){
					 		        		collection.add(Long.parseLong(str));
					 		        	}else if (Integer.class.isAssignableFrom(genericType)){
					 		        		collection.add(Long.parseLong(str));
					 		        	}else{
					 		        		collection.add(str);
					 		        	}
				 		        	}
					        	}

					        	return collection;
			        	}
		        	}
		        }
		        
		        return super.convertValue(context, target, member, propertyName, value, toType);
		    }
		});
	}

	
	/**
	 * Sets the properties.
	 * 
	 * @param xmlStreamReader the xml stream reader
	 * @param target the target
	 */
	protected void setProperties(XMLStreamReader xmlStreamReader, T target){
		final int attributeCount = xmlStreamReader.getAttributeCount();
		
		for (int i = 0;i < attributeCount;i++){
			setProperty(target, 
					xmlStreamReader.getAttributeName(i).getLocalPart(), 
					xmlStreamReader.getAttributeValue(i));
		}
	}
	
	/**
	 * Sets the property.
	 * 
	 * @param target the target
	 * @param property the property
	 * @param value the value
	 * 
	 * @return true, if successful
	 */
	protected boolean setProperty(T target, String property, Object value){
		try {
			Ognl.setValue(property, context, target, value);
			return true;
		} catch (OgnlException e) {
			logger.warn("Failed to set property: " + property, e);
			return false;
		}
	}
	
	/**
	 * Sets the text.
	 *
	 * @param target the target
	 * @param txt the txt
	 */
	protected void setText(T target, String txt){
	}

	/* (non-Javadoc)
	 * @see com.imilia.server.xml.StaxHandler#handle(javax.xml.stream.XMLStreamReader)
	 */
	@SuppressWarnings("unchecked")
	public T handle(XMLStreamReader xmlStreamReader) throws Exception {
		setProperties(xmlStreamReader, t);
		
		while (xmlStreamReader.hasNext()){
			switch(xmlStreamReader.next()){
			case XMLEvent.START_ELEMENT:
				String localName = xmlStreamReader.getName().getLocalPart();
				
				Field f = ClassUtils.findField(t.getClass(), localName);
				
				if (f != null && Collection.class.isAssignableFrom(f.getType())){
					Collection<Object> c = (Collection<Object>) Ognl.getValue(localName, t);
					CollectionHandler collectionHandler = 
						new CollectionHandler(factory, c, getStaxObjectFactory());
					collectionHandler.handle(xmlStreamReader);
				}else{
					StaxHandler<?> handler = factory.create(xmlStreamReader.getName(), xmlStreamReader, 
							getStaxObjectFactory());
					
					if (handler != null){
						setValue(xmlStreamReader.getName(),	handler.handle(xmlStreamReader));
					}else{
						logger.debug("No handler defined for: " +xmlStreamReader.getName());
					}
				}
				break;
				
			case XMLEvent.CHARACTERS:
				setText(getT(), xmlStreamReader.getText());
				break;
			case XMLEvent.END_ELEMENT:
				return t;
			}
		}
		return t;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param property the property
	 * @param o the o
	 */
	protected void setValue(QName property, Object o){
		try {
			Ognl.setValue(property.getLocalPart(), 
					context, 
					t,
					o);
		} catch (OgnlException e) {
			logger.error("Ognl exception", e);
		}
	}

	/**
	 * Gets the t.
	 *
	 * @return the t
	 */
	public final T getT() {
		return t;
	}

	/**
	 * Gets the context.
	 *
	 * @return the context
	 */
	@SuppressWarnings("rawtypes")
	public final Map getContext() {
		return context;
	}

	/**
	 * Gets the factory.
	 *
	 * @return the factory
	 */
	public final StaxHandlerFactory getFactory() {
		return factory;
	}


	public StaxObjectFactory getStaxObjectFactory() {
		return staxObjectFactory;
	}


}
