/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2011 - all rights reserved
 *
 * Created on: Jun 16, 2011
 * Created by: emcgreal
 */
package com.imilia.server.xml;

import javax.xml.stream.XMLStreamReader;

import com.imilia.utils.i18n.DomainLocale;

/**
 * A factory for creating objects when parsing an XML document.
 */
public class DefaultStaxObjectFactory implements StaxObjectFactory {

	/** The domain locale. */
	private DomainLocale domainLocale;
	
	/**
	 * Instantiates a new default stax object factory.
	 */
	public DefaultStaxObjectFactory() {
		super();
		domainLocale = DomainLocale.en_US;
	}
	
	/**
	 * Instantiates a new default stax object factory.
	 *
	 * @param domainLocale the domain locale
	 */
	public DefaultStaxObjectFactory(DomainLocale domainLocale) {
		super();
		this.domainLocale = domainLocale;
	}
	
	/* (non-Javadoc)
	 * @see com.imilia.server.xml.StaxObjectFactory#create(javax.xml.stream.XMLStreamReader, java.lang.Class)
	 */
	@Override
	public <T> T create(XMLStreamReader xmlReader, Class<T> clazz)
			throws Exception {
		return clazz.newInstance();
	}

	/* (non-Javadoc)
	 * @see com.imilia.server.xml.StaxObjectFactory#getDomainLocale()
	 */
	@Override
	public DomainLocale getDomainLocale() {
		return domainLocale;
	}
	
	/**
	 * Sets the domain locale.
	 *
	 * @param domainLocale the new domain locale
	 */
	public void setDomainLocale(DomainLocale domainLocale) {
		this.domainLocale = domainLocale;
	}

}
