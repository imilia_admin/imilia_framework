/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2011 - all rights reserved
 * Created on: Jul 4, 2011
 * Created by: emcgreal
 */

package com.imilia.server.xml;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import org.joda.time.base.BaseDateTime;

import com.imilia.utils.PrettyPrinter;
import com.imilia.utils.classes.ClassFinder;
import com.imilia.utils.classes.ClassMatcher;
import com.imilia.utils.classes.ClassUtils;
import com.imilia.utils.strings.StringUtils;

/**
 * The Class XSDGeneratorImpl.
 */
public class XSDGeneratorImpl extends PrettyPrinter implements XSDGenerator{
	
	/** The Constant xsElementNameOpen. */
	private final static String xsElementNameOpen = "<xs:element name=\""; 
	
	/** The Constant xsElementNameClose. */
	private final static String xsElementNameClose = "\""; 
	
	/** The Constant xsElementOpenEnd. */
	private final static String xsElementOpenEnd = ">"; 
	
	/** The Constant xsElementNameCloseEnd. */
	private final static String xsElementNameCloseEnd = "\"/>"; 

	/** The Constant xsElementNameCloseTypeOpen. */
	private final static String xsElementNameCloseTypeOpen = "\" type=\""; 
	
	/** The Constant xsElementNameCloseTypeClose. */
	private final static String xsElementNameCloseTypeClose = "\""; 
	
	/** The Constant xsElementRequired. */
	private final static String xsElementRequired = " minOccurs=\"1\" maxOccurs=\"1\""; 
	
	/** The Constant xsElementOptional. */
	private final static String xsElementOptional = " minOccurs=\"0\" maxOccurs=\"1\""; 
	
	/** The Constant xsElementClose. */
	private final static String xsElementClose = "</xs:element>"; 
	
	/** The Constant xsAttribteNameOpen. */
	private final static String xsAttribteNameOpen = "<xs:attribute name=\""; 
	
	/** The Constant xsAttributeCloseNameType. */
	private final static String xsAttributeCloseNameType = "\" type=\""; 
	
	/** The Constant xsAttributeClose. */
	private final static String xsAttributeClose = "\"/>";
	
	/** The Constant xsAttributeRequired. */
	private final static String xsAttributeRequired = "\" use=\"required";
	
	/** The Constant xsComplexTypeOpen. */
	private final static String xsComplexTypeOpen = "<xs:complexType>";
	
	/** The Constant xsSequenceOpen. */
	private final static String xsSequenceOpen = "<xs:sequence>";
	
	/** The Constant xsSequenceClose. */
	private final static String xsSequenceClose = "</xs:sequence>";
	
	/** The Constant xsAllOpen. */
	private final static String xsAllOpen = "<xs:all>";
	
	/** The Constant xsAllClose. */
	private final static String xsAllClose = "</xs:all>";
	
	/** The Constant xsComplexTypeClose. */
	private final static String xsComplexTypeClose = "</xs:complexType>";

	/** The Constant xsSimpleTypeNameOpen. */
	private final static String xsSimpleTypeNameOpen = "<xs:simpleType name=\"";
	
	/** The Constant xsSimpleTypeNameClose. */
	private final static String xsSimpleTypeNameClose = "\">";
	
	/** The Constant xsRescrictionBaseOpen. */
	private final static String xsRescrictionBaseOpen = "<xs:restriction base=\"";
	
	/** The Constant xsRescrictionBaseClose. */
	private final static String xsRescrictionBaseClose = "\">";
	
	/** The Constant xsRescrictionClose. */
	private final static String xsRescrictionClose = "</xs:restriction>";
	
	/** The Constant xsSimpleTypeClose. */
	private final static String xsSimpleTypeClose = "</xs:simpleType>";
	
	/** The Constant xsEnumerationValueOpen. */
	private final static String xsEnumerationValueOpen = "<xs:enumeration value=\""; 
	
	/** The Constant xsEnumerationValueClose. */
	private final static String xsEnumerationValueClose = "\">";
	
	/** The Constant xsEnumerationClose. */
	private final static String xsEnumerationClose = "</xs:enumeration>";

	/** The Constant xsAnnotationOpen. */
	private final static String xsAnnotationOpen = "<xs:annotation>";
	
	/** The Constant xsAnnotationClose. */
	private final static String xsAnnotationClose = "</xs:annotation>";

	/** The Constant xsDocumentationOpen. */
	private final static String xsDocumentationOpen = "<xs:documentation>";
	
	/** The Constant xsDocumentationClose. */
	private final static String xsDocumentationClose = "</xs:documentation>";
	  
	/** The enums. */
	private Set<Class<?>> enums = new HashSet<Class<?>>();
	
	/** The classes. */
	private List<Class<?>> classes;
	
	/** The class matcher. */
	private ClassMatcher classMatcher;
	
	/**
	 * Instantiates a new xSD generator impl.
	 */
	public XSDGeneratorImpl() {
		super();
	}

	/* (non-Javadoc)
	 * @see com.imilia.server.xml.XSDGenerator#openSchema()
	 */
	public void openSchema(){
		println("<?xml version=\"1.0\" encoding=\"UTF8\" ?>");
		println("<xs:schema xmlns:xs=\"http://www.w3.org/2001/XMLSchema\"");
		println("\ttargetNamespace=\"http://www.imilia.com/bookina/schema\">");
		indent();
	}
	
	/* (non-Javadoc)
	 * @see com.imilia.server.xml.XSDGenerator#closeSchema()
	 */
	public void closeSchema(){
		outdent();
		println("</xs:schema>");
	}
	
	/**
	 * Generate.
	 *
	 * @param basePackage the base package
	 * @throws Exception the exception
	 */
	public void generate(String basePackage) throws Exception{
		
		final ClassFinder classFinder = new ClassFinder();
		classes = classFinder.findTypes(basePackage, classMatcher);
		
		for (int i = 0;i < classes.size();i++){
			generate(classes.get(i));
		}
		
		printEnums();
	}
	
	/**
	 * Generate.
	 *
	 * @param clazz the clazz
	 */
	public void generate(Class<?> clazz){
		final String simpleClassName = clazz.getSimpleName();
		final String elementName = simpleClassName;
		
		indent();

		println(xsElementNameOpen, elementName, xsElementNameClose, xsElementOpenEnd);
		indent();
		println(xsComplexTypeOpen);
		indent();
		println(xsAllOpen);
		indent();

		final List<Field> fields = ClassUtils.findFields(clazz);
		for (Field f:fields){
			if (classMatcher.isCandidate(f)){
				generate(f);
			}
		}
		
		outdent();
		println(xsAllClose);
		outdent();
		println(xsComplexTypeClose);
		outdent();
		println(xsElementClose);
		outdent();
	}
	
	/**
	 * Generate.
	 *
	 * @param f the f
	 */
	public void generate(Field f) {
		if (isAttribute(f)){
			printAttribute(f.getName(), f);
		}else if (isEnum(f)){
			enums.add(f.getType());
			printAttribute(f.getName(), f);
		}else if (isCollection(f)){
			println(xsElementNameOpen, f.getName(), xsElementNameClose, 
					xsElementOpenEnd);
			indent();
			println(xsComplexTypeOpen);
			indent();
			println(xsSequenceOpen);
			indent();
			String collectionTypeName = getCollectionType(f);
			println(xsElementNameOpen, collectionTypeName, xsElementNameCloseTypeOpen, 
					collectionTypeName,
					xsElementNameCloseEnd);
			outdent();
			println(xsSequenceClose);
			outdent();
			println(xsComplexTypeClose);
			
			outdent();
			println(xsElementClose);
		}else{
			printTabs();
			print(xsElementNameOpen, f.getName(), xsElementNameCloseTypeOpen, 
					f.getType().getSimpleName(), xsElementNameCloseTypeClose);
			if (isRequired(f)){
				print(xsElementRequired);
			}else{
				print(xsElementOptional);
			}
			print(xsElementOpenEnd);
			println();
			
			if (classMatcher.isCandidate(f.getType())){
				if (!classes.contains(f.getType())){
					classes.add(f.getType());
				}
			}
		}
	}

	/**
	 * Prints the attribute.
	 *
	 * @param name the name
	 * @param field the field
	 */
	private void printAttribute(String name, Field field) {
		final String mappedType = mapType(field.getType());
		
		printTabs();
		print(xsAttribteNameOpen, name, xsAttributeCloseNameType, mappedType);
		if (isRequired(field)){
			print(xsAttributeRequired);
		}
		print(xsAttributeClose);
		println();
	}
	
	
	/**
	 * Prints the enums.
	 */
	protected void printEnums(){
		indent();
		for (Class<?> c:enums){
			
			println(xsSimpleTypeNameOpen, StringUtils.lowerFirstLetter(c.getSimpleName()), xsSimpleTypeNameClose);
			indent();
			println(xsRescrictionBaseOpen, "xs:integer", xsRescrictionBaseClose);
			indent();
			Object[] enumConstants = c.getEnumConstants();
			
			int i = 0;
			for (Object o:enumConstants){
				println(xsEnumerationValueOpen, Integer.toString(i++), xsEnumerationValueClose);
				indent();
				println(xsAnnotationOpen);
				indent();
				println(xsDocumentationOpen, o.toString(), xsDocumentationClose);
				outdent();
				println(xsAnnotationClose);
				outdent();
				println(xsEnumerationClose); 
			}
			outdent();
			println(xsRescrictionClose);
			outdent();
			println(xsSimpleTypeClose);
			
		}
		
		outdent();
	}
	
	/**
	 * Map type.
	 *
	 * @param type the type
	 * @return the string
	 */
	private String mapType(Class<?> type) {
		String mapped = StringUtils.lowerFirstLetter(type.getSimpleName());
		if (int.class == type || Integer.class == type){
			mapped = "integer";
		}
		
		return (type.isEnum() ? "" : "xs:") + mapped;
	}
	
	/**
	 * Checks if is required.
	 *
	 * @param f the f
	 * @return true, if is required
	 */
	protected boolean isRequired(Field f){
		if (f.isAnnotationPresent(NotNull.class)){
			return true;
		}

		ManyToOne m = f.getAnnotation(ManyToOne.class);
		
		if (m != null){
			return !m.optional();
		}
		return false;
	}

	/**
	 * Checks if is attribute.
	 *
	 * @param f the f
	 * @return true, if is attribute
	 */
	protected boolean isAttribute(Field f){
		final Class<?> c= f.getType();
		
		return (c.isPrimitive() || c.equals(String.class) || 
				BaseDateTime.class.isAssignableFrom(c) ||
				Date.class.isAssignableFrom(c));
	}
	
	/**
	 * Checks if is enum.
	 *
	 * @param f the f
	 * @return true, if is enum
	 */
	public boolean isEnum(Field f){
		return f.getType().isEnum();
	}
	
	/**
	 * Checks if is collection.
	 *
	 * @param f the f
	 * @return true, if is collection
	 */
	public boolean isCollection(Field f){
		return Collection.class.isAssignableFrom(f.getType());
	}
	
	/**
	 * Gets the collection type.
	 *
	 * @param f the f
	 * @return the collection type
	 */
	protected String getCollectionType(Field f){
		String listName = f.getName();

		int x = 1;
		if (listName.endsWith("es")){
			x = 2;
		}
		listName = listName.substring(0, listName.length() - x);
		
		return StringUtils.capitalizeFirstLetter(listName);
	}
	
	
	/**
	 * Gets the class matcher.
	 *
	 * @return the class matcher
	 */
	public ClassMatcher getClassMatcher() {
		return classMatcher;
	}

	/**
	 * Sets the class matcher.
	 *
	 * @param classMatcher the new class matcher
	 */
	public void setClassMatcher(ClassMatcher classMatcher) {
		this.classMatcher = classMatcher;
	}
}
