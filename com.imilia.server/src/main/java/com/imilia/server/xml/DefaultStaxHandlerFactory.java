/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2011 - all rights reserved
 *
 * Created on: May 20, 2011
 * Created by: emcgreal
 */
package com.imilia.server.xml;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamReader;

import com.imilia.utils.strings.StringUtils;

/**
 * A factory for creating DefaultStaxHandler objects.
 */
public class DefaultStaxHandlerFactory implements StaxHandlerFactory {

	/** The map. */
	private HashMap<String, Class<?>> map = new HashMap<String, Class<?>>();
	
	/** The use default naming convention. */
	private boolean useDefaultNamingConvention = true;
	
	/* (non-Javadoc)
	 * @see com.imilia.server.xml.StaxHandlerFactory#create(javax.xml.namespace.QName)
	 */
	@Override
	public StaxHandler<?> create(QName elementName, XMLStreamReader xmlReader, StaxObjectFactory staxObjectFactory) throws Exception {

		// Check for type xsi:type
		final String type =  xmlReader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type");
		
		Class<?> clazz = null;
		if (type != null){
			clazz = Class.forName(type);
		}else{
			clazz = getClass(elementName);
		}

		return create(xmlReader, clazz, staxObjectFactory);
	}
	
	/**
	 * Creates the.
	 *
	 * @param xmlReader the xml reader
	 * @param clazz the clazz
	 * @return the stax handler
	 * @throws Exception the exception
	 */
	protected StaxHandler<?> create(XMLStreamReader xmlReader, Class<?> clazz, StaxObjectFactory staxObjectFactory) throws Exception{
		if (Collection.class.isAssignableFrom(clazz)){
			return new CollectionHandler(this, new ArrayList<Object>(), staxObjectFactory);
		}
		return new DefaultStaxHandler<Object>(this, staxObjectFactory.create(xmlReader, clazz), staxObjectFactory);
	}

	/**
	 * Gets the class.
	 *
	 * @param elementName the element name
	 * @return the class
	 */
	protected Class<?> getClass(QName elementName){
		final String s = elementName.getLocalPart();
		Class<?> clazz = map.get(s);
		
		if (clazz == null && useDefaultNamingConvention){
			// Try capitalizing first letter e.g Calendar
			final String upperClass = StringUtils.capitalizeFirstLetter(s);
			clazz = map.get(upperClass);

			if (clazz == null){
				// Try lower e.g. calendar
				final String lowerClass = StringUtils.lowerFirstLetter(s);

				clazz = map.get(lowerClass);

				// Last resort try plural 
				
				if (clazz == null && s.endsWith("s")){
					int len = s.length() -1;
					clazz = map.get(lowerClass.substring(0, len));
					
					if (clazz == null){
						clazz = map.get(upperClass.substring(0, len));
					}
					
					if (clazz != null){
						// Default collection
						clazz = ArrayList.class;
					}
				}
			}
		}
		
		
		if (clazz == null){
			throw new IllegalArgumentException("Unknown element type:" + elementName);
		}
		return clazz;
	}
	/**
	 * Register.
	 *
	 * @param elementName the element name
	 * @param clazz the clazz
	 */
	public void register(String elementName, Class<?> clazz){
		map.put(elementName, clazz);
	}
	
	/**
	 * Register.
	 *
	 * @param clazzs the clazzs
	 */
	public void register(Class<?>...clazzs){
		for (Class<?> c:clazzs){
			map.put(c.getSimpleName(), c);
		}
	}
	

	/**
	 * Checks if is use default naming convention.
	 *
	 * @return true, if is use default naming convention
	 */
	public boolean isUseDefaultNamingConvention() {
		return useDefaultNamingConvention;
	}

	/**
	 * Sets the use default naming convention.
	 *
	 * @param useDefaultNamingConvention the new use default naming convention
	 */
	public void setUseDefaultNamingConvention(boolean useDefaultNamingConvention) {
		this.useDefaultNamingConvention = useDefaultNamingConvention;
	}

	/**
	 * Gets the map.
	 *
	 * @return the map
	 */
	public HashMap<String, Class<?>> getMap() {
		return map;
	}

	/**
	 * Sets the map.
	 *
	 * @param map the map
	 */
	public void setMap(HashMap<String, Class<?>> map) {
		this.map = map;
	}
	
	/**
	 * Gets the registered classes.
	 *
	 * @return the registered classes
	 */
	public List<Class<?>> getRegisteredClasses(){
		List<Class<?>> list = new ArrayList<Class<?>>();
		
		list.addAll(map.values());
		
		return list;
	}
	
	/**
	 * Sets the registered classes.
	 *
	 * @param classes the new registered classes
	 */
	public void setRegisteredClasses(List<Class<?>> classes){
		for (Class<?> c:classes){
			map.put(c.getSimpleName(), c);
		}
	}
}
