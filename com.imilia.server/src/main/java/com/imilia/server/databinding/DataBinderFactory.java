/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 *
 * Created on: May 6, 2009
 * Created by: emcgreal
 */

package com.imilia.server.databinding;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.joda.time.DateMidnight;
import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.PropertyEditorRegistry;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.context.MessageSource;
import org.springframework.validation.DataBinder;

import com.imilia.server.databinding.propertyeditors.BooleanPropertyEditor;
import com.imilia.server.databinding.propertyeditors.CustomCalendarEditorImilia;
import com.imilia.server.databinding.propertyeditors.CustomDateEditorImilia;
import com.imilia.server.databinding.propertyeditors.CustomNumberEditorImilia;
import com.imilia.server.databinding.propertyeditors.JodaDateMidnightPropertyEditor;
import com.imilia.server.databinding.propertyeditors.JodaDateTimePropertyEditor;
import com.imilia.server.databinding.propertyeditors.JodaIntervalPropertyEditor;
import com.imilia.server.databinding.propertyeditors.LocalizedPropertyEditor;
import com.imilia.server.databinding.propertyeditors.LocalizedPropertyPropertyEditor;
import com.imilia.utils.i18n.DomainLocale;
import com.imilia.utils.i18n.Localized;
import com.imilia.utils.i18n.LocalizedProperty;

/**
 * A factory for creating DataBinder objects.
 */
public class DataBinderFactory {

	/**
	 * Gets the data binder.
	 * 
	 * @param model the model
	 * @param modelName the model name
	 * @param locale the locale
	 * 
	 * @return the data binder
	 */
	static public DataBinder getDataBinder(Object model, String modelName, Locale locale) {
		DataBinder dataBinder = new DataBinder(model, modelName);
		
		// This needs to be set since Spring 3.0.0
		// See thread http://forum.springsource.org/showthread.php?t=83354
		dataBinder.setAutoGrowNestedPaths(false);
		
		registerDefaultEditors(dataBinder, locale);

		return dataBinder;
	}

	/**
	 * Register default editors.
	 * 
	 * @param propertyEditorRegistry the property editor registry
	 * @param locale the locale
	 */
	public static void registerDefaultEditors(PropertyEditorRegistry propertyEditorRegistry,
			Locale locale) {
		registerCalendar(propertyEditorRegistry, locale);
		registerDate(propertyEditorRegistry, locale);
		registerTimestamp(propertyEditorRegistry, locale);
		registerFloat(propertyEditorRegistry, locale);
		registerInteger(propertyEditorRegistry, locale);
		registerDouble(propertyEditorRegistry, locale);
		registerJodaDateTime(propertyEditorRegistry, locale);
		registerJodaDateMidnight(propertyEditorRegistry, locale);
		registerJodaInterval(propertyEditorRegistry, locale);
	}

	/**
	 * Register calendar.
	 * 
	 * @param propertyEditorRegistry the property editor registry
	 * @param locale the locale
	 */
	public static void registerCalendar(PropertyEditorRegistry propertyEditorRegistry, Locale locale) {
		// Calendar
		DateFormat dfC = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM,
				locale);
		// nur reales Datum wird genommen (also z.B. kein 32.13.2007)
		dfC.setLenient(false);
		propertyEditorRegistry.registerCustomEditor(Calendar.class, new CustomCalendarEditorImilia(
				dfC, true));
	}

	/**
	 * Register date.
	 * 
	 * @param propertyEditorRegistry the property editor registry
	 * @param locale the locale
	 */
	public static void registerDate(PropertyEditorRegistry propertyEditorRegistry, Locale locale) {
		// Date
		DateFormat df = DateFormat.getDateInstance(DateFormat.MEDIUM, locale);
		df.setLenient(false);
		propertyEditorRegistry.registerCustomEditor(Date.class,
				new CustomDateEditorImilia(df, true));
	}

	/**
	 * Register date.
	 * 
	 * @param propertyEditorRegistry the property editor registry
	 * @param locale the locale
	 * @param propertyName the property name
	 */
	public static void registerDate(PropertyEditorRegistry propertyEditorRegistry, Locale locale,
			String propertyName) {
		// Date
		DateFormat df = DateFormat.getDateInstance(DateFormat.MEDIUM, locale);
		df.setLenient(false);
		propertyEditorRegistry.registerCustomEditor(Date.class, propertyName,
				new CustomDateEditorImilia(df, true));
	}

	/**
	 * Register date time.
	 * 
	 * @param propertyEditorRegistry the property editor registry
	 * @param locale the locale
	 * @param propertyName the property name
	 */
	public static void registerDateTime(PropertyEditorRegistry propertyEditorRegistry,
			Locale locale, String propertyName) {
		// Date
		DateFormat df = DateFormat
				.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM, locale);
		df.setLenient(false);
		propertyEditorRegistry.registerCustomEditor(Date.class, propertyName,
				new CustomDateEditorImilia(df, true));
	}

	/**
	 * Register time.
	 * 
	 * @param propertyEditorRegistry the property editor registry
	 * @param locale the locale
	 * @param propertyName the property name
	 */
	public static void registerTime(PropertyEditorRegistry propertyEditorRegistry, Locale locale,
			String propertyName) {
		// Date
		DateFormat df = DateFormat.getTimeInstance(DateFormat.MEDIUM, locale);
		df.setLenient(false);
		propertyEditorRegistry.registerCustomEditor(Date.class, propertyName,
				new CustomDateEditorImilia(df, true));
	}

	/**
	 * Register date time.
	 * 
	 * @param propertyEditorRegistry the property editor registry
	 * @param locale the locale
	 * @param propertyName the property name
	 * @param pattern the pattern
	 */
	public static void registerDateTime(PropertyEditorRegistry propertyEditorRegistry,
			Locale locale, String propertyName, String pattern) {
		// Date
		DateFormat df = new SimpleDateFormat(pattern, locale);
		df.setLenient(false);
		propertyEditorRegistry.registerCustomEditor(Date.class, propertyName,
				new CustomDateEditorImilia(df, true));
	}

	/**
	 * Register timestamp.
	 * 
	 * @param propertyEditorRegistry the property editor registry
	 * @param locale the locale
	 */
	public static void registerTimestamp(PropertyEditorRegistry propertyEditorRegistry,
			Locale locale) {
		// Timestamp -
		DateFormat dtf = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM,
				locale);
		dtf.setLenient(false);
		propertyEditorRegistry.registerCustomEditor(Timestamp.class, new CustomDateEditorImilia(
				dtf, true));
	}
	
	/**
	 * Register custom number.
	 * 
	 * @param propertyEditorRegistry the property editor registry
	 * @param customNumberEditorImilia the custom number editor imilia
	 * @param clazz the clazz
	 */
	public static void registerCustomNumber(PropertyEditorRegistry propertyEditorRegistry, CustomNumberEditorImilia customNumberEditorImilia, Class<? extends Number> clazz) {
		propertyEditorRegistry.registerCustomEditor(clazz, customNumberEditorImilia);
	}
	
	/**
	 * Register custom number.
	 * 
	 * @param propertyEditorRegistry the property editor registry
	 * @param property the property
	 * @param customNumberEditorImilia the custom number editor imilia
	 * @param clazz the clazz
	 */
	public static void registerCustomNumber(PropertyEditorRegistry propertyEditorRegistry, String property, CustomNumberEditorImilia customNumberEditorImilia, Class<? extends Number> clazz) {
		propertyEditorRegistry.registerCustomEditor(clazz, property, customNumberEditorImilia);
	}

	/**
	 * Register float.
	 * 
	 * @param propertyEditorRegistry the property editor registry
	 * @param locale the locale
	 */
	public static void registerFloat(PropertyEditorRegistry propertyEditorRegistry, Locale locale) {
		// Float
		propertyEditorRegistry.registerCustomEditor(float.class, new CustomNumberEditorImilia(
				Float.class, NumberFormat.getInstance(locale), true));
		propertyEditorRegistry.registerCustomEditor(Float.class, new CustomNumberEditorImilia(
				Float.class, NumberFormat.getInstance(locale), true));
	}

	/**
	 * Register double.
	 * 
	 * @param propertyEditorRegistry the property editor registry
	 * @param locale the locale
	 */
	public static void registerDouble(PropertyEditorRegistry propertyEditorRegistry, Locale locale) {
		// Double - with two decimal places as standard
		NumberFormat f = NumberFormat.getInstance(locale);
		if (f instanceof DecimalFormat) {
			((DecimalFormat) f).applyPattern("#,##0.00");
		}
		propertyEditorRegistry.registerCustomEditor(double.class, new CustomNumberEditorImilia(
				Double.class, f, true));
		propertyEditorRegistry.registerCustomEditor(Double.class, new CustomNumberEditorImilia(
				Double.class, f, true));
	}

	/**
	 * Register integer.
	 * 
	 * @param propertyEditorRegistry the property editor registry
	 * @param locale the locale
	 */
	public static void registerInteger(PropertyEditorRegistry propertyEditorRegistry, Locale locale) {
		// Integer
		propertyEditorRegistry.registerCustomEditor(int.class, new CustomNumberEditorImilia(
				Integer.class, true));
		propertyEditorRegistry.registerCustomEditor(Integer.class, new CustomNumberEditorImilia(
				Integer.class, true));

		// String
		propertyEditorRegistry.registerCustomEditor(String.class, new StringTrimmerEditor(true));
	}

	/**
	 * Register joda date time.
	 * 
	 * @param propertyEditorRegistry the property editor registry
	 * @param locale the locale
	 */
	public static void registerJodaDateTime(PropertyEditorRegistry propertyEditorRegistry,
			Locale locale) {
		propertyEditorRegistry.registerCustomEditor(DateTime.class, new JodaDateTimePropertyEditor(
				DateTimeFormat.shortDateTime().withLocale(locale), true));
	}

	/**
	 * Register joda date time.
	 * 
	 * @param propertyEditorRegistry the property editor registry
	 * @param property the property
	 * @param dateTimeFormatter the date time formatter
	 */
	public static void registerJodaDateTime(PropertyEditorRegistry propertyEditorRegistry,
			 String property, DateTimeFormatter dateTimeFormatter) {
		propertyEditorRegistry.registerCustomEditor(DateTime.class, property, 
				new JodaDateTimePropertyEditor(
				dateTimeFormatter, true));
	}

	 
	/**
	 * Register joda date time.
	 * 
	 * @param propertyEditorRegistry the property editor registry
	 * @param dateTimeFormatter the date time formatter
	 */
	public static void registerJodaDateTime(PropertyEditorRegistry propertyEditorRegistry, DateTimeFormatter dateTimeFormatter) {
		propertyEditorRegistry.registerCustomEditor(DateTime.class, new JodaDateTimePropertyEditor(
				dateTimeFormatter, true));
	}
	
	/**
	 * Register joda interval.
	 * 
	 * @param propertyEditorRegistry the property editor registry
	 * @param locale the locale
	 */
	public static void registerJodaInterval(PropertyEditorRegistry propertyEditorRegistry,
			Locale locale) {
		propertyEditorRegistry.registerCustomEditor(Interval.class, 
				new JodaIntervalPropertyEditor(
					DateTimeFormat.mediumDateTime().withLocale(locale), 
					DateTimeFormat.mediumDateTime().withLocale(locale), 
					"-",
					true));
	}
	
	/**
	 * Register joda interval.
	 * 
	 * @param propertyEditorRegistry the property editor registry
	 * @param locale the locale
	 * @param propertyName the property name
	 * @param beginFormat the begin format
	 * @param endFormat the end format
	 * @param delimiter the delimiter
	 */
	public static void registerJodaInterval(PropertyEditorRegistry propertyEditorRegistry,
			Locale locale, 
			String propertyName,
			String beginFormat,
			String endFormat,
			String delimiter) {
		
		propertyEditorRegistry.registerCustomEditor(Interval.class,
				propertyName,
				new JodaIntervalPropertyEditor(
					DateTimeFormat.forPattern(beginFormat).withLocale(locale), 
					DateTimeFormat.forPattern(endFormat).withLocale(locale), 
					delimiter,
					true));
	}
	
	/**
	 * Register joda date midnight.
	 * 
	 * @param propertyEditorRegistry the property editor registry
	 * @param locale the locale
	 */
	public static void registerJodaDateMidnight(PropertyEditorRegistry propertyEditorRegistry,
			Locale locale) {
		propertyEditorRegistry.registerCustomEditor(DateMidnight.class,
				new JodaDateMidnightPropertyEditor(DateTimeFormat.shortDate().withLocale(locale), true));
	}
	
	

	/**
	 * Register localeized.
	 * 
	 * @param propertyEditorRegistry the property editor registry
	 * @param messageSource the message source
	 * @param locale the locale
	 */
	public static void registerLocaleized(PropertyEditorRegistry propertyEditorRegistry,
			MessageSource messageSource, Locale locale) {
		propertyEditorRegistry.registerCustomEditor(Localized.class, new LocalizedPropertyEditor(
				messageSource, locale));
	}

	/**
	 * Register localeized property.
	 * 
	 * @param propertyEditorRegistry the property editor registry
	 * @param domainLocale the domain locale
	 */
	public static void registerLocaleizedProperty(PropertyEditorRegistry propertyEditorRegistry,
		 DomainLocale domainLocale) {
		propertyEditorRegistry.registerCustomEditor(LocalizedProperty.class,
				new LocalizedPropertyPropertyEditor(domainLocale));
	}
	
	public static void registerBooleanProperty(PropertyEditorRegistry propertyEditorRegistry,
			MessageSource messageSource, Locale locale) {
		propertyEditorRegistry.registerCustomEditor(Boolean.class,
				new BooleanPropertyEditor(messageSource, locale));
		propertyEditorRegistry.registerCustomEditor(boolean.class,
				new BooleanPropertyEditor(messageSource, locale));
	}

}
