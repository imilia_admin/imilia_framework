package com.imilia.server.databinding.propertyeditors;

import java.beans.PropertyEditorSupport;

import org.joda.time.DateMidnight;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.util.StringUtils;

import com.imilia.datetime.JodaUtils;


public class JodaDateMidnightPropertyEditor extends PropertyEditorSupport {
	// ------------------------------ FIELDS ------------------------------
	private final DateTimeFormatter dateFormat;
	private final boolean allowEmpty;
	// --------------------------- CONSTRUCTORS ---------------------------

	/**
	 * Create a new DateTimeEditor instance, using the given format for
	 * parsing and rendering.
	 * <p/>
	 * The "allowEmpty" parameter states if an empty String should be allowed
	 * for parsing, i.e. get interpreted as null value. Otherwise, an
	 * IllegalArgumentException gets thrown.
	 *
	 * @param dateFormat DateFormat to use for parsing and rendering
	 * @param allowEmpty if empty strings should be allowed
	 */
	public JodaDateMidnightPropertyEditor( DateTimeFormatter dateFormat, boolean allowEmpty ) {
		this.dateFormat = dateFormat;
		this.allowEmpty = allowEmpty;
	}

	// ------------------------ OVERRIDING METHODS ------------------------

	public String getAsText() {
		if(getValue() == null) return ""; //return null if the property is null
		DateMidnight value = (( DateMidnight ) getValue()); //if the property is not null, read it as a DateMidnight
		return value != null ? dateFormat.print(value) : "";
	}

	public void setAsText( String text ) throws IllegalArgumentException {
		if ( allowEmpty && !StringUtils.hasText( text ) ) {
			// Treat empty String as null value.
			setValue( null );
		} else {
			setValue(parse(text));
		}
	}

	public DateMidnight parse(String s){
		try{
			DateTime val = dateFormat.parseDateTime(JodaUtils.fixDateMidnight(s));

			return val.toDateMidnight();
		}catch (IllegalArgumentException ex) {
			IllegalArgumentException iae =
					new IllegalArgumentException("Could not parse date: " + ex.getMessage());
			iae.initCause(ex);
			throw iae;
		}
	}
}
