/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 *
 * Created on: Sep 22, 2009
 * Created by: emcgreal
 */
package com.imilia.server.databinding.propertyeditors;

import java.beans.PropertyEditorSupport;

import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.util.StringUtils;

/**
 * The Class JodaIntervalPropertyEditor.
 */
public class JodaIntervalPropertyEditor extends PropertyEditorSupport {
	// ------------------------------ FIELDS ------------------------------
	/** The date format begin. */
	private final DateTimeFormatter dateFormatBegin;
	
	/** The date format end. */
	private final DateTimeFormatter dateFormatEnd;
	
	/** The delimiter. */
	private final String delimiter;
	
	/** The allow empty. */
	private final boolean allowEmpty;

	// --------------------------- CONSTRUCTORS ---------------------------

	/**
	 * Create a new DateTimeEditor instance, using the given format for
	 * parsing and rendering.
	 * <p/>
	 * The "allowEmpty" parameter states if an empty String should be allowed
	 * for parsing, i.e. get interpreted as null value. Otherwise, an
	 * IllegalArgumentException gets thrown.
	 * 
	 * @param allowEmpty if empty strings should be allowed
	 * @param dateFormatBegin the date format begin
	 * @param dateFormatEnd the date format end
	 * @param delimiter the delimiter
	 */
	public JodaIntervalPropertyEditor( 
			DateTimeFormatter dateFormatBegin,
			DateTimeFormatter dateFormatEnd,
			String delimiter,
			boolean allowEmpty ) {
		this.dateFormatBegin = dateFormatBegin;
		this.dateFormatEnd = dateFormatEnd;
		this.delimiter = delimiter;
		this.allowEmpty = allowEmpty;
	}

	// ------------------------ OVERRIDING METHODS ------------------------

	/* (non-Javadoc)
	 * @see java.beans.PropertyEditorSupport#getAsText()
	 */
	public String getAsText() {
		final StringBuffer buffer = new StringBuffer();
		Interval value = (Interval) getValue(); 
		
		if (value != null){
			buffer.append(dateFormatBegin.print(value.getStart()));
			buffer.append(delimiter);
			buffer.append(dateFormatEnd.print(value.getEnd()));
		}
		
		return buffer.toString();
	}

	/* (non-Javadoc)
	 * @see java.beans.PropertyEditorSupport#setAsText(java.lang.String)
	 */
	public void setAsText( String text ) throws IllegalArgumentException {
		if ( allowEmpty && !StringUtils.hasText( text ) ) {
			// Treat empty String as null value.
			setValue( null );
		} else {
			String[] split = text.split(delimiter);
			
			DateTime lb = dateFormatBegin.parseDateTime(split[0]); 
			DateTime ub = dateFormatBegin.parseDateTime(split[1]); 
			setValue( new Interval(lb, ub) );
		}
	}
}
