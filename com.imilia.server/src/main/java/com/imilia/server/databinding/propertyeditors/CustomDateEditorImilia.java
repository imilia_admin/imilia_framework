/**
 * Imilia Interactive Mobile Applications  GmbH.
 * Copyright (c) 2008
 * @author emcgreal
 * @created 14.02.2008
 * Last modified by:   $Author:$
 * Last modfication:   $Date:$
 * Version:            $Revision:$
 */

package com.imilia.server.databinding.propertyeditors;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.springframework.beans.propertyeditors.CustomDateEditor;

/**
 * Erweitert den CustomDateEditor um die Möglichkeit
 * das Datum auf ein Intervall hin zu überprüfen.
 * Dies ist wichtig, da der SQL Server nur ein
 * eingeschränktes Zeitintervall verwalten kann.
 * 
 * @author thil
 */
public class CustomDateEditorImilia extends CustomDateEditor {
	
	/**
	 * Minimales zu verarbeitendes Datum
	 */
	private static Date dateMin;
	
	/**
	 * Maximales zu verarbeitendes Datum
	 */
	private static Date dateMax;
	
	/**
	 * Statischer Konstruktor
	 */
	{
		Calendar cMin = new GregorianCalendar();
		cMin.set(1800, 0, 1);
		dateMin = cMin.getTime();
		
		Calendar cMax = new GregorianCalendar();
		cMax.set(9998, 0, 1);
		dateMax = cMax.getTime();
	}
	
	/**
	 * CTOR
	 * 
	 * @param dateFormat
	 * @param allowEmpty
	 */
	public CustomDateEditorImilia(DateFormat dateFormat, boolean allowEmpty) {
		super(dateFormat,allowEmpty);
	}

	/**
	 * CTOR
	 * 
	 * @param dateFormat
	 * @param allowEmpty
	 * @param exactDateLength
	 */
	public CustomDateEditorImilia(DateFormat dateFormat, boolean allowEmpty, int exactDateLength) {
		super(dateFormat,allowEmpty, exactDateLength);
	}
	
	/**
	 * Nach der eigentlichen Verarbeitung wird der Zeitintervall überprüft.
	 */
	public void setAsText(String text) throws IllegalArgumentException {
		
		super.setAsText(text);
		Object o = getValue();
		
		if (o instanceof Date) {
			Date d = (Date)o;
			if (d.before(dateMin) || d.after(dateMax)) {
				throw new IllegalArgumentException("Date out of bounds.");
			}
		}
	}
}
