/**
 * Imilia Interactive Mobile Applications  GmbH.
 * Copyright (c) 2009
 * @author emcgreal
 * @created 22.06.2009
 */

package com.imilia.server.databinding.propertyeditors;

import java.beans.PropertyEditorSupport;
import java.text.NumberFormat;

import org.springframework.util.NumberUtils;
import org.springframework.util.StringUtils;

/**
 * The Class CustomNumberEditorImilia.
 */
public class CustomNumberEditorImilia extends PropertyEditorSupport {

	/** The number class. */
	private final Class<? extends Number> numberClass;

	/** The number format. */
	private final NumberFormat numberFormat;

	/** The allow empty. */
	private final boolean allowEmpty;

	/**
	 * Instantiates a new custom number editor imilia.
	 *
	 * @param numberClass the number class
	 * @param numberFormat the number format
	 * @param allowEmpty the allow empty
	 *
	 * @throws IllegalArgumentException the illegal argument exception
	 */
	public CustomNumberEditorImilia(Class<? extends Number> numberClass, NumberFormat numberFormat,
			boolean allowEmpty) throws IllegalArgumentException {
		this.numberClass = numberClass;
		this.numberFormat = numberFormat;
		this.allowEmpty = allowEmpty;

		if (numberFormat == null){
			throw new IllegalArgumentException("NumberFormat must be supplied");
		}
	}

	/**
	 * Instantiates a new custom number editor imilia.
	 *
	 * @param numberClass the number class
	 * @param allowEmpty the allow empty
	 *
	 * @throws IllegalArgumentException the illegal argument exception
	 */
	public CustomNumberEditorImilia(Class<? extends Number> numberClass, boolean allowEmpty)
			throws IllegalArgumentException {
		this.numberClass = numberClass;
		this.numberFormat = null;
		this.allowEmpty = allowEmpty;
	}

	/* (non-Javadoc)
	 * @see java.beans.PropertyEditorSupport#setAsText(java.lang.String)
	 */
	@Override
	public void setAsText(String text) throws IllegalArgumentException {
		if (this.allowEmpty && !StringUtils.hasText(text)) {
			// Treat empty String as value 0.00 (not null)
			if (this.numberClass.equals(Double.class)) {
				setValue(0.00);
			} else if (this.numberClass.equals(Float.class)) {
				setValue(0F);
			} else {
				setValue(0);
			}

		} else if (this.numberFormat != null) {
			// Use given NumberFormat for parsing text.
			setValue(NumberUtils.parseNumber(text, this.numberClass,
					this.numberFormat));
		} else {
			// Use default valueOf methods for parsing text.
			setValue(NumberUtils.parseNumber(text, this.numberClass));
		}
	}

	/* (non-Javadoc)
	 * @see java.beans.PropertyEditorSupport#getAsText()
	 */
	@Override
	public String getAsText() {
		Object value = getValue();

		// If null then use 0.0 as the value and format it
		if (value == null) {
			if (this.numberFormat != null) {
				return this.numberFormat.format(0.0);
			} else {
				return "0";
			}
			
		}

		if (this.numberFormat != null) {
			// Use NumberFormat for rendering value.
			return this.numberFormat.format(value);
		} else {
			// Use toString method for rendering value.
			return value.toString();
		}
	}
}
