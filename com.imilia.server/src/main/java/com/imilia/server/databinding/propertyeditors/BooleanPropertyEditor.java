/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 *
 * Created on: 29.07.2009
 * Created by: emcgreal
 */
package com.imilia.server.databinding.propertyeditors;

import java.util.Locale;

import org.springframework.context.MessageSource;

/**
 * The Class BooleanPropertyEditor.
 */
public class BooleanPropertyEditor extends LocalizedPropertyEditor {

	final String sTrue;
	final String sFalse;

	/** The clazz. */
	/**
	 * Instantiates a new localized enum property editor.
	 * 
	 * @param messageSource the message source
	 * @param locale the locale
	 * @param clazz the clazz
	 */
	public BooleanPropertyEditor(MessageSource messageSource, Locale locale) {
		super(messageSource, locale);
		sTrue = getMessageSource().getMessage("Generic.true",null, getLocale());
		sFalse = getMessageSource().getMessage("Generic.false",null, getLocale());
	}

	/* (non-Javadoc)
	 * @see com.imilia.server.databinding.propertyeditors.LocalizedPropertyEditor#setAsText(java.lang.String)
	 */
	@Override
	public void setAsText(String text) throws IllegalArgumentException {
		
		if (sTrue.equals(text)){
			setValue(Boolean.TRUE);
		}else if (sFalse.equals(text)){
			setValue(Boolean.FALSE);
		}else{
			throw new IllegalArgumentException(text);
		}
	}
	
	public String getAsText() {
		Object value = getValue();

		if (value instanceof Boolean){
			if ((Boolean)value){
				return sTrue;
			}else{
				return sFalse;
			}
		}
		
		return null;
	}

}
