/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 *
 * Created on: 29.07.2009
 * Created by: emcgreal
 */
package com.imilia.server.databinding.propertyeditors;

import java.util.Locale;

import org.springframework.context.MessageSource;

import com.imilia.utils.i18n.Localized;

/**
 * The Class LocalizedEnumPropertyEditor.
 * 
 * <p>This is a generic localized enum property editor. It tries to find
 * a match between the text and the localized text for the enum. 
 * </p>
 */
public class LocalizedEnumPropertyEditor extends LocalizedPropertyEditor {

	/** The clazz. */
	private final Class<? extends Enum<?>> clazz;
	
	/**
	 * Instantiates a new localized enum property editor.
	 * 
	 * @param messageSource the message source
	 * @param locale the locale
	 * @param clazz the clazz
	 */
	public LocalizedEnumPropertyEditor(MessageSource messageSource, Locale locale, Class<? extends Enum<?>> clazz) {
		super(messageSource, locale);
		this.clazz = clazz;
	}

	/* (non-Javadoc)
	 * @see com.imilia.server.databinding.propertyeditors.LocalizedPropertyEditor#setAsText(java.lang.String)
	 */
	@Override
	public void setAsText(String text) throws IllegalArgumentException {
		if (text == null){
			setValue(null);
		}
		
		Enum<?>[] enums = clazz.getEnumConstants();
		
		for (Enum<?> e:enums){
			Localized localized = (Localized)e;
			if (text.equals(localized.getLocalizedMessage(getMessageSource(), getLocale()))){
				setValue(e);
			}
		}
	}

}
