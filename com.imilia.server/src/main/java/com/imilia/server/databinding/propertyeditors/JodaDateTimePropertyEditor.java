package com.imilia.server.databinding.propertyeditors;

import java.beans.PropertyEditorSupport;
import java.io.UnsupportedEncodingException;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.util.StringUtils;


public class JodaDateTimePropertyEditor extends PropertyEditorSupport {
	// ------------------------------ FIELDS ------------------------------
	private final DateTimeFormatter dateFormat;
	private final boolean allowEmpty;
	
	private final boolean urlDecode; 

	// --------------------------- CONSTRUCTORS ---------------------------


	/**
	 * Instantiates a new joda date time property editor.
	 *
	 * @param dateFormat the date format
	 */
	public JodaDateTimePropertyEditor( DateTimeFormatter dateFormat ) {
		this.dateFormat = dateFormat;
		this.allowEmpty = true;
		this.urlDecode = false;
	}
	
	/**
	 * Create a new DateTimeEditor instance, using the given format for
	 * parsing and rendering.
	 * <p/>
	 * The "allowEmpty" parameter states if an empty String should be allowed
	 * for parsing, i.e. get interpreted as null value. Otherwise, an
	 * IllegalArgumentException gets thrown.
	 * 
	 * @param dateFormat DateFormat to use for parsing and rendering
	 * @param allowEmpty if empty strings should be allowed
	 */
	public JodaDateTimePropertyEditor( DateTimeFormatter dateFormat, boolean allowEmpty ) {
		this.dateFormat = dateFormat;
		this.allowEmpty = allowEmpty;
		this.urlDecode = false;
	}
	
	
	/**
	 * Instantiates a new joda date time property editor.
	 *
	 * @param dateFormat the date format
	 * @param allowEmpty the allow empty
	 * @param urlDecode the url decode
	 */
	public JodaDateTimePropertyEditor( DateTimeFormatter dateFormat, boolean allowEmpty, boolean urlDecode ) {
		this.dateFormat = dateFormat;
		this.allowEmpty = allowEmpty;
		this.urlDecode = true;
	}

	// ------------------------ OVERRIDING METHODS ------------------------

	public String getAsText() {
		if (getValue() == null) 
			return ""; //return null if the property is null
		
		DateTime value = (DateTime) getValue(); //if the property is not null, read it as a DateTime
		return value != null ? dateFormat.print(value) : "";
	}

	public void setAsText( String text ) throws IllegalArgumentException {
		if ( allowEmpty && !StringUtils.hasText( text ) ) {
			// Treat empty String as null value.
			setValue( null );
		} else {
			try {
				DateTime val = parse( text );
				setValue(val);
			} catch (UnsupportedEncodingException e) {
				throw new IllegalArgumentException(e);
			} 
		}
	}
	
	public DateTime parse(String text) throws UnsupportedEncodingException{
		if (urlDecode){
			text = decode(text);
		}
		return dateFormat.parseDateTime(text);
	}

	private String decode(String text) throws UnsupportedEncodingException{
		// Can't use URLDecoder as it removes any + and replaces it with a blank
		return text.replace("%2B", "+");
		//return URLDecoder.decode(text, "UTF8");
	}
	
	public boolean isUrlDecode() {
		return urlDecode;
	}
}
