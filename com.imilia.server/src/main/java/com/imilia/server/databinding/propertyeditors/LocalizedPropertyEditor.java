/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 *
 * Created on: May 6, 2009
 * Created by: emcgreal
 */
package com.imilia.server.databinding.propertyeditors;

import java.beans.PropertyEditorSupport;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;

import com.imilia.utils.i18n.Localized;

/**
 * The Class LocalizedPropertyEditor.
 */
public class LocalizedPropertyEditor extends PropertyEditorSupport {

	/** Logger used to log warnings, errors and debug messages */
	private final static Logger logger = LoggerFactory
			.getLogger(LocalizedPropertyEditor.class);

	// ------------------------------ FIELDS ------------------------------
	/** The message source. */
	private MessageSource messageSource;

	/** The locale. */
	private Locale locale;

	// --------------------------- CONSTRUCTORS ---------------------------

	/**
	 * Instantiates a new localized property editor.
	 * 
	 * @param messageSource
	 *            the message source
	 * @param locale
	 *            the locale
	 */
	public LocalizedPropertyEditor(MessageSource messageSource, Locale locale) {
		this.messageSource = messageSource;
		this.locale = locale;
	}

	// ------------------------ OVERRIDING METHODS ------------------------

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.beans.PropertyEditorSupport#getAsText()
	 */
	public String getAsText() {
		if (getValue() == null)
			return ""; // return null if the property is null

		final Localized localized = (Localized) getValue();

		return localized != null ? 
			localized.getLocalizedMessage(messageSource,locale) : "";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.beans.PropertyEditorSupport#setAsText(java.lang.String)
	 */
	public void setAsText(String text) throws IllegalArgumentException {
		logger.error("Calling setAsText on a localized - shouldn't happen");
	}

	public MessageSource getMessageSource() {
		return messageSource;
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	public Locale getLocale() {
		return locale;
	}

	public void setLocale(Locale locale) {
		this.locale = locale;
	}
}
