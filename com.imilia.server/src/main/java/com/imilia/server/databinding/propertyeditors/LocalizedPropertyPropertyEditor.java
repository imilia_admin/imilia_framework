/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 *
 * Created on: May 6, 2009
 * Created by: emcgreal
 */
package com.imilia.server.databinding.propertyeditors;

import java.beans.PropertyEditorSupport;

import com.imilia.utils.i18n.DomainLocale;
import com.imilia.utils.i18n.LocalizedProperty;

/**
 * The Class LocalizedPropertyEditor.
 */
public class LocalizedPropertyPropertyEditor extends PropertyEditorSupport {

	// ------------------------------ FIELDS ------------------------------

	/** The locale. */
	private DomainLocale domainLocale;

	// --------------------------- CONSTRUCTORS ---------------------------

	/**
	 * Instantiates a new localized property editor.
	 *
	 * @param messageSource
	 *            the message source
	 * @param locale
	 *            the locale
	 */
	public LocalizedPropertyPropertyEditor(DomainLocale locale) {
		this.domainLocale = locale;
	}

	// ------------------------ OVERRIDING METHODS ------------------------

	/*
	 * (non-Javadoc)
	 *
	 * @see java.beans.PropertyEditorSupport#getAsText()
	 */
	public String getAsText() {
		if (getValue() == null)
			return ""; // return null if the property is null

		final LocalizedProperty localized = (LocalizedProperty) getValue();

		return localized != null ?
			localized.getText(domainLocale) : "";
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.beans.PropertyEditorSupport#setAsText(java.lang.String)
	 */
	public void setAsText(String text) throws IllegalArgumentException {
		final LocalizedProperty localized = (LocalizedProperty) getValue();
		localized.setText(domainLocale, text);
	}
}
