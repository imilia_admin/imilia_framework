/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 * Created on: Nov 9, 2009
 * Created by: emcgreal
 */
package com.imilia.server.service;

/**
 * The Interface ImageService.
 */
public interface ImageService extends Service {
	
	/**
	 * Creates the jpg thumnail.
	 * 
	 * @param rawImage the raw image
	 * @param width the width
	 * @param height the height
	 * 
	 * @return the byte[]
	 */
	public byte[] createJPGThumnail(byte[] rawImage, int width, int height);
}
