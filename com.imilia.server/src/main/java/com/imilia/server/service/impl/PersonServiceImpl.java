/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 * 
 * Created on: Mar 5, 2009
 * Created by: emcgreal
 */
package com.imilia.server.service.impl;

import java.util.Iterator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.imilia.server.domain.common.Address;
import com.imilia.server.domain.common.Person;
import com.imilia.server.service.AddressService;
import com.imilia.server.service.PersonService;
import com.imilia.server.validation.ValidationException;

/**
 * The Class PersonServiceImpl.
 */
public class PersonServiceImpl extends DomainObjectServiceImpl<Person> implements PersonService {
	
	/** The address service. */
	@Autowired
	private AddressService addressService;


	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public void save(Person person) throws ValidationException {
		validate(person);
		final Address address = person.getAddress();
		if (address != null){
			addressService.save(address);
		}
		super.save(person);
	}
	
	/**
	 * Gets the address service.
	 * 
	 * @return the address service
	 */
	public AddressService getAddressService() {
		return addressService;
	}

	/**
	 * Sets the address service.
	 * 
	 * @param addressService the new address service
	 */
	public void setAddressService(AddressService addressService) {
		this.addressService = addressService;
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public void save(Iterator<Person> people) {
		ebeanServer.save(people);
		
	}
}
