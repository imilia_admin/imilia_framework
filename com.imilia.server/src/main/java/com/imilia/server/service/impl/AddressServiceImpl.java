/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 * 
 * Created on: Mar 5, 2009
 * Created by: emcgreal
 */
package com.imilia.server.service.impl;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.imilia.server.domain.common.Address;
import com.imilia.server.domain.common.PhoneContact;
import com.imilia.server.service.AddressService;
import com.imilia.server.validation.ValidationException;

/**
 * The Class AddressServiceImpl.
 */
public class AddressServiceImpl extends DomainObjectServiceImpl<Address> implements AddressService {

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public void deletePhoneContact(PhoneContact phoneContact) {
		ebeanServer.delete(phoneContact);
		
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public void save(Address address) throws ValidationException {
		// First validate
		validate(address);
		for (PhoneContact phoneContact:address.getPhoneContacts()){
			validate(phoneContact);
		}
		
		PhoneContact phoneContact = address.getPrimaryPhoneContact();

		// Chicken and egg problem
		if (address.isNew()){
			address.setPrimaryPhoneContact(null);
			ebeanServer.save(address);
			
			// Add an empty default phone contact 
			if (phoneContact == null){
				phoneContact = new PhoneContact();
				// number is mandatory
				phoneContact.setNumber(" ");
			}
			address.setPrimaryPhoneContact(phoneContact);
		}
		
		ebeanServer.save(address);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public void save(PhoneContact phoneContact) throws ValidationException {
		validate(phoneContact);
		ebeanServer.save(phoneContact);
	}
}
