/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 * Created on: Jan 4, 2009
 * Created by: emcgreal
 */
package com.imilia.server.service;

import java.beans.PropertyChangeListener;
import java.util.List;

import com.imilia.server.domain.common.Person;
import com.imilia.server.domain.filter.Recipe;
import com.imilia.server.domain.filter.RecipeHolder;
import com.imilia.server.domain.filter.RecipeResult;
import com.imilia.utils.i18n.DomainLocale;
import com.imilia.server.validation.ValidationException;

/**
 * The Interface RecipeService.
 */
public interface RecipeService extends DomainObjectService<Recipe<?>> {
	
	/**
	 * Execute.
	 * 
	 * @param recipe the recipe
	 * @param domainLocale the domain locale
	 * @param recipeResult the recipe result
	 * 
	 * @throws ValidationException the validation exception
	 */
	public <T> void execute(Recipe<T> recipe, DomainLocale domainLocale, RecipeResult<T> recipeResult) throws ValidationException;

	/**
	 * Find.
	 * 
	 * @param l the l
	 * 
	 * @return the recipe<?>
	 */
	public Recipe<?> find(long l);
	
	
	/**
	 * Find.
	 * 
	 * @param name the name
	 * 
	 * @return the list< recipe>
	 */
	@SuppressWarnings("rawtypes")
	public List<Recipe> find(String name);
	
	/**
	 * Find.
	 * 
	 * @param owner the owner
	 * 
	 * @return the list< recipe holder>
	 */
	public List<RecipeHolder> findRecipeHolders(Person owner);
	
	/**
	 * Find.
	 * 
	 * @param owner the owner
	 * @param clazz the clazz
	 * 
	 * @return the list< recipe holder>
	 */
	public List<RecipeHolder> findRecipeHolders(Person owner, Class<?> clazz);
	
	/**
	 * Delete recipe holder.
	 * 
	 * @param recipeHolder the recipe holder
	 * 
	 * @throws ValidationException the validation exception
	 */
	public void deleteRecipeHolder(RecipeHolder recipeHolder) throws ValidationException;
	
	/**
	 * Save.
	 * 
	 * @param recipeHolder the recipe holder
	 * 
	 * @throws ValidationException the validation exception
	 */
	public void save(RecipeHolder recipeHolder) throws ValidationException;
	
	/**
	 * Refresh.
	 * 
	 * @param recipeHolder the recipe holder
	 */
	public void refresh(RecipeHolder recipeHolder);
	
	/**
	 * Adds the property change listener.
	 * 
	 * @param propertyChangeListener the property change listener
	 * @param recipeHolder the recipe holder
	 */
	public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener, RecipeHolder recipeHolder);
			
	/**
	 * Removes the property change listener.
	 * 
	 * @param propertyChangeListener the property change listener
	 * @param recipeHolder the recipe holder
	 */
	public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener, RecipeHolder recipeHolder);
	
	/**
	 * Gets the recipe holder service.
	 * 
	 * @return the recipe holder service
	 */
	public RecipeHolderService getRecipeHolderService();

	/**
	 * Sets the recipe holder service.
	 * 
	 * @param recipeHolderService the new recipe holder service
	 */
	public void setRecipeHolderService(RecipeHolderService recipeHolderService) ;

}
