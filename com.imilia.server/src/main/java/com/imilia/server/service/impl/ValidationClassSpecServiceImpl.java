package com.imilia.server.service.impl;

import com.imilia.server.domain.validation.ValidationClassSpec;
import com.imilia.server.service.ValidationClassSpecService;

public class ValidationClassSpecServiceImpl extends DomainObjectServiceImpl<ValidationClassSpec>
		implements ValidationClassSpecService {
}
