/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2010 - all rights reserved
 * Created on: May 19, 2010
 * Created by: emcgreal
 */
package com.imilia.server.service.sms;

/**
 * The Class SMSException.
 */
public class SMSException extends Exception {
	
	private final SMSReturnCode smsError;

	/**
	 * Instantiates a new SMS exception.
	 * 
	 * @param smsError the sms error
	 */
	public SMSException(SMSReturnCode smsError) {
		super();
		this.smsError = smsError;
	}
	
	/**
	 * Instantiates a new SMS exception.
	 * 
	 * @param smsError the sms error
	 * @param message the message
	 */
	public SMSException(SMSReturnCode smsError, String message) {
		super(message);
		this.smsError = smsError;
	}

	public SMSException(SMSReturnCode smsError, Throwable cause, String message) {
		super(message, cause);
		this.smsError = smsError;
	}
	
	
	public final SMSReturnCode getSmsError() {
		return smsError;
	}


}
