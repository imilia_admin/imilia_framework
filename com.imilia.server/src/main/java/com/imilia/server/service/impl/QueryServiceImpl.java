package com.imilia.server.service.impl;

import java.util.List;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.avaje.ebean.Query;
import com.imilia.server.domain.CrudAware;
import com.imilia.server.domain.query.QueryResult;
import com.imilia.server.domain.query.entities.QueryTemplate;
import com.imilia.server.domain.query.execution.QueryExecution;
import com.imilia.server.domain.query.execution.QueryCrudExecution;
import com.imilia.server.service.QueryService;
import com.imilia.server.service.QueryRestrictionService;

public class QueryServiceImpl extends EbeanAwareServiceImpl 
	implements QueryService {
	
	private final static Logger logger = Logger.getLogger(QueryServiceImpl.class); 

	@Autowired
	private QueryRestrictionService restrictionService;

	
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public QueryTemplate getQueryTemplate(String name) {
		return getEbeanServer().find(QueryTemplate.class).where().eq("name", name).findUnique();
	}

	
	@Override
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public <T extends CrudAware> void query(QueryExecution<T> queryExecution, QueryResult<T> result) {
		result.clear();
		result.setLimitExceeded(false);
		
		
		List<QueryCrudExecution<T>> queries = queryExecution.getCrudQueries();
		
		int resultCountLimit = queryExecution.getQuery().getResultCountLimit();
		
		for (QueryCrudExecution<T> query:queries){
		
			final String jql = query.toJQL();
			
			
			
			if (logger.isDebugEnabled()){
				logger.debug("Executing query: " + queryExecution.getQuery().getName() + " JQL: " + jql + " Params:" + query.getParameters());
			}
			
			@SuppressWarnings("unchecked")
			final Query<T> q = (Query<T>) getEbeanServer().createQuery(
					queryExecution.getQuery().getClazz(), jql);
			
			if (!query.getParameters().isEmpty()){
				for (String p:query.getParameters().keySet()){
					q.setParameter(p, query.getParameters().get(p));
				}
			}
			if (resultCountLimit > 0){
				q.setMaxRows(resultCountLimit +1);
			}
			
			final List<T> list = q.findList();
			
			for (T t:list){
				t.setCrud(query.getCrud());
			}
			result.addAll(list);
			
			final int size = result.size();
			if (result.size() > resultCountLimit) {
				// Remove the last one
				result.remove(size - 1);
				
				result.setLimitExceeded(true);
				break;
			}
			
			resultCountLimit -= size;
			
			if (resultCountLimit <= 0){
				break;
			}
		}
		
		result.fireEvent();
	}

	public QueryRestrictionService getRestrictionService() {
		return restrictionService;
	}

	public void setRestrictionService(QueryRestrictionService restrictionService) {
		this.restrictionService = restrictionService;
	}

	
	
}
