/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2010 - all rights reserved
 * Created on: May 19, 2010
 * Created by: emcgreal
 */
package com.imilia.server.service.sms;

/**
 * The Enum SMSError.
 */
public enum SMSReturnCode {
	
	Succeeded,
	
	/** The Unknown provider. */
	UnknownProvider,
	
	/** The Bad credentials. */
	BadCredentials,
	
	/** The Invalid ip. */
	InvalidIP,
	
	/** The No credit main account. */
	NoCreditMainAccount,
	
	/** The No credit sub account. */
	NoCreditSubAccount,
	
	/** The Rejected. */
	Rejected,
	
	/** The Gateway unavailable. */
	GatewayUnavailable,
	
	/** The SMS was repeated too often. */
	RepeatedTooOften,
	
	/** The SMS is lacking pricing info. */
	LackingPricingInfo,
	
	UnknownError
}
