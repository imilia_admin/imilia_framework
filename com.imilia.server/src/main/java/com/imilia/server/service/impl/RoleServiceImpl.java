/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2014 - all rights reserved
 * Created on: Feb 22, 2014
 * Created by: emcgreal
 */
package com.imilia.server.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.csvreader.CsvReader;
import com.imilia.server.domain.Crud;
import com.imilia.server.domain.access.CrudTarget;
import com.imilia.server.domain.access.ModelCrud;
import com.imilia.server.domain.access.Role;
import com.imilia.server.domain.i18n.LocaleTextHolder;
import com.imilia.server.domain.model.annotations.Model;
import com.imilia.server.service.RoleService;
import com.imilia.utils.i18n.DomainLocale;
import com.imilia.utils.strings.StringUtils;

/**
 * The Class RoleServiceImpl.
 */
public class RoleServiceImpl extends DomainObjectServiceImpl<Role> implements
		RoleService {

	private final static int ROLE_IDX = 0;
	private final static int TARGET_IDX = 1;
	private final static int CRUD_IDX = 2;
	
	/** The Constant logger. */
	private final static Logger logger = LoggerFactory.getLogger(RoleServiceImpl.class);

	/* (non-Javadoc)
	 * @see com.imilia.server.service.RoleService#importRoles(com.csvreader.CsvReader, com.imilia.server.domain.i18n.DomainLocale)
	 */
	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<Role> importRoles(CsvReader reader,DomainLocale domainLocale) throws Exception {
		HashMap<String, Role> roles = new HashMap<String, Role>();
		
		final List<String[]> rows = new ArrayList<>();
		
		
		if (reader.readHeaders()){
			while(reader.readRecord()){
				rows.add(reader.getValues());
			}
		}
				
		Iterator<String[]> iter = rows.iterator();
		
		int rowIndex = 0;
		
		//---------------------------------------------------------------------
		// Create the roles
		while (iter.hasNext()){
			String[] row = iter.next();
			String role = row[ROLE_IDX];
			
			if ("*".equals(row[TARGET_IDX])){
				Role r = roles.get(role);
				if (r == null){
					r = new Role(role, 
							LocaleTextHolder.create(domainLocale, StringUtils.capitalizeFirstLetter(role)),
							new Crud(Long.parseLong(row[CRUD_IDX])));
					
					roles.put(role, r);
				}else{
					logger.error("Duplicate role definition. Row[" + rowIndex +"]. " + row);
				}
			}
			rowIndex++;
		}
		
		//---------------------------------------------------------------------
		// Create the model permissions
		iter = rows.iterator();
		rowIndex = 0;
		while (iter.hasNext()){
			final String[] row = iter.next();
			
			final String roleName = row[ROLE_IDX];
			final String target = row[TARGET_IDX];
			
			
			// Something like x.y.*
			if (!"*".equals(target) && target.endsWith("*")){
				Role role = roles.get(roleName);
				if (role == null){
					logger.error("Missing definition of role {" + roleName + "} Row[" + rowIndex +"]. " + row);
					continue;
				}else{
					String modelClassName = target.substring(0, target.length()-2);
					
					Class<?> clazz = Class.forName(modelClassName);
					
					Model model = clazz.getAnnotation(Model.class);
					
					if (model == null){
						logger.error("Class: " + modelClassName + " has no @Model annotation");
						continue;
					}else{
						ModelCrud mp = role.getModelCrud(modelClassName);
						
						if (mp == null){
							mp = new ModelCrud(role, modelClassName, new Crud(Long.parseLong(row[CRUD_IDX])));
						}
					}
				}
			}
			rowIndex++;
		}
		
		//---------------------------------------------------------------------
		// Create the target permissions
		iter = rows.iterator();
		rowIndex = 0;
		while (iter.hasNext()){
			final String[] row = iter.next();
			
			final String roleName = row[ROLE_IDX];
			final String target = row[TARGET_IDX];
			
			// not * or x.y.z.*
			if (!"*".equals(target) && !target.endsWith("*")){
				Role role = roles.get(roleName);
				if (role == null){
					logger.error("Missing definition of role {" + roleName + "} Row[" + rowIndex +"]. " + row);
					continue;
				}else{
					String modelClassName = target.substring(0, target.lastIndexOf('.'));
					
					Class<?> clazz = Class.forName(modelClassName);
					
					Model model = clazz.getAnnotation(Model.class);
					
					if (model == null){
						logger.error("Class: " + modelClassName + " has no @Model annotation");
						continue;
					}else{
						ModelCrud mp = role.getModelCrud(modelClassName);
						
						if (mp == null){
							mp = new ModelCrud(role, modelClassName, role.getCrud());
						}
						
						new CrudTarget(mp, target.substring(target.lastIndexOf('.')+1), new Crud(Long.parseLong(row[CRUD_IDX])));
					}
				}
			}
			rowIndex++;
		}
			
		getEbeanServer().save(roles.values());

		return new ArrayList<Role>(roles.values());
	}

	@Override
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<Role> loadRoles() {
		return getEbeanServer().
				find(Role.class)
				.fetch("name")
				.fetch("modelPermissions")
				.fetch("modelPermissions.permissionTargets")
				.findList();
	}

	@Override
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<Role> findRoles(String... authorities) {
		return getEbeanServer().find(Role.class).where().in("authority", Arrays.asList(authorities)).findList();
	}


}
