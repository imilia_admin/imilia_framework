package com.imilia.server.service.impl;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import com.imilia.server.service.ImageService;

/**
 * The Class ImageServiceImpl.
 */
public class ImageServiceImpl extends ServiceImpl implements
		ImageService {

	/** The Constant logger. */
	private static final Logger logger = LoggerFactory.getLogger(ImageServiceImpl.class);
	
	/**
	 * Scale.
	 * 
	 * @param image the image
	 * @param width the width
	 * @param height the height
	 * 
	 * @return the image
	 */
	public Image scale(Image image, int width, int height) {
		return image.getScaledInstance(width, height, Image.SCALE_SMOOTH);
	}

	/* (non-Javadoc)
	 * @see com.imilia.server.service.ImageService#createJPGThumnail(byte[], int, int)
	 */
	public byte[] createJPGThumnail(byte[] rawImage, int width, int height) {
		ByteArrayOutputStream os = new ByteArrayOutputStream();

		ImageIcon icon = new ImageIcon(rawImage);

		Image scaledImage = icon.getImage().getScaledInstance(width, height,
				Image.SCALE_SMOOTH);
		ImageIcon thumb = new ImageIcon(scaledImage);

		int scaledWidth = thumb.getIconWidth();
		int scaledHeight = thumb.getIconHeight();
		
		if (scaledWidth == -1){
			scaledWidth = 32;
		}
		
		if (scaledHeight == -1){
			scaledHeight = 32;
		}
		
		BufferedImage bi = new BufferedImage(scaledWidth, scaledHeight, BufferedImage.TYPE_INT_RGB);
		Graphics g = bi.getGraphics();
		g.drawImage(thumb.getImage(), 0, 0, null);
		try {
			ImageIO.write(bi, "jpg", os);

		} catch (IOException ioe) {
			logger.error("Error occured saving thumbnail", ioe);
		}
		return os.toByteArray();
	}
}
