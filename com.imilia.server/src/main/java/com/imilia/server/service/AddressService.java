/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 * 
 * Created on: Mar 5, 2009
 * Created by: emcgreal
 */
package com.imilia.server.service;

import com.imilia.server.domain.common.Address;
import com.imilia.server.domain.common.PhoneContact;
import com.imilia.server.validation.ValidationException;

/**
 * The Interface AddressService.
 */
public interface AddressService extends DomainObjectService<Address> {
	
	/**
	 * Delete phone contact.
	 * 
	 * @param phoneContact the phone contact
	 */
	public void deletePhoneContact(PhoneContact phoneContact);
	
	
	/**
	 * Save.
	 * 
	 * @param phoneContact the phone contact
	 * 
	 * @throws ValidationException the validation exception
	 */
	public void save(PhoneContact phoneContact) throws ValidationException;

}
