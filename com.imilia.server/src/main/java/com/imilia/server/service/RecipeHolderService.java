/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 * Created on: Jan 4, 2009
 * Created by: emcgreal
 */
package com.imilia.server.service;

import java.util.List;

import com.imilia.server.domain.common.Person;
import com.imilia.server.domain.filter.RecipeHolder;

/**
 * The Interface RecipeHolderService.
 */
public interface RecipeHolderService extends DomainObjectService<RecipeHolder> {
	
	/**
	 * Find.
	 * 
	 * @param owner the owner
	 * 
	 * @return the list< recipe holder>
	 */
	public List<RecipeHolder> find(Person owner);
	
	/**
	 * Find.
	 * 
	 * @param owner the owner
	 * @param clazz the clazz
	 * 
	 * @return the list< recipe holder>
	 */
	public List<RecipeHolder> find(Person owner, Class<?> clazz);
	
}
