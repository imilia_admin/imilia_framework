/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 *
 * Created on: Jun 17, 2009
 * Created by: emcgreal
 */
package com.imilia.server.service.impl;

import java.util.Collection;

import org.joda.time.DateMidnight;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.avaje.ebean.SqlUpdate;
import com.imilia.server.domain.access.Role;
import com.imilia.server.domain.access.Subscription;
import com.imilia.server.domain.common.Address;
import com.imilia.server.domain.common.Person;
import com.imilia.server.domain.common.UserAccount;
import com.imilia.server.domain.importer.DomainConfigImporter;
import com.imilia.server.service.AuthenticationService;
import com.imilia.server.service.InitDatabaseService;
import com.imilia.server.service.QueryRestrictionService;
import com.imilia.server.service.QueryViewService;
import com.imilia.server.service.RoleService;
import com.imilia.server.validation.ValidationException;
import com.imilia.utils.i18n.DomainLocale;

/**
 * The Class InitDatabaseServiceImpl.
 */
public class InitDatabaseServiceImpl extends EbeanAwareServiceImpl implements InitDatabaseService {

	/** Logger used to log warnings, errors and debug messages. */
	private final static Logger logger = LoggerFactory.getLogger(InitDatabaseServiceImpl.class);

	/** The authentication service. */
	@Autowired
	protected AuthenticationService authenticationService;
	
	@Autowired
	private RoleService roleService;
	
	@Autowired
	private QueryViewService queryViewService;
	
	@Autowired
	private QueryRestrictionService restrictionService;
	
	@Autowired
	private DomainConfigImporter domainConfigImporter;
	
	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.imilia.server.service.InitDatabaseService#createBootstrapEntries()
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public void createBootstrapEntries() throws Exception {
		createInitJob();
		importDomainConfig();
	}
	

	public void createInitJob() throws Exception{
		SqlUpdate update = ebeanServer.createSqlUpdate("SET FOREIGN_KEY_CHECKS = 0;");
		ebeanServer.execute(update);
		logger.info("disabled foreign keys");

		// Create the user account
		final UserAccount userAccount = new UserAccount();
		userAccount.setUsername(UserAccount.INIT_JOB);
		userAccount.setPassword(authenticationService.encode(UserAccount.INIT_JOB_PW));
		
		// Add a dummy to bootstrap the authentication
		SecurityContextHolder.getContext().setAuthentication(new Authentication() {
			
			public String getName() {
				return null;
			}
			
			public void setAuthenticated(boolean isAuthenticated)
					throws IllegalArgumentException {
			}
			
			public boolean isAuthenticated() {
				return false;
			}
			
			public Object getPrincipal() {
				return userAccount;
			}
			
			public Object getDetails() {
				return null;
			}
			
			public Object getCredentials() {
				return null;
			}
			
			public Collection<GrantedAuthority> getAuthorities() {
				return null;
			}
		});			
		
		ebeanServer.save(userAccount);

		// Create the person
		final Person person = new Person();
		person.setDomainLocale(DomainLocale.en_US);
		person.setLastName("Job");
		person.setFirstName("Init");
		ebeanServer.save(person);
		logger.info("created initJob person");
		
		// Update the user account
		userAccount.setPerson(person);
		userAccount.setCreatedBy(userAccount);
		ebeanServer.save(userAccount);

		logger.info("created initJob UserAccount");

		update = ebeanServer.createSqlUpdate("SET FOREIGN_KEY_CHECKS = 1;");
		ebeanServer.execute(update);

		logger.info("enabled foreign keys");
	}
	
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public void createDefaultUserForRole(DomainLocale domainLocale) throws Exception {
		for (Role r:getEbeanServer().find(Role.class).findList()){
			createRoleUser(r, domainLocale);
		}
	}
	
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public void importDomainConfig() throws Exception {
		domainConfigImporter.importDomainConfig();
	}

	protected void createRoleUser(Role r, DomainLocale domainLocale) {
		Person p = new Person();
		p.setFirstName(r.getAuthority());
		p.setLastName(r.getAuthority());
		p.setDob(new DateMidnight(1970, 1,1));
		p.setDomainLocale(domainLocale);
		
		Address a = new Address();
		a.setStreet(r.getAuthority() + " Street");
		p.setAddress(a);

		ebeanServer.save(p);

		UserAccount userAccount = new UserAccount();
		userAccount.setUsername(r.getAuthority());
		
		userAccount.setAccountNonLocked(true);
			
		try {
			userAccount.setPassword(authenticationService.encode(UserAccount.INIT_JOB_PW));
		} catch (ValidationException e) {
			logger.error("Create people failed", e);
		}
			
		userAccount.setPerson(p);
		p.setUserAccount(userAccount);
		ebeanServer.save(userAccount);
		
		createSubscription(p,r);
	}
	
	protected void createSubscription(Person p, Role r){
		Subscription s = new Subscription();
		s.setPerson(p);
		s.addRoles(r);
		
		ebeanServer.save(s);
	}
	
	/**
	 * Authenticate init job.
	 */
	public void authenticateInitJob(){
		authenticationService.login(UserAccount.INIT_JOB_AUTHENTICATION_REQUEST);
	}


	/* (non-Javadoc)
	 * @see com.imilia.server.service.InitDatabaseService#createSeedData()
	 */
	public void createSeedData(DomainLocale domainLocale) throws Exception {
		createDefaultUserForRole(domainLocale);
	}

	// ------------------------------------------------------------------------
	// Getter and setters
	// ------------------------------------------------------------------------

	/**
	 * @return the authenticationService
	 */
	public AuthenticationService getAuthenticationService() {
		return authenticationService;
	}



	/**
	 * @param authenticationService the authenticationService to set
	 */
	public void setAuthenticationService(AuthenticationService authenticationService) {
		this.authenticationService = authenticationService;
	}



	public RoleService getRoleService() {
		return roleService;
	}


	public void setRoleService(RoleService roleService) {
		this.roleService = roleService;
	}

	public QueryRestrictionService getRestrictionService() {
		return restrictionService;
	}


	public void setRestrictionService(QueryRestrictionService restrictionService) {
		this.restrictionService = restrictionService;
	}


	public QueryViewService getQueryViewService() {
		return queryViewService;
	}


	public void setQueryViewService(QueryViewService queryViewService) {
		this.queryViewService = queryViewService;
	}


	public DomainConfigImporter getDomainConfigImporter() {
		return domainConfigImporter;
	}


	public void setDomainConfigImporter(DomainConfigImporter domainConfigImporter) {
		this.domainConfigImporter = domainConfigImporter;
	}
}
