/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2010 - all rights reserved
 * Created on: May 19, 2010
 * Created by: emcgreal
 */
package com.imilia.server.service.sms;

import java.util.HashMap;

import com.imilia.server.domain.sms.SMSMessage;

/**
 * The Interface SMSProviderService.
 * 
 * @author emcgreal
 */
public interface SMSProviderService {
	
	/**
	 * Send.
	 * 
	 * @param smsMessage the sms message
	 * 
	 * @throws SMSException the SMS exception
	 */
	public String send(SMSMessage smsMessage) throws SMSException;
	
	/**
	 * Send sms.
	 *
	 * @param map the map
	 * @return the string
	 * @throws SMSException the sMS exception
	 */
	public String sendSMS(final HashMap<String, String> map) throws SMSException;
	
	public SMSReturnCode mapResult(String result);
}
