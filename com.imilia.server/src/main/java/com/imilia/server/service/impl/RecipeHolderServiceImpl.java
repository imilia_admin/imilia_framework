/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2010 - all rights reserved
 * Created on: Oct 6, 2010
 * Created by: emcgreal
 */
package com.imilia.server.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.imilia.server.domain.common.Person;
import com.imilia.server.domain.filter.RecipeHolder;
import com.imilia.server.service.RecipeHolderService;

/**
 * The Class RecipeHolderServiceImpl.
 */
public class RecipeHolderServiceImpl extends DomainObjectServiceImpl<RecipeHolder> implements RecipeHolderService {

	/**
	 * Instantiates a new recipe holder service impl.
	 */
	public RecipeHolderServiceImpl() {
	}

	/* (non-Javadoc)
	 * @see com.imilia.server.service.RecipeHolderService#find(com.imilia.server.domain.common.Person)
	 */
	public List<RecipeHolder> find(Person owner) {
		List<RecipeHolder> holders = ebeanServer.find(RecipeHolder.class).
			where().
			eq("owner.oid", owner.getOid()).
			orderBy("recipe.name").
			findList();
		for (RecipeHolder rh:holders){
			rh.getRecipe().deserialize();
		}
		return holders;
	}

	/* (non-Javadoc)
	 * @see com.imilia.server.service.RecipeHolderService#find(com.imilia.server.domain.common.Person, java.lang.Class)
	 */
	public List<RecipeHolder> find(Person owner, Class<?> clazz) {
		List<RecipeHolder> filteredHolders = new ArrayList<RecipeHolder>();
		final List<RecipeHolder> holders = find(owner);
		for (RecipeHolder rh:holders){
			if (rh.getRecipe().getClazz() == clazz){
				filteredHolders.add(rh);
			}
		}
		return filteredHolders;
	}
	
	
}
