package com.imilia.server.service;

import java.util.List;

import com.csvreader.CsvReader;
import com.imilia.server.domain.access.Role;
import com.imilia.utils.i18n.DomainLocale;

public interface RoleService extends DomainObjectService<Role>{
	public List<Role> importRoles(CsvReader reader, DomainLocale domainLocale) throws Exception;
	
	
	public List<Role> loadRoles();
	
	public List<Role> findRoles(String...authorities);
}
