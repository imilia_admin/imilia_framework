/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2010 - all rights reserved
 * Created on: Feb 9, 2010
 * Created by: emcgreal
 * 
 * Based on: 
 * Name: 
 * 		PasswordStrengthChecker.java
 * Author: 
 * 		Jim Sloey - jsloey@justwild.us
 * Requirements:
 * 		Java 1.4 or greater
 * Usage:
 *		Bundled usage: java -jar PasswordStrengthChecker.jar <password>
 *		Unbundled usage: java PasswordStrengthChecker <password>
 * History:
 * 		Created May 19, 2006 by Jim Sloey
 * Derived from: 
 * 		Steve Moitozo's passwdmeter
 * 		See http://www.geekwisdom.com/dyn/passwdmeter
 * License:
 * 		Open Software License 2.1 or Academic Free License 2.1 
 * 		See http://www.opensource.org
 * Description:
 * 		Need a simple way to check the strength of a password?
 * 		To check in the HTML on the front end try Steve Moitozo's 
 * 		Javascript example at http://www.geekwisdom.com/dyn/passwdmeter
 * Source URL:
 * 		http://justwild.us/examples/password/
 */
 

package com.imilia.server.service.impl;

import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;

import com.imilia.server.domain.CurrentUser;
import com.imilia.utils.i18n.Localized;

/**
 * The Class PasswordStrengthChecker.
 */
public final class PasswordStrengthChecker {
	
	/** The Constant logger. */
	private final static Logger logger = LoggerFactory.getLogger(PasswordStrengthChecker.class);
	
	@Autowired
	private CurrentUser currentUser;
	
	@Autowired
	//@Qualifier("com.imilia.server.messages")
	private MessageSource messageSource;
	
	/**
	 * The Enum Strength.
	 */
	public enum Strength implements Localized{
		
		/** The Invalid. */
		Invalid,
		
		/** The Very weak. */
		VeryWeak,
		
		/** The Weak. */
		Weak,
		
		/** The Mediocre. */
		Mediocre,
		
		/** The Strong. */
		Strong,
		
		/** The Very strong. */
		VeryStrong;
		
		/* (non-Javadoc)
		 * @see com.imilia.server.domain.common.Localeized#getMessageId()
		 */
		public String getMessageId() {
			return Strength.class.getSimpleName() + "." + name();
		}

		/* (non-Javadoc)
		 * @see com.imilia.server.domain.Localized#getLocalizedMessage(org.springframework.context.MessageSource, java.util.Locale)
		 */
		public String getLocalizedMessage(MessageSource messageSource, Locale locale) {
			return messageSource.getMessage(getMessageId(), null, locale);
		}
	};
	
	// Rules variables
	/** The mixed case. */
	private int mixedCase = 1;
	
	/** The min length. */
	private int minLength = 8;
	
	/** The max length. */
	private int maxLength = 30;
	
	/** The numeric count. */
	private int numericCount = 1;
	
	/** The special count. */
	private int specialCount = 1;
	
	/** The min strength. */
	private int minStrength = 30;

	// Points ------------------------------------
	private int pointsForLengthGreater7 	= 14;
	private int pointsForLengthGreater15 	= 18;
	private int pointsForLowerCase 			= 1;
	private int pointsForUpperCase 			= 5;
	private int pointsForNumber	 			= 5;
	private int pointsFor1Number 			= 2;
	private int pointsFor2Numbers 			= 3;
	private int pointsForSpecial 			= 5;
	private int pointsForUpperLower 		= 2;
	private int pointsForLetterNumbers 		= 2;
	private int pointsForLetterNumberSpecials 				= 4;
	private int pointsForUpperLowerLetterNumberSpecials 	= 4;
	
	/**
	 * Instantiates a new password check.
	 */
	public PasswordStrengthChecker() {
		super();
	}

	
	/**
	 * Check password strength.
	 * 
	 * @param passwd the passwd
	 * @param logBuffer the log buffer
	 * 
	 * @return the strength
	 */
	public int checkPasswordStrength(final String passwd, final StringBuffer logBuffer) {
		int score = 0;
		if (passwd == null || "".equals(passwd.trim())){
			return 0;
		}
		
		// PASSWORD LENGTH ------------------------------------------
		final int length = passwd.length();
		
		// length 8 or more ----------------------------------------------
		if (length > 7 && passwd.length() < 16) {
			score += pointsForLengthGreater7;
			appendText(getTextForLengthGt7(length), logBuffer);
		} // length 16 or more
		else if (length > 15){
			score += pointsForLengthGreater15;
			appendText(getTextForLengthGt15(length), logBuffer);
		}
		
		// LETTERS --------------------------------------------------
		Pattern p = Pattern.compile(".??[a-z]");
		Matcher m = p.matcher(passwd);
		// [verified] at least one lower case letter
		int lower = 0;
		// Count the lower case 
		while (m.find()){
			lower += 1;
		}

		// At least one lower case letter gives a point
		if (lower > 0) {
			score += pointsForLowerCase;
			appendText(getTextForLowercase(), logBuffer);
		}

		// [verified] at least one upper case letter -----------------
		p = Pattern.compile(".??[A-Z]");
		m = p.matcher(passwd);
		int upper = 0;
		while (m.find()){
			upper += 1;
		}
		if (upper > 0) {
			score += pointsForUpperCase;
			appendText(getTextForUppercase(), logBuffer);
		}
		// NUMBERS --------------------------------------------------
		p = Pattern.compile(".??[0-9]");
		m = p.matcher(passwd);
		
		// [verified] at least one number
		int numbers = 0;
		Set<String> numbersSet = new HashSet<String>();
		while (m.find()){
			String s = m.group();
			if (!numbersSet.contains(s)){
				numbers += 1;
			}
			numbersSet.add(s);
		}
		if (numbers > 0) {
			score += pointsForNumber;
			appendText(getTextForNumber(), logBuffer);

			if (numbers > 1) {
				score += pointsFor1Number;
				appendText(getTextForNumber2(), logBuffer);
				if (numbers > 2) {
					score += pointsFor2Numbers;
					appendText(getTextForNumber3(), logBuffer);
				}
			}
		}
		// SPECIAL CHAR
		p = Pattern.compile(".??[.,:,;,!,@,#,$,%,^,&,*,?,_,~,<,>]");
		m = p.matcher(passwd);
		// [verified] at least one special character
		int special = 0;
		Set<String> specialsSet = new HashSet<String>();
		
		while (m.find()){
			String s = m.group();
			if (!specialsSet.contains(s)){
				special += 1;
			}
			specialsSet.add(s);
		}
		if (special > 0) {
			score += pointsForSpecial;
			appendText(getTextForSpecial(), logBuffer);
			if (special > 1) {
				score += pointsForSpecial;
				appendText(getTextForSpecialGt1(), logBuffer);
			}
		}
		// COMBOS
		// [verified] both upper and lower case
		if (upper > 0 && lower > 0){
			score += pointsForUpperLower;
			appendText(getTextForMixedCase(), logBuffer);
		}

		// [verified] both letters and numbers
		if ((upper > 0 || lower > 0) && numbers > 0){
			score += pointsForLetterNumbers;
			appendText(getTextForLettersNumbers(), logBuffer);
		}

		// [verified] letters, numbers and special characters
		if ((upper > 0 || lower > 0) && numbers > 0 && special > 0) {
			score += pointsForLetterNumberSpecials;
			appendText(getTextForLettersNumbersSpecials(), logBuffer);
		}
		// [verified] upper, lower, numbers, and special characters
		if (upper > 0 && lower > 0 && numbers > 0 && special > 0){
			score += pointsForUpperLowerLetterNumberSpecials;
			appendText(getTextForMixedLettersNumbersSpecials(), logBuffer);
		}

		return score;
	}
	
	/**
	 * To strength.
	 * 
	 * @param score the score
	 * 
	 * @return the strength
	 */
	public Strength toStrength(int score){
		Strength strength = Strength.Invalid;
		
		if (score > 0 && score < 16) {
			strength = Strength.VeryWeak;
		} else if (score > 15 && score < 20) {
			strength = Strength.Weak;
		} else if (score >=20 && score < 30) {
			strength = Strength.Mediocre;
		} else if (score >= 30 && score < 40) {
			strength = Strength.Strong;
		} else if (score >= 40) {
			strength = Strength.VeryStrong;
		}
		
		return strength;
	}

	/**
	 * The main method.
	 * 
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		
		if (args.length != 1) {
			logger.error("No password supplied");
		}
		
		final PasswordStrengthChecker passwordCheck = new PasswordStrengthChecker();
		final StringBuffer buffer = new StringBuffer();
		int score = passwordCheck.checkPasswordStrength(args[0], buffer);
		final Strength strength = passwordCheck.toStrength(score);
		logger.info("Password strength check score: " + score + " strength: " + strength.name() + "\n" + buffer.toString());
		
	}

	/**
	 * Gets the text for length gt7.
	 * 
	 * @param length the length
	 * 
	 * @return the text for length gt7
	 */
	private String getTextForLengthGt7(int length){
		return getMessageSource().getMessage(
				"com.imilia.server.service.impl.PasswordStrengthChecker.text.length.gt.7", 
				new Object[]{length}, getCurrentUser().getDomainLocaleVariant().getLocale());
	}
	
	private String getTextForLengthGt15(int length){
		return getMessageSource().getMessage(
				"com.imilia.server.service.impl.PasswordStrengthChecker.text.length.gt.15", 
				new Object[]{length}, getCurrentUser().getDomainLocaleVariant().getLocale());
	}
	
	private String getTextForLowercase(){
		return getMessageSource().getMessage(
				"com.imilia.server.service.impl.PasswordStrengthChecker.text.lowercase", 
				null, getCurrentUser().getDomainLocaleVariant().getLocale());
	}
	
	private String getTextForUppercase(){
		return getMessageSource().getMessage(
				"com.imilia.server.service.impl.PasswordStrengthChecker.text.uppercase", 
				null, getCurrentUser().getDomainLocaleVariant().getLocale());
	}
	
	private String getTextForNumber(){
		return getMessageSource().getMessage(
				"com.imilia.server.service.impl.PasswordStrengthChecker.text.number",
				null, getCurrentUser().getDomainLocaleVariant().getLocale());
	}
	
	private String getTextForNumber2(){
		return getMessageSource().getMessage(
				"com.imilia.server.service.impl.PasswordStrengthChecker.text.number.2",
				null, getCurrentUser().getDomainLocaleVariant().getLocale());
	}
	
	private String getTextForNumber3(){
		return getMessageSource().getMessage(
				"com.imilia.server.service.impl.PasswordStrengthChecker.text.number.3",
				null, getCurrentUser().getDomainLocaleVariant().getLocale());
	}
	
	private String getTextForSpecial(){
		return getMessageSource().getMessage(
				"com.imilia.server.service.impl.PasswordStrengthChecker.text.special",
				null, getCurrentUser().getDomainLocaleVariant().getLocale());
	}
	
	private String getTextForSpecialGt1(){
		return getMessageSource().getMessage(
				"com.imilia.server.service.impl.PasswordStrengthChecker.text.special.gt.1",
				null, getCurrentUser().getDomainLocaleVariant().getLocale());
	}
	private String getTextForMixedCase(){
		return getMessageSource().getMessage(
				"com.imilia.server.service.impl.PasswordStrengthChecker.text.combo.mixed.case",
				null, getCurrentUser().getDomainLocaleVariant().getLocale());
	}
	private String getTextForLettersNumbers(){
		return getMessageSource().getMessage(
				"com.imilia.server.service.impl.PasswordStrengthChecker.text.combo.letters.numbers",
				null, getCurrentUser().getDomainLocaleVariant().getLocale());
	}
	private String getTextForLettersNumbersSpecials(){
		return getMessageSource().getMessage(
				"com.imilia.server.service.impl.PasswordStrengthChecker.text.combo.letters.numbers.specials",
				null, getCurrentUser().getDomainLocaleVariant().getLocale());
	}
	private String getTextForMixedLettersNumbersSpecials(){
		return getMessageSource().getMessage(
				"com.imilia.server.service.impl.PasswordStrengthChecker.text.combo.mixed.letters.numbers.specials",
				null, getCurrentUser().getDomainLocaleVariant().getLocale());
	}
	// Getter and setters -----------------------------------------------------
	/**
	 * Gets the mixed case.
	 * 
	 * @return the mixed case
	 */
	public final int getMixedCase() {
		return mixedCase;
	}

	/**
	 * Sets the mixed case.
	 * 
	 * @param mixedCase the new mixed case
	 */
	public final void setMixedCase(int mixedCase) {
		this.mixedCase = mixedCase;
	}

	/**
	 * Gets the min length.
	 * 
	 * @return the min length
	 */
	public final int getMinLength() {
		return minLength;
	}

	/**
	 * Sets the min length.
	 * 
	 * @param minLength the new min length
	 */
	public final void setMinLength(int minLength) {
		this.minLength = minLength;
	}

	/**
	 * Gets the max length.
	 * 
	 * @return the max length
	 */
	public final int getMaxLength() {
		return maxLength;
	}

	/**
	 * Sets the max length.
	 * 
	 * @param maxLength the new max length
	 */
	public final void setMaxLength(int maxLength) {
		this.maxLength = maxLength;
	}

	/**
	 * Gets the numeric count.
	 * 
	 * @return the numeric count
	 */
	public final int getNumericCount() {
		return numericCount;
	}

	/**
	 * Sets the numeric count.
	 * 
	 * @param numericCount the new numeric count
	 */
	public final void setNumericCount(int numericCount) {
		this.numericCount = numericCount;
	}

	/**
	 * Gets the special count.
	 * 
	 * @return the special count
	 */
	public final int getSpecialCount() {
		return specialCount;
	}

	/**
	 * Sets the special count.
	 * 
	 * @param specialCount the new special count
	 */
	public final void setSpecialCount(int specialCount) {
		this.specialCount = specialCount;
	}

	/**
	 * Gets the min strength.
	 * 
	 * @return the min strength
	 */
	public final int getMinStrength() {
		return minStrength;
	}

	/**
	 * Sets the min strength.
	 * 
	 * @param minStrength the new min strength
	 */
	public final void setMinStrength(int minStrength) {
		this.minStrength = minStrength;
	}


	public final int getPointsForLengthGreater7() {
		return pointsForLengthGreater7;
	}


	public final void setPointsForLengthGreater7(int pointsForLengthGreater8) {
		this.pointsForLengthGreater7 = pointsForLengthGreater8;
	}


	public final int getPointsForLengthGreater15() {
		return pointsForLengthGreater15;
	}


	public final void setPointsForLengthGreater15(int pointsForLengthGreater15) {
		this.pointsForLengthGreater15 = pointsForLengthGreater15;
	}


	public final int getPointsForLowerCase() {
		return pointsForLowerCase;
	}


	public final void setPointsForLowerCase(int pointsForLowerCase) {
		this.pointsForLowerCase = pointsForLowerCase;
	}


	public final int getPointsForUpperCase() {
		return pointsForUpperCase;
	}


	public final void setPointsForUpperCase(int pointsForUpperCase) {
		this.pointsForUpperCase = pointsForUpperCase;
	}


	public final int getPointsForNumber() {
		return pointsForNumber;
	}


	public final void setPointsForNumber(int pointsForNumber) {
		this.pointsForNumber = pointsForNumber;
	}


	public final int getPointsFor1Number() {
		return pointsFor1Number;
	}


	public final void setPointsFor1Number(int pointsFor1Number) {
		this.pointsFor1Number = pointsFor1Number;
	}


	public final int getPointsFor2Numbers() {
		return pointsFor2Numbers;
	}


	public final void setPointsFor2Numbers(int pointsFor2Numbers) {
		this.pointsFor2Numbers = pointsFor2Numbers;
	}


	public final int getPointsForSpecial() {
		return pointsForSpecial;
	}


	public final void setPointsForSpecial(int pointsForSpecial) {
		this.pointsForSpecial = pointsForSpecial;
	}


	public final int getPointsForUpperLower() {
		return pointsForUpperLower;
	}


	public final void setPointsForUpperLower(int pointsForUpperLower) {
		this.pointsForUpperLower = pointsForUpperLower;
	}


	public final int getPointsForLetterNumbers() {
		return pointsForLetterNumbers;
	}


	public final void setPointsForLetterNumbers(int pointsForLetterNumbers) {
		this.pointsForLetterNumbers = pointsForLetterNumbers;
	}


	public final int getPointsForLetterNumberSpecials() {
		return pointsForLetterNumberSpecials;
	}


	public final void setPointsForLetterNumberSpecials(
			int pointsForLetterNumberSpecials) {
		this.pointsForLetterNumberSpecials = pointsForLetterNumberSpecials;
	}


	public final int getPointsForUpperLowerLetterNumberSpecials() {
		return pointsForUpperLowerLetterNumberSpecials;
	}


	public final void setPointsForUpperLowerLetterNumberSpecials(
			int pointsForUpperLowerLetterNumberSpecials) {
		this.pointsForUpperLowerLetterNumberSpecials = pointsForUpperLowerLetterNumberSpecials;
	}


	public final CurrentUser getCurrentUser() {
		return currentUser;
	}


	public final void setCurrentUser(CurrentUser currentUser) {
		this.currentUser = currentUser;
	}


	public final MessageSource getMessageSource() {
		return messageSource;
	}


	public final void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
	
	private void appendText(String text, StringBuffer logBuffer){
		if (logBuffer.length() > 0){
			logBuffer.append(", ");
		}
		logBuffer.append(text);
	}
	
	public int getMaxPossiblePoints(){
		return
		pointsForLengthGreater7 +
		pointsForLengthGreater15 +
		pointsForLowerCase +
		pointsForUpperCase +
		pointsForNumber	+
		pointsFor1Number + 
		pointsFor2Numbers +
		pointsForSpecial +
		pointsForUpperLower +
		pointsForLetterNumbers +
		pointsForLetterNumberSpecials +
		pointsForUpperLowerLetterNumberSpecials; 		
	}
}
