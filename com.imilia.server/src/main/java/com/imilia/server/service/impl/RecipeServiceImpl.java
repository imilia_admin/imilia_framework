/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2010 - all rights reserved
 * Created on: Oct 6, 2010
 * Created by: emcgreal
 */
package com.imilia.server.service.impl;

import java.beans.PropertyChangeListener;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.imilia.server.domain.common.Person;
import com.imilia.server.domain.filter.Recipe;
import com.imilia.server.domain.filter.RecipeHolder;
import com.imilia.server.domain.filter.RecipeResult;
import com.imilia.server.service.RecipeHolderService;
import com.imilia.server.service.RecipeService;
import com.imilia.utils.i18n.DomainLocale;
import com.imilia.server.validation.ValidationException;

/**
 * The Class RecipeServiceImpl.
 */
public class RecipeServiceImpl extends DomainObjectServiceImpl<Recipe<?>> implements RecipeService {
	
	/** The recipe holder service. */
	@Autowired
	private RecipeHolderService recipeHolderService;

	/**
	 * Instantiates a new recipe service impl.
	 */
	public RecipeServiceImpl() {
	}

	/* (non-Javadoc)
	 * @see com.imilia.server.service.RecipeService#execute(com.imilia.server.domain.filter.Recipe, com.imilia.server.domain.i18n.DomainLocale, com.imilia.server.domain.filter.RecipeResult)
	 */
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public <T> void execute(Recipe<T> recipe, DomainLocale domainLocale,
			RecipeResult<T> recipeResult) throws ValidationException {
		recipe.execute(ebeanServer, domainLocale, recipeResult);
	}

	/* (non-Javadoc)
	 * @see com.imilia.server.service.RecipeService#find(long)
	 */
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public Recipe<?> find(long l) {
		return ebeanServer.find(Recipe.class, l);
	}

	/* (non-Javadoc)
	 * @see com.imilia.server.service.RecipeService#find(java.lang.String)
	 */
	@SuppressWarnings("rawtypes")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<Recipe> find(String name) {
		return ebeanServer.find(Recipe.class).where().eq("clazz", name).findList();
	}

	/**
	 * Gets the recipe holder service.
	 * 
	 * @return the recipe holder service
	 */
	public RecipeHolderService getRecipeHolderService() {
		return recipeHolderService;
	}

	/**
	 * Sets the recipe holder service.
	 * 
	 * @param recipeHolderService the new recipe holder service
	 */
	public void setRecipeHolderService(RecipeHolderService recipeHolderService) {
		this.recipeHolderService = recipeHolderService;
	}

	/* (non-Javadoc)
	 * @see com.imilia.server.service.RecipeService#findRecipeHolders(com.imilia.server.domain.common.Person)
	 */
	public List<RecipeHolder> findRecipeHolders(Person owner) {
		return recipeHolderService.find(owner);
	}

	/* (non-Javadoc)
	 * @see com.imilia.server.service.RecipeService#findRecipeHolders(com.imilia.server.domain.common.Person, java.lang.Class)
	 */
	public List<RecipeHolder> findRecipeHolders(Person owner, Class<?> clazz) {
		return recipeHolderService.find(owner, clazz);
	}

	/* (non-Javadoc)
	 * @see com.imilia.server.service.RecipeService#deleteRecipeHolder(com.imilia.server.domain.filter.RecipeHolder)
	 */
	public void deleteRecipeHolder(RecipeHolder recipeHolder) throws ValidationException {
		recipeHolderService.purge(recipeHolder);	
	}

	/* (non-Javadoc)
	 * @see com.imilia.server.service.RecipeService#save(com.imilia.server.domain.filter.RecipeHolder)
	 */
	public void save(RecipeHolder recipeHolder) throws ValidationException {
		recipeHolder.getRecipe().serialize();
		recipeHolderService.save(recipeHolder);
	}

	/* (non-Javadoc)
	 * @see com.imilia.server.service.RecipeService#refresh(com.imilia.server.domain.filter.RecipeHolder)
	 */
	public void refresh(RecipeHolder recipeHolder) {
		recipeHolderService.refresh(recipeHolder);
	}

	/* (non-Javadoc)
	 * @see com.imilia.server.service.RecipeService#addPropertyChangeListener(java.beans.PropertyChangeListener, com.imilia.server.domain.filter.RecipeHolder)
	 */
	public void addPropertyChangeListener(
			PropertyChangeListener propertyChangeListener,
			RecipeHolder recipeHolder) {
		recipeHolderService.addPropertyChangeListener(propertyChangeListener, recipeHolder);
		
	}

	/* (non-Javadoc)
	 * @see com.imilia.server.service.RecipeService#removePropertyChangeListener(java.beans.PropertyChangeListener, com.imilia.server.domain.filter.RecipeHolder)
	 */
	public void removePropertyChangeListener(
			PropertyChangeListener propertyChangeListener,
			RecipeHolder recipeHolder) {
		recipeHolderService.removePropertyChangeListener(propertyChangeListener, recipeHolder);
	}

}
