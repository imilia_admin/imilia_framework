/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 *
 * Created on: Jun 17, 2009
 * Created by: emcgreal
 */
package com.imilia.server.service;

import com.imilia.utils.i18n.DomainLocale;


/**
 * The Interface InitDatabaseService.
 */
public interface InitDatabaseService extends Service {

	/**
	 * Creates the bootstrap entries.
	 * @throws Exception 
	 *
	 */
	public void createBootstrapEntries() throws Exception;

	/**
	 * Authenticate init job.
	 */
	public void authenticateInitJob();


	/**
	 * Creates the seed data.
	 *
	 * @param domainLocale the domain locale
	 * @throws Exception the exception
	 */
	public void createSeedData(DomainLocale domainLocale) throws Exception;
}
