package com.imilia.server.service.impl;

import org.springframework.beans.factory.annotation.Autowired;

import com.avaje.ebean.EbeanServer;

public class EbeanAwareServiceImpl extends ServiceImpl {
	
	/** The Ebean server. */
	@Autowired
	protected EbeanServer ebeanServer;

	/**
	 * @return the ebeanServer
	 */
	public EbeanServer getEbeanServer() {
		return ebeanServer;
	}

	/**
	 * @param ebeanServer the ebeanServer to set
	 */
	public void setEbeanServer(EbeanServer ebeanServer) {
		this.ebeanServer = ebeanServer;
	}
}
