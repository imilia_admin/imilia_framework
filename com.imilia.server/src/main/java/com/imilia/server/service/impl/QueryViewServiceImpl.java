/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2014 - all rights reserved
 * Created on: Mar 18, 2014
 * Created by: emcgreal
 */
package com.imilia.server.service.impl;

import com.imilia.server.domain.query.entities.QueryView;
import com.imilia.server.service.QueryViewService;

/**
 * The Class QueryViewServiceImpl.
 *
 * @author emcgreal
 */
public class QueryViewServiceImpl extends DomainObjectServiceImpl<QueryView> implements
		QueryViewService {
}
