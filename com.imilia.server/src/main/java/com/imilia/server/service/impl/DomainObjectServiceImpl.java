/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 * 
 * Created on: Mar 5, 2009
 * Created by: emcgreal
 */
package com.imilia.server.service.impl;

import java.beans.PropertyChangeListener;
import java.util.List;

import ognl.Ognl;
import ognl.OgnlException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.imilia.server.domain.DomainObject;
import com.imilia.server.domain.enums.ObjectStatus;
import com.imilia.server.service.DomainObjectService;
import com.imilia.server.validation.ValidationException;

/**
 * The Class DomainObjectServiceImpl.
 */
public abstract class DomainObjectServiceImpl<T extends DomainObject> 
	extends EbeanAwareServiceImpl implements DomainObjectService<T> {
	
	/** The Constant logger. */
	private final static Logger logger = LoggerFactory.getLogger(DomainObjectServiceImpl.class);



	/**
	 * Instantiates a new service impl.
	 */
	public DomainObjectServiceImpl() {
		super();
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.imilia.server.service.UserAccountService#save(com.imilia.server.domain
	 * .common.UserAccount)
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public void save(T domainObject) throws ValidationException {

		// Validate the domain object
		validate(domainObject);

		// save the account in the database
		ebeanServer.save(domainObject);
	}
	
	/* (non-Javadoc)
	 * @see com.imilia.server.service.DomainObjectService#save(java.util.List)
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public void save(List<T> domainObjects) throws ValidationException {

		// Validate the domain objects 
		for(T t:domainObjects){ 
			validate(t);
		}

		// save the accounts in the database
		ebeanServer.save(domainObjects);
	}
	
	

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.imilia.server.service.DomainObjectService#delete(com.imilia.server
	 * .domain.DomainObject)
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public void delete(T domainObject) throws ValidationException {

		// Validate the domain object - will throw a Validation exception if
		// invalid
		validate(domainObject);

		domainObject.setObjectStatus(ObjectStatus.Deleted);
		// delete the account in the database
		ebeanServer.save(domainObject);
	}
	
	/**
	 * Delete marks the domain object with object status Delete.
	 * 
	 * @param d the d
	 * 
	 * @throws ValidationException the validation exception
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public void deleteDomainObject(DomainObject d) throws ValidationException {

		// Validate the domain object - will throw a Validation exception if
		// invalid
		validate(d);

		d.setObjectStatus(ObjectStatus.Deleted);
		// delete the account in the database
		ebeanServer.save(d);
	}
	

	/* (non-Javadoc)
	 * @see com.imilia.server.service.DomainObjectService#purge(com.imilia.server.domain.DomainObject)
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public void purge(T domainObject) throws ValidationException {

		// Validate the domain object - will throw a Validation exception if
		// invalid
		validate(domainObject);

		// delete the account in the database
		ebeanServer.delete(domainObject);
	}
	
	/* (non-Javadoc)
	 * @see com.imilia.server.service.DomainObjectService#purge(com.imilia.server.domain.DomainObject)
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public void purgeDomainObject(DomainObject o) throws ValidationException {

		// Validate the domain object - will throw a Validation exception if
		// invalid
		validate(o);

		// delete the account in the database
		ebeanServer.delete(o);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.imilia.server.service.DomainObjectService#find(java.lang.Class,
	 * long)
	 */
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public T find(Class<T> clazz, long oid) {
		return ebeanServer.find(clazz, oid);
	}

	/* (non-Javadoc)
	 * @see com.imilia.server.service.DomainObjectService#isNew(com.imilia.server.domain.DomainObject)
	 */
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public boolean isNew(T domainObject) {
		return ebeanServer.getBeanState(domainObject).isNew();
	}
	
	/* (non-Javadoc)
	 * @see com.imilia.server.service.DomainObjectService#isDirty(com.imilia.server.domain.DomainObject, java.lang.String[])
	 */
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public boolean isDirty(T domainObject, String ... children) {
		if (domainObject == null){
			logger.warn("Trying to check for isDirty on null");
			return false;
		}
		
		if (ebeanServer.getBeanState(domainObject).isDirty()){
			return true;
		}
		
		for (String path:children){
			// get the child and see if its dirty
			DomainObject bean = getChild(domainObject, path);				
			if (bean != null){
				if (ebeanServer.getBeanState(bean).isDirty()){
					return true;
				}
			}
		}
		 
		return false;
	}

	/* (non-Javadoc)
	 * @see com.imilia.server.service.DomainObjectService#refresh(com.imilia.server.domain.DomainObject)
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public void refresh(T domainObject, String ... children) {
		if (domainObject == null){
			logger.warn("Trying to refresh on null");
			return;
		}
		
		ebeanServer.refresh(domainObject);
		
		for (String path:children){
			// get the child and see if its dirty
			DomainObject bean = getChild(domainObject, path);				
			if (bean != null){
				ebeanServer.refresh(domainObject);
			}
		}
	}
	
	/* (non-Javadoc)
	 * @see com.imilia.server.service.DomainObjectService#addPropertyChangeListener(java.beans.PropertyChangeListener, com.imilia.server.domain.DomainObject, java.lang.String[])
	 */
	public void addPropertyChangeListener(
			PropertyChangeListener propertyChangeListener, T domainObject,
			String... children) {
		if (domainObject == null){
			logger.warn("Trying to add a property change listener to null");
			return;
		}
		ebeanServer.getBeanState(domainObject).addPropertyChangeListener(propertyChangeListener);
		
		for (String path:children){
			// get the child and see if its dirty
			DomainObject bean = getChild(domainObject, path);				
			if (bean != null){
				ebeanServer.getBeanState(bean).addPropertyChangeListener(propertyChangeListener);
			}
		}
		
	}

	/* (non-Javadoc)
	 * @see com.imilia.server.service.DomainObjectService#removePropertyChangeListener(java.beans.PropertyChangeListener, com.imilia.server.domain.DomainObject, java.lang.String[])
	 */
	public void removePropertyChangeListener(
			PropertyChangeListener propertyChangeListener, T domainObject,
			String... children) {
		if (domainObject == null){
			logger.warn("Trying to remove a property change listener from null");
			return;
		}
		ebeanServer.getBeanState(domainObject).removePropertyChangeListener(propertyChangeListener);
		
		for (String path:children){
			// get the child and see if its dirty
			DomainObject bean = getChild(domainObject, path);				
			if (bean != null){
				ebeanServer.getBeanState(bean).removePropertyChangeListener(propertyChangeListener);
			}
		}
	}

	// Getter and setters -----------------------------------------------------
	/**
	 * Gets the child.
	 * 
	 * @param domainObject the domain object
	 * @param path the path
	 * 
	 * @return the child
	 */
	private DomainObject getChild(T domainObject, String path){
		try {
			// get the child and see if its dirty
			final Object bean = Ognl.getValue(path, domainObject);
			
			if (bean instanceof DomainObject){
				return (DomainObject) bean;
			}else{
				logger.error("Invalid path to domain object:" + path);
			}
		} catch (OgnlException e) {
			logger.error("Invalid path:" + path, e);
		}
		
		return null;
	};

	/* (non-Javadoc)
	 * @see com.imilia.server.service.DomainObjectService#validate(com.imilia.server.domain.DomainObject)
	 */
	public void validate(T domainObject) throws ValidationException{
		super.validate(domainObject);
	}


	/* (non-Javadoc)
	 * @see com.imilia.server.service.DomainObjectService#getEditable(java.lang.Class, long)
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public <X> X getEditable(Class<X> clazz, long oid) {
		// Maybe we need to do something else later to ensure it is editable
		return getEbeanServer().find(clazz, oid);
	}
}
