/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 * 
 * Created on: Mar 5, 2009
 * Created by: emcgreal
 */
package com.imilia.server.service;

import java.beans.PropertyChangeListener;
import java.util.List;

import com.imilia.server.domain.DomainObject;
import com.imilia.server.validation.ValidationException;

/**
 * The Interface DomainObjectService.
 */
public interface DomainObjectService <T extends DomainObject> extends Service {
	
	/**
	 * Save.
	 * 
	 * @param domainObject the domain object to save (insert or update)
	 * 
	 * @throws ValidationException the validation exception
	 */
	public void save(T domainObject) throws ValidationException; 
	
	/**
	 * Save.
	 *
	 * @param domainObject the domain object
	 * @throws ValidationException the validation exception
	 */
	public void save(List<T> domainObject) throws ValidationException; 
	
	/**
	 * Delete marks the object with object status Delete.
	 * 
	 * @param domainObject the domain object to delete
	 * 
	 * @throws ValidationException the validation exception
	 */
	public void delete(T domainObject) throws ValidationException;
	
	/**
	 * Delete marks the domain object with object status Delete.
	 * 
	 * @param d the d
	 * 
	 * @throws ValidationException the validation exception
	 */
	public void deleteDomainObject(DomainObject d) throws ValidationException;
	
	/**
	 * Purge.
	 * 
	 * @param domainObject the domain object to purge from the database
	 * 
	 * @throws ValidationException the validation exception
	 */
	public void purge(T domainObject) throws ValidationException;
	
	/**
	 * Purge domain object.
	 * 
	 * @param o the o
	 * 
	 * @throws ValidationException the validation exception
	 */
	public void purgeDomainObject(DomainObject o) throws ValidationException;
	
	/**
	 * Find.
	 * 
	 * @param clazz the clazz
	 * @param oid the oid
	 * 
	 * @return the t
	 */
	public T find(Class<T> clazz, long oid);
	
	/**
	 * Refresh.
	 * 
	 * @param domainObject the domain object
	 * @param children the children
	 */
	public void refresh(T domainObject, String ... children);
	
	/**
	 * Adds the property change listener.
	 * 
	 * @param propertyChangeListener the property change listener
	 * @param domainObject the domain object
	 * @param children the children
	 */
	public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener, T domainObject, String...children);
	
	/**
	 * Removes the property change listener.
	 * 
	 * @param propertyChangeListener the property change listener
	 * @param domainObject the domain object
	 * @param children the children
	 */
	public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener, T domainObject, String...children);
	
	/**
	 * Checks if the domain object is new.
	 * 
	 * @param domainObject the domain object
	 * 
	 * @return true, if is new
	 */
	public boolean isNew(T domainObject);
	
	/**
	 * Checks if the domain object is dirty.
	 * 
	 * @param domainObject the domain object
	 * @param children the children
	 * 
	 * @return true, if is dirty
	 */
	public boolean isDirty(T domainObject, String ... children);
	
	/**
	 * Validate.
	 * 
	 * @param domainObject the domain object
	 * 
	 * @throws ValidationException the validation exception
	 */
	public void validate(T domainObject) throws ValidationException;
	
	/**
	 * Gets the editable.
	 * 
	 * @param clazz the clazz
	 * @param oid the oid
	 * 
	 * @return the editable
	 */
	public <X> X getEditable(Class<X> clazz, long oid);
}
