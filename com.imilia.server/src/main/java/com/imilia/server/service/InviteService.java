/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2011 - all rights reserved
 *
 * Created on: Sep 13, 2011
 * Created by: emcgreal
 */
package com.imilia.server.service;

import java.util.List;
import java.util.Map;

import com.imilia.server.domain.common.UserAccount;
import com.imilia.utils.template.TemplateProvider;
import com.imilia.server.validation.ValidationException;

// TODO: Auto-generated Javadoc
/**
 * The Interface InviteService.
 */
/**
 * @author emcgreal
 *
 */
public interface InviteService  extends Service {
	
	/** The Constant SENDER. */
	public final static String SENDER = "sender";
	
	/** The Constant SUBJECT. */
	public final static String SUBJECT = "subject";
	
	/** The Constant APP. */
	public final static String APP = "app";
	
	/** The Constant BASE_URL. */
	public final static String BASE_URL = "baseURL";

	/** The Constant URL. */
	public final static String URL = "url";

	/** The Constant ENCODED_URL. */
	public final static String ENCODED_URL = "encoded_url";
	
	/**
	 * Invite.
	 *
	 * @param userAccounts the user accounts
	 * @param templateProvider the template provider
	 * @param model the model
	 * @param failed the failed
	 * @throws Exception the exception
	 */
	public void invite(List<UserAccount> userAccounts, TemplateProvider templateProvider, Map<String, Object> model, List<UserAccount> failed) throws Exception;
	

	/**
	 * Unlock.
	 *
	 * @param userAccounts the user accounts
	 * @param templateProvider the template provider
	 * @param model the model
	 * @param failed the failed
	 * @throws Exception the exception
	 */
	public void unlock(List<UserAccount> userAccounts, TemplateProvider templateProvider, Map<String, Object> model, List<UserAccount> failed) throws Exception;

	/**
	 * Un lock and reset passwords.
	 *
	 * @param userAccounts the user accounts
	 * @param templateProvider the template provider
	 * @param model the model
	 * @param failed the failed
	 * @throws Exception the exception
	 */
	public void unLockAndResetPasswords(List<UserAccount> userAccounts, TemplateProvider templateProvider, Map<String, Object> model, List<UserAccount> failed) throws Exception;
	
	
	/**
	 * Generate safe password.
	 *
	 * @return the string
	 */
	public String generateSafePassword();
	
	/**
	 * Reset password.
	 *
	 * @param userAccount the user account
	 * @return the string
	 * @throws ValidationException the validation exception
	 */
	public String resetPassword(UserAccount userAccount) throws ValidationException;
	
	public boolean isTestMode();

	public void setTestMode(boolean testMode);
}
