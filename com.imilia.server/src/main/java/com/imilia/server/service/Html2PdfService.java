/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 *
 * Created on: Nov 25, 2009
 * Created by: emcgreal
 */
package com.imilia.server.service;

import java.io.OutputStream;

import org.w3c.dom.Document;

/**
 * The Interface Html2PdfService.
 */
public interface Html2PdfService extends Service {
	
	/**
	 * Prints the html to PDF
	 * 
	 * <p>The xhtml is expected to be well formed. Use tidy XHTML if you need to tidy up
	 * the code.</p>
	 * 
	 * @param htmlInput the html input
	 * @param os the os
	 */
	public void print(String xhtmlInput, OutputStream os);

	/**
	 * Prints the document to PDF
	 * 
	 * @param document the document
	 * @param os the os
	 */
	public void print(Document document, OutputStream os);

	/**
	 * Tidy xhtml.
	 * 
	 * @param rawHtml the raw html
	 * @param outputStream the output stream
	 */
	public void tidyXHTML(String rawHtml, OutputStream outputStream);
	
	/**
	 * Tidy the input html and returns a W3 Document.
	 * 
	 * @param untidy the untidy
	 * 
	 * @return the document
	 */
	public Document tidy(String untidy);

	/**
	 * Tidy.
	 * 
	 * @param untidy the untidy
	 * @param os the os
	 */
	public void tidy(String untidy, OutputStream os);

	
	/**
	 * Tidy and print xhtml.
	 * 
	 * @param rawHtml the raw html
	 * @param outputSream the output stream
	 */
	public void tidyAndPrintXHTML(String rawHtml, OutputStream outputStream);
	
	/**
	 * Wrap tidy and print xhtml.
	 * 
	 * @param rawHtml the raw html
	 * @param outputSream the output stream
	 */
	public void wrapTidyAndPrintXHTML(String rawHtml, OutputStream outputStream);
	/**
	 * Wrap in xhtml.
	 * 
	 * @param htmlSnippet the html snippet
	 * 
	 * @return the string
	 */
	public String wrapInXHTML(String htmlSnippet);
}
