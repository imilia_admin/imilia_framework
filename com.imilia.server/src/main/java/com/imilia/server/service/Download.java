package com.imilia.server.service;

import java.io.ByteArrayOutputStream;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.imilia.server.domain.enums.MimeType;

/**
 * The class Download is used to encapsulate a file to be downloaded.
 */
public class Download{

	private final static DateTimeFormatter defaultDateTimeFormat = DateTimeFormat.forPattern("yyyyMMddHHmmss");
	/** The file name. */
	private String fileName;

	/** The mime type. */
	private MimeType mimeType;

	/** The output stream. */
	private ByteArrayOutputStream outputStream;

	/**
	 * Instantiates a new download.
	 *
	 * @param fileName the file name
	 * @param mimeType the mime type
	 */
	public Download(String fileName, MimeType mimeType) {
		super();
		this.fileName = fileName;
		this.mimeType = mimeType;
		outputStream = new ByteArrayOutputStream();
	}

	/**
	 * Gets the file name.
	 *
	 * @return the file name
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * Sets the file name.
	 *
	 * @param fileName the new file name
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * Gets the mime type.
	 *
	 * @return the mime type
	 */
	public MimeType getMimeType() {
		return mimeType;
	}

	/**
	 * Sets the mime type.
	 *
	 * @param mimeType the new mime type
	 */
	public void setMimeType(MimeType mimeType) {
		this.mimeType = mimeType;
	}

	/**
	 * Gets the output stream.
	 *
	 * @return the output stream
	 */
	public ByteArrayOutputStream getOutputStream() {
		return outputStream;
	}

	/**
	 * Sets the output stream.
	 *
	 * @param outputStream the new output stream
	 */
	public void setOutputStream(ByteArrayOutputStream outputStream) {
		this.outputStream = outputStream;
	}

	public static String generateFileName(String prefix, String suffix){
		return prefix + defaultDateTimeFormat.print(new DateTime()) + suffix;
	}
}