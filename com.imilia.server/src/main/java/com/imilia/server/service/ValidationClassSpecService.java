/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2010 - all rights reserved
 * Created on: May 14, 2010
 * Created by: emcgreal
 */
package com.imilia.server.service;

import com.imilia.server.domain.validation.ValidationClassSpec;

/**
 * The Interface ValidationClassSpecService.
 */
public interface ValidationClassSpecService extends  DomainObjectService<ValidationClassSpec> {

}
