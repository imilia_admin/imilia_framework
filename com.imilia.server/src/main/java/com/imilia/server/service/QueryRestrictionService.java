/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2014 - all rights reserved
 * Created on: Feb 27, 2014
 * Created by: emcgreal
 */
package com.imilia.server.service;

import java.util.List;

import com.csvreader.CsvReader;
import com.imilia.server.domain.access.Role;
import com.imilia.server.domain.query.Query;
import com.imilia.server.domain.query.entities.QueryRestriction;
import com.imilia.utils.i18n.DomainLocale;

/**
 * The Interface RestrictionService.
 */
public interface QueryRestrictionService extends DomainObjectService<QueryRestriction>{ 
	
	/**
	 * Gets the restrictions.
	 *
	 * @param queryTemplate the query template
	 * @param roles the roles
	 * @return the restrictions
	 */
	public List<QueryRestriction> getRestrictions(Query<?> queryTemplate, List<Role> roles);
	
	/**
	 * Import query restrictions.
	 *
	 * @param reader the reader
	 * @param domainLocale the domain locale
	 * @return the list
	 * @throws Exception the exception
	 */
	public List<QueryRestriction> importQueryRestrictions(CsvReader reader, DomainLocale domainLocale) throws Exception;
}
