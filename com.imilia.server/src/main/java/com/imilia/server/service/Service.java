/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 * 
 * Created on: Mar 5, 2009
 * Created by: emcgreal
 */
package com.imilia.server.service;


/**
 * The Interface Service is a marker interface for all services
 */
public interface Service {
}
