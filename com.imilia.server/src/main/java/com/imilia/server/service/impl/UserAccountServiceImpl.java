/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 * 
 * Created on: Mar 3, 2009
 * Created by: emcgreal
 */
package com.imilia.server.service.impl;

import java.util.List;

import javax.persistence.PersistenceException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.imilia.server.domain.access.AuthenticationRequest;
import com.imilia.server.domain.common.Address;
import com.imilia.server.domain.common.Person;
import com.imilia.server.domain.common.UserAccount;
import com.imilia.server.service.UserAccountService;
import com.imilia.server.validation.ValidationException;

/**
 * The Class UserAccountServiceImpl.
 */
public class UserAccountServiceImpl extends DomainObjectServiceImpl<UserAccount> implements UserAccountService {

	/** Logger used to log warnings, errors and debug messages. */
	private final static Logger logger = LoggerFactory.getLogger(UserAccountServiceImpl.class);

	// UserDetailsService -----------------------------------------------------
	/*
	 * (non-Javadoc)
	 * 
	 * @seeorg.springframework.security.userdetails.UserDetailsService#
	 * loadUserByUsername(java.lang.String)
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor={Exception.class})
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException,
			DataAccessException {
		try {
			final UserAccount userAccount = ebeanServer.find(UserAccount.class).where().eq(
					"username", username).findUnique();

			// If no user with this username was found then throw this exception
			if (userAccount == null) {
				throw new UsernameNotFoundException(username);
			}
			
			// FIXME Force loading - problem with Spring Txns and lazy loading
			//userAccount.getPerson().getDomainLocale().getLocaleCode();
			return userAccount;
		} catch (PersistenceException ex) {
			logger.info("Failed to query user account with uername: " + username);
			throw new DataRetrievalFailureException(ex.getMessage(), ex);
		}
	}

	/* (non-Javadoc)
	 * @see com.imilia.server.service.UserAccountService#load(com.imilia.server.domain.common.UserAccount)
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor={Exception.class})
	public void load(UserAccount userAccount){
		ebeanServer.refresh(userAccount);
		// Force the loading of the person 
		userAccount.getPerson();
	}
	
	/**
	 * Login succeeded.
	 * 
	 * @param userAccount the user account
	 * @param authenticationRequest the authentication request
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public void loginSucceeded(UserAccount userAccount, final AuthenticationRequest authenticationRequest) {
		load(userAccount);

		if (userAccount != null){
			userAccount.logSucceeded(authenticationRequest);
			userAccount.setIgnoreAuditor(true);
			try {
				save(userAccount);
			} catch (ValidationException e) {
				logger.error("Failed to update useraccount after successful login", e);
			}
		}
	}
	
	/**
	 * Login failed.
	 * 
	 * @param authenticationRequest the authentication request
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public void loginFailed( final AuthenticationRequest authenticationRequest) {
		final UserAccount userAccount = (UserAccount) loadUserByUsername(authenticationRequest.getUsername());
		
		if (userAccount != null){
			userAccount.logFailed(authenticationRequest);
			int remainingAttempts = authenticationRequest.getMaxLoginAttemptsAllowed() - userAccount.getLoginAttempts();
			
			if (remainingAttempts < 0){
				remainingAttempts = 0;
			}
			
			authenticationRequest.setRemainingAttempts(remainingAttempts);
			userAccount.setIgnoreAuditor(true);
			try {
				save(userAccount);
			} catch (ValidationException e) {
				logger.error("Failed to update user after failed login", e);
			}
		}
	}

	
	/* (non-Javadoc)
	 * @see com.imilia.server.service.UserAccountService#loadUserByEmail(java.lang.String)
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor={Exception.class})
	public UserAccount loadUserByEmail(String email) throws PersistenceException{
		
			final Address address = ebeanServer.find(Address.class).where().eq(
					"email", email).findUnique();

			// If no user with this email was found then throw this exception
			if (address == null) {
				return null;
			}
			
			final Person person = ebeanServer.find(Person.class).where().eq(
					"address", address).findUnique();

			return person.getUserAccount();
		
	}
	
	/* (non-Javadoc)
	 * @see com.imilia.server.service.UserAccountService#loadUserByName(java.lang.String)
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor={Exception.class})
	public UserAccount loadUserByName(String username) throws PersistenceException{
		
			final UserAccount userAccount = ebeanServer.find(UserAccount.class).where().eq(
				"username", username).findUnique();

			if (userAccount == null) {
				return null;
			}
			
			return userAccount;
		
	}

	/* (non-Javadoc)
	 * @see com.imilia.server.service.UserAccountService#findUserAccountsWithUsernameLike(java.lang.String)
	 */
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED, rollbackFor={Exception.class})
	public List<UserAccount> findUserAccountsWithUsernameLike(String username) {
		return ebeanServer.find(UserAccount.class).where().like(
				"username", username).findList();
	}

	/* (non-Javadoc)
	 * @see com.imilia.server.service.UserAccountService#unlock(java.util.List)
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor={Exception.class})
	public void unlock(List<UserAccount> accounts) throws ValidationException {
		for (UserAccount u:accounts){
			u.setLocked(false);
			u.setLoginAttempts(0);
		}
		
		ebeanServer.save(accounts);
	}

	/* (non-Javadoc)
	 * @see com.imilia.server.service.impl.DomainObjectServiceImpl#save(com.imilia.server.domain.DomainObject)
	 */
	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor={Exception.class})
	public void save(UserAccount domainObject) throws ValidationException {
		// check for duplicate username
		final UserAccount userAccount = ebeanServer.find(UserAccount.class).where().eq(
				"username", domainObject.getUsername()).findUnique();
		if ((domainObject.isNew() && userAccount != null) || (!domainObject.isNew() && userAccount != null && userAccount.getOid() != domainObject.getOid())){
				// username already in use
				throw ValidationException.createFieldError(domainObject,
						"UserAccount", "username", domainObject.getUsername(), false, 
						new String[]{"com.imilia.server.domain.common.UserAccount.username.used"}, 
							null, 
						"");
		}
		super.save(domainObject);
	}
}
