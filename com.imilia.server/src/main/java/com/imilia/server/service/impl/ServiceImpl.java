/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 * 
 * Created on: Mar 5, 2009
 * Created by: emcgreal
 */
package com.imilia.server.service.impl;

import java.util.Collection;
import java.util.Set;

import javax.validation.ConstraintViolation;

import org.springframework.validation.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;

import com.avaje.ebean.TxIsolation;
import com.avaje.ebean.TxScope;
import com.imilia.server.domain.validation.ConditionalValidation;
import com.imilia.server.domain.validation.ConditionalValidator;
import com.imilia.server.service.Service;
import com.imilia.server.validation.ValidationException;

/**
 * The Class ServiceImpl.
 */
public abstract class ServiceImpl implements Service {
	
	protected final TxScope txScopeRequiredReadOnly = TxScope.required().setReadOnly(true);
	
	protected final TxScope txScopeRequiredWrite = 
		TxScope.required().
			setIsolation(TxIsolation.READ_COMMITED).
			setReadOnly(false).
			setRollbackFor(Exception.class);
	

	/** Logger used to log warnings, errors and debug messages. */
	private final static Logger logger = LoggerFactory.getLogger(ServiceImpl.class);

	/** The bean validator. */
	@Autowired @Qualifier("jsr303Validator") 
	private Validator beanValidator;

	@Autowired
	private ConditionalValidator conditionalValidator; 
	

	/**
	 * Instantiates a new service impl.
	 */
	public ServiceImpl() {
		super();
	}

	/**
	 * Gets the bean validator.
	 * 
	 * @return the beanValidator
	 */
	public Validator getBeanValidator() {
		return beanValidator;
	}

	/**
	 * Sets the bean validator.
	 * 
	 * @param beanValidator
	 *            the beanValidator to set
	 */
	public void setBeanValidator(Validator beanValidator) {
		this.beanValidator = beanValidator;
	}

	/**
	 * Validate.
	 * <p>
	 * Validates the target object using the Spring based validation annotations
	 * </p>
	 * 
	 * @param targetObject
	 *            the target object
	 * 
	 * @throws ValidationException
	 *             thrown if the targetObject is invalid
	 */
	public void validate(Object targetObject) throws ValidationException {
		// Create a default Error object and validate
		if (targetObject != null){
			validate(targetObject, new BeanPropertyBindingResult(targetObject, targetObject.getClass()
					.getSimpleName()));
		}
	}

	public void validateCollection(Collection<?> objects) throws ValidationException {
		if (objects != null){
			for (Object o:objects){
				// Create a default Error object and validate
				validate(o, new BeanPropertyBindingResult(o, o.getClass()
					.getSimpleName()));
			}
		}
	}
	/**
	 * Validate.
	 * <p>
	 * Validates the target object using the Spring based validation annotations
	 * </p>
	 * 
	 * @param targetObject
	 *            the target object
	 * @param e
	 *            the errors object
	 * 
	 * @throws ValidationException
	 *             the validation exception
	 */
	public void validate(Object targetObject, Errors e) throws ValidationException {
		// Validate the target object
		beanValidator.validate(targetObject, e);
		
		if (e.hasErrors()) {
			if (logger.isDebugEnabled()) {
				logger.debug("Validation failed: " + e);
			}
			throw new ValidationException(e, targetObject);
		}
		
		conditionalValidate(targetObject, e);
	}
	
	public void conditionalValidate(Object targetObject, Errors e) throws ValidationException {
		if (targetObject instanceof ConditionalValidation){
			// Validate the target object
			conditionalValidator.validate(targetObject, e);
			
			if (e.hasErrors()) {
				if (logger.isDebugEnabled()) {
					logger.debug("Validation failed: " + e);
				}
				throw new ValidationException(e, targetObject);
			}
		}
	}

	public ConditionalValidator getConditionalValidator() {
		return conditionalValidator;
	}

	public void setConditionalValidator(ConditionalValidator conditionalValidator) {
		this.conditionalValidator = conditionalValidator;
	}

}
