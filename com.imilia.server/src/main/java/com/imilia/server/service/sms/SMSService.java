/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2010 - all rights reserved
 * Created on: May 19, 2010
 * Created by: emcgreal
 */
package com.imilia.server.service.sms;

import java.util.HashMap;
import java.util.Locale;

import com.imilia.server.domain.sms.SMSMessage;
import com.imilia.server.service.Service;
import com.imilia.server.validation.ValidationException;

/**
 * The Interface SMSService.
 */
public interface SMSService extends Service {
	
	/**
	 * Gets the.
	 * 
	 * @param providerName the provider name
	 * 
	 * @return the sMS provider service
	 */
	public SMSProviderService get(String providerName);
	
	/**
	 * Send.
	 * 
	 * @param smsMessage the sms message
	 * 
	 * @throws ValidationException the validation exception
	 * @throws SMSException the SMS exception
	 */
	public String send(SMSMessage smsMessage) throws ValidationException, SMSException;
	
	/**
	 * Send sms.
	 *
	 * @param provider the provider
	 * @param map the map
	 * @return the string
	 * @throws SMSException the sMS exception
	 */
	public String send(String provider, HashMap<String, String> map) throws SMSException;
	
	/**
	 * Validate mobile number.
	 *
	 * @param mobileNumber the mobile number
	 * @param defaultRegio the default region e.g. "Germany" (DE)
	 * @return true, if successful
	 */
	public boolean validateMobileNumber(String mobileNumber, Locale defaultRegion);
}
