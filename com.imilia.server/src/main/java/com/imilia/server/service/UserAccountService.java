/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 * 
 * Created on: Mar 5, 2009
 * Created by: emcgreal
 */
package com.imilia.server.service;

import java.util.List;

import javax.persistence.PersistenceException;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.imilia.server.domain.access.AuthenticationRequest;
import com.imilia.server.domain.common.UserAccount;
import com.imilia.server.validation.ValidationException;

/**
 * The Interface UserAccountService.
 */
public interface UserAccountService extends UserDetailsService, DomainObjectService<UserAccount> {

	/**
	 * Loads the user account, person and domain locale.
	 * 
	 * @param userAccount the user account
	 */
	public void load(UserAccount userAccount);
	
	/**
	 * Load user by email.
	 * 
	 * @param email the email
	 * 
	 * @return the user account
	 * 
	 * @throws PersistenceException the persistence exception
	 */
	public UserAccount loadUserByEmail(String email) throws PersistenceException;
	
	/**
	 * Load user by name.
	 * 
	 * @param username the username
	 * 
	 * @return the user account
	 * 
	 * @throws PersistenceException the persistence exception
	 */
	public UserAccount loadUserByName(String username) throws PersistenceException;
	
	
	/**
	 * Find user accounts with username like.
	 *
	 * @param username the username
	 * @return the list
	 */
	public List<UserAccount> findUserAccountsWithUsernameLike(String username);
	
	
	/**
	 * Unlock.
	 *
	 * @param accounts the accounts
	 * @throws ValidationException the validation exception
	 */
	public void unlock(List<UserAccount> accounts) throws ValidationException;
	
	/**
	 * Login succeeded.
	 *
	 * @param userAccount the user account
	 * @param authenticationRequest the authentication request
	 */
	public void loginSucceeded(UserAccount userAccount, final AuthenticationRequest authenticationRequest);
	
	/**
	 * Login failed.
	 *
	 * @param authenticationRequest the authentication request
	 */
	public void loginFailed( final AuthenticationRequest authenticationRequest);

}
