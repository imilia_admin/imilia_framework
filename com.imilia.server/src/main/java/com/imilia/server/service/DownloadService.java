/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 *
 * Created on: May 4, 2009
 * Created by: eddiemcgreal
 */
package com.imilia.server.service;


/**
 * The Interface DownloadService.
 */
public interface DownloadService extends Service {
	
	/**
	 * Download.
	 * 
	 * @param download the download
	 */
	public void download(Download download);
}
