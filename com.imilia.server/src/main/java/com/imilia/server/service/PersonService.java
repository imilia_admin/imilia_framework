/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 * 
 * Created on: Mar 5, 2009
 * Created by: emcgreal
 */
package com.imilia.server.service;

import java.util.Iterator;

import com.imilia.server.domain.common.Person;

/**
 * The Interface PersonService.
 */
public interface PersonService extends DomainObjectService<Person> {
	public void save(Iterator<Person> people);
}
