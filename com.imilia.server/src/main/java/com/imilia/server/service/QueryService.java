/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2014 - all rights reserved
 * Created on: Feb 27, 2014
 * Created by: emcgreal
 */
package com.imilia.server.service;

import com.imilia.server.domain.CrudAware;
import com.imilia.server.domain.query.QueryResult;
import com.imilia.server.domain.query.entities.QueryTemplate;
import com.imilia.server.domain.query.execution.QueryExecution;

/**
 * The Interface QueryService.
 */
public interface QueryService extends Service {
	
	/**
	 * Query - Execute the template.
	 *
	 * @param <T> the generic type
	 * @param ctx the ctx
	 * @param result the result
	 */
	public <T extends CrudAware> void query( QueryExecution<T> ctx, QueryResult<T> result);

	/**
	 * Gets the query template.
	 *
	 * @param name the name
	 * @return the query template
	 */
	public QueryTemplate getQueryTemplate(String name);
	
}
