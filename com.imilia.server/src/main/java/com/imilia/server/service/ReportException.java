/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 *
 * Created on: Apr 24, 2009
 * Created by: eddiemcgreal
 */
package com.imilia.server.service;

/**
 * The Class ReportException.
 */
public class ReportException extends Exception {

	/**
	 * Instantiates a new report exception.
	 */
	public ReportException() {
		super();
	}

	/**
	 * Instantiates a new report exception.
	 * 
	 * @param message the message
	 * @param t the cause
	 */
	public ReportException(String message, Throwable t) {
		super(message, t);
	}

	/**
	 * Instantiates a new report exception.
	 * 
	 * @param message the message
	 */
	public ReportException(String message) {
		super(message);
	}

	/**
	 * Instantiates a new report exception.
	 * 
	 * @param t the cause
	 */
	public ReportException(Throwable t) {
		super(t);
	}

}
