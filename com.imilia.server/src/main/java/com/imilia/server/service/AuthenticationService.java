/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 * 
 * Created on: Mar 10, 2009
 * Created by: emcgreal
 */
package com.imilia.server.service;


import org.springframework.context.MessageSource;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

import com.imilia.server.domain.access.AuthenticationRequest;
import com.imilia.server.domain.common.UserAccount;
import com.imilia.server.domain.password.PasswordCandidate;
import com.imilia.server.service.impl.PasswordStrengthChecker;
import com.imilia.server.validation.ValidationException;

/**
 * The Interface AuthenticationService is used to authenticate a user
 * 
 * <p>
 * The default implementation uses a UserAccountService to check the
 * username and password. However, an LDAP implementation may also be used
 * and this is configured using spring for the concrete application.
 * </p>
 */
public interface AuthenticationService extends Service {

	/**
	 * Login. Calls authenticate() and if successful sets the authenticated UserAccount in the current user
	 * 
	 * @param authenticationRequest the authentication request
	 * 
	 * @return the authentication
	 * 
	 * @throws AuthenticationException the authentication exception
	 */
	public Authentication login(AuthenticationRequest authenticationRequest)
			throws AuthenticationException;

	/**
	 * Authenticate. Attempts 
	 * 
	 * @param authenticationRequest the authentication request
	 * 
	 * @return the authentication
	 * 
	 * @throws AuthenticationException the authentication exception
	 */
	public Authentication authenticate(AuthenticationRequest authenticationRequest)
		throws AuthenticationException;
	
	/**
	 * Sets the authenticated.
	 * 
	 * @param authentication the new authenticated
	 */
	public void setAuthenticated(Authentication authentication);
	
	/**
	 * Encodes the password using the configured encoding with corresponding
	 * salt etc.
	 * 
	 * @param password the password
	 * 
	 * @return the encoded string
	 * 
	 * @throws ValidationException the validation exception if null or does not meet strength
	 */
	public String encode(String password) throws ValidationException;

	/**
	 * Change password.
	 *
	 * @param userAccount the user account
	 * @param candidate the candidate
	 * @param passwordStrengthChecker the password strength checker
	 * @param messageSource the message source
	 * @throws ValidationException the validation exception
	 */
	public void changePassword(UserAccount userAccount, 
				PasswordCandidate candidate, 
				PasswordStrengthChecker passwordStrengthChecker,
				MessageSource messageSource)
			throws ValidationException;
	

	
}
