/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2014 - all rights reserved
 * Created on: Mar 14, 2014
 * Created by: emcgreal
 */
package com.imilia.server.service;

import java.io.InputStream;
import java.util.List;

import com.imilia.server.domain.query.entities.QueryView;
import com.imilia.utils.i18n.DomainLocale;

/**
 * The Interface QueryViewService.
 */
public interface QueryViewService extends DomainObjectService<QueryView> {
}
