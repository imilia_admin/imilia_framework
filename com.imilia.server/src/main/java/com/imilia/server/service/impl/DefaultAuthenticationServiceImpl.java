/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 *
 * Created on: Mar 10, 2009
 * Created by: emcgreal
 */
package com.imilia.server.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.SaltSource;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.imilia.server.domain.CurrentUser;
import com.imilia.server.domain.access.AuthenticationRequest;
import com.imilia.server.domain.common.UserAccount;
import com.imilia.server.domain.password.PasswordCandidate;
import com.imilia.server.service.AuthenticationService;
import com.imilia.server.service.UserAccountService;
import com.imilia.server.validation.ValidationException;

/**
 * The Class DefaultAuthenticationServiceImpl.
 */
public class DefaultAuthenticationServiceImpl extends ServiceImpl 
	implements AuthenticationService {

	/** Logger used to log warnings, errors and debug messages. */
	private final static Logger logger = LoggerFactory.getLogger(DefaultAuthenticationServiceImpl.class);

	// IOC --------------------------------------------------------------------
	/** The authentication manager. */
	private AuthenticationManager authenticationManager;

	/** The user account service. */
	private UserAccountService userAccountService;

	/** The password encoder. */
	private PasswordEncoder passwordEncoder;

	/** The salt source. */
	private SaltSource saltSource;

	/* (non-Javadoc)
	 * @see com.imilia.server.service.AuthenticationService#authenticate(java.lang.String, java.lang.String)
	 */
	public Authentication authenticate(final AuthenticationRequest authenticationRequest)
		throws AuthenticationException{
		
		try{
			final UsernamePasswordAuthenticationToken passwordAuthenticationToken =
				new UsernamePasswordAuthenticationToken(
						authenticationRequest.getUsername(), authenticationRequest.getPassword());
	
			final Authentication authentication = authenticationManager
					.authenticate(passwordAuthenticationToken);
			
			final UserAccount userAccount = (UserAccount) authentication.getPrincipal();
			
			if (!userAccount.isEnabled() || userAccount.isDeleted()){
				logger.info("Account: " + authenticationRequest.getUsername() + 
						" (OID=" +
						userAccount.getOid()+ 
						") is not accessible");
				throw new DisabledException("Access denied");
			}
			setAuthenticated(authentication);
			
			userAccountService.loginSucceeded(userAccount, authenticationRequest);
			return authentication;
		}catch(AuthenticationException aex){
			userAccountService.loginFailed(authenticationRequest);
			throw aex;
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.imilia.server.service.AuthenticationService#login(java.lang
	 * .String, java.lang.String)
	 */
	public Authentication login(final AuthenticationRequest authenticationRequest)
			throws AuthenticationException {
	
		final Authentication authentiction = authenticate(authenticationRequest);

		// set the current user to point to the user account
		UserAccount userAccount = (UserAccount) authentiction.getPrincipal();

		
		if (logger.isDebugEnabled()){
			logger.debug("Sucessfully authenticated: " + authenticationRequest.getUsername());
		}
		return authentiction;
	}
	
	/* (non-Javadoc)
	 * @see com.imilia.server.service.AuthenticationService#setAuthenticated(org.springframework.security.core.Authentication)
	 */
	public void setAuthenticated(Authentication authentication){
		SecurityContextHolder.getContext().setAuthentication(authentication);
	}

	
	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.imilia.server.service.AuthenticationService#changePassword(com.imilia
	 * .server.domain.common.UserAccount,
	 * com.imilia.server.domain.password.PasswordCandidate)
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public void changePassword(UserAccount userAccount, PasswordCandidate candidate,
			PasswordStrengthChecker passwordStrengthChecker,
			MessageSource messageSource)
			throws ValidationException {
		if (logger.isDebugEnabled()) {
			logger.debug("Attempting to change password for useraccount: "
					+ userAccount.getUsername());
		}
		// Validate the candiate
		candidate.setBeanValidator(getBeanValidator());
		candidate.validate();

		// Check the password strength conforms to the policy
		candidate.checkPasswordStrength(passwordStrengthChecker, 
				messageSource, 
				userAccount.getPerson().getDomainLocale().getLocale());
		
		// Set the encoded password
		String encodedPassword = encode(candidate.getPassword1());
		
		// Check that the current it not the same as the new
		if (encodedPassword.equals(userAccount.getPassword())){
			throw ValidationException.createFieldError(candidate,
					"PasswordCandiate", "password1", candidate.getPassword1(), false, 
					new String[]{"com.imilia.server.domain.password.PasswordCandidate.password.used"}, 
						null, 
					"");
		}
		userAccount.setPassword(encodedPassword);

		// Save the user account
		userAccountService.save(userAccount);

		if (logger.isDebugEnabled()) {
			logger.debug("Successfully changed password for useraccount: "
					+ userAccount.getUsername());
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.imilia.server.service.AuthenticationService#encode(java.lang.String)
	 */
	public String encode(String password) {
		final String encoded = passwordEncoder.encodePassword(password, saltSource.getSalt(null));

		return encoded;
	}

	// Getter and setters -----------------------------------------------------
	/**
	 * Gets the user account service.
	 * 
	 * @return the userAccountService
	 */
	public UserAccountService getUserAccountService() {
		return userAccountService;
	}

	/**
	 * Sets the user account service.
	 * 
	 * @param userAccountService the userAccountService to set
	 */
	public void setUserAccountService(UserAccountService userAccountService) {
		this.userAccountService = userAccountService;
	}

	/**
	 * Gets the password encoder.
	 * 
	 * @return the passwordEncoder
	 */
	public PasswordEncoder getPasswordEncoder() {
		return passwordEncoder;
	}

	/**
	 * Sets the password encoder.
	 * 
	 * @param passwordEncoder the passwordEncoder to set
	 */
	public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
		this.passwordEncoder = passwordEncoder;
	}

	/**
	 * Gets the authentication manager.
	 * 
	 * @return the authenticationManager
	 */
	public AuthenticationManager getAuthenticationManager() {
		return authenticationManager;
	}

	/**
	 * Sets the authentication manager.
	 * 
	 * @param authenticationManager the authenticationManager to set
	 */
	public void setAuthenticationManager(AuthenticationManager authenticationManager) {
		this.authenticationManager = authenticationManager;
	}

	/**
	 * Gets the salt source.
	 * 
	 * @return the saltSource
	 */
	public SaltSource getSaltSource() {
		return saltSource;
	}

	/**
	 * Sets the salt source.
	 * 
	 * @param saltSource the saltSource to set
	 */
	public void setSaltSource(SaltSource saltSource) {
		this.saltSource = saltSource;
	}
}
