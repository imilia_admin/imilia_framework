/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2014 - all rights reserved
 * Created on: Feb 27, 2014
 * Created by: emcgreal
 */
package com.imilia.server.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.MappedSuperclass;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.csvreader.CsvReader;
import com.imilia.server.domain.Crud;
import com.imilia.server.domain.access.Role;
import com.imilia.server.domain.query.Query;
import com.imilia.server.domain.query.entities.QueryRestriction;
import com.imilia.server.service.QueryRestrictionService;
import com.imilia.utils.ResourceBean;
import com.imilia.utils.classes.ClassUtils;
import com.imilia.utils.i18n.DomainLocale;
import com.imilia.utils.strings.StringUtils;

/**
 * The Class RestrictionServiceImpl.
 */
public class RestrictionServiceImpl extends	DomainObjectServiceImpl<QueryRestriction> 
	implements QueryRestrictionService {

	/** The Constant logger. */
	private final static Logger logger = Logger.getLogger(RestrictionServiceImpl.class);
	
	/** The Constant ROLE_IDX. */
	private final static int ROLE_IDX = 0;
	
	/** The Constant QUERY_IDX. */
	private final static int QUERY_IDX = 1;
	
	/** The Constant RESTRICTION_IDX. */
	private final static int RESTRICTION_IDX = 2;
	
	/** The Constant CRUD_IDX. */
	private final static int CRUD_IDX = 3;
	
	/** The resource loader. */
	@Autowired
	private ResourceLoader resourceLoader;
	
	/* (non-Javadoc)
	 * @see com.imilia.server.service.RestrictionService#getRestrictions(com.imilia.server.domain.query.QueryTemplate, java.util.List)
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<QueryRestriction> getRestrictions(Query<?> queryTemplate,
			List<Role> roles) {
		String queryName = queryTemplate.getName();
		
		String className = queryTemplate.getClazz().getName();
		
		Set<String> names = new HashSet<>();
		names.add(queryName);
		
		if (queryName.startsWith(className)){
			names.add(className);
			List<Class<?>> superTypes = ClassUtils.getSuperClasses(queryTemplate.getClazz(), Entity.class, MappedSuperclass.class);
			
			for (Class<?> superType:superTypes){
				names.add(superType.getName());
			}
		}
		
		return getEbeanServer().find(QueryRestriction.class).where().in("queryName", names).in("restrictionRoles.role", roles).findList();
	}

	/* (non-Javadoc)
	 * @see com.imilia.server.service.RestrictionService#importQueryRestrictions(com.csvreader.CsvReader, com.imilia.server.domain.i18n.DomainLocale)
	 */
	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<QueryRestriction> importQueryRestrictions(CsvReader reader,
			DomainLocale domainLocale) throws Exception {
		List<QueryRestriction> restrictions = new ArrayList<>();
//		
//		final HashMap<String, Role> roles = new HashMap<>();
//		
//		for (Role r:getEbeanServer().find(Role.class).findList()){
//			roles.put(r.getAuthority(), r);
//		}
//		
//		
//		if (reader.readHeaders()){
//			while(reader.readRecord()){
//				
//				String[] row = reader.getValues();
//				
//				String restrictionText = row[RESTRICTION_IDX];
//				restrictionText = StringUtils.empty2Null(restrictionText);
//				if (restrictionText != null && !restrictionText.startsWith("{")){
//					restrictionText = new String(new ResourceBean(resourceLoader.getResource(restrictionText)).getBytes());
//				}
//				
//				QueryRestriction restriction = 	new QueryRestriction(
//						row[QUERY_IDX],
//						restrictionText,
//						new Crud(row[CRUD_IDX]));
//				restrictions.add(restriction);
//
//				final String[] roleNames = row[ROLE_IDX].split(",");
//
//				for (String r:roleNames){
//					Role role = roles.get(r.trim());
//
//					if (role != null){
//						restriction.addRole(role);
//					}else{
//						logger.error("Role not found:" + r);
//					}
//				}
//			}
//		}
//		
//		getEbeanServer().save(restrictions);
		return restrictions;
	}

	/**
	 * Gets the resource loader.
	 *
	 * @return the resource loader
	 */
	public ResourceLoader getResourceLoader() {
		return resourceLoader;
	}

	/**
	 * Sets the resource loader.
	 *
	 * @param resourceLoader the new resource loader
	 */
	public void setResourceLoader(ResourceLoader resourceLoader) {
		this.resourceLoader = resourceLoader;
	}
}
