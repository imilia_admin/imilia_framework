/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2013 - all rights reserved
 *
 * Created on: 18.01.2013
 * Created by: emcgreal
 */
package com.imilia.utils.csv;

import java.util.List;

import com.csvreader.CsvReader;

/**
 * The Class ColChangeCsvRecordChanger.
 */
public class ColChangeCsvRecordChanger implements CsvRecordCursor {

	/** The col to check for changes indicating a new record */
	private int col;
	
	/**
	 * Instantiates a new col change csv record changer.
	 *
	 * @param col the col
	 */
	public ColChangeCsvRecordChanger(int col) {
		super();
		this.col = col;
	}



	/* (non-Javadoc)
	 * @see com.imilia.utils.csv.CsvRecordCursor#next(com.csvreader.CsvReader, java.util.List)
	 */
	@Override
	public boolean next(CsvReader reader, List<String[]> record)
			throws Exception {
		record.clear();
		
		String value = null;
		do{
			String[] row = reader.getValues();
			
			if (row.length > col){
				if (value == null){
					value = row[col];
				}else if (!value.equals(row[col])){
					return !record.isEmpty();
				}
				record.add(row);
			}
		}
		while(reader.readRecord());
		
		return !record.isEmpty();
	}



	/**
	 * Gets the col.
	 *
	 * @return the col
	 */
	public int getCol() {
		return col;
	}



	/**
	 * Sets the col.
	 *
	 * @param col the new col
	 */
	public void setCol(int col) {
		this.col = col;
	}

}
