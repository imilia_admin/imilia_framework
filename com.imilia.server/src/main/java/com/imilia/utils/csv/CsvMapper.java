/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2013 - all rights reserved
 *
 * Created on: Jan 16, 2013
 * Created by: emcgreal
 */
package com.imilia.utils.csv;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.FieldError;

import com.csvreader.CsvReader;
import com.csvreader.CsvWriter;
import com.imilia.server.validation.ValidationException;

/**
 * The Class CsvMapper.
 */
public class CsvMapper {

	/** The Constant logger. */
	private final static Logger logger = LoggerFactory.getLogger(CsvMapper.class);
	
	/** The Constant CSV_FILE_FORMAT_ERROR. */
	private static final String CSV_MISSING_HEADER = "com.imilia.utils.csv.CsvMapper.missing.header";
	
	/** The input delimiter. */
	private char inputDelimiter = ';';
	
	/** The output delimiter. */
	private char outputDelimiter = ';';
	
	/** The input encoding. */
	private String inputEncoding = "UTF-8";
	
	/** The output encoding. */
	private String outputEncoding = "UTF-8";
	
	/**
	 * Creates the reader.
	 *
	 * @param inputFile the input file
	 * @return the csv reader
	 * @throws Exception the exception
	 */
	public CsvReader createReader(String inputFile) throws Exception{
		final InputStream in =  new FileInputStream( new File(inputFile));
		final InputStreamReader inReader = new InputStreamReader(in, Charset.forName(inputEncoding));
		return new CsvReader(inReader, inputDelimiter);
	}
	
	/**
	 * Creates the writer.
	 *
	 * @param outputFile the output file
	 * @return the csv writer
	 * @throws Exception the exception
	 */
	public CsvWriter createWriter(String outputFile) throws Exception{
		final OutputStream out =  new FileOutputStream( new File(outputFile));
		return new CsvWriter(out, ';', Charset.forName(outputEncoding));
	}
	
	/**
	 * Gets the input delimiter.
	 *
	 * @return the input delimiter
	 */
	public char getInputDelimiter() {
		return inputDelimiter;
	}
	
	/**
	 * Sets the input delimiter.
	 *
	 * @param inputDelimiter the new input delimiter
	 */
	public void setInputDelimiter(char inputDelimiter) {
		this.inputDelimiter = inputDelimiter;
	}
	
	/**
	 * Gets the output delimiter.
	 *
	 * @return the output delimiter
	 */
	public char getOutputDelimiter() {
		return outputDelimiter;
	}
	
	/**
	 * Sets the output delimiter.
	 *
	 * @param outputDelimiter the new output delimiter
	 */
	public void setOutputDelimiter(char outputDelimiter) {
		this.outputDelimiter = outputDelimiter;
	}
	
	/**
	 * Validate headers.
	 *
	 * @param headers the headers
	 * @param required the required
	 * @throws ValidationException the validation exception
	 */
	protected void validateHeaders(String[] headers, String[] required) throws ValidationException {
		ArrayList<String> missingPaths = new ArrayList<String>();
		
		for (int i = 0; i < required.length; i++) {
			boolean missing = true;
			for (int j = 0; j < headers.length; j++) {
				if (headers[j].equals(required[i])) {
					missing = false;
					break;
				}
			}
			if (missing) missingPaths.add(required[i]);
		}
		
		if (!missingPaths.isEmpty()) {
			logger.debug("Invalid .csv file");
			
			FieldError o = new FieldError(headers.getClass().getCanonicalName(),
					missingPaths.toString(),
					headers,
					true,
					new String[] { CSV_MISSING_HEADER },
					new Object[] {
						missingPaths.toString()
					}, CSV_MISSING_HEADER);
			BeanPropertyBindingResult errors =
				new BeanPropertyBindingResult(headers, "paths");
			errors.addError(o);
			throw new ValidationException(errors, headers);
		}
	}
	
	/**
	 * Read rows.
	 *
	 * @param reader the reader
	 * @param rowCount2Read the row count2 read
	 * @param read the read
	 * @return true, if successful
	 * @throws Exception the exception
	 */
	protected boolean readRows(CsvReader reader, int rowCount2Read, List<String[]> read) throws Exception{
		read.clear();
		for (int i = 0;i < rowCount2Read;i++){
			if (reader.readRecord()){
				read.add(reader.getValues());
			}
		}
		
		return read.size() == rowCount2Read;
	}
}
