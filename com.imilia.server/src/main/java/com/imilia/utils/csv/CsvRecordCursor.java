/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2013 - all rights reserved
 *
 * Created on: 18.01.2013
 * Created by: emcgreal
 */
package com.imilia.utils.csv;

import java.util.List;

import com.csvreader.CsvReader;

/**
 * The Interface CsvRecordCursor.
 */
public interface CsvRecordCursor {
	
	/**
	 * Next.
	 *
	 * @param reader the reader
	 * @param record the record
	 * @return true, if successful
	 * @throws Exception the exception
	 */
	public boolean next(CsvReader reader,  List<String[]> record) throws Exception;
}
