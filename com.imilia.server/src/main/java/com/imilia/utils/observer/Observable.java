/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 * Created on: Jan 23, 2009
 * Created by: emcgreal
 */

package com.imilia.utils.observer;

import java.util.Iterator;

/**
 * The Interface Observable allows observers watch for changes
 * <p>
 * Typically an observable will notify all its observers when a 
 * particular event occurs. It contains a set of observers 
 * (no duplicates) and informs them (in no defined order) of
 * an event when fireEvent is called 
 * </p>
 */
public interface Observable {

	/**
	 * Adds the observer.
	 * 
	 * @param observer the observer
	 */
	public void addObserver(Observer observer);
	
	/**
	 * Remove observer.
	 * 
	 * @param observer the observer to remove
	 */
	public void removeObserver(Observer observer);

	/**
	 * Gets the observers.
	 * 
	 * @return the observers
	 */
	public Iterator<Observer> getObservers();
	
	/**
	 * Clear.
	 */
	public void removeAllObservers();
	
	/**
	 * Notify observers of the event (null)
	 */
	public void fireEvent();
	
	/**
	 * Notify observers of the event
	 * 
	 * @param event the event - null if no differentiation between events
	 */
	public void fireEvent(Object event);
}
