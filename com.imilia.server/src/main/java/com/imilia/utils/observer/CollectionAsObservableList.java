/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 * Created on: Jun 22, 2009
 * Created by: emcgreal
 */

package com.imilia.utils.observer;

import java.util.Collection;

import ognl.Ognl;
import ognl.OgnlException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



/**
 * The Class CollectionAsObservableList.
 *
 * <p>Use this class to wrap e.g. a set of objects that need to be sorted and
 * updated when the model changes. The observer must be a separate object so that
 * each has an unique hash id - list have the same hash if they contain the same content
 * </p>
 */
public class CollectionAsObservableList<T> extends ObservableArrayList<T>{

	/** The logger. */
	private final Logger logger = LoggerFactory.getLogger(CollectionAsObservableList.class);

	/** The model. */
	private final Object model;

	/** The path2 collection. */
	private final String path2Collection;

	/** The observer. */
	private final Observer observer;
	/**
	 * Instantiates a new collection as observable list.
	 *
	 * @param root the root
	 * @param path the path
	 */
	public CollectionAsObservableList(Object root, String path) {
		super();
		this.model = root;
		this.path2Collection = path;

		observer = new Observer(){

			public void update(Observable observable, Object arg) {
				clear();

				Collection<T> collection = getCollection();

				if (collection != null){
					addAll(collection);
				}

				fireEvent();
			}};
	}

	/**
	 * Gets the model.
	 *
	 * @return the model
	 */
	public Object getModel() {
		return model;
	}

	/**
	 * Gets the path2 collection.
	 *
	 * @return the path2 collection
	 */
	public String getPath2Collection() {
		return path2Collection;
	}

	/**
	 * Gets the observer.
	 *
	 * @return the observer
	 */
	public Observer getObserver(){
		return observer;
	}


	/**
	 * Gets the collection.
	 *
	 * @return the collection
	 */
	@SuppressWarnings("unchecked")
	private Collection<T> getCollection(){
		try {
			return (Collection<T>) Ognl.getValue(path2Collection, model);
		} catch (OgnlException e) {
			logger.warn("Failed to get collection", e);
		}
		return null;
	}
}
