/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2008 - all rights reserved
 * Created on: Jan 23, 2009
 * Created by: emcgreal
 */
package com.imilia.utils.observer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 * The Class ObservableWrappedList implements the list interface and
 * fires events when the list is changed in any way.
 * <p>
 * All methods of the list interface are implemented and delegated to the
 * the underlying list.
 * Any successful modifications to the list result in an event being fired.
 * </p>
 */
public class ObservableWrappedList<T> extends DefaultObservable
	implements	ObservableList<T> {

	/** The list. */
	private List<T> list;

	/** The fire events on change. */
	private boolean fireEventsOnChange = true;

	/**
	 * Instantiates a new observable wrapped list.
	 */
	public ObservableWrappedList() {
		super();
		list = new ArrayList<T>();
	}

	/**
	 * Instantiates a new observable wrapped list.
	 *
	 * @param list the list
	 */
	public ObservableWrappedList(List<T> list) {
		super();
		this.list = list;
	}

	/**
	 * Fire live change if fireEventsOnChange is set
	 *
	 * @see setFireEventsOnChange
	 */
	protected void fireLiveChange(){
		if (fireEventsOnChange){
			fireLiveChange();
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.util.List#add(java.lang.Object)
	 */
	public boolean add(T t) {
		boolean ret = list.add(t);

		if (ret) {
			fireLiveChange();
		}

		return ret;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.util.List#add(int, java.lang.Object)
	 */
	public void add(int index, T t) {
		list.add(index, t);
		fireLiveChange();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.util.List#addAll(java.util.Collection)
	 */
	public boolean addAll(Collection<? extends T> collection) {
		boolean ret = list.addAll(collection);

		if (ret) {
			fireLiveChange();
		}
		return ret;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.util.List#addAll(int, java.util.Collection)
	 */
	public boolean addAll(int index, Collection<? extends T> collection) {
		boolean ret = list.addAll(index, collection);

		if (ret){
			fireLiveChange();
		}
		return ret;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.util.List#clear()
	 */
	public void clear() {
		list.clear();
		fireLiveChange();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.util.List#contains(java.lang.Object)
	 */
	public boolean contains(Object t) {
		return list.contains(t);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.util.List#containsAll(java.util.Collection)
	 */
	public boolean containsAll(Collection<?> collection) {
		return list.contains(collection);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.util.List#get(int)
	 */
	public T get(int index) {
		return list.get(index);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.util.List#indexOf(java.lang.Object)
	 */
	public int indexOf(Object o) {
		return list.indexOf(o);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.util.List#isEmpty()
	 */
	public boolean isEmpty() {
		return list.isEmpty();
	}

	/**
	 * Checks if is not empty.
	 *
	 * @return true, if is not empty
	 */
	public boolean isNotEmpty() {
		return !list.isEmpty();
	}
	
	/*
	 * (non-Javadoc)
	 *
	 * @see java.util.List#iterator()
	 */
	public Iterator<T> iterator() {
		return list.iterator();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.util.List#lastIndexOf(java.lang.Object)
	 */
	public int lastIndexOf(Object o) {
		return list.lastIndexOf(o);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.util.List#listIterator()
	 */
	public ListIterator<T> listIterator() {
		return list.listIterator();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.util.List#listIterator(int)
	 */
	public ListIterator<T> listIterator(int index) {
		return list.listIterator(index);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.util.List#remove(java.lang.Object)
	 */
	public boolean remove(Object o) {
		boolean ret = list.remove(o);

		if (ret) {
			fireLiveChange();
		}
		return ret;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.util.List#remove(int)
	 */
	public T remove(int index) {
		T t = list.remove(index);

		if (t != null) {
			fireLiveChange();
		}
		return t;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.util.List#removeAll(java.util.Collection)
	 */
	public boolean removeAll(Collection<?> collection) {
		boolean ret = list.removeAll(collection);

		if (ret) {
			fireLiveChange();
		}
		return ret;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.util.List#retainAll(java.util.Collection)
	 */
	public boolean retainAll(Collection<?> collection) {
		boolean ret = list.retainAll(collection);

		if (ret) {
			fireLiveChange();
		}
		return ret;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.util.List#set(int, java.lang.Object)
	 */
	public T set(int index, T o) {
		T t = list.set(index, o);

		if (t != null) {
			fireLiveChange();
		}
		return t;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.util.List#size()
	 */
	public int size() {
		return list.size();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.util.List#subList(int, int)
	 */
	public List<T> subList(int fromIndex, int toIndex) {
		return list.subList(fromIndex, toIndex);

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.util.List#toArray()
	 */
	public Object[] toArray() {
		return list.toArray();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.util.List#toArray(T[])
	 */
	@SuppressWarnings("hiding")
	public <T> T[] toArray(T[] array) {
		return list.toArray(array);
	}

	/**
	 * @return the fireEventsOnChange
	 */
	public boolean isFireEventsOnChange() {
		return fireEventsOnChange;
	}

	/**
	 * @param fireEventsOnChange the fireEventsOnChange to set
	 */
	public void setFireEventsOnChange(boolean fireEventsOnChange) {
		this.fireEventsOnChange = fireEventsOnChange;
	}
}
