/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 * Created on: Jan 23, 2009
 * Created by: emcgreal
 */
package com.imilia.utils.observer;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * The Class DefaultObservableModel.
 *
 * @author emcgreal
 */
public class DefaultObservable implements Observable {

	/** The Constant logger. */
	private final static Logger logger = LoggerFactory.getLogger(DefaultObservable.class);

	/** Set containing all observers. */
	private final List<Observer> observers;

	/** The hold fire. */
	private boolean holdFire = false;
	
	/**
	 * Instantiates a new default observable model.
	 */
	public DefaultObservable() {
		observers = new ArrayList<Observer>();
	}

	/**
	 * Instantiates a new default observable model with the observers
	 * @param observers
	 */
	public DefaultObservable(List<Observer> observers) {
		super();
		this.observers = observers;
	}

	/* (non-Javadoc)
	 * @see com.imilia.utils.observer.Observable#add(com.imilia.utils.observer.Observer)
	 */
	public void addObserver(Observer observer) {
		// Prevent duplicates
		if (!observers.contains(observer)){
			observers.add(observer);
		}
	}

	/* (non-Javadoc)
	 * @see com.imilia.utils.observer.Observable#fireEvent()
	 */
	public void fireEvent() {
		fireEvent(null);
	}

	/* (non-Javadoc)
	 * @see com.imilia.utils.observer.Observable#fireEvent(java.lang.Object)
	 */
	public void fireEvent(Object event) {
		if (!holdFire){
			// Copy the set as observers may remove themselves from during the call
			// thus throwing a java.util.ConcurrentModificationException
			Set<Observer> copy = new HashSet<Observer>(observers);
			for (Observer o:copy){
				o.update(this, event);
			}
		}else{
			logger.debug("Holding fire");
		}
	}

	/* (non-Javadoc)
	 * @see com.imilia.utils.observer.Observable#remove(com.imilia.utils.observer.Observer)
	 */
	public void removeObserver(Observer observer) {
		observers.remove(observer);
	}

	public Iterator<Observer> getObservers() {
		return observers.iterator();
	}

	/* (non-Javadoc)
	 * @see com.imilia.utils.observer.Observable#removeAllObservers()
	 */
	public void removeAllObservers() {
		observers.clear();
	}

	public boolean isHoldFire() {
		return holdFire;
	}

	public void setHoldFire(boolean holdFire) {
		this.holdFire = holdFire;
	}
}
