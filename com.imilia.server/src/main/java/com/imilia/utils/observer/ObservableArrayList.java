/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 * Created on: Jan 23, 2009
 * Created by: emcgreal
 */
package com.imilia.utils.observer;

import java.util.ArrayList;
import java.util.Iterator;


/**
 * The Class ObservableList extends a list as observable
 * <p>
 * Useful class when dealing with list that are generated by
 * and should be observed but not on a method level. There
 * is no automatic firing of an event if the list is changed.
 * </p>
 *
 * @param <T> the generic type
 */
public class ObservableArrayList<T> extends ArrayList<T> implements ObservableList<T>{
	
	/** The default observable. */
	private DefaultObservable defaultObservable = new DefaultObservable();

	/* (non-Javadoc)
	 * @see com.imilia.utils.observer.Observable#addObserver(com.imilia.utils.observer.Observer)
	 */
	public void addObserver(Observer observer) {
		defaultObservable.addObserver(observer);
	}

	/* (non-Javadoc)
	 * @see com.imilia.utils.observer.Observable#fireEvent()
	 */
	public void fireEvent() {
		final Iterator<Observer> iter = defaultObservable.getObservers();
		while(iter.hasNext()){
			iter.next().update(this, null);
		}
	}

	/* (non-Javadoc)
	 * @see com.imilia.utils.observer.Observable#fireEvent(java.lang.Object)
	 */
	public void fireEvent(Object event) {
		final Iterator<Observer> iter = defaultObservable.getObservers();
		while(iter.hasNext()){
			iter.next().update(this, event);
		}
	}

	/* (non-Javadoc)
	 * @see com.imilia.utils.observer.Observable#removeObserver(com.imilia.utils.observer.Observer)
	 */
	public void removeObserver(Observer observer) {
		defaultObservable.removeObserver(observer);
	}

	/* (non-Javadoc)
	 * @see com.imilia.utils.observer.Observable#getObservers()
	 */
	public Iterator<Observer> getObservers() {
		return defaultObservable.getObservers();
	}

	/* (non-Javadoc)
	 * @see com.imilia.utils.observer.Observable#removeAllObservers()
	 */
	public void removeAllObservers() {
		defaultObservable.removeAllObservers();
	}
	
	/**
	 * Checks if is not empty.
	 *
	 * @return true, if is not empty
	 */
	public boolean isNotEmpty(){
		return !isEmpty();
	}
}
