/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 * Created on: Jan 23, 2009
 * Created by: emcgreal
 */
package com.imilia.utils.observer;

/**
 * The Interface Observer allows an observer to observe an observable 
 * <p>
 * This is one of the basic interfaces used to implement the observer 
 * pattern {@link http://en.wikipedia.org/wiki/Observer_pattern}.  
 * </p>
 */
public interface Observer {
	
	/**
	 * Update.
	 * 
	 * @param observable the observable
	 * @param arg the arg
	 */
	public void update(final Observable observable, Object arg);
}
