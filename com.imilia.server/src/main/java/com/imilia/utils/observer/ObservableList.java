package com.imilia.utils.observer;

import java.util.List;

public interface ObservableList<E> extends List<E>, Observable {

}
