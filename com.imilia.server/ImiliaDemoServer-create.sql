create table address (
  oid                       bigint auto_increment not null,
  created_on                datetime not null,
  created_by_oid            bigint not null,
  changed_on                datetime not null,
  changed_by_oid            bigint not null,
  object_status             integer,
  street                    varchar(255),
  house_number              varchar(20),
  zip                       varchar(30),
  area                      varchar(255),
  city                      varchar(255),
  country                   varchar(255),
  email                     varchar(255),
  url                       varchar(255),
  comment                   varchar(255),
  flags                     bigint,
  validation_class_spec_oid bigint,
  version                   integer not null,
  constraint ck_address_object_status check (object_status in (0,1,2)),
  constraint pk_address primary key (oid))
;

create table crud_target (
  oid                       bigint auto_increment not null,
  created_on                datetime not null,
  created_by_oid            bigint not null,
  changed_on                datetime not null,
  changed_by_oid            bigint not null,
  object_status             integer,
  name                      varchar(255),
  model_oid                 bigint not null,
  version                   integer not null,
  constraint ck_crud_target_object_status check (object_status in (0,1,2)),
  constraint pk_crud_target primary key (oid))
;

create table locale_text (
  oid                       bigint auto_increment not null,
  created_on                datetime not null,
  created_by_oid            bigint not null,
  changed_on                datetime not null,
  changed_by_oid            bigint not null,
  object_status             integer,
  domain_locale             integer,
  locale_text_holder_oid    bigint not null,
  text                      text,
  maxlength                 integer,
  version                   integer not null,
  constraint ck_locale_text_object_status check (object_status in (0,1,2)),
  constraint ck_locale_text_domain_locale check (domain_locale in (0,1)),
  constraint pk_locale_text primary key (oid))
;

create table locale_text_holder (
  oid                       bigint auto_increment not null,
  created_on                datetime not null,
  created_by_oid            bigint not null,
  changed_on                datetime not null,
  changed_by_oid            bigint not null,
  object_status             integer,
  position                  integer,
  version                   integer not null,
  constraint ck_locale_text_holder_object_status check (object_status in (0,1,2)),
  constraint pk_locale_text_holder primary key (oid))
;

create table model_crud (
  oid                       bigint auto_increment not null,
  created_on                datetime not null,
  created_by_oid            bigint not null,
  changed_on                datetime not null,
  changed_by_oid            bigint not null,
  object_status             integer,
  role_oid                  bigint,
  name                      varchar(255),
  version                   integer not null,
  constraint ck_model_crud_object_status check (object_status in (0,1,2)),
  constraint pk_model_crud primary key (oid))
;

create table person (
  oid                       bigint auto_increment not null,
  created_on                datetime not null,
  created_by_oid            bigint not null,
  changed_on                datetime not null,
  changed_by_oid            bigint not null,
  object_status             integer,
  alias                     varchar(50),
  title                     varchar(50),
  first_name                varchar(150),
  last_name                 varchar(150) not null,
  gender                    integer,
  dob                       date,
  comment                   varchar(255),
  address_oid               bigint,
  domain_locale             integer not null,
  flags                     bigint,
  version                   integer not null,
  constraint ck_person_object_status check (object_status in (0,1,2)),
  constraint ck_person_gender check (gender in (0,1)),
  constraint ck_person_domain_locale check (domain_locale in (0,1)),
  constraint pk_person primary key (oid))
;

create table phone_contact (
  oid                       bigint auto_increment not null,
  created_on                datetime not null,
  created_by_oid            bigint not null,
  changed_on                datetime not null,
  changed_by_oid            bigint not null,
  object_status             integer,
  number                    varchar(255),
  comment                   varchar(255),
  flags                     bigint,
  phone_number_type         integer,
  address_oid               bigint,
  version                   integer not null,
  constraint ck_phone_contact_object_status check (object_status in (0,1,2)),
  constraint ck_phone_contact_phone_number_type check (phone_number_type in (0,1,2,3,4,5,6)),
  constraint pk_phone_contact primary key (oid))
;

create table query_join (
  oid                       bigint auto_increment not null,
  created_on                datetime not null,
  created_by_oid            bigint not null,
  changed_on                datetime not null,
  changed_by_oid            bigint not null,
  object_status             integer,
  path                      varchar(255),
  visibility                integer,
  query_view_oid            bigint,
  version                   integer not null,
  constraint ck_query_join_object_status check (object_status in (0,1,2)),
  constraint ck_query_join_visibility check (visibility in (0,1,2,3)),
  constraint pk_query_join primary key (oid))
;

create table query_property (
  oid                       bigint auto_increment not null,
  created_on                datetime not null,
  created_by_oid            bigint not null,
  changed_on                datetime not null,
  changed_by_oid            bigint not null,
  object_status             integer,
  property_name             varchar(255),
  visibility                integer,
  flags                     bigint,
  query_view_oid            bigint,
  version                   integer not null,
  constraint ck_query_property_object_status check (object_status in (0,1,2)),
  constraint ck_query_property_visibility check (visibility in (0,1,2,3)),
  constraint pk_query_property primary key (oid))
;

create table query_restriction (
  oid                       bigint auto_increment not null,
  created_on                datetime not null,
  created_by_oid            bigint not null,
  changed_on                datetime not null,
  changed_by_oid            bigint not null,
  object_status             integer,
  crud_flags                bigint,
  restriction               text,
  query_template_oid        bigint,
  version                   integer not null,
  constraint ck_query_restriction_object_status check (object_status in (0,1,2)),
  constraint pk_query_restriction primary key (oid))
;

create table query_role_restriction (
  oid                       bigint auto_increment not null,
  created_on                datetime not null,
  created_by_oid            bigint not null,
  changed_on                datetime not null,
  changed_by_oid            bigint not null,
  object_status             integer,
  role_oid                  bigint,
  restriction_oid           bigint,
  version                   integer not null,
  constraint ck_query_role_restriction_object_status check (object_status in (0,1,2)),
  constraint pk_query_role_restriction primary key (oid))
;

create table query_template (
  oid                       bigint auto_increment not null,
  created_on                datetime not null,
  created_by_oid            bigint not null,
  changed_on                datetime not null,
  changed_by_oid            bigint not null,
  object_status             integer,
  name                      varchar(255),
  query_text                longtext,
  query_view_oid            bigint,
  version                   integer not null,
  constraint ck_query_template_object_status check (object_status in (0,1,2)),
  constraint uq_query_template_name unique (name),
  constraint pk_query_template primary key (oid))
;

create table query_view (
  oid                       bigint auto_increment not null,
  created_on                datetime not null,
  created_by_oid            bigint not null,
  changed_on                datetime not null,
  changed_by_oid            bigint not null,
  object_status             integer,
  name                      varchar(255),
  flags                     bigint,
  clazz                     varchar(255),
  parent_oid                bigint,
  version                   integer not null,
  constraint ck_query_view_object_status check (object_status in (0,1,2)),
  constraint pk_query_view primary key (oid))
;

create table role (
  oid                       bigint auto_increment not null,
  created_on                datetime not null,
  created_by_oid            bigint not null,
  changed_on                datetime not null,
  changed_by_oid            bigint not null,
  object_status             integer,
  authority                 varchar(255),
  name_oid                  bigint not null,
  visibility                integer,
  version                   integer not null,
  constraint ck_role_object_status check (object_status in (0,1,2)),
  constraint ck_role_visibility check (visibility in (0,1,2,3)),
  constraint uq_role_authority unique (authority),
  constraint pk_role primary key (oid))
;

create table subscription (
  subscription_type         integer(31) not null,
  oid                       bigint auto_increment not null,
  created_on                datetime not null,
  created_by_oid            bigint not null,
  changed_on                datetime not null,
  changed_by_oid            bigint not null,
  object_status             integer,
  person_oid                bigint,
  version                   integer not null,
  constraint ck_subscription_object_status check (object_status in (0,1,2)),
  constraint pk_subscription primary key (oid))
;

create table subscription_role (
  oid                       bigint auto_increment not null,
  created_on                datetime not null,
  created_by_oid            bigint not null,
  changed_on                datetime not null,
  changed_by_oid            bigint not null,
  object_status             integer,
  subscription_oid          bigint,
  role_oid                  bigint,
  version                   integer not null,
  constraint ck_subscription_role_object_status check (object_status in (0,1,2)),
  constraint pk_subscription_role primary key (oid))
;

create table user_account (
  oid                       bigint auto_increment not null,
  created_on                datetime not null,
  created_by_oid            bigint not null,
  changed_on                datetime not null,
  changed_by_oid            bigint not null,
  object_status             integer,
  username                  varchar(50) not null,
  password                  varchar(28) not null,
  password_last_changed     datetime,
  password_next_change      datetime,
  valid_to                  datetime,
  account_non_locked        tinyint(1) default 0,
  enabled                   tinyint(1) default 0,
  person_oid                bigint,
  last_login                datetime,
  login_attempts            integer,
  login_attempt             datetime,
  login_ip                  varchar(20),
  version                   integer not null,
  constraint ck_user_account_object_status check (object_status in (0,1,2)),
  constraint uq_user_account_username unique (username),
  constraint pk_user_account primary key (oid))
;

create table validation_class_spec (
  oid                       bigint auto_increment not null,
  created_on                datetime not null,
  created_by_oid            bigint not null,
  changed_on                datetime not null,
  changed_by_oid            bigint not null,
  object_status             integer,
  clazz                     varchar(255) not null,
  version                   integer not null,
  constraint ck_validation_class_spec_object_status check (object_status in (0,1,2)),
  constraint pk_validation_class_spec primary key (oid))
;

create table validation_property_spec (
  oid                       bigint auto_increment not null,
  created_on                datetime not null,
  created_by_oid            bigint not null,
  changed_on                datetime not null,
  changed_by_oid            bigint not null,
  object_status             integer,
  property_name             varchar(255) not null,
  flags                     bigint,
  validation_class_spec_oid bigint not null,
  version                   integer not null,
  constraint ck_validation_property_spec_object_status check (object_status in (0,1,2)),
  constraint pk_validation_property_spec primary key (oid))
;

alter table address add constraint fk_address_createdBy_1 foreign key (created_by_oid) references user_account (oid) on delete restrict on update restrict;
create index ix_address_createdBy_1 on address (created_by_oid);
alter table address add constraint fk_address_changedBy_2 foreign key (changed_by_oid) references user_account (oid) on delete restrict on update restrict;
create index ix_address_changedBy_2 on address (changed_by_oid);
alter table address add constraint fk_address_validationClassSpec_3 foreign key (validation_class_spec_oid) references validation_class_spec (oid) on delete restrict on update restrict;
create index ix_address_validationClassSpec_3 on address (validation_class_spec_oid);
alter table crud_target add constraint fk_crud_target_createdBy_4 foreign key (created_by_oid) references user_account (oid) on delete restrict on update restrict;
create index ix_crud_target_createdBy_4 on crud_target (created_by_oid);
alter table crud_target add constraint fk_crud_target_changedBy_5 foreign key (changed_by_oid) references user_account (oid) on delete restrict on update restrict;
create index ix_crud_target_changedBy_5 on crud_target (changed_by_oid);
alter table crud_target add constraint fk_crud_target_model_6 foreign key (model_oid) references model_crud (oid) on delete restrict on update restrict;
create index ix_crud_target_model_6 on crud_target (model_oid);
alter table locale_text add constraint fk_locale_text_createdBy_7 foreign key (created_by_oid) references user_account (oid) on delete restrict on update restrict;
create index ix_locale_text_createdBy_7 on locale_text (created_by_oid);
alter table locale_text add constraint fk_locale_text_changedBy_8 foreign key (changed_by_oid) references user_account (oid) on delete restrict on update restrict;
create index ix_locale_text_changedBy_8 on locale_text (changed_by_oid);
alter table locale_text add constraint fk_locale_text_localeTextHolder_9 foreign key (locale_text_holder_oid) references locale_text_holder (oid) on delete restrict on update restrict;
create index ix_locale_text_localeTextHolder_9 on locale_text (locale_text_holder_oid);
alter table locale_text_holder add constraint fk_locale_text_holder_createdBy_10 foreign key (created_by_oid) references user_account (oid) on delete restrict on update restrict;
create index ix_locale_text_holder_createdBy_10 on locale_text_holder (created_by_oid);
alter table locale_text_holder add constraint fk_locale_text_holder_changedBy_11 foreign key (changed_by_oid) references user_account (oid) on delete restrict on update restrict;
create index ix_locale_text_holder_changedBy_11 on locale_text_holder (changed_by_oid);
alter table model_crud add constraint fk_model_crud_createdBy_12 foreign key (created_by_oid) references user_account (oid) on delete restrict on update restrict;
create index ix_model_crud_createdBy_12 on model_crud (created_by_oid);
alter table model_crud add constraint fk_model_crud_changedBy_13 foreign key (changed_by_oid) references user_account (oid) on delete restrict on update restrict;
create index ix_model_crud_changedBy_13 on model_crud (changed_by_oid);
alter table model_crud add constraint fk_model_crud_role_14 foreign key (role_oid) references role (oid) on delete restrict on update restrict;
create index ix_model_crud_role_14 on model_crud (role_oid);
alter table person add constraint fk_person_createdBy_15 foreign key (created_by_oid) references user_account (oid) on delete restrict on update restrict;
create index ix_person_createdBy_15 on person (created_by_oid);
alter table person add constraint fk_person_changedBy_16 foreign key (changed_by_oid) references user_account (oid) on delete restrict on update restrict;
create index ix_person_changedBy_16 on person (changed_by_oid);
alter table person add constraint fk_person_address_17 foreign key (address_oid) references address (oid) on delete restrict on update restrict;
create index ix_person_address_17 on person (address_oid);
alter table phone_contact add constraint fk_phone_contact_createdBy_18 foreign key (created_by_oid) references user_account (oid) on delete restrict on update restrict;
create index ix_phone_contact_createdBy_18 on phone_contact (created_by_oid);
alter table phone_contact add constraint fk_phone_contact_changedBy_19 foreign key (changed_by_oid) references user_account (oid) on delete restrict on update restrict;
create index ix_phone_contact_changedBy_19 on phone_contact (changed_by_oid);
alter table phone_contact add constraint fk_phone_contact_address_20 foreign key (address_oid) references address (oid) on delete restrict on update restrict;
create index ix_phone_contact_address_20 on phone_contact (address_oid);
alter table query_join add constraint fk_query_join_createdBy_21 foreign key (created_by_oid) references user_account (oid) on delete restrict on update restrict;
create index ix_query_join_createdBy_21 on query_join (created_by_oid);
alter table query_join add constraint fk_query_join_changedBy_22 foreign key (changed_by_oid) references user_account (oid) on delete restrict on update restrict;
create index ix_query_join_changedBy_22 on query_join (changed_by_oid);
alter table query_join add constraint fk_query_join_queryView_23 foreign key (query_view_oid) references query_view (oid) on delete restrict on update restrict;
create index ix_query_join_queryView_23 on query_join (query_view_oid);
alter table query_property add constraint fk_query_property_createdBy_24 foreign key (created_by_oid) references user_account (oid) on delete restrict on update restrict;
create index ix_query_property_createdBy_24 on query_property (created_by_oid);
alter table query_property add constraint fk_query_property_changedBy_25 foreign key (changed_by_oid) references user_account (oid) on delete restrict on update restrict;
create index ix_query_property_changedBy_25 on query_property (changed_by_oid);
alter table query_property add constraint fk_query_property_queryView_26 foreign key (query_view_oid) references query_view (oid) on delete restrict on update restrict;
create index ix_query_property_queryView_26 on query_property (query_view_oid);
alter table query_restriction add constraint fk_query_restriction_createdBy_27 foreign key (created_by_oid) references user_account (oid) on delete restrict on update restrict;
create index ix_query_restriction_createdBy_27 on query_restriction (created_by_oid);
alter table query_restriction add constraint fk_query_restriction_changedBy_28 foreign key (changed_by_oid) references user_account (oid) on delete restrict on update restrict;
create index ix_query_restriction_changedBy_28 on query_restriction (changed_by_oid);
alter table query_restriction add constraint fk_query_restriction_queryTemplate_29 foreign key (query_template_oid) references query_template (oid) on delete restrict on update restrict;
create index ix_query_restriction_queryTemplate_29 on query_restriction (query_template_oid);
alter table query_role_restriction add constraint fk_query_role_restriction_createdBy_30 foreign key (created_by_oid) references user_account (oid) on delete restrict on update restrict;
create index ix_query_role_restriction_createdBy_30 on query_role_restriction (created_by_oid);
alter table query_role_restriction add constraint fk_query_role_restriction_changedBy_31 foreign key (changed_by_oid) references user_account (oid) on delete restrict on update restrict;
create index ix_query_role_restriction_changedBy_31 on query_role_restriction (changed_by_oid);
alter table query_role_restriction add constraint fk_query_role_restriction_role_32 foreign key (role_oid) references role (oid) on delete restrict on update restrict;
create index ix_query_role_restriction_role_32 on query_role_restriction (role_oid);
alter table query_role_restriction add constraint fk_query_role_restriction_restriction_33 foreign key (restriction_oid) references query_restriction (oid) on delete restrict on update restrict;
create index ix_query_role_restriction_restriction_33 on query_role_restriction (restriction_oid);
alter table query_template add constraint fk_query_template_createdBy_34 foreign key (created_by_oid) references user_account (oid) on delete restrict on update restrict;
create index ix_query_template_createdBy_34 on query_template (created_by_oid);
alter table query_template add constraint fk_query_template_changedBy_35 foreign key (changed_by_oid) references user_account (oid) on delete restrict on update restrict;
create index ix_query_template_changedBy_35 on query_template (changed_by_oid);
alter table query_template add constraint fk_query_template_queryView_36 foreign key (query_view_oid) references query_view (oid) on delete restrict on update restrict;
create index ix_query_template_queryView_36 on query_template (query_view_oid);
alter table query_view add constraint fk_query_view_createdBy_37 foreign key (created_by_oid) references user_account (oid) on delete restrict on update restrict;
create index ix_query_view_createdBy_37 on query_view (created_by_oid);
alter table query_view add constraint fk_query_view_changedBy_38 foreign key (changed_by_oid) references user_account (oid) on delete restrict on update restrict;
create index ix_query_view_changedBy_38 on query_view (changed_by_oid);
alter table query_view add constraint fk_query_view_parent_39 foreign key (parent_oid) references query_view (oid) on delete restrict on update restrict;
create index ix_query_view_parent_39 on query_view (parent_oid);
alter table role add constraint fk_role_createdBy_40 foreign key (created_by_oid) references user_account (oid) on delete restrict on update restrict;
create index ix_role_createdBy_40 on role (created_by_oid);
alter table role add constraint fk_role_changedBy_41 foreign key (changed_by_oid) references user_account (oid) on delete restrict on update restrict;
create index ix_role_changedBy_41 on role (changed_by_oid);
alter table role add constraint fk_role_name_42 foreign key (name_oid) references locale_text_holder (oid) on delete restrict on update restrict;
create index ix_role_name_42 on role (name_oid);
alter table subscription add constraint fk_subscription_createdBy_43 foreign key (created_by_oid) references user_account (oid) on delete restrict on update restrict;
create index ix_subscription_createdBy_43 on subscription (created_by_oid);
alter table subscription add constraint fk_subscription_changedBy_44 foreign key (changed_by_oid) references user_account (oid) on delete restrict on update restrict;
create index ix_subscription_changedBy_44 on subscription (changed_by_oid);
alter table subscription add constraint fk_subscription_person_45 foreign key (person_oid) references person (oid) on delete restrict on update restrict;
create index ix_subscription_person_45 on subscription (person_oid);
alter table subscription_role add constraint fk_subscription_role_createdBy_46 foreign key (created_by_oid) references user_account (oid) on delete restrict on update restrict;
create index ix_subscription_role_createdBy_46 on subscription_role (created_by_oid);
alter table subscription_role add constraint fk_subscription_role_changedBy_47 foreign key (changed_by_oid) references user_account (oid) on delete restrict on update restrict;
create index ix_subscription_role_changedBy_47 on subscription_role (changed_by_oid);
alter table subscription_role add constraint fk_subscription_role_subscription_48 foreign key (subscription_oid) references subscription (oid) on delete restrict on update restrict;
create index ix_subscription_role_subscription_48 on subscription_role (subscription_oid);
alter table subscription_role add constraint fk_subscription_role_role_49 foreign key (role_oid) references role (oid) on delete restrict on update restrict;
create index ix_subscription_role_role_49 on subscription_role (role_oid);
alter table user_account add constraint fk_user_account_createdBy_50 foreign key (created_by_oid) references user_account (oid) on delete restrict on update restrict;
create index ix_user_account_createdBy_50 on user_account (created_by_oid);
alter table user_account add constraint fk_user_account_changedBy_51 foreign key (changed_by_oid) references user_account (oid) on delete restrict on update restrict;
create index ix_user_account_changedBy_51 on user_account (changed_by_oid);
alter table user_account add constraint fk_user_account_person_52 foreign key (person_oid) references person (oid) on delete restrict on update restrict;
create index ix_user_account_person_52 on user_account (person_oid);
alter table validation_class_spec add constraint fk_validation_class_spec_createdBy_53 foreign key (created_by_oid) references user_account (oid) on delete restrict on update restrict;
create index ix_validation_class_spec_createdBy_53 on validation_class_spec (created_by_oid);
alter table validation_class_spec add constraint fk_validation_class_spec_changedBy_54 foreign key (changed_by_oid) references user_account (oid) on delete restrict on update restrict;
create index ix_validation_class_spec_changedBy_54 on validation_class_spec (changed_by_oid);
alter table validation_property_spec add constraint fk_validation_property_spec_createdBy_55 foreign key (created_by_oid) references user_account (oid) on delete restrict on update restrict;
create index ix_validation_property_spec_createdBy_55 on validation_property_spec (created_by_oid);
alter table validation_property_spec add constraint fk_validation_property_spec_changedBy_56 foreign key (changed_by_oid) references user_account (oid) on delete restrict on update restrict;
create index ix_validation_property_spec_changedBy_56 on validation_property_spec (changed_by_oid);
alter table validation_property_spec add constraint fk_validation_property_spec_validationClassSpec_57 foreign key (validation_class_spec_oid) references validation_class_spec (oid) on delete restrict on update restrict;
create index ix_validation_property_spec_validationClassSpec_57 on validation_property_spec (validation_class_spec_oid);


