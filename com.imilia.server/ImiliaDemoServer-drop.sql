SET FOREIGN_KEY_CHECKS=0;

drop table address;

drop table crud_target;

drop table locale_text;

drop table locale_text_holder;

drop table model_crud;

drop table person;

drop table phone_contact;

drop table query_join;

drop table query_property;

drop table query_restriction;

drop table query_role_restriction;

drop table query_template;

drop table query_view;

drop table role;

drop table subscription;

drop table subscription_role;

drop table user_account;

drop table validation_class_spec;

drop table validation_property_spec;

SET FOREIGN_KEY_CHECKS=1;

