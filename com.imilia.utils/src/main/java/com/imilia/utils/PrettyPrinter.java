/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2011 - all rights reserved
 * Created on: Jun 28, 2011
 * Created by: emcgreal
 */
package com.imilia.utils;

import java.io.PrintWriter;

/**
 * The Class PrettyPrinter.
 */
public class PrettyPrinter {
	
	/** The print writer. */
	private PrintWriter printWriter;
	
	/** The tab count. */
	private int tabCount;

	/**
	 * Instantiates a new pretty printer.
	 */
	public PrettyPrinter(){
	}

	/**
	 * Instantiates a new pretty printer.
	 *
	 * @param printWriter the print writer
	 */
	public PrettyPrinter(PrintWriter printWriter){
		this.printWriter = printWriter;
	}
	
	/**
	 * Prints the.
	 *
	 * @param strings the strings
	 */
	public void print(String ...strings ){
		for (String s:strings){
			printWriter.print(s);
		}
	}
	
	/**
	 * Println.
	 *
	 * @param strings the strings
	 */
	public void println(String ...strings ){
		printTabs();
		print(strings);
		printWriter.println();
	}

	/**
	 * Println.
	 */
	public void println(){
		printWriter.println();
	}

	/**
	 * Prints the tabs.
	 */
	public void printTabs(){
		for (int i =0;i < tabCount;i++){
			printWriter.print("\t");
		}
	}
	
	/**
	 * Indent.
	 */
	public void indent(){
		tabCount+=1;
	}

	/**
	 * Outdent.
	 */
	public void outdent(){
		tabCount-=1;
	}

	public int getTabCount() {
		return tabCount;
	}

	public void setTabCount(int tabCount) {
		this.tabCount = tabCount;
	}

	public PrintWriter getPrintWriter() {
		return printWriter;
	}

	public void setPrintWriter(PrintWriter printWriter) {
		this.printWriter = printWriter;
	}
}
