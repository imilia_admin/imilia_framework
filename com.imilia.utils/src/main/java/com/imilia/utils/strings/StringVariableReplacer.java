package com.imilia.utils.strings;

public interface StringVariableReplacer {
	public String getReplacement(String variableName);
}
