/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2010 - all rights reserved
 * Created on: Apr 13, 2010
 * Created by: emcgreal
 */
package com.imilia.utils.strings;

import java.text.DecimalFormat;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * The Class StringUtils.
 */
public class StringUtils {
	
	/** The Constant NAMES_PATTERN. */
	private final static Pattern NAMES_PATTERN = Pattern.compile("\\{([^/]+?)\\}");
	/**
	 * Make the first letter of the string a capital.
	 * 
	 * @param s the s
	 * 
	 * @return the string
	 */
	public final static String capitalizeFirstLetter(String s){
		if (s != null && s.length() > 0){
			return s.substring(0, 1).toUpperCase() + s.substring(1, s.length());
		}
		return s;
	} 
		
	/**
	 * Make the first letter of the string a lower case.
	 * 
	 * @param s the s
	 * 
	 * @return the string
	 */
	public final static String lowerFirstLetter(String s){
		if (s != null && s.length() > 0){
			return s.substring(0, 1).toLowerCase() + s.substring(1, s.length());
		}
		return s;
	} 
	
	/**
	 * Contains number - checks to see if string contains at least one number.
	 * 
	 * @param s the s
	 * 
	 * @return true, if any digit found [0-9]+
	 */
	public final static boolean containsNumber(final String s) {
		if (s != null){
	      for (int i = 0;i < s.length();i++){ 
	         if (Character.isDigit(s.charAt(i))){ 
	        	 return true;
	         }
	      }
		}  
	    return false;
	}
	
	/**
	 * To string with2 decimal places.
	 * 
	 * @param d the d
	 * 
	 * @return the string
	 */
	static public String toStringWith2DecimalPlaces(Double d){
		if (d == null){
			return "";
		}
		DecimalFormat df = (DecimalFormat) DecimalFormat.getInstance();
		df.setGroupingUsed(false);
		df.applyPattern("#,##0.00");
		
		return df.format(d);
	}
	
	/**
	 * Generate file name.
	 *
	 * @param perfix the perfix
	 * @param suffix the suffix
	 * @return the string
	 */
	public final static String generateFileName(String perfix, String suffix){
		StringBuffer stringBuffer = new StringBuffer();
		
		stringBuffer.append(perfix);
		
		DateTimeFormatter df = DateTimeFormat.forPattern("yyyyMMddHHMMss");
		
		stringBuffer.append(df.print(new DateTime()));
		stringBuffer.append(".");
		stringBuffer.append(suffix);
		
		return stringBuffer.toString();
	}
	
	/**
	 * Char to int.
	 *
	 * @param c the c
	 * @return the int
	 */
	public static int charToInt(Character c){
		if (c == null){
			return 0;
		}
		
		String s = Character.toString(c);
		
		return Integer.parseInt(s);
	}
	

	/**
	 * Replace params.
	 *
	 * @param source the source containing params in the form of {paramname}
	 * @param values the values
	 * @return the string
	 */
	public static String replaceParams(String source, Map<String, String> values){
		final Matcher matcher = NAMES_PATTERN.matcher(source);
		final StringBuffer sb = new StringBuffer();
		while (matcher.find()) {
			String variableName = matcher.group(1);
			String variableValue = values.get(variableName);
			
			String replacement = Matcher.quoteReplacement(variableValue);
			matcher.appendReplacement(sb, replacement);
		}
		matcher.appendTail(sb);
		return sb.toString();
	}
	
	/**
	 * Different.
	 *
	 * @param s1 the s1
	 * @param s2 the s2
	 * @return true, if successful
	 */
	public static boolean different(String s1, String s2){
		return 	(s1 != null && !s1.equals(s2)) || (s1 == null && s2 != null);
	}
	
	/**
	 * Empty2 null.
	 *
	 * @param s the s
	 * @return the string
	 */
	public static String empty2Null(String s){
		if (s != null && s.trim().length() == 0){
			return null;
		}
		
		return s;
	}
	
	/**
	 * Choose first non empty.
	 *
	 * @param strings the strings
	 * @return the string
	 */
	public static String chooseFirstNonEmpty(String...strings){
		for (String s:strings){
			if (s != null && s.trim().length() != 0){
				return s;
			}
		}
		return null;
	}

	/**
	 * Checks if is non empty.
	 *
	 * @param string the string
	 * @return true, if is non empty
	 */
	public static boolean isNonEmpty(String string) {
		return string != null && string.length() > 0;
	}
	
	
	public static String replaceVariables(String text, StringVariableReplacer replacer){
		Pattern pattern = Pattern.compile("\\$\\{(.+?)\\}");
		Matcher matcher = pattern.matcher(text);
		//populate the replacements map ...
		StringBuilder builder = new StringBuilder();
		int i = 0;
		while (matcher.find()) {
		    String replacement = replacer.getReplacement(matcher.group(1));
		    builder.append(text.substring(i, matcher.start()));
		    if (replacement == null)
		        builder.append(matcher.group(0));
		    else
		        builder.append(replacement);
		    i = matcher.end();
		}
		builder.append(text.substring(i, text.length()));
		return builder.toString();
	}
}
