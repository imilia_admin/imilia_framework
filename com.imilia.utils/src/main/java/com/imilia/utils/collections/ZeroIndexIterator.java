/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2013 - all rights reserved
 *
 * Created on: Jan 15, 2013
 * Created by: emcgreal
 */
package com.imilia.utils.collections;

import java.util.Collection;
import java.util.List;

/**
 * The Class ZeroIndexIterator.
 *
 * @param <T> the generic type
 */
public class ZeroIndexIterator<T> {

	/** The list. */
	private final List<T> list;
	
	/**
	 * Instantiates a new zero index iterator.
	 *
	 * @param list the list
	 */
	public ZeroIndexIterator(List<T> list) {
		super();
		this.list = list;
	}
	
	/**
	 * Checks for next.
	 *
	 * @return true, if successful
	 */
	public boolean hasNext(){
		return !list.isEmpty();
	}
	
	/**
	 * Next.
	 *
	 * @return the t
	 */
	public T next(){
		return list.get(0);
	}
	
	/**
	 * Removes the all.
	 *
	 * @param collection the collection
	 */
	public void removeAll(Collection<T> collection){
		list.removeAll(collection);
	}
	
	/**
	 * Gets the count.
	 *
	 * @return the count
	 */
	public int getCount(){
		return list.size();
	}
}
