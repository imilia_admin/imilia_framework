/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2013 - all rights reserved
 *
 * Created on: Jan 15, 2013
 * Created by: emcgreal
 */
package com.imilia.utils.collections;

import java.util.Iterator;
import java.util.List;

/**
 * The Class ListSegmenter.
 *
 * @param <T> the generic type
 */
public class ListSegmenter<T> {
	
	/** The list. */
	private final List<T> list;
	
	/** The segment size. */
	private int segmentSize;
	
	/** The iter. */
	private Iterator<T> iter;
	
	/** The remove from original list. */
	private boolean removeFromOriginalList;
	
	/**
	 * Instantiates a new list segmenter.
	 *
	 * @param list the list
	 */
	public ListSegmenter(List<T> list){
		this.list = list;
		this.segmentSize = 100;
		this.removeFromOriginalList = true;
	}
	
	/**
	 * Instantiates a new list segmenter.
	 *
	 * @param list the list
	 * @param segmentSize the segment size
	 */
	public ListSegmenter(List<T> list, int segmentSize){
		this.list = list;
		this.segmentSize = segmentSize;
		this.removeFromOriginalList = true;
	}
	
	/**
	 * Instantiates a new list segmenter.
	 *
	 * @param list the list
	 * @param segmentSize the segment size
	 * @param removeFromOriginalList the remove from original list
	 */
	public ListSegmenter(List<T> list, int segmentSize, boolean removeFromOriginalList){
		this.list = list;
		this.segmentSize = segmentSize;
		this.removeFromOriginalList = removeFromOriginalList;
	}
	
	/**
	 * Next.
	 *
	 * @param segment the segment
	 * @return true, if successful
	 */
	public boolean next(List<T> segment){
		segment.clear();
		
		if (iter == null){
			iter = list.iterator();
		}
		
		if (iter.hasNext()){
			segment.add(iter.next());
			
			if (removeFromOriginalList){
				iter.remove();
			}
			
			while(segment.size() < segmentSize && iter.hasNext()){
				segment.add(iter.next());

				if (removeFromOriginalList){
					iter.remove();
				}
			}
			return true;
		}
		return false;
	}
}
