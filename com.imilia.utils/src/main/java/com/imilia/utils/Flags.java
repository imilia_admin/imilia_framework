/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 * Created on: Jan 23, 2009
 * Created by: emcgreal
 */

package com.imilia.utils;

import java.util.Collection;

/**
 * The Class Flags.
 * 
 * @author eddiemcgreal
 * 
 * Flags
 */
public class Flags {
    
    /** The flags. */
    long flags;
    
    /**
     * Instantiates a new flags.
     */
    public Flags() {
        super();
    }

    /**
     * Instantiates a new flags.
     * 
     * @param flags the flags
     */
    public Flags(long flags) {
        super();
        this.flags = flags;
    }
    
	/**
	 * Sets the.
	 * 
	 * @param flag the flag
	 */
	public void set(long flag) {
        this.flags |= flag;
	}

	/**
	 * Checks if is sets the.
	 * 
	 * @param flag the flag
	 * 
	 * @return true, if is sets the
	 */
	public boolean isSet(long flag) {
	    return (this.flags & flag) != 0;
	}
	
	
	
	/**
	 * Clear.
	 * 
	 * @param flag to clear
	 */
	public void clear(long flag) {
        this.flags &= ~flag;
	}

	/**
	 * Sets the.
	 * 
	 * @param state the state
	 * @param flag the flag
	 * 
	 * @return the long
	 */
	public static long set(long state, long flag) {
        return (state |= flag);
	}

	/**
	 * Sets the.
	 * 
	 * @param state the state
	 * @param flag the flag
	 * @param setFlag true sets the flag - false clears the flag
	 * 
	 * @return the long
	 */
	public static long set(long state, long flag, boolean setFlag) {
		if (setFlag){
			return (state |= flag);
		}else{
			 return state &= ~flag;
		}
	}

	
	/**
	 * Flag at.
	 * 
	 * @param index the index
	 * 
	 * @return the long
	 */
	public static long flagAt(long index){
		return 0x0001 << index;
	}
	
	/**
	 * Checks if is sets the.
	 * 
	 * @param state the state
	 * @param flag the flag
	 * 
	 * @return true, if is sets the
	 */
	public static boolean isSet(long state, long flag) {
	    return (state & flag) == flag;
	}
	
	/**
	 * Any set.
	 * 
	 * @param state the state
	 * @param flag the flag
	 * 
	 * @return true, if successful
	 */
	public static boolean anySet(long state, long flag) {
	    return (state & flag) != 0;
	}	
	
	/**
	 * Clear.
	 * 
	 * @param state the state
	 * @param flag the flag
	 * 
	 * @return the long
	 */
	public static long clear(long state, long flag) {
        return state &= ~flag;
	}
	
	/**
	 * Sets the flags.
	 * 
	 * @param flags the new flags
	 */
	public void setFlags(long flags ){
	    this.flags = flags;
	}
	
	/**
	 * Gets the flags.
	 * 
	 * @return the flags
	 */
	public long getFlags(){
	    return flags;
	}
	
	/**
	 * Builds the.
	 * 
	 * @param flags the flags
	 * 
	 * @return the long
	 */
	public static long build(Collection<Long> flags){
		long result = 0;
		
		for (Long l:flags){
			result |= l;
		}
		return result;
	}
}
