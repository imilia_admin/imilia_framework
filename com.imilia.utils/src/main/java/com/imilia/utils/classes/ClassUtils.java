/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2010 - all rights reserved
 * Created on: May 11, 2010
 * Created by: emcgreal
 */
package com.imilia.utils.classes;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Collection;

/**
 * The Class ClassUtils.
 */
public class ClassUtils {
	
	/**
	 * Find field.
	 * 
	 * @param clazz the clazz
	 * @param fieldName the field name
	 * 
	 * @return the field
	 */
	public final static Field findField(Class<?> clazz, String fieldName){
		while (clazz != null) {
			try {
				Field f = clazz.getDeclaredField(fieldName);

				return f;
			} catch (NoSuchFieldException ex) {
			}

			clazz = clazz.getSuperclass();
		}

		return null;
	}
	
	/**
	 * Find fields.
	 * 
	 * @param clazz the clazz
	 * @param a the a
	 * 
	 * @return the list< field>
	 */
	public final static List<Field> findFields(Class<?> clazz){
		List<Field> list = new ArrayList<Field>();
		while (clazz != null) {
			Field[] fields = clazz.getDeclaredFields();
			
			if (fields != null){
				for (Field f:fields){
					list.add(f);
				}
			}

			clazz = clazz.getSuperclass();
			
			if (clazz != null && clazz.getPackage().getName().startsWith("nextapp")){
				break;
			}
		}
		return list;
	}
	
	public final static List<Field> findFields(Class<?> clazz, String...stopAtPackagePrefix){
		List<Field> list = new ArrayList<Field>();
		while (clazz != null) {
			Field[] fields = clazz.getDeclaredFields();
			
			if (fields != null){
				for (Field f:fields){
					list.add(f);
				}
			}

			clazz = clazz.getSuperclass();
			
			if (clazz != null){
				for (String s:stopAtPackagePrefix){
					if (clazz.getPackage().getName().startsWith(s)){
						clazz = null;
					}
				}
			}
		}
		return list;
	}
	
	public final static List<Field> filterList(List<Field> fields, Class<?>...cls){
		final List<Field> list = new ArrayList<Field>();
		
		for (Field f:fields){
			for (Class<?> c:cls){
				if (c.isAssignableFrom(f.getType())){
					list.add(f);
				}
			}
		}
		return list;
	}
	/**
	 * Find fields.
	 * 
	 * @param clazz the clazz
	 * @param a the a
	 * 
	 * @return the list< field>
	 */
	public  final static List<Field> findFields(Class<?> clazz, Class<? extends Annotation> a){
		List<Field> list = findFields(clazz);
		
		final Iterator<Field> iter = list.iterator();
		
		while (iter.hasNext()) {
			Field f = iter.next();
			if (!f.isAnnotationPresent(a)){
				iter.remove();
			}
		}
		return list;
	}
	
	public final static Field findFieldFromPath(Class<?> clazz, String path){
		final String[] steps = path.split("\\.");
		
		Field field = null;
		for (String step:steps){
			if (field != null){
				if (Collection.class.isAssignableFrom(field.getType())){
					ParameterizedType listType = (ParameterizedType) field.getGenericType();
					clazz = (Class<?>) listType.getActualTypeArguments()[0];
				}
			}
			field = findField(clazz, step);
			clazz = field.getType();
		}
		return field;
	}
	
	public static List<Class<?>> getSuperClasses(Class<?> clazz, Class<? extends Annotation>...annotations){
		List<Class<?>> superTypes = new ArrayList<>();
		
		Class<?> parent = clazz.getSuperclass();
		
		while (parent != null){
			for (Class<? extends Annotation> a: annotations){
				if (parent.isAnnotationPresent(a)){
					superTypes.add(parent);
					break;
				}
			}
			parent = parent.getSuperclass();
		}
		return superTypes;
	}
}
