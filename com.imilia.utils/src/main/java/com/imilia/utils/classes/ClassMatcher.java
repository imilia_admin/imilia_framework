package com.imilia.utils.classes;

import java.lang.reflect.Field;

import org.springframework.core.type.classreading.MetadataReader;

/**
 * The Interface ClassMatcher.
 */
public interface ClassMatcher {
	
	/**
	 * Checks if is candidate.
	 *
	 * @param metadataReader the metadata reader
	 * @return true, if is candidate
	 * @throws ClassNotFoundException the class not found exception
	 */
	public boolean isCandidate(MetadataReader metadataReader) throws ClassNotFoundException;
	
	/**
	 * Checks if is candidate.
	 *
	 * @param clazz the clazz
	 * @return true, if is candidate
	 */
	public boolean isCandidate(Class<?> clazz);
	
	/**
	 * Checks if is candidate.
	 *
	 * @param field the field
	 * @return true, if is candidate
	 */
	public boolean isCandidate(Field field);
}
