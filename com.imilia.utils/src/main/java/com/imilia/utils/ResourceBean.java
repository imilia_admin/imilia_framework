/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2011 - all rights reserved
 *
 * Created on: Sep 27, 2011
 * Created by: emcgreal
 */
package com.imilia.utils;

import java.io.IOException;

import org.springframework.core.io.Resource;

import com.imilia.utils.files.FileUtils;

/**
 * The Class ResourceBean.
 */
public class ResourceBean {
	
	/** The resource. */
	private Resource resource;
	
	/** The bytes. */
	private byte[] bytes;
	
	
	public ResourceBean() {
	}
	
	public ResourceBean(Resource resource) {
		super();
		this.resource = resource;
	}

	/**
	 * Gets the resource.
	 *
	 * @return the resource
	 */
	public Resource getResource() {
		return resource;
	}

	/**
	 * Sets the resource.
	 *
	 * @param resource the new resource
	 */
	public void setResource(Resource resource) {
		this.resource = resource;
	}
	
	/**
	 * Gets the bytes.
	 *
	 * @return the bytes
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public byte[] getBytes() throws IOException{
		if (bytes == null){
			bytes = FileUtils.inputStreamToBytes(resource.getInputStream());
		}
		return bytes;
	}
	
}
