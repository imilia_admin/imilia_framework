/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2008 - all rights reserved
 * Created on: Jan 26, 2009
 * Created by: emcgreal
 */
package com.imilia.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.imilia.utils.files.FileDiff;


/**
 * The Class ImiliaTestCase.
 * 
 * @author emcgreal
 */
public abstract class ImiliaTestCase {
	// The logger
	/** The Constant logger. */
	private static final Logger logger = LoggerFactory.getLogger(ImiliaTestCase.class);
	
	/** The Constant ClassSuffix. */
	private final static String ClassSuffix = ".class"; 
	
	/**
	 * Instantiates a new imilia test case.
	 */
	public ImiliaTestCase() {
		super();
	}


	/**
	 * Compare files.
	 * 
	 * @param output the output
	 * @param url the url
	 * 
	 * @return true, if compare files
	 * 
	 * @throws MalformedURLException 	 * @throws Exception the exception
	 */
	public static boolean compareFiles(URL url, byte[] output) throws Exception{
		String s = url.toExternalForm();
		int index = s.lastIndexOf('.');
		if (index  > -1){
			// cut the old suffix
			s = s.substring(0, index) + "_out" + s.substring(index);
			
		}
		return compareFiles(url, output, new URL(s));
	}
	

	/**
	 * Compare files.
	 * 
	 * @param url the url
	 * @param output the output
	 * @param outputUrl the output url
	 * 
	 * @return true, if successful
	 * 
	 * @throws Exception the exception
	 */
	public static boolean compareFiles(URL url, byte[] output, URL outputUrl) throws Exception{
		try{
			File fileOut = serialize2File(output, outputUrl);
			
			StringWriter writer = new StringWriter();
			PrintWriter printWriter = new PrintWriter(writer);
			FileDiff fileDiff = new FileDiff(printWriter);
			boolean res = fileDiff.doDiff(url.toURI(), fileOut.toURI());
			printWriter.flush();
			printWriter.close();
			
			return res;

		}catch(Exception ex){
			return false;
		}
	}

	public static boolean compareFiles(URL url1, URL url2) throws Exception{
		try{
			StringWriter writer = new StringWriter();
			PrintWriter printWriter = new PrintWriter(writer);
			FileDiff fileDiff = new FileDiff(printWriter);
			boolean res = fileDiff.doDiff(url1.toURI(), url2.toURI());
			printWriter.flush();
			printWriter.close();
			
			if (!res){
				logger.error(writer.toString());
			}
			return res;

		}catch(Exception ex){
			return false;
		}
	}
	/**
	 * Serialize2 file.
	 * 
	 * @param output the output
	 * @param outputUrl the output url
	 * 
	 * @return the file
	 * 
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws Exception the exception
	 */
	public static File serialize2File(byte[] output, URL outputUrl) throws Exception{
		logger.debug("Serializing output to: " + outputUrl.getPath());
		File fileOut = new File(outputUrl.toURI());
		fileOut.createNewFile();
		
		FileOutputStream fo = new FileOutputStream(fileOut);
		fo.write(output);
		fo.flush();
		fo.close();
		
		return fileOut;
	}
	
	/**
	 * Serialize2 file.
	 * 
	 * @param output the output
	 * @param outputUrl the output url
	 * @param encoding the encoding
	 * 
	 * @return the file
	 * 
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws Exception the exception
	 */
	public static File serialize2File(String output, URL outputUrl, String encoding) throws Exception{
		logger.debug("Serializing output to: " + outputUrl.getPath() + " using encoding: " + encoding);

		File fileOut = new File(outputUrl.toURI());
		fileOut.createNewFile();

		BufferedWriter out = 
			new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileOut),encoding));
		
		
		out.write(output);
		out.close();

		return fileOut;
	}
	
	/**
	 * Gets the output url.
	 * 
	 * @param url the url
	 * 
	 * @return the output url
	 * 
	 * @throws Exception the exception
	 */
	public static URL getOutputURL(URL url) throws Exception{
		String s = url.toExternalForm();
		int index = s.lastIndexOf('.');
		if (index  > -1){
			// cut the old suffix
			s = s.substring(0, index) + "_out" + s.substring(index);
			
		}
		return new URL(s);
	}
	
	/**
	 * Gets the output folder.
	 * 
	 * @param clazz the clazz
	 * 
	 * @return the output folder
	 */
	public static String getOutputFolder(Class<?> clazz){
		final String canonicalName = clazz.getCanonicalName();
		
		final URL url = ClassLoader.getSystemResource(canonicalName.replace('.', '/') + ClassSuffix);
		
		final String path = url.getPath();
		
		return path.substring(0, path.lastIndexOf('/'));
	}
	
	public static String getOutputResourcesFolder(Class<?> clazz){
		final String canonicalName = clazz.getCanonicalName();
		
		final URL url = ClassLoader.getSystemResource(canonicalName.replace('.', '/') + ClassSuffix);
		
		String path = url.getPath();
		path = path.replace("classes", "resources");
		
		return path.substring(0, path.lastIndexOf('/'));
	}
	
	/**
	 * Gets the output folder url.
	 * 
	 * @param clazz the clazz
	 * 
	 * @return the output folder url
	 */
	public static String getOutputFolderURL(Class<?> clazz){
		return "file:" + getOutputFolder(clazz);
	}

	public static String getOutputResourcesFolderURL(Class<?> clazz){
		return "file:" + getOutputResourcesFolder(clazz);
	}
}
