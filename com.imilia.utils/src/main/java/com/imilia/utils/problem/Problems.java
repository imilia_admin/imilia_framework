/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2006
 * Created on Mar 28, 2006
 */
package com.imilia.utils.problem;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * @author eddiemcgreal
 *
 */
public class Problems extends ArrayList<Problem>{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1851689668684558730L;

	/**
	 * 
	 */
	public Problems() {
		super();
	}

	
	/* (non-Javadoc)
	 * @see com.imilia.api.xqx.exprtree.ExprTreeBuilder#getErrors()
	 */
	public Iterator<Problem> getProblems(final Problem.Level level) {
		return new Iterator<Problem>(){
			
			Iterator<Problem> problems = iterator();
			Problem problem = null;
			public boolean hasNext() {
				while(problems.hasNext()){
					problem = problems.next();
					if (problem.getLevel() == level){
						return true;
					}
				}
				return false;
			}

			public Problem next() {
				return problem;
			}

			public void remove() {
			}
			
		};
	}

	/* 
	 * Has a problem with the given level
	 */
	public boolean hasProblems(final Problem.Level level) {
		Iterator<Problem> problems = iterator();
		while(problems.hasNext()){
			Problem problem = problems.next();
			if (problem.getLevel() == level){
				return true;
			}
		}
		return false;
	}
	public void dumpProblems(StringBuffer buffer){
		dumpProblems(iterator(), buffer);
	}

	public String dumpProblems(){
		StringBuffer buffer = new StringBuffer();
		Problems.dumpProblems(iterator(), buffer);
		return buffer.toString();
	}	
	
	public static void dumpProblems(Iterator<Problem> iter, StringBuffer buffer){
		while (iter.hasNext()){
			if (buffer.length() > 0){
				buffer.append("\n");
			}
			buffer.append(iter.next().toString());
		}
	}
}
