/**
 * Imilia Interactive Mobile Applications GmbH.
 * Copyright (c) 2006 
 * Created: 17.02.2006	
 */
package com.imilia.utils.problem;

import com.imilia.utils.Point;


public class Problem{
	private String text;
	private Point<Integer> position;
	private Object object;
	private Problem.Level level;
	private int code;
	
	public enum Level{ 
			WARN {public String getLabel() {return "Warning";}}, 
			ERROR {public String getLabel() {return "Error";}}; 
	
		public abstract String getLabel();
	};
	
	public Problem(){
	}

	public Problem(String text, Point<Integer> pos, Object obj, Problem.Level level, int code){
		this.text = text;
		this.position = pos;
		this.object = obj;
		this.level = level;
		this.code = code;
	}
	
	public Problem(String text, Point<Integer> pos, Object obj, Problem.Level level){
		this.text = text;
		this.position = pos;
		this.object = obj;
		this.level = level;
	}
	
	
	
	public String toString(){
		return level.getLabel() + ": " + text + " " + position + " " + (object != null ? object.toString() : "");  
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Point<Integer> getPosition() {
		return position;
	}

	public void setPosition(Point<Integer> position) {
		this.position = position;
	}

	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}

	public Problem.Level getLevel() {
		return level;
	}

	public void setLevel(Problem.Level level) {
		this.level = level;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}
}