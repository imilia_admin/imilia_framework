/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2010 - all rights reserved
 * Created on: Aug 10, 2010
 * Created by: emcgreal
 */
package com.imilia.utils;

import java.util.Locale;

import org.springframework.context.MessageSource;

import com.imilia.utils.i18n.Localized;


/**
 * The Class Flag.
 */
public class Flag implements Localized {
	
	/** The value. */
	private final long value;
	
	/** The message id. */
	private final String messageId;
	
	/**
	 * Instantiates a new flag.
	 * 
	 * @param l the l
	 * @param messageId the message id
	 */
	public Flag(long l, String messageId){
		this.value = l;
		this.messageId = messageId;
	}

	/**
	 * Gets the value.
	 * 
	 * @return the value
	 */
	public final long getValue() {
		return value;
	}
	
	public final int getIntValue() {
		return (int) value;
	}

	/**
	 * Gets the message id.
	 * 
	 * @return the message id
	 */
	public final String getMessageId() {
		return messageId;
	}

	/* (non-Javadoc)
	 * @see com.imilia.server.domain.Localized#getLocalizedMessage(org.springframework.context.MessageSource, java.util.Locale)
	 */
	public String getLocalizedMessage(MessageSource messageSource, Locale locale) {
		return messageSource.getMessage(messageId, null, locale);
	}
}
