/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2013 - all rights reserved
 *
 * Created on: Jan 15, 2013
 * Created by: emcgreal
 */
package com.imilia.utils.logutils;


/**
 * The Class LogEntry.
 */
public class LogEntry{
	

	public enum State{
		Info,
		Warning,
		Error
	}
	
	/** The log state. */
	private final State state;
	

	/** The message. */
	private final String message;
	/** The row. */
	private final int row;
	
	/** The column. */
	private final int column;
	
	
	/**
	 * Instantiates a new log entry.
	 *
	 * @param logState the log state
	 * @param message the message
	 * @param row the row
	 * @param column the column
	 */
	public LogEntry(State logState,  String message, int row, int column) {
		super();
		this.state = logState;
		this.row = row;
		this.column = column;
		this.message = message;
	}

	/**
	 * Gets the log state.
	 *
	 * @return the log state
	 */
	public State getState() {
		return state;
	}

	/**
	 * Gets the row.
	 *
	 * @return the row
	 */
	public int getRow() {
		return row;
	}

	/**
	 * Gets the column.
	 *
	 * @return the column
	 */
	public int getColumn() {
		return column;
	}

	/**
	 * Gets the message.
	 *
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
}