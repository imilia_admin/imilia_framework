/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2011 - all rights reserved
 * Created on: Sep 1, 2011
 * Created by: emcgreal
 */
package com.imilia.utils.logutils;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import com.imilia.utils.logutils.LogEntry.State;


/**
 * The Class ImportContext.
 */
public final class LogContext {
	
	
	
	/** The log entries. */
	private final List<LogEntry> logEntries = new ArrayList<LogEntry>();
	
	/**
	 * Instantiates a new import context.
	 *
	 */
	public LogContext() {
		super();
	}

	/**
	 * Gets the log entries.
	 *
	 * @return the log entries
	 */
	public List<LogEntry> getLogEntries() {
		return logEntries;
	}

	/**
	 * Info.
	 *
	 * @param message the message
	 * @param row the row
	 */
	public void info(String message, int row){
		log(State.Info, message, row, 0);
	}

	/**
	 * Warn.
	 *
	 * @param message the message
	 * @param row the row
	 */
	public void warn(String message, int row){
		log(State.Warning, message, row, 0);
	}

	/**
	 * Error.
	 *
	 * @param message the message
	 * @param row the row
	 */
	public void error(String message, int row){
		log(State.Error, message, row, 0);
	}

	/**
	 * Log.
	 *
	 * @param state the state
	 * @param message the message
	 * @param row the row
	 * @param column the column
	 */
	public void log(State state,String message, int row, int column){
		logEntries.add(new LogEntry(state, message, row, column));
	}
	
	
	/**
	 * Gets the max state.
	 *
	 * @return the max state
	 */
	public LogEntry.State getMaxState(){
		LogEntry.State max = null;
		for (LogEntry e:logEntries){
			if (max == null){
				max = e.getState();
			}else if(max.ordinal() < e.getState().ordinal()){
				max = e.getState();
			}
		}
		
		return max;
	}
	
	/**
	 * Checks for errors.
	 *
	 * @return true, if successful
	 */
	public boolean hasErrors(){
		for (LogEntry e:logEntries){
			if (State.Error.equals(e.getState())){
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Prints the.
	 *
	 * @param printer the printer
	 */
	public void print(PrintWriter printer){
		for (LogEntry e:logEntries){
			printer.print(e.getState());
			printer.print(";");
			printer.print(e.getMessage());
			printer.print(";");
			printer.print(e.getRow());
			printer.print(";");
			printer.print(e.getColumn());
			printer.println();
		}
	}
	
	/**
	 * Gets the info warnings and errors count.
	 *
	 * @return the info warning error count as an int array
	 */
	public int[] getInfoWarningErrorCount(){
		final int[] couters = new int[3];
		
		for (LogEntry e:logEntries){
			switch(e.getState()){
			case Info:
				couters[0]++;
				break;
			case Warning:
				couters[1]++;
				break;
			case Error:
				couters[2]++;
				break;
			}
		}
		
		return couters;
	}
}
