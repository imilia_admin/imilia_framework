/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 * 
 * Created on: Mar 3, 2009
 * Created by: emcgreal
 */
package com.imilia.utils.i18n;

import java.util.Locale;

import org.springframework.context.MessageSource;

/**
 * The Interface Localized is implemented by classes that can provide a locale specific message. 
 * 
 * <p>The localized message can be the localized name of e.g. an enum or a summary text
 * of a domain object e.g. Phone contact Mobile:0170123456. 
 * </p>
 */
public interface Localized {
	
	/**
	 * Gets the localized message.
	 * 
	 * @param messageSource the message source
	 * @param locale the locale
	 * 
	 * @return the localized message
	 */
	public String getLocalizedMessage(MessageSource messageSource, Locale locale);
}
