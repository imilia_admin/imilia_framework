/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 * Created on: Jan 23, 2009
 * Created by: emcgreal
 */
package com.imilia.utils.i18n;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;

import org.joda.time.Period;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;
import org.springframework.context.MessageSource;

// TODO: Auto-generated Javadoc
/**
 * The enum DomainLocale encapsulates a Locale within the domain
 * <p>
 * A domain locale is a persistent locale which is used as a reference in locale
 * texts
 * </p>
 * .
 */
public enum DomainLocale implements Localized {
	// The order of the enums may never change unless you update the db!
	/** The en_ us. */
	en_US, // 0
	/** The de_ de. */
	de_DE; // 1

	/** The time format24 hour. */
	private static DateTimeFormatter timeFormat24Hour = DateTimeFormat
			.forPattern("HH:mm");

	/** The Constant periodFormatHHmm. */
	private final static PeriodFormatter periodFormatHHmm = new PeriodFormatterBuilder()
			.printZeroAlways().minimumPrintedDigits(2).appendHours()
			.appendSeparator(":").appendMinutes().toFormatter();

	/**
	 * Gets the short day date time format.
	 * 
	 * @return the short day date time format
	 */
	public String getShortDayDateTimeFormat() {
		switch (this) {
		case de_DE:
			return "EE dd.MM. HH:mm";

		case en_US:
		default:
			return "EE MM/dd HH:mm";
		}
	}

	/**
	 * Gets the short date format.
	 * 
	 * @return the short date format
	 */
	public String getShortDateFormat() {
		switch (this) {
		case de_DE:
			return "dd.MM.yyyy";

		case en_US:
		default:
			return "MM/dd/yyyy";
		}
	}

	/**
	 * Gets the short day date format.
	 * 
	 * @return the short day date format
	 */
	public String getShortDayDateFormat() {
		switch (this) {
		case de_DE:
			return "EE dd.MM.yyyy";

		case en_US:
		default:
			return "EE MM/dd/yyyy";
		}
	}
	
	/**
	 * Gets the very short day date format.
	 * 
	 * @return the very short day date format
	 */
	public String getVeryShortDayDateFormat() {
		switch (this) {
		case de_DE:
			return "EE dd.MM.";

		case en_US:
		default:
			return "EE MM/dd";
		}
	}

	/**
	 * Gets the short date time format.
	 * 
	 * @return the short date time format
	 */
	public String getShortDateTimeFormat() {
		switch (this) {
		case de_DE:
			return "dd.MM.yyyy HH:mm";

		case en_US:
		default:
			return "MM/dd/yyyy HH:mm";
		}
	}
	
	public String getDateTimeFormat() {
		switch (this) {
		case de_DE:
			return "dd.MM.yyyy HH:mm:ss";

		case en_US:
		default:
			return "MM/dd/yyyy HH:mm:ss";
		}
	}

	/**
	 * Gets the simple locale.
	 * 
	 * @return the simple locale
	 */
	public Locale getSimpleLocale() {
		switch (this) {
		case de_DE:
			return Locale.GERMAN;

		case en_US:
		default:
			return Locale.ENGLISH;
		}
	}

	/**
	 * Gets the time format24 hour.
	 * 
	 * @return the time format24 hour
	 */
	public DateTimeFormatter getTimeFormat24Hour() {
		return timeFormat24Hour;
	}

	/**
	 * Gets the locale.
	 * 
	 * @return the locale
	 */
	public Locale getLocale() {
		Locale locale = null;
		StringTokenizer st = new StringTokenizer(name(), "_");
		List<String> list = new ArrayList<String>();
		while (st.hasMoreTokens()) {
			list.add(st.nextToken());
		}
		if (list.size() == 1) {
			locale = new Locale(list.get(0));
		} else if (list.size() == 2) {
			locale = new Locale(list.get(0), list.get(1));
		} else if (list.size() == 3) {
			locale = new Locale(list.get(0), list.get(1), list.get(2));
		}
		return locale;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.imilia.server.domain.common.Localeized#getMessageId()
	 */
	/**
	 * Gets the message id.
	 * 
	 * @return the message id
	 */
	public String getMessageId() {
		return DomainLocale.class.getSimpleName() + "." + name();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.imilia.server.domain.Localized#getLocalizedMessage(org.springframework
	 * .context.MessageSource, java.util.Locale)
	 */
	public String getLocalizedMessage(MessageSource messageSource, Locale locale) {
		return messageSource.getMessage(getMessageId(), null, locale);
	}
	
	/**
	 * Format h hmm.
	 * 
	 * @param period the period
	 * 
	 * @return the string
	 */
	public final static String formatHHmm(Period period){
		return periodFormatHHmm.print(period);
	}
}
