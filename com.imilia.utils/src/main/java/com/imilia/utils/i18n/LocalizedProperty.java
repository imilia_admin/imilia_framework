/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 *
 * Created on: Jul 18, 2009
 * Created by: emcgreal
 */
package com.imilia.utils.i18n;


/**
 * The Interface LocalizedProperty.
 */
public interface LocalizedProperty {

	/**
	 * Gets the text.
	 *
	 * @param locale the locale
	 *
	 * @return the text
	 */
	public String getText(DomainLocale locale);

	/**
	 * Sets the text.
	 *
	 * @param text the text
	 * @param locale the locale
	 */
	public void setText(DomainLocale locale, String text);
}
