/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2011 - all rights reserved
 *
 * Created on: Sep 26, 2011
 * Created by: emcgreal
 */
package com.imilia.utils.template;

import com.imilia.utils.i18n.DomainLocale;

/**
 * The Interface TemplateProvider.
 */
public interface TemplateProvider {
	
	/**
	 * Gets the template.
	 *
	 * @param domainLocale the domain locale
	 * @return the template
	 */
	public byte[] getTemplate(DomainLocale domainLocale);
}
