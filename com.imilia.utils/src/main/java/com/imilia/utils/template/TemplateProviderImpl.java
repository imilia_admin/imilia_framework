/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2011 - all rights reserved
 *
 * Created on: Sep 27, 2011
 * Created by: emcgreal
 */
package com.imilia.utils.template;

import java.io.IOException;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.imilia.utils.ResourceBean;
import com.imilia.utils.i18n.DomainLocale;

/**
 * The Class TemplateProviderImpl.
 */
public class TemplateProviderImpl implements TemplateProvider {

	private final static Logger logger = LoggerFactory.getLogger(TemplateProviderImpl.class);
	
	/** The templates. */
	private HashMap<DomainLocale, ResourceBean> templates = new HashMap<DomainLocale,ResourceBean>();
	
	/* (non-Javadoc)
	 * @see com.imilia.utils.template.TemplateProvider#getTemplate(com.imilia.server.domain.i18n.DomainLocale)
	 */
	public byte[] getTemplate(DomainLocale domainLocale) {
		try {
			return templates.get(domainLocale).getBytes();
		} catch (IOException e) {
			logger.error("Load template failed:" + domainLocale, e);
		}
		
		return null;
	}

	public HashMap<DomainLocale, ResourceBean> getTemplates() {
		return templates;
	}

	public void setTemplates(HashMap<DomainLocale, ResourceBean> templates) {
		this.templates = templates;
	}
}
