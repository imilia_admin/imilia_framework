/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2010 - all rights reserved
 * Created on: Jul 11, 2010
 * Created by: emcgreal
 */
package com.imilia.utils;

/**
 * The Class Point.
 */
public final class Point<T> {
	
	/** The x. */
	private final T x;
	
	/** The y. */
	private final T y;
	
	
	/**
	 * Instantiates a new point.
	 * 
	 * @param x the x
	 * @param y the y
	 */
	public Point(T x, T y){
		super();
		this.x = x;
		this.y = y;
	}
	
	/**
	 * Gets the x.
	 * 
	 * @return the x
	 */
	public final T getX() {
		return x;
	}
	
	/**
	 * Gets the y.
	 * 
	 * @return the y
	 */
	public final T getY() {
		return y;
	}
}
