/**
 * Imilia Interactive Mobile Applications GmbH
 * Copyright (c) 2009 - all rights reserved
 *
 * Created on: Aug 26, 2009
 * Created by: emcgreal
 */
package com.imilia.utils;

/**
 * The Class StopWatch.
 */
public class StopWatch {
	
	/** The start. */
	long start;
	
	/** The stop. */
	long stop;
	
	/**
	 * Instantiates a new stop watch.
	 */
	public StopWatch() {
		super();
	}

	/**
	 * Start.
	 * 
	 * @return the long
	 */
	public long start(){
		start = System.currentTimeMillis();
		stop = start;
		return start;
	}
	
	/**
	 * Stop.
	 * 
	 * @return the long
	 */
	public long stop(){
		stop = System.currentTimeMillis();
		return stop;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString(){
		final long elapsed = stop - start;
        int seconds = (int) (elapsed / 1000);
        int ms = (int) (elapsed % 1000);
        StringBuffer buffer = new StringBuffer(Integer.toString(ms));
        
        while(buffer.length() < 3){
        	buffer.insert(0,'0');
        }
        buffer.insert(0, ".");
        buffer.insert(0, seconds);
        return buffer.toString();
	}
}
