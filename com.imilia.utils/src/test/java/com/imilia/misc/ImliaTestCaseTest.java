package com.imilia.misc;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.net.URL;

import org.junit.Test;

import com.imilia.utils.ImiliaTestCase;

public class ImliaTestCaseTest extends ImiliaTestCase{

	@Test
	public void testGetOutputFolder(){
		final String outputFolder = getOutputFolder(this.getClass());
		
		assertNotNull(outputFolder);
		final String packageName = this.getClass().getPackage().getName();
		
		assertTrue(outputFolder.endsWith(packageName.replace('.', '/')));
	}
	
	@Test
	public void testGetOutputURL() throws Exception{
		final String outputFolder = getOutputFolderURL(this.getClass());
		
		final URL url = new URL(outputFolder + "text.txt");
		final String ext = url.toExternalForm(); 
		assertNotNull(ext);
		
		final URL out = getOutputURL(url);
		
		assertNotNull(out);
	}
	
	
	@Test
	public void testUTF8() throws Exception{
		
		final String testString = "äßöü";
		
		
		final URL refFileUrl = new URL(ImiliaTestCase.getOutputResourcesFolderURL(this.getClass())+"/" + "testUTF8.txt");
		
		final URL outFileUrl = ImiliaTestCase.getOutputURL(refFileUrl);
		ImiliaTestCase.serialize2File(testString, outFileUrl, "UTF8");

		// Compare the output
		assertTrue("Output file does not match reference file:" + refFileUrl,
				ImiliaTestCase.compareFiles(refFileUrl, outFileUrl));
	}
}
