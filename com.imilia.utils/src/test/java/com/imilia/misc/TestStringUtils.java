package com.imilia.misc;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.imilia.utils.strings.StringUtils;
import com.imilia.utils.strings.StringVariableReplacer;

public class TestStringUtils {

	@Test
	public void testVariableReplacement(){
		String testText = "this is a ${var1} for the ${var2}";
		
		String s = StringUtils.replaceVariables(testText, new StringVariableReplacer() {
			
			@Override
			public String getReplacement(String variableName) {
				switch(variableName){
				case "var1":
					return "test";
				case "var2":
					return "StringVariableReplacer";
				}
				return null;
			}
		});
		
		assertTrue("this is a test for the StringVariableReplacer".equals(s));
	}
}
