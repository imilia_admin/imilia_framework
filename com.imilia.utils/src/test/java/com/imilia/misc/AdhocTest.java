package com.imilia.misc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.joda.time.Period;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;
import org.junit.Test;

public class AdhocTest {
	private final static Logger logger = LoggerFactory.getLogger(AdhocTest.class);
	
	public static void main(String[] args) {
		long l = 10 *60 * 1000;
		
		Period d = new Period(l);
		
	    PeriodFormatter dateFormat = new PeriodFormatterBuilder()
	    .printZeroAlways().minimumPrintedDigits(2)
        .appendHours()
        .appendSeparator(":")
        .appendMinutes()
        .toFormatter();
		
	    String s = dateFormat.print(d);
	    
	    logger.info(s);
	   
	}
	
	@Test
	public void testIt(){
		
	}
}
