package com.imilia.server.service.impl;

import java.net.URISyntaxException;

import net.fortuna.ical4j.model.TimeZone;
import net.fortuna.ical4j.model.TimeZoneRegistry;
import net.fortuna.ical4j.model.TimeZoneRegistryFactory;
import net.fortuna.ical4j.model.ValidationException;
import net.fortuna.ical4j.model.component.VEvent;
import net.fortuna.ical4j.model.component.VTimeZone;
import net.fortuna.ical4j.model.property.CalScale;
import net.fortuna.ical4j.model.property.Method;
import net.fortuna.ical4j.model.property.Organizer;
import net.fortuna.ical4j.model.property.ProdId;
import net.fortuna.ical4j.model.property.Sequence;
import net.fortuna.ical4j.model.property.Uid;

import org.joda.time.DateTime;

import com.imilia.server.service.ICalendarService;

public class ICalendarServiceImpl extends ServiceImpl implements
		ICalendarService {

	final private static TimeZoneRegistry registry = TimeZoneRegistryFactory
			.getInstance().createRegistry();

//	private String timeZone = "Europe/Berlin";

	public ICalendarServiceImpl() {
	}

	public byte[] createEvent(DateTime begin, DateTime end, String summary) {

		final VEvent event = new VEvent(
				new net.fortuna.ical4j.model.DateTime(begin.getMillis()), 
				new net.fortuna.ical4j.model.DateTime(end.getMillis()), summary);
		
		try {
			event.getProperties().add(new Organizer("www.booinaflash.com"));
		} catch (URISyntaxException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		event.getProperties().add(Method.CANCEL);

		TimeZone timezone = registry.getTimeZone("Europe/Berlin");
		VTimeZone tz = timezone.getVTimeZone();

		// add timezone info..
		event.getProperties().add(tz.getTimeZoneId());
		
		// generate unique identifier..
		//UidGenerator ug = new UidGenerator("uidGen");
		Uid uid = new Uid("1.2@bookina.com");
		event.getProperties().add(uid);
		event.getProperties().add(new Sequence(0));

		// Create a calendar
		net.fortuna.ical4j.model.Calendar icsCalendar = new net.fortuna.ical4j.model.Calendar();
		icsCalendar.getProperties().add(new ProdId("-//Imilia//BookinaFlash//EN"));
		icsCalendar.getProperties().add(CalScale.GREGORIAN);

		try {
			event.validateCancel();
		} catch (ValidationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// Add the event and print
		icsCalendar.getComponents().add(event);
		
		String s = icsCalendar.toString();
		return s.getBytes();
	}

}
