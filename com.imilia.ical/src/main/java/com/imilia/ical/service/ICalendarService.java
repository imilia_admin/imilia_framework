package com.imilia.server.service;

import org.joda.time.DateTime;

public interface ICalendarService extends Service {
	public byte[] createEvent(DateTime begin, DateTime end, String summary);
}
